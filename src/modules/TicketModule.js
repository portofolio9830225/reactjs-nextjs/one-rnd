import TicketModule from '../services/ticket.service';

const readById = (id, formula_id) => {
	return TicketModule.readById(id, formula_id).then(
		(response) => {
			return Promise.resolve(response.data, response.data.message);
		},
		(error) => {
			const message =
				(error.message && error.response.data && error.response.data.message) ||
				error.message ||
				error.toString();
			return Promise.reject(message);
		},
	);
};

export default { readById };
