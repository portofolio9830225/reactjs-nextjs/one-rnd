import formulaService from '../../services/azhar/formula.service';

const readTable = async (page, countPerPage, query) => {
	// return formulaService.list(page, countPerPage);
	return formulaService.list(page, countPerPage, query).then(
		(response) => {
			return Promise.resolve(response.data, response.data.message);
		},
		(error) => {
			const message =
				(error.response && error.response.data && error.response.data.message) ||
				error.message ||
				error.toString();
			return Promise.reject(message);
		},
	);
};

export default { readTable };
