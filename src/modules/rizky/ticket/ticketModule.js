import ticketService from '../../../services/rizky/ticket/ticket.service';

const readTicketById = (query_string) => {
	return ticketService.getTicket(query_string).then(
		(response) => {
			return Promise.resolve(response.data, response.data.message);
		},
		(error) => {
			const message =
				(error.response && error.response.data && error.response.data.message) ||
				error.message ||
				error.string();
			return Promise.reject(message);
		},
	);
};

export default { readTicketById };
