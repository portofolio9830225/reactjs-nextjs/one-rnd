import sample_incoming_remarkService from "../../../services/rizky/sample_incoming_remark/sample_incoming_remark.service";

const list = () => {
    return sample_incoming_remarkService.list().then(
		(response) => {
			return Promise.resolve(response.data, response.data.message);
		},
		(error) => {
			const message =
				(error.response && error.response.data && error.response.data.message) ||
				error.message ||
				error.toString();
			return Promise.reject(message);
		},
	);
};

export default {list}