import ticketremarkServices from "../../../services/rizky/ticket-remark/ticket-remark.service";

const readlist = () => {
	return ticketremarkServices.list().then(
		(response) => {
			return Promise.resolve(response.data, response.data.message);
		},
		(error) => {
			const message =
				(error.response && error.response.data && error.response.data.message) ||
				error.message ||
				error.toString();
			return Promise.reject(message);
		},
	);
};

export default { readlist }