import additionalInformationServices from "../../../services/rizky/additional_information/additional_information.services";

const readAdditionalInformation = () => {
	return additionalInformationServices.list().then(
		(response) => {
			return Promise.resolve(response.data, response.data.message);
		},
		(error) => {
			const message =
				(error.response && error.response.data && error.response.data.message) ||
				error.message ||
				error.toString();
			return Promise.reject(message);
		},
	);
};

export default { readAdditionalInformation }