import React, { useState, useEffect, useMemo } from 'react';
import PropTypes from 'prop-types';
import Swal from 'sweetalert2';
import { useTranslation } from 'react-i18next';
import moment from 'moment';
import Accordion, { AccordionItem } from '../components/bootstrap/Accordion';
import Card, { CardBody, CardHeader, CardLabel, CardTitle } from '../components/bootstrap/Card';
import PageWrapper from '../layout/PageWrapper/PageWrapper';
import FormulaModule from '../modules/FormulaModule';
import Page from '../layout/Page/Page';
import PageLayoutHeader from '../pages/common/Headers/PageLayoutHeader';
import FormGroup from '../components/bootstrap/forms/FormGroup';
import Input from '../components/bootstrap/forms/Input';
import Button from '../components/bootstrap/Button';
import TrialPlantModule from '../modules/TrialPlantModule';
import priorityModule from '../modules/PriorityModule';
import DarkDataTable from '../components/DarkDataTable';
import useDarkMode from '../hooks/useDarkMode';
import showNotification from '../components/extras/showNotification';
import ParameterTrial from '../components/custom/ParameterTrial';
import FormulaMaterial from '../components/custom/FormulaMaterial';
import { getRequester } from '../helpers/helpers';
import DetailTicket from '../components/custom/rizky/ticket/DetailTicket';
import FormCustomModalDetail from '../components/custom/FormCustomModalDetail';
import Modal, { ModalBody } from '../components/bootstrap/Modal';

const STATUS = ['Siap Trial Plant', 'Sedang Trial Plant'];

const handleSubmitStartTrial = (
	values,
	handleReloadData,
	handleShowFormTrial,
	dismissLoafingSubmit,
) => {
	TrialPlantModule.startTrial(values)
		.then((res) => {
			showNotification('Success!', res.status, 'success');
			handleReloadData();
			handleShowFormTrial('', '');
		})
		.catch((err) => {
			showNotification('Warning!', err, 'danger');
			dismissLoafingSubmit(false);
		});
	return values;
};

const handleSubmitFinishTrial = (
	values,
	handleReloadData,
	handleShowFormTrial,
	dismissLoafingSubmit,
) => {
	TrialPlantModule.finishTrial(values)
		.then((res) => {
			showNotification('Success!', res.status, 'success');
			handleReloadData();
			handleShowFormTrial('', '');
		})
		.catch((err) => {
			showNotification('Warning!', err, 'danger');
			dismissLoafingSubmit(false);
		});
	return values;
};

const FormButton = (dt) => {
	const { handleShowFormTrial, row } = dt;
	const newInitialValues = {};
	newInitialValues.ticket_id = row.ticket_id;
	newInitialValues.formula_id = row._id;
	newInitialValues.document_number = row.document_number;
	newInitialValues.customer_name = row.customer_name;
	return (
		<>
			<FormCustomModalDetail initialValues={newInitialValues} />

			{row.status_code == 'siap_trial_plant' ? (
				<Button
					type='button'
					color='success'
					onClick={() => {
						handleShowFormTrial('Start Trial', row);
					}}>
					Start Trial
				</Button>
			) : (
				<Button
					type='button'
					color='danger'
					onClick={() => {
						handleShowFormTrial('Finish Trial', row);
					}}>
					Finish Trial
				</Button>
			)}
		</>
	);
};

const CustomButton = (dt) => {
	const { row, listPriority, handleReloadData, handleShowFormTrial } = dt;

	// restructure the obj
	const initialValues = {};
	initialValues.id = row._id;

	return (
		<FormButton
			initialValues={initialValues}
			listPriority={listPriority}
			handleReloadData={handleReloadData}
			handleShowFormTrial={handleShowFormTrial}
			row={row}
		/>
	);
};

const CustomDataTable = ({
	listPriority,
	data,
	loading,
	totalRows,
	handlePageChange,
	handlePerRowsChange,
	handleReloadData,
	handleShowFormTrial,
}) => {
	const { darkModeStatus } = useDarkMode();

	const columns = useMemo(
		() => [
			{
				name: 'Register Date',
				selector: (row) => moment(row.created_at).format('YYYY-MMM-DD'),
				sortable: true,
			},

			{
				name: 'Document Number',
				selector: (row) => row.document_number,
				sortable: true,
			},
			{
				name: 'Customer',
				selector: (row) => row.customer_name,
				sortable: true,
			},
			{
				name: 'Job Number',
				selector: (row) => row.job_number,
				sortable: true,
			},
			{
				name: 'Status',
				selector: (row) => row.status,
				sortable: true,
			},
			{
				name: 'PIC',
				selector: (row) => (row.pic_name ? row.pic_name : row.pic),
				sortable: true,
			},
			{
				name: 'Action',
				// eslint-disable-next-line react/no-unstable-nested-components
				cell: (dt) => {
					return (
						<CustomButton
							row={dt}
							listPriority={listPriority}
							handleReloadData={handleReloadData}
							handleShowFormTrial={handleShowFormTrial}
						/>
					);
				},
			},
		],
		[listPriority, handleReloadData, handleShowFormTrial],
	);

	return (
		<DarkDataTable
			columns={columns}
			data={data}
			progressPending={loading}
			pagination
			paginationServer
			paginationTotalRows={totalRows}
			onChangeRowsPerPage={handlePerRowsChange}
			onChangePage={handlePageChange}
			theme={darkModeStatus ? 'custom_dark' : 'light'}
		/>
	);
};

const TrialBigPlant = () => {
	const { darkModeStatus } = useDarkMode();
	const [data, setData] = useState([]);
	const [listPriority, setPriority] = useState([]);
	const { username } = getRequester();

	const [loading, setLoading] = useState(false);
	const [loadingSubmit, setLoadingSubmit] = useState(false);
	const [totalRows, setTotalRows] = useState(0);
	const [perPage, setPerPage] = useState(10);
	const [page, setPage] = useState(1);
	const [reloadData, setReloadData] = useState(false);
	const [targeDateStart, setTargeDateStart] = useState(null);
	const [targeDateEnd, setTargeDateEnd] = useState(null);
	const [formTrialShow, setFormTrialShow] = useState(false);

	const [titleTrial, setTitleTrial] = useState(null);
	const [dataFormula, setDataFormula] = useState(null);
	const [dataFile, setDataFile] = useState([]);
	const [isReadOnlyFormula] = useState(true);
	const [isReadOnlyParameter] = useState(true);

	const [isReset, setReset] = useState(false);
	const [init, setInit] = useState(true);

	const [list_parameter] = useState([
		{
			value: 'mixing_sequences',
			label: 'mixing_sequences',
		},
		{
			value: 'homogenizer',
			label: 'homogenizer',
		},
		{
			value: 'pasteurization',
			label: 'Pasteurization',
		},
		{
			value: 'mass_balance',
			label: 'Mass Balance',
		},
		{
			value: 'spray_dryer',
			label: 'Spray dryer',
		},
	]);

	const handleShowFormTrial = async (title, data_formula) => {
		setTitleTrial(title);
		setDataFormula(data_formula);
		setFormTrialShow(!formTrialShow);
		setInit(true);
		setLoadingSubmit(false);
	};

	const dismissLoafingSubmit = () => {
		setLoadingSubmit(false);
	};

	const fetchTrial = async (nPage, nPerPage) => {
		setLoading(true);
		const query = {
			page: nPage,
			sizePerPage: nPerPage,
			showAll: false,
			status: STATUS,
		};
		return FormulaModule.readFormula(query).then((res) => {
			setData(res.foundData);
			setTotalRows(res.countData);
			setLoading(false);
		});
	};

	const fetchPriority = async () => {
		return priorityModule.readSelect().then((res) => {
			setPriority(res);
		});
	};

	const handleReloadData = () => {
		setReloadData(!reloadData);
	};

	const onChangeFile = (e) => {
		setDataFile(e.target.files);
	};

	const submitForm = (type) => {
		const { _id } = dataFormula;
		const formData = new FormData();
		formData.append('_id', _id);
		formData.append('requester', username);

		const keys = Object.keys(dataFile);

		keys.forEach((element) => {
			formData.append('files', dataFile[element]);
		});

		if (_id) {
			return Swal.fire({
				title: 'Are you sure?',
				text: 'Please check your entries !',
				icon: 'info',
				showCancelButton: true,
				confirmButtonText: 'Yes',
			}).then((result) => {
				setLoadingSubmit(true);
				if (result.value) {
					if (type === 'Finish Trial') {
						handleSubmitFinishTrial(
							formData,
							handleReloadData,
							handleShowFormTrial,
							dismissLoafingSubmit,
						);
					} else {
						handleSubmitStartTrial(
							formData,
							handleReloadData,
							handleShowFormTrial,
							dismissLoafingSubmit,
						);
					}
				}
			});
		}
		return _id;
	};

	const filter = () => {
		if (moment(moment(targeDateStart)).isAfter(moment(targeDateEnd))) {
			Swal.fire({
				title: 'Warning',
				text: 'start date cant be bigger than finish date',
				icon: 'warning',
				showCancelButton: false,
				confirmButtonText: 'Yes',
			});
		} else {
			fetchTrial(
				1,
				10,
				targeDateStart ? moment(targeDateStart).format('YYYY-MM-DD') : null,
				targeDateEnd ? moment(targeDateEnd).format('YYYY-MM-DD') : null,
			);
		}
	};

	useEffect(() => {
		fetchTrial(page, perPage, null, null);
		fetchPriority();
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [page, perPage, reloadData]);

	useEffect(() => {
		fetchTrial(1, 10, null, null);
		fetchPriority();
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	const { t } = useTranslation('menu');
	const [title] = useState({ title: 'Scheduling' });
	return (
		<PageWrapper title={title.title}>
			<PageLayoutHeader />
			<Page container='fluid'>
				{!formTrialShow && (
					<Card stretch>
						<CardHeader borderSize={1}>
							<CardLabel>
								<CardTitle>{t('trial_big_plant')}</CardTitle>
							</CardLabel>
						</CardHeader>
						<CardBody>
							<div className='row pb-4'>
								<div className='col-sm-2 col-md-2'>
									<FormGroup
										id='target_date_start'
										label='Schedule Date Start'
										isColForLabel
										labelClassName='col-sm-4 text-capitalize'
										childWrapperClassName='col-sm-8'>
										<Input
											type='date'
											onInput={(value) => {
												setTargeDateStart(value.target.value);
											}}
											selected={targeDateStart}
										/>
									</FormGroup>
								</div>
								<div className='col-sm-2 col-md-2'>
									<FormGroup
										id='target_date_finish'
										label='Schedule Date Finish'
										isColForLabel
										labelClassName='col-sm-4 text-capitalize'
										childWrapperClassName='col-sm-8'>
										<Input
											type='date'
											onInput={(value) => {
												setTargeDateEnd(value.target.value);
											}}
											selected={targeDateEnd}
										/>
									</FormGroup>
								</div>
								<div className='col-sm-2 col-md-2'>
									<Button
										icon='FilterAlt'
										color='info'
										type='button'
										className='mx-1'
										onClick={filter}
										isLight={darkModeStatus}>
										Filter
									</Button>
								</div>
							</div>
							<CustomDataTable
								data={data}
								loading={loading}
								totalRows={totalRows}
								listPriority={listPriority}
								handlePageChange={setPage}
								handlePerRowsChange={setPerPage}
								handleReloadData={handleReloadData}
								handleShowFormTrial={handleShowFormTrial}
							/>
						</CardBody>
					</Card>
				)}
				{formTrialShow && (
					<Card stretch>
						<CardHeader borderSize={1}>
							<CardLabel>
								<CardTitle>
									{titleTrial} - {dataFormula.document_number} |{' '}
									{dataFormula.job_number ? dataFormula.job_number : ''}
								</CardTitle>
							</CardLabel>

							<div>
								<DetailTicket label={dataFormula.document_number} />
								<Button
									icon='Close'
									type='button'
									color='danger'
									onClick={() => {
										setDataFile(null);
										handleShowFormTrial(null, null);
									}}
								/>
							</div>
						</CardHeader>

						<CardBody>
							<div className='row'>
								<div className='col-md-4'>
									<FormGroup
										id='upload_document'
										label='Upload Document'
										className='col-md-12'>
										<Input
											multiple
											type='file'
											placeholder='File'
											onChange={(e) => onChangeFile(e)}
										/>
									</FormGroup>
								</div>
							</div>
							<br />
							<div className='row'>
								<div className='col-md-12'>
									<Accordion
										id='accordion-form'
										activeItemId={formTrialShow ? false : 'form-default'}>
										<AccordionItem id='form-default' title='Formula'>
											<FormulaMaterial
												listMaterial={[]}
												listVendor={[]}
												listUom={[]}
												initialValues={
													dataFormula ? dataFormula.formula : []
												}
												isReadOnly={isReadOnlyFormula}
												setInit={setInit}
											/>
										</AccordionItem>
									</Accordion>
								</div>
							</div>
							<br />

							<div className='row'>
								<div className='col-md-12'>
									<Accordion
										id='accordion-form'
										activeItemId={formTrialShow ? false : 'form-default'}>
										<AccordionItem id='form-default' title='Parameter Detail'>
											<ParameterTrial
												initialValues={
													dataFormula.details ? dataFormula.details : []
												}
												list_parameter={list_parameter}
												isReset={isReset}
												setReset={setReset}
												isReadOnly={isReadOnlyParameter}
												init={init}
												setInit={setInit}
											/>
										</AccordionItem>
									</Accordion>
								</div>

								<div className='col-md-12'>
									<br />
									<br />
									<Button
										icon='Save'
										type='submit'
										color='success'
										onClick={() => {
											submitForm(titleTrial);
										}}
										className='float-end'>
										{titleTrial}
									</Button>
								</div>
							</div>
						</CardBody>
					</Card>
				)}

				<Modal
					isOpen={loadingSubmit}
					size='sm'
					isCentered
					setIsOpen={() => {}}
					isStaticBackdrop>
					<ModalBody
						style={{ backgroundColor: '#6c5dd3', color: 'white' }}
						className='text-center'>
						<button className='btn btn-primary' type='button' disabled>
							<span
								className='spinner-grow spinner-grow-sm'
								role='status'
								aria-hidden='true'
							/>
							&nbsp;
							<span
								className='spinner-grow spinner-grow-sm'
								role='status'
								aria-hidden='true'
							/>
							&nbsp;
							<span
								className='spinner-grow spinner-grow-sm'
								role='status'
								aria-hidden='true'
							/>
							&nbsp;
							<span className='sr-only'>Loading...</span>
						</button>
					</ModalBody>
				</Modal>
			</Page>
		</PageWrapper>
	);
};

CustomDataTable.propTypes = {
	listPriority: PropTypes.oneOfType([PropTypes.instanceOf(Object)]),
	data: PropTypes.arrayOf(PropTypes.instanceOf(Object)),
	loading: PropTypes.bool,
	totalRows: PropTypes.number,
	handlePageChange: PropTypes.func,
	handlePerRowsChange: PropTypes.func,
	handleReloadData: PropTypes.func,
	handleShowFormTrial: PropTypes.func,
};
CustomDataTable.defaultProps = {
	listPriority: {},
	data: [],
	loading: false,
	totalRows: 0,
	handlePageChange: null,
	handlePerRowsChange: null,
	handleReloadData: null,
	handleShowFormTrial: null,
};

export default TrialBigPlant;
