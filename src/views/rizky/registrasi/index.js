// eslint-disable-next-line eslint-comments/disable-enable-pair
/* eslint-disable prettier/prettier */
import React, {
	useState,
	useEffect,
	useRef,
	useImperativeHandle,
	useReducer,
	forwardRef,
	useMemo,
	useContext,
} from 'react';
// eslint-disable-next-line import/no-extraneous-dependencies
import { useTranslation } from 'react-i18next';
import { Form, Formik, useFormik } from 'formik';
import Swal from 'sweetalert2';
import moment from 'moment';
import PropTypes from 'prop-types';
import PageWrapper from '../../../layout/PageWrapper/PageWrapper';
import Page from '../../../layout/Page/Page';
import PageLayoutHeader from '../../../pages/common/Headers/PageLayoutHeader';
import Card, {
	CardBody,
	CardHeader,
	CardLabel,
	CardTitle,
} from '../../../components/bootstrap/Card';
import FormGroup from '../../../components/bootstrap/forms/FormGroup';
import InputGroup from '../../../components/bootstrap/forms/InputGroup';
import RegistrasiModule from '../../../modules/rizky/registrasi/registrasiModule';
import customersModule from '../../../modules/rizky/customers/customersModule';
import currenciesModule from '../../../modules/rizky/currencies/currenciesModule';
import MODULE from '../../../modules/rizky/additionalInformastion/additional-informastionModule';
import showNotification from '../../../components/extras/showNotification';
import Input from '../../../components/bootstrap/forms/Input';
import useDarkMode from '../../../hooks/useDarkMode';
import DarkDataTable from '../../../components/DarkDataTable';
import DateTimeButton from '../../../components/custom/rizky/atoms/DateTimeButton';
import Button from '../../../components/bootstrap/Button';
import Modal, {
	ModalBody,
	ModalHeader,
	ModalTitle,
	ModalFooter,
} from '../../../components/bootstrap/Modal';
import CustomSelect from '../../../components/CustomSelect';
import DetailTicketAndFormulaComponent from '../../../components/custom/DetailTicketAndFormulaComponent';
import { getRequester } from '../../../helpers/helpers';
import ticketRemarkModule from '../../../modules/rizky/ticket-remark/ticket-remarkModule';

const DataContext = React.createContext();

const handleSubmitUpload = (values, reloadTable) => {
	if (values) {
		delete values.loading;
		return Swal.fire({
			title: 'Are you sure?',
			text: 'Please check your entries !',
			icon: 'info',
			showCancelButton: true,
			confirmButtonText: 'Yes',
		}).then((result) => {
			if (result.value) {
				RegistrasiModule.uploadFile(values)
					.then((res) => {
						reloadTable();
						showNotification('Success!', res.status, 'success');
					})
					.catch((err) => {
						showNotification('Warning!', err, 'danger');
					});
			} else if (result.dismiss === Swal.DismissReason.cancel) {
				Swal.fire('Cancelled', 'Your data is safe :)', 'error');
			}
		});
	}
	return values;
};
const handleSubmitUploadSelesai = (values, reloadTable) => {
	if (values) {
		delete values.loading;
		return Swal.fire({
			title: 'Are you sure?',
			text: 'Please check your entries !',
			icon: 'info',
			showCancelButton: true,
			confirmButtonText: 'Yes',
		}).then((result) => {
			if (result.value) {
				RegistrasiModule.uploadFileSelesaiAnalisa(values)
					.then((res) => {
						reloadTable();
						showNotification('Success!', res.status, 'success');
					})
					.catch((err) => {
						showNotification('Warning!', err, 'danger');
					});
			} else if (result.dismiss === Swal.DismissReason.cancel) {
				Swal.fire('Cancelled', 'Your data is safe :)', 'error');
			}
		});
	}
	return values;
};

const Table = forwardRef((props, ref) => {
	// eslint-disable-next-line react/prop-types
	const { customers } = useContext(DataContext);
	const { darkModeStatus } = useDarkMode();
	const [data, setData] = useState([]);
	const [loading, setLoading] = useState(false);
	const [totalRows, setTotalRows] = useState(0);
	const [perPage, setPerPage] = useState(10);

	const fetchData = async (page, params = {}) => {
		setLoading(true);
		// setCustomers(props);

		params = {
			...params,
			page,
			sizePerPage: perPage,
		};

		return RegistrasiModule.readTickets(new URLSearchParams(params)).then((res) => {
			setData(res.foundData);
			setTotalRows(res.countData);
			setLoading(false);
		});
	};

	const handlePageChange = (page) => {
		fetchData(page);
	};

	const handlePerRowsChange = async (newPerPage, page) => {
		setLoading(true);

		const params = {
			page,
			sizePerPage: newPerPage,
		};

		return RegistrasiModule.readTickets(new URLSearchParams(params)).then((res) => {
			setData(res.foundData);
			setPerPage(newPerPage);
			setLoading(false);
		});
	};

	useEffect(() => {
		fetchData(1); // fetch page 1 of data

		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	// eslint-disable-next-line react-hooks/exhaustive-deps
	const reloadTable = () => {
		fetchData(1);
	};

	useImperativeHandle(ref, () => ({
		inReloadTable(params) {
			fetchData(1, params);
		},
	}));

	const columns = useMemo(
		() => [
			{
				name: 'Ticket ID',
				selector: (row) => row.document_number,
				sortable: true,
			},
			{
				name: 'Sample',
				selector: (row) => row.sample_product,
				sortable: true,
			},
			{
				name: 'Customer',
				selector: (row) => row.customer_name,
				sortable: true,
			},
			{
				name: 'Target Date',
				selector: (row) => moment(row.target_date).format('DD MMM YYYY'),
				sortable: true,
			},
			{
				name: 'Category',
				selector: (row) => row.category,
				sortable: true,
			},
			{
				name: 'PIC',
				selector: (row) => row.pic_name,
				sortable: true,
			},
			{
				name: 'Action',
				width: '300px',
				// eslint-disable-next-line react/no-unstable-nested-components
				cell: (dt) => {
					return (
						<CustomButton
							customers={customers.customers}
							row={dt}
							reloadTable={() => reloadTable()}
						/>
					);
				},
			},
		],
		[reloadTable, customers.customers],
	);

	return (
		<DarkDataTable
			columns={columns}
			data={data}
			progressPending={loading}
			pagination
			paginationServer
			paginationTotalRows={totalRows}
			onChangeRowsPerPage={handlePerRowsChange}
			onChangePage={handlePageChange}
			theme={darkModeStatus ? 'custom_dark' : 'light'}
		/>
	);
});

const FormSiapAnalisaFormulasi = (dt) => {
	const [isOpen, setIsOpen] = useState(false);
	const [selectedFile, setSelectedFile] = useState();
	const [isFilePicked, setIsFilePicked] = useState(false);
	const { initialValues, reloadTable } = dt;

	const onSubmit = (values, { setSubmitting, resetForm, setStatus, setErrors }) => {
		values.formData = new FormData();
		values.formData.append('files', selectedFile);
		values.formData.append('idticket', initialValues._id);
		if (isFilePicked) {
			try {
				handleSubmitUpload(values, reloadTable);
				resetForm(initialValues);
				setStatus({ success: true });
			} catch (error) {
				setStatus({ success: false });
				setSubmitting(false);
				setErrors({ submit: error.message });
			}
		}
	};

	const handleChangeFile = (value) => {
		setSelectedFile(value.target.files[0]);
		setIsFilePicked(true);
	};

	return (
		<>
			<div style={{ paddingRight: '5px' }}>
				<Button isOutline type='button' color='primary' onClick={() => setIsOpen(true)}>
					Proses Analisa
				</Button>
			</div>
			<Modal isOpen={isOpen} setIsOpen={setIsOpen} size='md' titleId='modal-pay-menu'>
				<ModalHeader setIsOpen={setIsOpen} className='p-4'>
					<ModalTitle id='modal-pay-menu'>Upload Form Analisa</ModalTitle>
				</ModalHeader>
				<Formik enableReinitialize initialValues={{ ...initialValues }} onSubmit={onSubmit}>
					{(formikField) => {
						return (
							<Form>
								<ModalBody className='px-4 pb-5'>
									<InputGroup>
										<Input type='file' onChange={(e) => handleChangeFile(e)} />
										<Button
											isOutline
											color='dark'
											icon='CloudUpload'
											type='submit'
											isDisable={
												!formikField.isValid && !!formikField.submitCount
											}>
											Upload
										</Button>
									</InputGroup>
								</ModalBody>
							</Form>
						);
					}}
				</Formik>
			</Modal>
		</>
	);
};

const FormSelesaiAnalisaFormulasi = (dt) => {
	const [isOpen, setIsOpen] = useState(false);
	const [selectedFile, setSelectedFile] = useState();
	const [isFilePicked, setIsFilePicked] = useState(false);
	const { initialValues, reloadTable } = dt;

	const onSubmit = (values, { setSubmitting, resetForm, setStatus, setErrors }) => {
		values.formData = new FormData();
		values.formData.append('files', selectedFile);
		values.formData.append('idticket', initialValues._id);
		if (isFilePicked) {
			try {
				handleSubmitUploadSelesai(values, reloadTable);
				resetForm(initialValues);
				setStatus({ success: true });
			} catch (error) {
				setStatus({ success: false });
				setSubmitting(false);
				setErrors({ submit: error.message });
			}
		}
	};

	const handleChangeFile = (value) => {
		setSelectedFile(value.target.files[0]);
		setIsFilePicked(true);
	};

	return (
		<>
			<div style={{ paddingRight: '5px' }}>
				<Button isOutline type='button' color='primary' onClick={() => setIsOpen(true)}>
					Selesai Analisa
				</Button>
			</div>
			<Modal isOpen={isOpen} setIsOpen={setIsOpen} size='md' titleId='modal-pay-menu'>
				<ModalHeader setIsOpen={setIsOpen} className='p-4'>
					<ModalTitle id='modal-pay-menu'>Upload Form Hasil Analisa</ModalTitle>
				</ModalHeader>
				<Formik enableReinitialize initialValues={{ ...initialValues }} onSubmit={onSubmit}>
					{(formikField) => {
						return (
							<Form>
								<ModalBody className='px-4 pb-5'>
									<InputGroup>
										<Input type='file' onChange={(e) => handleChangeFile(e)} />
										<Button
											isOutline
											color='dark'
											icon='CloudUpload'
											type='submit'
											isDisable={
												!formikField.isValid && !!formikField.submitCount
											}>
											Upload
										</Button>
									</InputGroup>
								</ModalBody>
							</Form>
						);
					}}
				</Formik>
			</Modal>
		</>
	);
};

const onSearch = (state, formik, customDataTableRef) => {
	if (!state.dateRange.startDate && !state.dateRange.endDate) {
		if (formik.values.search) {
			customDataTableRef.current.inReloadTable({
				search_invoice_no: formik.values.search,
			});
		} else {
			customDataTableRef.current.inReloadTable();
		}
	} else if (state.dateRangeHasChange) {
		if (formik.values.search) {
			customDataTableRef.current.inReloadTable({
				search_invoice_no: formik.values.search,
				searchdate_created_at: JSON.stringify({
					$gte: moment(state.dateRange.startDate).format('YYYY-MM-DD'),
					$lte: moment(state.dateRange.endDate).format('YYYY-MM-DD'),
				}),
			});
		} else {
			customDataTableRef.current.inReloadTable({
				searchdate_created_at: JSON.stringify({
					$gte: moment(state.dateRange.startDate).format('YYYY-MM-DD'),
					$lte: moment(state.dateRange.endDate).format('YYYY-MM-DD'),
				}),
			});
		}
	} else if (formik.values.search) {
		customDataTableRef.current.inReloadTable({
			search_invoice_no: formik.values.search,
		});
	} else {
		customDataTableRef.current.inReloadTable();
	}
};

const handleSubmit = (values, reloadTable, handleReset, resetForm, initialValues) => {
	if (values.customer_name && values.category && values.initial_project && values.note) {
		delete values.loading;
		return Swal.fire({
			title: 'Are you sure?',
			text: 'Please check your entries !',
			icon: 'info',
			showCancelButton: true,
			confirmButtonText: 'Yes',
		}).then((result) => {
			if (result.value) {
				RegistrasiModule.store(values)
					.then((res) => {
						reloadTable();
						showNotification('Success!', res.status, 'success');
						resetForm(initialValues);
						handleReset();
					})
					.catch((err) => {
						showNotification('Warning!', err, 'danger');
					});
			} else if (result.dismiss === Swal.DismissReason.cancel) {
				Swal.fire('Cancelled', 'Your data is safe :)', 'error');
			}
		});
	}
	return values;
};

const handleSubmitEdit = (values, reloadTable) => {
	if (values.customer_name && values.category && values.initial_project && values.note) {
		return Swal.fire({
			title: 'Are you sure?',
			text: 'Please check your entries !',
			icon: 'info',
			showCancelButton: true,
			confirmButtonText: 'Yes',
		}).then((result) => {
			if (result.value) {
				RegistrasiModule.update(values)
					.then((res) => {
						reloadTable();
						showNotification('Success!', res.status, 'success');
					})
					.catch((err) => {
						showNotification('Warning!', err, 'danger');
					});
			} else if (result.dismiss === Swal.DismissReason.cancel) {
				Swal.fire('Cancelled', 'Your data is safe :)', 'error');
			}
		});
	}
	return values;
};

const FormEdit = (dt) => {
	const { initialValues, reloadTable } = dt;

	const { customers, note, additional_information, categories, currencies } =
		useContext(DataContext);

	const [isOpen, setIsOpen] = useState(false);
	const [customerSelected, setCustomerSelected] = useState(
		initialValues.customer_name
			? {
					label: initialValues.customer_name,
					value: initialValues.customer_name,
					text: initialValues.customer_name,
			  }
			: null,
	);
	const [noteSelected, setNoteSelected] = useState(
		initialValues.note
			? {
					label: initialValues.note,
					value: initialValues.note,
					text: initialValues.note,
			  }
			: null,
	);
	const [catagoriesSelected, setCategoriesSelected] = useState(
		initialValues.category
			? {
					label: initialValues.category,
					value: initialValues.category,
					text: initialValues.category,
			  }
			: null,
	);
	const [currenciesSelected, setCurrenciesSelected] = useState(
		initialValues.currencies
			? {
					label: initialValues.currencies,
					value: initialValues.currencies,
					text: initialValues.currencies,
			  }
			: null,
	);
	const [sampelList, setsampelList] = useState(initialValues.sample);
	const [additionalList, setAdditionalList] = useState(initialValues.information);
	const onSubmit = (values, { setSubmitting, resetForm, setStatus, setErrors }) => {
		let newValue = {
			...values,
			sample: sampelList,
			information: additionalList,
		};
		delete newValue.sample_product;
		newValue.sample_product = values.sample_product === 'No' ? 'false' : 'true';
		if (customerSelected && catagoriesSelected && noteSelected) {
			if (additionalList.length > 0) {
				newValue = {
					...values,
					customer_name: customerSelected.value,
					category: catagoriesSelected.value,
					note: noteSelected.value,
					sample: sampelList,
					information: additionalList,
				};
			}
		}
		if (currenciesSelected) {
			if (additionalList.length > 0) {
				newValue = {
					...values,
					currency: currenciesSelected.value,
				};
			}
		}

		if (!customerSelected) {
			showNotification('Customer', 'customer does not exist, cannot create ticket', 'danger');
		}
		if (!catagoriesSelected) {
			showNotification('Target', 'target does not exist, cannot create ticket', 'danger');
		}
		if (additionalList.length === 0) {
			showNotification(
				'Additional Information',
				'Additional information does not exist, cannot create ticket',
				'danger',
			);
		}
		try {
			delete newValue.loading;
			handleSubmitEdit(newValue, reloadTable);
			resetForm(initialValues);
			setStatus({ success: true });
		} catch (error) {
			setStatus({ success: false });
			setSubmitting(false);
			setErrors({ submit: error.message });
		}
	};
	const handleremove = (index) => {
		const samplelist = [...sampelList];
		samplelist.forEach((e, i) => {
			if (i === index) {
				samplelist.splice(index, 1);
			}
		});
		setsampelList(samplelist);
	};

	const handleSelectCustomer = (e) => {
		return setCustomerSelected(e);
	};

	const handleCategoriesSelected = (e) => {
		return setCategoriesSelected(e);
	};

	const handleCurrenciesSelected = (e) => {
		return setCurrenciesSelected(e);
	};

	const handleSelectAdditional = (e, index) => {
		const obj = additionalList[index];
		obj.name = e.value;
		const newSampel = [
			...additionalList.slice(0, index),
			obj,
			...additionalList.slice(index + 1),
		];
		return setAdditionalList(newSampel);
	};

	const handleChangeAdditional = (e, index, key) => {
		const { target } = e;
		const { value } = target;
		let newSampel = [];
		const obj = additionalList[index];
		switch (key) {
			case 'name':
				obj.value = value;
				newSampel = [
					...additionalList.slice(0, index),
					obj,
					...additionalList.slice(index + 1),
				];
				break;
			default:
				obj.value = value;
				newSampel = [
					...additionalList.slice(0, index),
					obj,
					...additionalList.slice(index + 1),
				];
				break;
		}

		return setAdditionalList(newSampel);
	};

	const handleAddAditional = () => {
		return setAdditionalList([...additionalList, { name: '', value: '' }]);
	};

	const handleRemoveAdditional = (index) => {
		const additionallist = [...additionalList];
		additionallist.forEach((e, i) => {
			if (i === index) {
				additionallist.splice(index, 1);
			}
		});
		setAdditionalList(additionallist);
	};

	const handleChangeSampel = (e, index, key) => {
		const { target } = e;
		const { value } = target;
		let newSampel = [];
		const obj = sampelList[index];
		switch (key) {
			case 'sampel_code':
				obj.sampel_code = value;
				newSampel = [...sampelList.slice(0, index), obj, ...sampelList.slice(index + 1)];
				break;
			case 'sampel_name':
				obj.sampel_name = value;
				newSampel = [...sampelList.slice(0, index), obj, ...sampelList.slice(index + 1)];
				break;
			case 'batch':
				obj.batch = value;
				newSampel = [...sampelList.slice(0, index), obj, ...sampelList.slice(index + 1)];
				break;
			case 'qty':
				obj.qty = value;
				newSampel = [...sampelList.slice(0, index), obj, ...sampelList.slice(index + 1)];
				break;
			default:
				obj.uom = value;
				newSampel = [...sampelList.slice(0, index), obj, ...sampelList.slice(index + 1)];
				break;
		}

		return setsampelList(newSampel);
	};

	const handleaddclick = () => {
		return setsampelList([
			...sampelList,
			{
				id: sampelList.length + 1,
				sampel_code: '',
				sampel_name: '',
				batch: '',
				qty: '',
				uom: '',
			},
		]);
	};

	useEffect(() => {}, [customers.length]);

	const { t } = useTranslation('registrasi');

	const validate = (values) => {
		const errors = {};
		if (!values.date_of_communication) {
			errors.date_of_communication = 'Required';
		}
		if (!values.target_date) {
			errors.target_date = 'Required';
		}
		if (!values.initial_project) {
			errors.initial_project = 'Required';
		}

		return errors;
	};
	return (
		<>
			<Button
				isOutline
				icon='Edit'
				type='button'
				color='primary'
				onClick={() => setIsOpen(true)}
			/>

			<Modal
				isStaticBackdrop
				isOpen={isOpen}
				setIsOpen={setIsOpen}
				size='lg'
				titleId='modal-edit-menu-crud'>
				<ModalHeader setIsOpen={setIsOpen} className='p-4'>
					<ModalTitle id='modal-edit-menu-crud'> Update Data</ModalTitle>
				</ModalHeader>
				<Formik
					enableReinitialize
					initialValues={{ ...initialValues }}
					onSubmit={onSubmit}
					validate={validate}>
					{(formikField) => {
						return (
							<Form>
								<ModalBody className='px-4'>
									<div className='row'>
										<div className='col-md-6'>
											<div className='w-100'>
												<div className='fload-end'>
													<FormGroup
														id='date'
														label='Date'
														className='col-md-12 mb-3'>
														<Input
															readOnly
															type='date'
															onChange={formikField.handleChange}
															onBlur={formikField.handleBlur}
															value={formikField.values.date}
															isValid={formikField.isValid}
															isTouched={formikField.touched.date}
															invalidFeedback={
																formikField.errors.date
															}
															autoComplete='off'
														/>
													</FormGroup>
												</div>
											</div>
											<FormGroup
												id='sample_product'
												label='Sampel Product'
												className='col-md-12 mb-3'>
												<Input
													readOnly
													value={formikField.values.sample_product}
													autoComplete='off'
												/>
											</FormGroup>
											<FormGroup
												id='customer'
												label='Customer'
												className='col-md-12 mb-3'>
												<CustomSelect
													placeholder='Select Customer'
													onChange={(e) => handleSelectCustomer(e)}
													value={
														customerSelected || {
															text: initialValues.customer_name,
															value: initialValues.customer_code,
															label: `${initialValues.customer_code} - ${initialValues.customer_name}`,
														}
													}
													isSearchable
													options={customers}
												/>
											</FormGroup>
											<FormGroup
												id='target'
												label='Target'
												className='col-md-12'>
												<CustomSelect
													placeholder='Select Target'
													onChange={(e) => handleCategoriesSelected(e)}
													value={
														catagoriesSelected || {
															text: initialValues.category,
															value: initialValues.category,
															label: initialValues.category,
														}
													}
													options={categories}
												/>
											</FormGroup>
											<FormGroup
												id='note'
												label='Note'
												className='col-md-12 mb-3'>
												<CustomSelect
													placeholder='Select Note'
													onChange={(e) => setNoteSelected(e)}
													value={noteSelected}
													options={note}
												/>
											</FormGroup>
											<FormGroup
												id='initial_project'
												label='Initial Project'
												className='col-md-12 mb-3'>
												<Input
													onChange={formikField.handleChange}
													onBlur={formikField.handleBlur}
													value={formikField.values.initial_project}
													isValid={formikField.isValid}
													isTouched={formikField.touched.initial_project}
													invalidFeedback={
														formikField.errors.initial_project
													}
													autoComplete='off'
												/>
											</FormGroup>
										</div>
										<div className='col-md-6'>
											<div className='w-100'>
												<div className='fload-end'>
													<FormGroup
														id='target_date'
														label='Target Date'
														className='col-md-12 mb-3'>
														<Input
															type='date'
															onChange={formikField.handleChange}
															onBlur={formikField.handleBlur}
															value={formikField.values.target_date}
															isValid={formikField.isValid}
															isTouched={
																formikField.touched.target_date
															}
															invalidFeedback={
																formikField.errors.target_date
															}
															autoComplete='off'
														/>
													</FormGroup>
													<FormGroup
														id='date_of_communication'
														label='Date of Communication'
														className='col-md-12 mb-3'>
														<Input
															type='date'
															onChange={formikField.handleChange}
															onBlur={formikField.handleBlur}
															value={
																formikField.values
																	.date_of_communication
															}
															isValid={formikField.isValid}
															isTouched={
																formikField.touched
																	.date_of_communication
															}
															invalidFeedback={
																formikField.errors
																	.date_of_communication
															}
															autoComplete='off'
														/>
													</FormGroup>
												</div>
											</div>
											<FormGroup
												id='target_price'
												label='Target Price'
												className='col-md-12 mb-3'>
												<Input
													type='number'
													onChange={formikField.handleChange}
													onBlur={formikField.handleBlur}
													value={formikField.values.target_price}
													isValid={formikField.isValid}
													isTouched={formikField.touched.target_price}
													invalidFeedback={
														formikField.errors.target_price
													}
													autoComplete='off'
												/>
											</FormGroup>
											<FormGroup
												id='currencies'
												label='Currency'
												className='col-md-12'>
												<CustomSelect
													placeholder='Select Target'
													onChange={(e) => handleCurrenciesSelected(e)}
													value={
														currenciesSelected || {
															text: initialValues.currency,
															value: initialValues.currency,
															label: initialValues.currency,
														}
													}
													options={currencies}
												/>
											</FormGroup>
										</div>
									</div>
									<div className='mt-3'>
										<div className='row'>
											<CardHeader borderSize={1}>
												<CardLabel>
													<CardTitle>
														{t('Additional Information')}
													</CardTitle>
												</CardLabel>
											</CardHeader>
											<div className='col-md-12 mt-2 mb-2'>
												<Button
													type='button'
													color='success'
													onClick={handleAddAditional}
													className='col-md-1'>
													+
												</Button>
											</div>
										</div>
										<table className='table table-bordered'>
											<thead>
												<tr>
													<th>Pilih Additional Information</th>
													<th>Value</th>
												</tr>
											</thead>
											<tbody>
												{additionalList &&
													additionalList.map((el, i) => {
														return (
															<tr>
																<td>
																	<CustomSelect
																		placeholder='Select Additional'
																		onChange={(e) =>
																			handleSelectAdditional(
																				e,
																				i,
																			)
																		}
																		value={{
																			value: el.name,
																			text: el.name,
																			label: el.name,
																		}}
																		options={
																			additional_information
																		}
																	/>
																</td>
																<td>
																	<div>
																		<input
																			className='form-control'
																			onChange={(e) =>
																				handleChangeAdditional(
																					e,
																					i,
																					'name',
																				)
																			}
																			value={el.value || null}
																			name='value'
																			placeholder='Value'
																			aria-label='test'
																		/>
																	</div>
																</td>
																<td>
																	<Button
																		icon=''
																		type='button'
																		color='danger'
																		onClick={() =>
																			handleRemoveAdditional(
																				i,
																			)
																		}>
																		-
																	</Button>
																</td>
															</tr>
														);
													})}
											</tbody>
										</table>
									</div>
									<div className='mt-3'>
										<div className='row'>
											<CardHeader borderSize={1}>
												<CardLabel>
													<CardTitle>{t('Sample')}</CardTitle>
												</CardLabel>
											</CardHeader>
											<div className='col-md-12 mt-2 mb-2'>
												<Button
													type='button'
													color='success'
													onClick={handleaddclick}
													className='col-md-1'>
													+
												</Button>
											</div>
										</div>
										<table className='table table-bordered'>
											<thead>
												<tr>
													<th>Sample Code</th>
													<th>Sample Name</th>
													<th>Batch</th>
													<th>Quantity</th>
													<th>UOM</th>
												</tr>
											</thead>
											<tbody>
												{sampelList &&
													sampelList.map((el, i) => {
														return (
															<tr>
																<td>
																	<div>
																		<input
																			className='form-control'
																			onChange={(e) =>
																				handleChangeSampel(
																					e,
																					i,
																					'sampel_code',
																				)
																			}
																			value={
																				el.sampel_code ||
																				null
																			}
																			name='sample_code'
																			placeholder='Sampel Code'
																			aria-label='test'
																		/>
																	</div>
																</td>
																<td>
																	<div>
																		<input
																			className='form-control'
																			onChange={(e) =>
																				handleChangeSampel(
																					e,
																					i,
																					'sampel_name',
																				)
																			}
																			value={
																				el.sampel_name ||
																				null
																			}
																			name='sample_name'
																			placeholder='Sampel Name'
																			aria-label='test'
																		/>
																	</div>
																</td>
																<td>
																	<div>
																		<input
																			className='form-control'
																			onChange={(e) =>
																				handleChangeSampel(
																					e,
																					i,
																					'batch',
																				)
																			}
																			value={el.batch || null}
																			name='batch'
																			placeholder='Batch'
																			aria-label='test'
																		/>
																	</div>
																</td>
																<td>
																	<div>
																		<input
																			type='number'
																			className='form-control'
																			onChange={(e) =>
																				handleChangeSampel(
																					e,
																					i,
																					'qty',
																				)
																			}
																			value={el.qty || null}
																			name='qty'
																			placeholder='Qty'
																			aria-label='test'
																		/>
																	</div>
																</td>
																<td>
																	<div>
																		<input
																			className='form-control'
																			onChange={(e) =>
																				handleChangeSampel(
																					e,
																					i,
																					'uom',
																				)
																			}
																			value={el.uom || null}
																			name='uom'
																			placeholder='UOM'
																			aria-label='test'
																		/>
																	</div>
																</td>
																<td>
																	<Button
																		icon=''
																		type='button'
																		color='danger'
																		onClick={() =>
																			handleremove(i)
																		}>
																		-
																	</Button>
																</td>
															</tr>
														);
													})}
											</tbody>
										</table>
									</div>
								</ModalBody>
								<ModalFooter className='px-4 pb-4'>
									<div className='col-md-12 '>
										<Button
											icon='Save'
											isOutline
											type='submit'
											color='success'
											className='float-end'
											isDisable={
												!formikField.isValid && !!formikField.submitCount
											}>
											Update
										</Button>
									</div>
								</ModalFooter>
							</Form>
						);
					}}
				</Formik>
			</Modal>
		</>
	);
};

const FormDelete = (dt) => {
	const { initialValues, reloadTable } = dt;
	const onDelete = () => {
		Swal.fire({
			title: 'You want to deleting this row?',
			text: 'click yes to continue!',
			icon: 'info',
			showCancelButton: true,
			confirmButtonText: 'Yes',
		}).then((result) => {
			if (result.value) {
				if (result.value) {
					RegistrasiModule.destroy({
						_id: initialValues._id,
					})
						.then((res) => {
							reloadTable();
							showNotification('Success!', res.status, 'success');
						})
						.catch((err) => {
							showNotification('Warning!', err, 'danger');
						});
				} else if (result.dismiss === Swal.DismissReason.cancel) {
					Swal.fire('Cancelled', 'Your data is safe :)', 'error');
				}
			}
		});
	};

	return (
		<div style={{ paddingLeft: '5px' }}>
			<Button
				isOutline
				icon='Delete'
				type='button'
				color='danger'
				onClick={() => onDelete()}
			/>
		</div>
	);
};

const FormProsesFormulasi = (dt) => {
	const { initialValues, reloadTable } = dt;
	const onDelete = () => {
		Swal.fire({
			title: 'Are you sure to continue formulasi?',
			text: 'click yes to continue!',
			icon: 'info',
			showCancelButton: true,
			confirmButtonText: 'Yes',
		}).then((result) => {
			if (result.value) {
				if (result.value) {
					RegistrasiModule.prosesFormulasi({
						_id: initialValues._id,
					})
						.then((res) => {
							reloadTable();
							showNotification('Success!', res.status, 'success');
						})
						.catch((err) => {
							showNotification('Warning!', err, 'danger');
						});
				} else if (result.dismiss === Swal.DismissReason.cancel) {
					Swal.fire('Cancelled', 'Your data is safe :)', 'error');
				}
			}
		});
	};

	return (
		<div style={{ paddingRight: '5px' }}>
			<Button isOutline type='button' color='primary' onClick={() => onDelete()}>
				Formulasi
			</Button>
		</div>
	);
};

const CustomButton = (dt) => {
	const { row, reloadTable, customers } = dt;
	const { username } = getRequester();
	const { _id } = row;
	const initialValues = {
		loading: false,
		target_price: row.target_price,
		sample_product: row.sample_product,
		category: row.category,
		currency: row.currency,
		customer_name: row.customer_name,
		customer_code: row.customer_code,
		information: row.information,
		sample: row.sample,
		date: moment(row.date).format('YYYY-MM-DD'),
		date_of_communication: moment(row.date_of_communication).format('YYYY-MM-DD'),
		target_date: moment(row.target_date).format('YYYY-MM-DD'),
		ticket_id: _id,
		document_number: row.document_number,
		initial_project: row.initial_project,
		note: row.note,
		_id,
	};
	let history = 0;
	if (row.sample) {
		history = row.history.length - 1;
	}

	if (row.pic === username) {
		switch (row.history[history].status) {
			case 'Siap Analisa Sampel':
				return (
					<>
						<FormCustomModalDetail initialValues={initialValues} />
						<FormSiapAnalisaFormulasi
							initialValues={initialValues}
							reloadTable={() => reloadTable()}
						/>
						<FormEdit
							cust={customers}
							initialValues={initialValues}
							reloadTable={() => reloadTable()}
						/>
						<FormDelete
							initialValues={initialValues}
							reloadTable={() => reloadTable()}
						/>
					</>
				);
			case 'Sedang Analisa Sample':
				return (
					<>
						<FormCustomModalDetail initialValues={initialValues} />
						<FormSelesaiAnalisaFormulasi
							initialValues={initialValues}
							reloadTable={() => reloadTable()}
						/>
						<FormEdit
							cust={customers}
							initialValues={initialValues}
							reloadTable={() => reloadTable()}
						/>
						<FormDelete
							initialValues={initialValues}
							reloadTable={() => reloadTable()}
						/>
					</>
				);
			default:
				if (row.status_code === 'dokumen_dibuat') {
					return (
						<>
							<FormCustomModalDetail initialValues={initialValues} />
							<FormProsesFormulasi
								initialValues={initialValues}
								reloadTable={() => reloadTable()}
							/>
							<FormEdit
								cust={customers}
								initialValues={initialValues}
								reloadTable={() => reloadTable()}
							/>
							<FormDelete
								initialValues={initialValues}
								reloadTable={() => reloadTable()}
							/>
						</>
					);
				}
				return <FormCustomModalDetail initialValues={initialValues} />;
		}
	}
	return <FormCustomModalDetail initialValues={initialValues} />;
};

const FormInput = (dt) => {
	const { initialValues, reloadTable, customers } = dt;
	const { categories, note, currencies, additional_information } = useContext(DataContext);
	const [date, setDate] = useState(new Date());
	const [customerSelected, setCustomerSelected] = useState();
	const [catagoriesSelected, setCategoriesSelected] = useState();
	const [noteSelected, setNoteSelected] = useState();
	const [currenciesSelected, setCurrenciesSelected] = useState();
	const [sampelList, setsampelList] = useState([]);
	const [additionalList, setAdditionalList] = useState([]);
	const onSubmit = (values, { setSubmitting, setStatus, setErrors, resetForm }) => {
		let newValue = {
			...values,
			sample: sampelList,
			information: additionalList,
		};

		if (!customerSelected) {
			showNotification('Customer', 'customer does not exist, cannot create ticket', 'danger');
		}
		if (!noteSelected) {
			showNotification('Note', 'note does not exist, cannot create ticket', 'danger');
		}
		if (!catagoriesSelected) {
			showNotification('Target', 'target does not exist, cannot create ticket', 'danger');
		}
		if (additionalList.length === 0) {
			showNotification(
				'Additional Information',
				'Additional information does not exist, cannot create ticket',
				'danger',
			);
		}
		if (customerSelected && catagoriesSelected && noteSelected) {
			if (additionalList.length > 0) {
				newValue = {
					...newValue,
					information: additionalList,
				};
			}
			newValue = {
				...newValue,
				customer_name: customerSelected.value,
				category: catagoriesSelected.value,
				note: noteSelected.value,
				sample: sampelList,
			};
		}
		if (currenciesSelected) {
			if (additionalList.length > 0) {
				newValue = {
					...newValue,
					currency: currenciesSelected.value,
				};
			}
		}
		try {
			handleSubmit(newValue, reloadTable, handleReset, resetForm, initialValues);
			setStatus({ success: true });
		} catch (error) {
			setStatus({ success: false });
			setSubmitting(false);
			setErrors({ submit: error.message });
		}
	};

	const handleReset = () => {
		initialValues.target_price = null;
		initialValues.date_of_communication = null;
		initialValues.target_date = null;
		setCategoriesSelected(null);
		setCustomerSelected(null);
		setCurrenciesSelected(null);
		setAdditionalList([]);
		setsampelList([]);
	};

	const handleremove = (index) => {
		const sampellist = [...sampelList];
		sampellist.forEach((e, i) => {
			if (i === index) {
				sampellist.splice(index, 1);
			}
		});
		setsampelList(sampellist);
	};

	const handleSelectCustomer = (e) => {
		setCustomerSelected(e);
	};

	const handleCategoriesSelected = (e) => {
		setCategoriesSelected(e);
	};

	const handleCurrenciesSelected = (e) => {
		setCurrenciesSelected(e);
	};

	const handleSelectAdditional = (e, index) => {
		const obj = additionalList[index];
		obj.name = e.value;
		const newSampel = [
			...additionalList.slice(0, index),
			obj,
			...additionalList.slice(index + 1),
		];
		return setAdditionalList(newSampel);
	};

	const handleChangeAdditional = (e, index, key) => {
		const { target } = e;
		const { value } = target;
		let newSampel = [];
		const obj = additionalList[index];
		switch (key) {
			case 'name':
				obj.value = value;
				newSampel = [
					...additionalList.slice(0, index),
					obj,
					...additionalList.slice(index + 1),
				];
				break;
			default:
				obj.value = value;
				newSampel = [
					...additionalList.slice(0, index),
					obj,
					...additionalList.slice(index + 1),
				];
				break;
		}

		return setAdditionalList(newSampel);
	};

	const handleAddAditional = () => {
		return setAdditionalList([...additionalList, { name: '', value: '' }]);
	};

	const handleRemoveAdditional = (index) => {
		const additionallist = [...additionalList];
		additionallist.forEach((e, i) => {
			if (i === index) {
				additionallist.splice(index, 1);
			}
		});
		setAdditionalList(additionallist);
	};

	const handleChangeSampel = (e, index, key) => {
		const { target } = e;
		const { value } = target;
		let newSampel = [];
		const obj = sampelList[index];
		switch (key) {
			case 'sampel_code':
				obj.sampel_code = value;
				newSampel = [...sampelList.slice(0, index), obj, ...sampelList.slice(index + 1)];
				break;
			case 'sampel_name':
				obj.sampel_name = value;
				newSampel = [...sampelList.slice(0, index), obj, ...sampelList.slice(index + 1)];
				break;
			case 'batch':
				obj.batch = value;
				newSampel = [...sampelList.slice(0, index), obj, ...sampelList.slice(index + 1)];
				break;
			case 'qty':
				obj.qty = value;
				newSampel = [...sampelList.slice(0, index), obj, ...sampelList.slice(index + 1)];
				break;
			default:
				obj.uom = value;
				newSampel = [...sampelList.slice(0, index), obj, ...sampelList.slice(index + 1)];
				break;
		}
		return setsampelList(newSampel);
	};

	const handleaddclick = () => {
		return setsampelList([
			...sampelList,
			{
				id: sampelList.length + 1,
				sampel_code: '',
				sampel_name: '',
				batch: '',
				qty: '',
				uom: '',
			},
		]);
	};

	useEffect(() => {}, []);

	const validate = (values) => {
		const errors = {};
		if (!values.date_of_communication) {
			errors.date_of_communication = 'Required';
		}
		if (!values.target_date) {
			errors.target_date = 'Required';
		}
		if (!values.initial_project) {
			errors.initial_project = 'Required';
		}

		return errors;
	};

	const { t } = useTranslation('registrasi');

	return (
		<Formik
			enableReinitialize
			initialValues={{ ...initialValues }}
			onSubmit={onSubmit}
			validate={validate}>
			{(formikField) => {
				return (
					<Form>
						<div className='row'>
							<div className='col-md-6'>
								<div className='w-100 d-none'>
									<div className='fload-end'>
										<DateTimeButton
											onChange={(dateRange) => setDate(dateRange)}
											selected={date}
											label='Date'
										/>
									</div>
								</div>
								<FormGroup
									id='customer'
									label='Customer'
									className='col-md-12 mb-3'>
									<CustomSelect
										placeholder='Select Customer'
										onChange={(e) => handleSelectCustomer(e)}
										value={customerSelected}
										isSearchable
										options={customers}
									/>
								</FormGroup>
								<FormGroup id='target' label='Target' className='col-md-12 mb-3'>
									<CustomSelect
										placeholder='Select Target'
										onChange={(e) => handleCategoriesSelected(e)}
										value={catagoriesSelected}
										options={categories}
									/>
								</FormGroup>
								<FormGroup id='note' label='Note' className='col-md-12 mb-3'>
									<CustomSelect
										placeholder='Select Note'
										onChange={(e) => setNoteSelected(e)}
										value={noteSelected}
										options={note}
									/>
								</FormGroup>
								<FormGroup
									id='initial_project'
									label='Initial Project'
									className='col-md-12 mb-3'>
									<Input
										onChange={formikField.handleChange}
										onBlur={formikField.handleBlur}
										value={formikField.values.initial_project}
										isValid={formikField.isValid}
										isTouched={formikField.touched.initial_project}
										invalidFeedback={formikField.errors.initial_project}
										autoComplete='off'
									/>
								</FormGroup>
							</div>
							<div className='col-md-6'>
								<div className='w-100'>
									<div className='fload-end'>
										<FormGroup
											id='target_date'
											label='Target Date'
											className='col-md-12 mb-3'>
											<Input
												type='date'
												onChange={formikField.handleChange}
												onBlur={formikField.handleBlur}
												value={formikField.values.target_date}
												isValid={formikField.isValid}
												isTouched={formikField.touched.target_date}
												invalidFeedback={formikField.errors.target_date}
												autoComplete='off'
											/>
										</FormGroup>
										<FormGroup
											id='date_of_communication'
											label='Date of Communication'
											className='col-md-12 mb-3'>
											<Input
												type='date'
												onChange={formikField.handleChange}
												onBlur={formikField.handleBlur}
												value={formikField.values.date_of_communication}
												isValid={formikField.isValid}
												isTouched={
													formikField.touched.date_of_communication
												}
												invalidFeedback={
													formikField.errors.date_of_communication
												}
												autoComplete='off'
											/>
										</FormGroup>
									</div>
								</div>
								<FormGroup
									id='target_price'
									label='Target Price'
									className='col-md-12 mb-3'>
									<Input
										type='number'
										onChange={formikField.handleChange}
										onBlur={formikField.handleBlur}
										value={formikField.values.target_price}
										isValid={formikField.isValid}
										isTouched={formikField.touched.target_price}
										invalidFeedback={formikField.errors.target_price}
										autoComplete='off'
									/>
								</FormGroup>
								<FormGroup id='currencies' label='Currency' className='col-md-12'>
									<CustomSelect
										placeholder='Select Target'
										onChange={(e) => handleCurrenciesSelected(e)}
										value={currenciesSelected}
										options={currencies}
									/>
								</FormGroup>
							</div>
						</div>
						<div className='mt-3'>
							<div className='row'>
								<CardHeader borderSize={1}>
									<CardLabel>
										<CardTitle>{t('Additional Information')}</CardTitle>
									</CardLabel>
								</CardHeader>
								<div className='col-md-12 mt-2 mb-2'>
									<Button
										type='button'
										color='success'
										onClick={handleAddAditional}
										className='col-md-1'>
										+
									</Button>
								</div>
							</div>
							<table className='table table-bordered'>
								<thead>
									<tr>
										<th>Pilih Additional Information</th>
										<th>Value</th>
									</tr>
								</thead>
								<tbody>
									{additionalList &&
										additionalList.map((el, i) => {
											return (
												<tr>
													<td>
														<CustomSelect
															placeholder='Select Additional'
															onChange={(e) =>
																handleSelectAdditional(e, i)
															}
															value={{
																value: el.name,
																text: el.name,
																label: el.name,
															}}
															options={additional_information}
														/>
													</td>
													<td>
														<div>
															<input
																className='form-control'
																onChange={(e) =>
																	handleChangeAdditional(
																		e,
																		i,
																		'name',
																	)
																}
																value={el.value || null}
																name='value'
																placeholder='Value'
																aria-label='test'
															/>
														</div>
													</td>
													<td>
														<Button
															icon=''
															type='button'
															color='danger'
															onClick={() =>
																handleRemoveAdditional(i)
															}>
															-
														</Button>
													</td>
												</tr>
											);
										})}
								</tbody>
							</table>
						</div>
						<div className='mt-3'>
							<div className='row'>
								<CardHeader borderSize={1}>
									<CardLabel>
										<CardTitle>{t('Sample')}</CardTitle>
									</CardLabel>
								</CardHeader>
								<div className='col-md-12 mt-2 mb-2'>
									<Button
										type='button'
										color='success'
										onClick={handleaddclick}
										className='col-md-1'>
										+
									</Button>
								</div>
							</div>
							<table className='table table-bordered'>
								<thead>
									<tr>
										<th>Sample Code</th>
										<th>Sample Name</th>
										<th>Batch</th>
										<th>Quantity</th>
										<th>UOM</th>
									</tr>
								</thead>
								<tbody>
									{sampelList &&
										sampelList.map((el, i) => {
											return (
												<tr>
													<td>
														<div>
															<input
																className='form-control'
																onChange={(e) =>
																	handleChangeSampel(
																		e,
																		i,
																		'sampel_code',
																	)
																}
																name='sample-code'
																placeholder='Sampel Code'
																aria-label='test'
															/>
														</div>
													</td>
													<td>
														<div>
															<input
																className='form-control'
																onChange={(e) =>
																	handleChangeSampel(
																		e,
																		i,
																		'sampel_name',
																	)
																}
																name='sample-name'
																placeholder='Sample Name'
																aria-label='test'
															/>
														</div>
													</td>
													<td>
														<div>
															<input
																className='form-control'
																onChange={(e) =>
																	handleChangeSampel(
																		e,
																		i,
																		'batch',
																	)
																}
																name='batch'
																placeholder='Batch'
																aria-label='test'
															/>
														</div>
													</td>
													<td>
														<div>
															<input
																type='number'
																className='form-control'
																onChange={(e) =>
																	handleChangeSampel(e, i, 'qty')
																}
																name='qty'
																placeholder='Qty'
																aria-label='test'
															/>
														</div>
													</td>
													<td>
														<div>
															<input
																className='form-control'
																onChange={(e) =>
																	handleChangeSampel(e, i, 'uom')
																}
																name='uom'
																placeholder='UOM'
																aria-label='test'
															/>
														</div>
													</td>
													<td>
														<Button
															icon=''
															type='button'
															color='danger'
															onClick={() => handleremove(i)}>
															-
														</Button>
													</td>
												</tr>
											);
										})}
								</tbody>
							</table>
						</div>
						<div className='col-md-12 '>
							<Button
								icon='Save'
								isOutline
								type='submit'
								color='success'
								className='float-end'>
								Submit
							</Button>
						</div>
					</Form>
				);
			}}
		</Formik>
	);
};

const FilterTableCustom = ({ tableRef, listType, type }) => {
	const [newtype, setType] = useState(type);
	const [keyword, setKeyword] = useState('');
	const [startDate, setStartDate] = useState('');
	const [endDate, setEndDate] = useState('');

	const handleSearch = () => {
		const searchParams = {};

		if (startDate !== '' && endDate === '') {
			showNotification('Warning!', 'Start Date & End Date Required', 'danger');
		} else if (startDate && endDate) {
			const start = moment(startDate);
			const end = moment(endDate);

			if (start.isAfter(end)) {
				showNotification(
					'Warning!',
					'the date cannot be later than the end date',
					'danger',
				);
			}
			if (end.isBefore(start)) {
				showNotification(
					'Warning!',
					'the date cannot be less than the start date',
					'danger',
				);
			}

			if (!start.isAfter(end) && !end.isBefore(start)) {
				searchParams.start = startDate;
				searchParams.end = endDate;
			}
		}

		if (newtype.value) {
			searchParams[`filter_${newtype.value.toLowerCase()}`] = keyword;
		}

		tableRef.current.inReloadTable(searchParams);
	};

	const handleReset = () => {
		setStartDate('');
		setEndDate('');
		setType({ value: '', text: '', label: '' });
		setKeyword('');
		tableRef.current.inReloadTable({});
	};

	return (
		<div className='row'>
			<div className='col-sm-2 m-1'>
				<FormGroup id='type' label='Type'>
					<CustomSelect
						placeholder='Type'
						onChange={(e) => setType(e)}
						value={newtype}
						options={listType}
					/>
				</FormGroup>
			</div>
			<div className='col-sm-2 m-1'>
				<FormGroup id='keyword' label='Keyword'>
					<Input value={keyword} onChange={(e) => setKeyword(e.target.value)} />
				</FormGroup>
			</div>
			<div className='col-sm-2 m-1'>
				<FormGroup id='start' label='Start'>
					<Input
						type='date'
						onChange={(e) => setStartDate(e.target.value)}
						value={startDate}
					/>
				</FormGroup>
			</div>
			<div className='col-sm-2 m-1'>
				<FormGroup id='start' label='Start'>
					<Input
						type='date'
						onChange={(e) => setEndDate(e.target.value)}
						value={endDate}
					/>
				</FormGroup>
			</div>
			<div className='col-sm-2 m-1 d-flex align-items-end'>
				<Button variant='contained' color='primary' onClick={handleSearch}>
					Search
				</Button>
				<Button variant='contained' color='secondary' onClick={handleReset}>
					Reset
				</Button>
			</div>
		</div>
	);
};

FilterTableCustom.propTypes = {
	tableRef: PropTypes.instanceOf(Object),
	listType: PropTypes.instanceOf(Array),
	type: PropTypes.instanceOf(Object),
};

FilterTableCustom.defaultProps = {
	tableRef: { current: null },
	listType: [
		{
			value: 'Ticket_ID',
			text: 'Ticket ID',
			label: 'Ticket ID',
		},
		{
			value: 'Sample',
			text: 'Sample',
			label: 'Sample',
		},
		{
			value: 'Customer_name',
			text: 'Customer',
			label: 'Customer',
		},
		{
			value: 'Category',
			text: 'Category',
			label: 'Category',
		},
		{
			value: 'PIC',
			text: 'PIC',
			label: 'PIC',
		},
	],
	type: {
		value: 'Ticket_ID',
		text: 'Ticket ID',
		label: 'Ticket ID',
	},
};

const Registrasi = () => {
	const customDataTableRef = useRef(null);
	const [customers, setCustomers] = useState([]);

	const [additional_information, setAdditionalInformation] = useState([]);
	const [currencies, setCurrencies] = useState([]);
	const [categories, setCategories] = useState([]);
	const [note, setnote] = useState([]);

	const reloadTable = () => {
		customDataTableRef.current.inReloadTable();
	};

	// eslint-disable-next-line react/prop-types
	const searchRef = useRef(null);
	const initialState = {
		// eslint-disable-next-line react/destructuring-assignment,react/prop-types
		tab: 1,
		dateRangeHasChange: false,
		dateRange: {
			startDate: null,
			endDate: null,
			key: 'selection',
			enabled: false,
		},
	};

	const fetchCustomers = async () => {
		return customersModule.readCustomers().then((res) => {
			setCustomers(res.foundData);
		});
	};

	const fetchAdditional = async () => {
		return MODULE.readAdditionalInformation().then((res) => {
			setAdditionalInformation(res.foundData);
		});
	};

	const fetchCurrencies = async () => {
		return currenciesModule.readCurrencies().then((res) => {
			setCurrencies(res.foundData);
		});
	};
	const fetchCategories = async () => {
		setCategories([
			{
				value: 'New Product',
				label: 'New Product',
				text: 'New Product',
			},
			{
				value: 'Reformulation',
				label: 'Reformulation',
				text: 'Reformulation',
			},
		]);
	};

	const fetchNote = async () => {
		return ticketRemarkModule.readlist().then((res) => {
			setnote(res.foundData);
		});
	};

	const [state] = useReducer((states, updates) => ({ ...states, ...updates }), initialState);

	const formik = useFormik({
		initialValues: {
			search: '',
		},
	});

	useEffect(() => {
		searchRef.current.focus();
		fetchCustomers();
		fetchAdditional();
		fetchCurrencies();
		fetchCategories();
		fetchNote();
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	const initialValues = {
		loading: false,
		target_price: '',
		target_date: '',
		sample: [],
		information: [],
		customer_name: '',
		category: '',
		currency: '',
		date_of_communication: '',
		initial_project: '',
	};

	const contextValue = useMemo(() => {
		return {
			additional_information,
			categories,
			currencies,
			customers,
			note,
		};
	}, [additional_information, categories, currencies, customers, note]);

	const { t } = useTranslation('category');

	return (
		<DataContext.Provider value={contextValue}>
			<PageWrapper title='Dashboard'>
				<PageLayoutHeader />
				<Page container='fluid'>
					<Card className='col-md-9'>
						<CardHeader borderSize={1}>
							<CardLabel>
								<CardTitle>{t('Request Development Product')}</CardTitle>
							</CardLabel>
						</CardHeader>
						<CardBody>
							<div className='col-12 mb-3 g-4'>
								<FormInput
									customers={customers}
									reloadTable={() => reloadTable()}
									initialValues={initialValues}
								/>
							</div>
						</CardBody>
					</Card>
					<Card stretch>
						<CardHeader borderSize={1}>
							<CardLabel>
								<CardTitle>{t('Historical Data')}</CardTitle>
							</CardLabel>
						</CardHeader>
						<CardBody>
							<div className='mt-5 row'>
								{/* eslint-disable-next-line react/destructuring-assignment,react/prop-types */}
								<FormGroup
									label='Invoices Number'
									formText='Use either keyboard or scanner'
									className='col-12 col-md-5 col-lg-3 col-xl-2 d-none'>
									<InputGroup size='sm'>
										<Input
											id='search'
											value={formik.values.search}
											onChange={formik.handleChange}
											ref={searchRef}
										/>
										<Button
											color='primary'
											id='button-addon1'
											onClick={() =>
												onSearch(state, formik, customDataTableRef)
											}>
											Search
										</Button>
									</InputGroup>
								</FormGroup>

								<div className='col-12'>
									<FilterTableCustom
										className='d-inline-block'
										tableRef={customDataTableRef}
									/>
								</div>

								<div className='col-12 mt-4'>
									<Table customers={customers} ref={customDataTableRef} tab={1} />
								</div>
							</div>
						</CardBody>
					</Card>
				</Page>
			</PageWrapper>
		</DataContext.Provider>
	);
};

const FormCustomModalDetail = ({ initialValues }) => {
	const { darkModeStatus } = useDarkMode();
	const [isOpen, setOpen] = useState(false);

	return (
		<>
			<Button
				color='info'
				type='button'
				isLight={darkModeStatus}
				className='m-1'
				onClick={() => setOpen(true)}>
				i
			</Button>

			<Modal
				isOpen={isOpen}
				setIsOpen={setOpen}
				size='xl'
				titleId='modal-crud-evaluation'
				isStaticBackdrop>
				<ModalHeader setIsOpen={setOpen} className='p-4'>
					<ModalTitle id='modal-crud-evaluation'>
						Detail Ticket - ({initialValues?.document_number}){' '}
						{initialValues?.customer_name}
					</ModalTitle>
				</ModalHeader>
				<ModalBody>
					<DetailTicketAndFormulaComponent ticket_id={initialValues.ticket_id} />
				</ModalBody>
			</Modal>
		</>
	);
};

FormCustomModalDetail.propTypes = {
	initialValues: PropTypes.instanceOf(Object),
};
FormCustomModalDetail.defaultProps = {
	initialValues: null,
};

export default Registrasi;
