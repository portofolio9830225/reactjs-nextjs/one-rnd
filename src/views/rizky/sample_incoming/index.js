import React, { useState, useEffect, useRef, useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import { Form, Formik, useFormik } from 'formik';
import Swal from 'sweetalert2';
import Select from 'react-select';
import moment from 'moment';
import PropTypes from 'prop-types';
import * as FileSaver from 'file-saver';
import XLSX from 'sheetjs-style';
import PageWrapper from '../../../layout/PageWrapper/PageWrapper';
import Page from '../../../layout/Page/Page';
import PageLayoutHeader from '../../../pages/common/Headers/PageLayoutHeader';
import Card, {
	CardBody,
	CardHeader,
	CardLabel,
	CardTitle,
} from '../../../components/bootstrap/Card';
import FormGroup from '../../../components/bootstrap/forms/FormGroup';
import InputGroup from '../../../components/bootstrap/forms/InputGroup';
import sampleIncomingModule from '../../../modules/rizky/sample-incoming/sample-incomingModule';
import customersModule from '../../../modules/rizky/customers/customersModule';
import sampleIncomingRemarkModule from '../../../modules/rizky/sample_incoming_remark/sampleIncomingRemarkModule';
import clasificationsModule from '../../../modules/rizky/clasifications/clasificationsModule';
import DocumentSampleModule from '../../../modules/DocumentSampleModule';
import showNotification from '../../../components/extras/showNotification';
import Input from '../../../components/bootstrap/forms/Input';
import Textarea from '../../../components/bootstrap/forms/Textarea';
import useDarkMode from '../../../hooks/useDarkMode';
import DarkDataTable from '../../../components/DarkDataTable';
import Button from '../../../components/bootstrap/Button';
import COLORS from '../../../common/data/enumColors';
import Modal, {
	ModalBody,
	ModalHeader,
	ModalTitle,
	ModalFooter,
} from '../../../components/bootstrap/Modal';
import CustomSelect from '../../../components/CustomSelect';
import PICModule from '../../../modules/rizky/PIC/PICModule';
import UserModule from '../../../modules/UserModule';

const react_select_styles = (formikField, darkModeStatus) => {
	const select_invalid_styles = {
		control: (base, state) => ({
			...base,
			// state.isFocused can display different borderColor if you need it
			borderColor:
				(state.isFocused && COLORS.SECONDARY.code) ||
				(!state.isFocused && formikField.submitCount > 0 && COLORS.DANGER.code) ||
				(!state.isFocused && formikField.submitCount === 0 && COLORS.LIGHT.code),
			// overwrittes hover style
			'&:hover': {
				borderColor:
					(state.isFocused && COLORS.SECONDARY.code) ||
					(!state.isFocused && formikField.submitCount > 0 && COLORS.DANGER.code),
			},
		}),
		singleValue: (base) => ({
			...base,
			color: darkModeStatus ? COLORS.LIGHT.code : COLORS.DARK.code,
		}),
		input: (base) => ({
			...base,
			color: darkModeStatus ? COLORS.LIGHT.code : COLORS.DARK.code,
		}),
	};
	const select_valid_styles = {
		control: (base, state) => ({
			...base,
			// state.isFocused can display different borderColor if you need it
			borderColor:
				(state.isFocused && COLORS.SECONDARY.code) ||
				(!state.isFocused && formikField.submitCount > 0 && COLORS.SUCCESS.code) ||
				(!state.isFocused && formikField.submitCount === 0 && COLORS.LIGHT.code),

			// overwrittes hover style
			'&:hover': {
				borderColor:
					(state.isFocused && COLORS.SECONDARY.code) ||
					(!state.isFocused && formikField.submitCount > 0 && COLORS.SUCCESS.code),
			},
		}),
		singleValue: (base) => ({
			...base,
			color: darkModeStatus ? COLORS.LIGHT.code : COLORS.DARK.code,
		}),
		input: (base) => ({
			...base,
			color: darkModeStatus ? COLORS.LIGHT.code : COLORS.DARK.code,
		}),
	};
	return typeof formikField.errors.template_selection === 'undefined'
		? select_valid_styles
		: select_invalid_styles;
};

const react_select_themes = (theme, darkModeStatus) => {
	return {
		...theme,
		colors: darkModeStatus
			? {
					...theme.colors,
					neutral0: COLORS.DARK.code,
					neutral190: COLORS.LIGHT.code,
					primary: COLORS.SUCCESS.code,
					primary25: COLORS.PRIMARY.code,
			  }
			: theme.colors,
	};
};

const ExportExcel = (dt) => {
	const { excelData, fileName } = dt;
	const fileType =
		'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=UTF-8';
	const fileExtension = '.xlsx';

	const exportToExcel = async () => {
		const ws = XLSX.utils.json_to_sheet(excelData);
		const wb = { Sheets: { data: ws }, SheetNames: ['data'] };
		const excelBuffer = XLSX.write(wb, { bookType: 'xlsx', type: 'array' });
		const data = new Blob([excelBuffer], { type: fileType });
		FileSaver.saveAs(data, fileName + fileExtension);
	};

	if (excelData) {
		return (
			<Button
				onClick={() => exportToExcel(fileName)}
				color='primary'
				type='button'
				className='m-1'>
				Export
			</Button>
		);
	}
	return <div />;
};

const TableCustom = ({
	data,
	date,
	totalRows,
	perPage,
	loading,
	fetchData,
	curPage,
	showAll,
	customers,
	assets,
	setLoadingSubmit,
}) => {
	const { darkModeStatus } = useDarkMode();
	const handlePageChange = (page) => {
		fetchData(page, perPage, showAll, date);
	};

	const handlePerRowsChange = async (newPerPage, page) => {
		const all = totalRows === newPerPage;
		return fetchData(page, newPerPage, all, date);
	};

	const reloadTable = () => {
		fetchData(1);
	};
	const paginationComponentOptions = {
		selectAllRowsItem: true,
		selectAllRowsItemText: 'ALL',
	};

	const columns = useMemo(
		() => [
			{
				name: 'Register Date',
				selector: (row) => moment(row.register_date).format('DD MMM YYYY'),
				sortable: true,
			},
			{
				name: 'Material',
				selector: (row) => row.material_name,
				sortable: true,
			},
			{
				name: 'Classification',
				selector: (row) => row.clasification,
				sortable: true,
			},
			{
				name: 'PIC',
				selector: (row) => row.pic_name,
				sortable: true,
			},
			{
				name: 'Manufacture',
				selector: (row) => row.manufacture,
				sortable: true,
			},
			{
				name: 'Supplier',
				selector: (row) => row.supplier,
				sortable: true,
			},
			{
				name: 'Action',
				// eslint-disable-next-line react/no-unstable-nested-components
				cell: (dt) => {
					return (
						<CustomButton
							customers={customers.customers}
							row={dt}
							reloadTable={() => reloadTable()}
							assets={assets}
							setLoadingSubmit={setLoadingSubmit}
						/>
					);
				},
			},
		],
		// eslint-disable-next-line react-hooks/exhaustive-deps
		[curPage, perPage, showAll, date],
	);

	return (
		<DarkDataTable
			columns={columns}
			data={data}
			pagination
			progressPending={loading}
			paginationServer
			paginationTotalRows={totalRows}
			onChangeRowsPerPage={handlePerRowsChange}
			onChangePage={handlePageChange}
			paginationComponentOptions={paginationComponentOptions}
			theme={darkModeStatus ? 'custom_dark' : 'light'}
		/>
	);
};
TableCustom.propTypes = {
	data: PropTypes.instanceOf(Array),
	date: PropTypes.instanceOf(Object),
	assets: PropTypes.instanceOf(Object),
	customers: PropTypes.instanceOf(Object),
	loading: PropTypes.bool,
	totalRows: PropTypes.number,
	curPage: PropTypes.number,
	perPage: PropTypes.number,
	showAll: PropTypes.bool,
	fetchData: PropTypes.func,
	setLoadingSubmit: PropTypes.bool,
};
TableCustom.defaultProps = {
	data: [],
	date: {
		start: moment(new Date()).subtract(1, 'months').format('YYYY-MM-DD'),
		end: moment(new Date()).format('YYYY-MM-DD'),
	},
	assets: { clasification: [], documentSample: [], pic: [], remark: [] },
	loading: false,
	totalRows: 0,
	curPage: 1,
	perPage: 10,
	showAll: false,
	customers: {},
	setLoadingSubmit: false,
	fetchData: () => {},
};

const CustomButtonTable = ({ initialValues, row }) => {
	const newInitialValues = { ...initialValues };
	newInitialValues.formula_id = row._id;
	return <div />;
};

CustomButtonTable.propTypes = {
	initialValues: PropTypes.instanceOf(Object),
	row: PropTypes.instanceOf(Object),
};

CustomButtonTable.defaultProps = {
	initialValues: null,
	row: {},
};

const onSearch = (state, formik, customDataTableRef) => {
	if (!state.dateRange.startDate && !state.dateRange.endDate) {
		if (formik.values.search) {
			customDataTableRef.current.inReloadTable({
				search_invoice_no: formik.values.search,
			});
		} else {
			customDataTableRef.current.inReloadTable();
		}
	} else if (state.dateRangeHasChange) {
		if (formik.values.search) {
			customDataTableRef.current.inReloadTable({
				search_invoice_no: formik.values.search,
				searchdate_created_at: JSON.stringify({
					$gte: moment(state.dateRange.startDate).format('YYYY-MM-DD'),
					$lte: moment(state.dateRange.endDate).format('YYYY-MM-DD'),
				}),
			});
		} else {
			customDataTableRef.current.inReloadTable({
				searchdate_created_at: JSON.stringify({
					$gte: moment(state.dateRange.startDate).format('YYYY-MM-DD'),
					$lte: moment(state.dateRange.endDate).format('YYYY-MM-DD'),
				}),
			});
		}
	} else if (formik.values.search) {
		customDataTableRef.current.inReloadTable({
			search_invoice_no: formik.values.search,
		});
	} else {
		customDataTableRef.current.inReloadTable();
	}
	if (!state.dateRange.startDate && !state.dateRange.endDate) {
		if (formik.values.search) {
			customDataTableRef.current.inReloadTable({
				search_invoice_no: formik.values.search,
			});
		} else {
			customDataTableRef.current.inReloadTable();
		}
	} else if (state.dateRangeHasChange) {
		if (formik.values.search) {
			customDataTableRef.current.inReloadTable({
				search_invoice_no: formik.values.search,
				searchdate_created_at: JSON.stringify({
					$gte: moment(state.dateRange.startDate).format('YYYY-MM-DD'),
					$lte: moment(state.dateRange.endDate).format('YYYY-MM-DD'),
				}),
			});
		} else {
			customDataTableRef.current.inReloadTable({
				searchdate_created_at: JSON.stringify({
					$gte: moment(state.dateRange.startDate).format('YYYY-MM-DD'),
					$lte: moment(state.dateRange.endDate).format('YYYY-MM-DD'),
				}),
			});
		}
	} else if (formik.values.search) {
		customDataTableRef.current.inReloadTable({
			search_invoice_no: formik.values.search,
		});
	} else {
		customDataTableRef.current.inReloadTable();
	}
};

const handleSubmit = (init, values, reloadTable, handleReset, resetForm, setLoadingSubmit) => {
	if (init.date && init.clasification && init.material && init.qty && init.oum && init.location) {
		delete values.loading;
		return Swal.fire({
			title: 'Are you sure?',
			text: 'Please check your entries !',
			icon: 'info',
			showCancelButton: true,
			confirmButtonText: 'Yes',
		}).then((result) => {
			if (result.value) {
				setLoadingSubmit(true);
				sampleIncomingModule
					.create(values)
					.then((res) => {
						setLoadingSubmit(false);
						reloadTable();
						showNotification('Success!', res.status, 'success');
						handleReset(resetForm);
					})
					.catch((err) => {
						setLoadingSubmit(false);
						showNotification('Warning!', err, 'danger');
					});
			} else if (result.dismiss === Swal.DismissReason.cancel) {
				setLoadingSubmit(false);
				Swal.fire('Cancelled', 'Your data is safe :)', 'error');
			}
		});
	}
	return values;
};

const handleSubmitCopy = (values, reloadTable, setLoadingSubmit) => {
	if (values) {
		return Swal.fire({
			title: 'Are you sure?',
			text: 'Please check your entries !',
			icon: 'info',
			showCancelButton: true,
			confirmButtonText: 'Yes',
		}).then((result) => {
			if (result.value) {
				setLoadingSubmit(true);
				sampleIncomingModule
					.copy(values)
					.then((res) => {
						setLoadingSubmit(false);
						reloadTable();
						showNotification('Success!', res.status, 'success');
					})
					.catch((err) => {
						setLoadingSubmit(false);
						showNotification('Warning!', err, 'danger');
					});
			} else if (result.dismiss === Swal.DismissReason.cancel) {
				setLoadingSubmit(false);
				Swal.fire('Cancelled', 'Your data is safe :)', 'error');
			}
		});
	}
	return values;
};

const handleSubmitNotif = (values, reloadTable, setLoadingSubmit) => {
	if (values) {
		return Swal.fire({
			title: 'Are you sure?',
			text: 'Please check your entries !',
			icon: 'info',
			showCancelButton: true,
			confirmButtonText: 'Yes',
		}).then((result) => {
			if (result.value) {
				setLoadingSubmit(true);
				sampleIncomingModule
					.update(values)
					.then((res) => {
						setLoadingSubmit(false);
						reloadTable();
						showNotification('Success!', res.status, 'success');
					})
					.catch((err) => {
						setLoadingSubmit(false);
						showNotification('Warning!', err, 'danger');
					});
			} else if (result.dismiss === Swal.DismissReason.cancel) {
				setLoadingSubmit(false);
				Swal.fire('Cancelled', 'Your data is safe :)', 'error');
			}
		});
	}
	return values;
};

const handleSubmitEdit = (values, reloadTable, setLoadingSubmit) => {
	if (values) {
		return Swal.fire({
			title: 'Are you sure?',
			text: 'Please check your entries !',
			icon: 'info',
			showCancelButton: true,
			confirmButtonText: 'Yes',
		}).then((result) => {
			if (result.value) {
				setLoadingSubmit(true);
				sampleIncomingModule
					.update(values)
					.then((res) => {
						setLoadingSubmit(false);
						reloadTable();
						showNotification('Success!', res.status, 'success');
					})
					.catch((err) => {
						setLoadingSubmit(false);
						showNotification('Warning!', err, 'danger');
					});
			} else if (result.dismiss === Swal.DismissReason.cancel) {
				setLoadingSubmit(false);
				Swal.fire('Cancelled', 'Your data is safe :)', 'error');
			}
		});
	}
	return values;
};

const FormNotif = (dt) => {
	const inputRef = useRef(null);
	const [isOpen, setIsOpen] = useState(false);
	const { initialValues, reloadTable, assets, setLoadingSubmit } = dt;
	const [isMulti] = useState(true);
	const { darkModeStatus } = useDarkMode();
	const [userNotifSelected2, setUserNotifSelected2] = useState([]);
	const [userNotifSelected, setUserNotifSelected] = useState([]);
	const [userNotif, setUserNotif] = useState(assets.userNotif ? assets.userNotif : []);

	const changeUserNotifSelected = (e) => {
		const res = e.map((x) => {
			return {
				name: x.label,
				mail: x.value,
				date: new Date().toISOString(),
			};
		});
		setUserNotifSelected2(e);
		setUserNotifSelected(res);
	};

	const onSubmit = ({ setSubmitting, setStatus, setErrors }) => {
		if (userNotifSelected.length < 1) {
			showNotification(
				'Select User',
				'user does not exist, cannot send notification',
				'danger',
			);
		}
		const values = {
			send_notif: userNotifSelected,
			_id: initialValues._id,
		};
		try {
			handleSubmitNotif(values, reloadTable, setLoadingSubmit);
			setStatus({ success: true });
		} catch (error) {
			setStatus({ success: false });
			setSubmitting(false);
			setErrors({ submit: error.message });
		}
	};

	useEffect(() => {
		setUserNotif(assets.userNotif);
	}, [assets]);

	return (
		<div style={{ width: '90px' }}>
			<Button
				icon='Send'
				isOutline
				type='button'
				color='success'
				onClick={() => {
					setIsOpen(true);
				}}>
				Send
			</Button>
			<Modal
				isStaticBackdrop
				isOpen={isOpen}
				setIsOpen={setIsOpen}
				size='lg'
				titleId='modal-edit-menu-crud'>
				<ModalHeader setIsOpen={setIsOpen} className='p-4'>
					<ModalTitle id='modal-edit-menu-crud'> Send Notif</ModalTitle>
				</ModalHeader>
				<Formik enableReinitialize initialValues={{ ...initialValues }} onSubmit={onSubmit}>
					{(formikField) => {
						return (
							<Form>
								<ModalBody className='px-4'>
									<div className='row'>
										<div className='col-md-6'>
											<div className='w-100'>
												<div className='fload-end'>
													<FormGroup
														id='send_notif'
														label='Select User'
														className='mb-4'>
														<Select
															styles={react_select_styles(
																formikField,
																darkModeStatus,
															)}
															theme={(theme) =>
																react_select_themes(
																	theme,
																	darkModeStatus,
																)
															}
															onChange={(e) =>
																changeUserNotifSelected(e)
															}
															value={userNotifSelected2}
															options={userNotif}
															isMulti={isMulti}
															ref={inputRef}
														/>
													</FormGroup>
												</div>
											</div>
										</div>
									</div>
								</ModalBody>
								<ModalFooter className='px-4 pb-4'>
									<div className='col-md-12 '>
										<Button
											icon='Send'
											isOutline
											type='submit'
											color='success'
											className='float-end'>
											Send
										</Button>
									</div>
								</ModalFooter>
							</Form>
						);
					}}
				</Formik>
			</Modal>
		</div>
	);
};

const FormCopy = (dt) => {
	const { darkModeStatus } = useDarkMode();
	const inputRef = useRef(null);
	const { initialValues, reloadTable, assets, setLoadingSubmit } = dt;
	const [isOpen, setIsOpen] = useState(false);
	const [clasifications, setClasifications] = useState([]);
	const [remark, setRemark] = useState([]);
	const [documentSample, setDocumentSample] = useState(assets.pic ? assets.pic : []);
	const [pic, setPic] = useState(assets.pic ? assets.pic : []);
	const [clasificationsSelected, setClasificationsSelected] = useState(
		initialValues.clasification,
	);
	const [remarkSelected, setremarkSelected] = useState(initialValues.remark);
	const [picSelected, setpicSelected] = useState(initialValues.pic);
	const [listDocumentSampe, setListDocumentSampe] = useState(initialValues.list_document);
	const [selectedFile, setSelectedFile] = useState(null);
	const [selectedListDocument, setSelectedListDocument] = useState(
		initialValues.list_document_selected,
	);
	const [base64Download, setBase64Download] = useState(null);
	const [base64FileName, setBase64FileName] = useState(null);
	const [isMulti] = useState(true);

	const changeDocumentSampleSelected = (e) => {
		const res = e.map((x) => {
			return x.value;
		});
		setSelectedListDocument(res);
	};

	const onSubmit = (values, { setSubmitting, setStatus, setErrors }) => {
		const formData = new FormData();
		if (!values.date) {
			showNotification(
				'Date',
				'date does not exist, cannot update sample incoming',
				'danger',
			);
		}
		if (!clasificationsSelected) {
			showNotification(
				'Clasification',
				'clasification does not exist, cannot update sample incominf',
				'danger',
			);
		}
		if (!values.material_name) {
			showNotification(
				'Material',
				'material does not exist, cannot update sample incoming',
				'danger',
			);
		}
		if (!values.qty) {
			showNotification(
				'Quantity',
				'Quantity does not exist, cannot update sample incoming',
				'danger',
			);
		}
		if (!values.oum) {
			showNotification('UOM', 'UOM does not exist, cannot update sample incoming', 'danger');
		}
		if (!values.location) {
			showNotification(
				'Location',
				'Location does not exist, cannot update sample incoming',
				'danger',
			);
		}
		if (!values.remark) {
			showNotification(
				'Remark',
				'Remark does not exist, cannot update sample incoming',
				'danger',
			);
		}
		formData.append('date_receive', values.date);
		if (selectedFile === null) {
			formData.append('documnet', initialValues.document);
		} else {
			formData.append('files', selectedFile);
		}
		formData.append('clasification', clasificationsSelected.value);
		formData.append('material_name', values.material_name);
		formData.append('qty', values.qty);
		formData.append('uom', values.oum);
		formData.append('pic', picSelected.value);
		formData.append('pic_name', picSelected.label);
		formData.append('pic_email', picSelected.text);
		formData.append('Note', values.note);
		formData.append('manufacture', values.manufacture);
		formData.append('supplier', values.supplier);
		formData.append('origin', values.origin);
		formData.append('location', values.location);
		formData.append('remark', remarkSelected.value);
		formData.append('list_document', selectedListDocument);

		if (clasificationsSelected) {
			values.clasification = clasificationsSelected.value;
			try {
				handleSubmitCopy(formData, reloadTable, setLoadingSubmit);
				setStatus({ success: true });
			} catch (error) {
				setStatus({ success: false });
				setSubmitting(false);
				setErrors({ submit: error.message });
			}
		}
	};

	const handleChangeFile = (value) => {
		if (value.target.files[0].size > 2000000) {
			showNotification('Document', 'File size can not more than 2 MB', 'danger');
			setSelectedFile(null);
			value.target.value = '';
		} else {
			setSelectedFile(value.target.files[0]);
			value.target.value = selectedFile;
		}
	};

	const handleSelectClasifications = (e) => {
		return setClasificationsSelected(e);
	};

	useEffect(() => {
		setClasifications(assets.clasifications);
		setRemark(assets.remark);
		setDocumentSample(assets.documentSample);
		setPic(assets.pic);
	}, [assets]);

	return (
		<div style={{ width: '90px' }}>
			<Button
				icon='files'
				isOutline
				type='button'
				color='secondary'
				onClick={() => {
					if (initialValues.document) {
						sampleIncomingModule.download(initialValues.document).then((res) => {
							const base64 = `data:${res.res.mimetype};base64,${res.res.file}`;
							const fileName = `${initialValues.document}.${res.res.extension}`;
							setBase64Download(base64);
							setBase64FileName(fileName);
						});
					}
					setIsOpen(true);
				}}>
				Copy
			</Button>
			<Modal
				isStaticBackdrop
				isOpen={isOpen}
				setIsOpen={setIsOpen}
				size='lg'
				titleId='modal-edit-menu-crud'>
				<ModalHeader setIsOpen={setIsOpen} className='p-4'>
					<ModalTitle id='modal-edit-menu-crud'> Update Data</ModalTitle>
				</ModalHeader>
				<Formik enableReinitialize initialValues={{ ...initialValues }} onSubmit={onSubmit}>
					{(formikField) => {
						return (
							<Form>
								<ModalBody className='px-4'>
									<div className='row'>
										<div className='col-md-6'>
											<div className='w-100'>
												<div className='fload-end'>
													<FormGroup
														id='date'
														label='Date'
														className='col-md-12 mb-3'>
														<Input
															type='date'
															onChange={formikField.handleChange}
															onBlur={formikField.handleBlur}
															value={formikField.values.date}
															isValid={formikField.isValid}
															isTouched={formikField.touched.date}
															invalidFeedback={
																formikField.errors.date
															}
															autoComplete='off'
														/>
													</FormGroup>
												</div>
											</div>
											<FormGroup
												id='material_name'
												label='Material'
												className='col-md-12 mb-3'>
												<Input
													onChange={formikField.handleChange}
													onBlur={formikField.handleBlur}
													value={formikField.values.material_name}
													isValid={formikField.isValid}
													isTouched={formikField.touched.material_name}
													invalidFeedback={
														formikField.errors.material_name
													}
													autoComplete='off'
												/>
											</FormGroup>
											<FormGroup
												id='clasification'
												label='Clasification'
												className='col-md-12 mb-3'>
												<CustomSelect
													placeholder='Select Clasification'
													onChange={(e) => handleSelectClasifications(e)}
													value={clasificationsSelected}
													isSearchable
													options={clasifications}
													darkTheme={darkModeStatus}
												/>
											</FormGroup>
											<FormGroup
												id='qty'
												label='Quantity'
												className='col-md-12 mb-3'>
												<Input
													type='number'
													onChange={formikField.handleChange}
													onBlur={formikField.handleBlur}
													value={formikField.values.qty}
													isValid={formikField.isValid}
													isTouched={formikField.touched.qty}
													invalidFeedback={formikField.errors.qty}
													autoComplete='off'
												/>
											</FormGroup>
											<FormGroup
												id='oum'
												label='UOM'
												className='col-md-12 mb-3'>
												<Input
													onChange={formikField.handleChange}
													onBlur={formikField.handleBlur}
													value={formikField.values.oum}
													isValid={formikField.isValid}
													isTouched={formikField.touched.oum}
													invalidFeedback={formikField.errors.oum}
													autoComplete='off'
												/>
											</FormGroup>
											<FormGroup
												id='pic_name'
												label='PIC'
												className='col-md-12 mb-3'>
												<CustomSelect
													placeholder='Select PIC'
													onChange={(e) => setpicSelected(e)}
													value={picSelected}
													isSearchable
													options={pic}
													darkTheme={darkModeStatus}
												/>
											</FormGroup>
											<FormGroup
												id='note'
												label='Note'
												className='col-md-12 mb-3'>
												<Textarea
													onChange={formikField.handleChange}
													onBlur={formikField.handleBlur}
													value={formikField.values.note}
													isValid={formikField.isValid}
													isTouched={formikField.touched.note}
													invalidFeedback={formikField.errors.note}
													autoComplete='off'
												/>
											</FormGroup>
										</div>
										<div className='col-md-6'>
											<FormGroup
												id='manufacture'
												label='Manufacture'
												className='col-md-12 mb-3'>
												<Input
													onChange={formikField.handleChange}
													onBlur={formikField.handleBlur}
													value={formikField.values.manufacture}
													isValid={formikField.isValid}
													isTouched={formikField.touched.manufacture}
													invalidFeedback={formikField.errors.manufacture}
													autoComplete='off'
												/>
											</FormGroup>
											<FormGroup
												id='supplier'
												label='Supplier'
												className='col-md-12 mb-3'>
												<Input
													onChange={formikField.handleChange}
													onBlur={formikField.handleBlur}
													value={formikField.values.supplier}
													isValid={formikField.isValid}
													isTouched={formikField.touched.supplier}
													invalidFeedback={formikField.errors.supplier}
													autoComplete='off'
												/>
											</FormGroup>
											<FormGroup
												id='origin'
												label='Origin'
												className='col-md-12 mb-3'>
												<Input
													onChange={formikField.handleChange}
													onBlur={formikField.handleBlur}
													value={formikField.values.origin}
													isValid={formikField.isValid}
													isTouched={formikField.touched.origin}
													invalidFeedback={formikField.errors.origin}
													autoComplete='off'
												/>
											</FormGroup>

											{base64Download !== null && (
												<FormGroup
													id='document_'
													label='Document'
													className='col-md-12 mb-3'>
													<Button color='success'>
														<a
															type='button'
															download={base64FileName}
															href={base64Download}>
															Download File
														</a>
													</Button>
												</FormGroup>
											)}

											{base64Download === null && (
												<FormGroup
													id='document'
													label='Document'
													className='col-md-12 mb-3'>
													<Input
														ref={inputRef}
														type='file'
														onChange={(e) => handleChangeFile(e)}
													/>
												</FormGroup>
											)}

											<FormGroup
												id='location'
												label='Location'
												className='col-md-12 mb-3'>
												<Input
													onChange={formikField.handleChange}
													onBlur={formikField.handleBlur}
													value={formikField.values.location}
													isValid={formikField.isValid}
													isTouched={formikField.touched.location}
													invalidFeedback={formikField.errors.location}
													autoComplete='off'
												/>
											</FormGroup>
											<FormGroup
												id='document_sample'
												label='Document sample'
												className='mb-4'>
												<Select
													styles={react_select_styles(
														formikField,
														darkModeStatus,
													)}
													theme={(theme) =>
														react_select_themes(theme, darkModeStatus)
													}
													value={listDocumentSampe}
													options={documentSample}
													onChange={(e) => {
														setListDocumentSampe(e);
														changeDocumentSampleSelected(e);
													}}
													isMulti={isMulti}
												/>
											</FormGroup>
											<FormGroup
												id='remark'
												label='Remark'
												className='col-md-12 mb-3'>
												<CustomSelect
													placeholder='Select Remark'
													onChange={(e) => setremarkSelected(e)}
													value={remarkSelected}
													isSearchable
													options={remark}
													darkTheme={darkModeStatus}
												/>
											</FormGroup>
										</div>
									</div>
								</ModalBody>
								<ModalFooter className='px-4 pb-4'>
									<div className='col-md-12 '>
										<Button
											icon='Save'
											isOutline
											type='submit'
											color='success'
											className='float-end'>
											Copy
										</Button>
									</div>
								</ModalFooter>
							</Form>
						);
					}}
				</Formik>
			</Modal>
		</div>
	);
};

const FormEdit = (dt) => {
	const { darkModeStatus } = useDarkMode();
	const inputRef = useRef(null);
	const { initialValues, reloadTable, assets, setLoadingSubmit } = dt;
	const [isOpen, setIsOpen] = useState(false);
	const [clasifications, setClasifications] = useState([]);
	const [remark, setRemark] = useState([]);
	const [documentSample, setDocumentSample] = useState(assets.pic ? assets.pic : []);
	const [pic, setPic] = useState(assets.pic ? assets.pic : []);
	const [clasificationsSelected, setClasificationsSelected] = useState(
		initialValues.clasification,
	);
	const [remarkSelected, setremarkSelected] = useState(initialValues.remark);
	const [picSelected, setpicSelected] = useState(initialValues.pic);
	const [listDocumentSampe, setListDocumentSampe] = useState(initialValues.list_document);
	const [selectedFile, setSelectedFile] = useState(null);
	const [selectedListDocument, setSelectedListDocument] = useState(
		initialValues.list_document_selected,
	);
	const [base64Download, setBase64Download] = useState(null);
	const [base64FileName, setBase64FileName] = useState(null);
	const [isMulti] = useState(true);

	const changeDocumentSampleSelected = (e) => {
		const res = e.map((x) => {
			return x.value;
		});
		setSelectedListDocument(res);
	};

	const onSubmit = (values, { setSubmitting, setStatus, setErrors }) => {
		const formData = new FormData();
		if (!values.date) {
			showNotification(
				'Date',
				'date does not exist, cannot update sample incoming',
				'danger',
			);
		}
		if (!clasificationsSelected) {
			showNotification(
				'Clasification',
				'clasification does not exist, cannot update sample incominf',
				'danger',
			);
		}
		if (!values.material_name) {
			showNotification(
				'Material',
				'material does not exist, cannot update sample incoming',
				'danger',
			);
		}
		if (!values.qty) {
			showNotification(
				'Quantity',
				'Quantity does not exist, cannot update sample incoming',
				'danger',
			);
		}
		if (!values.oum) {
			showNotification('UOM', 'UOM does not exist, cannot update sample incoming', 'danger');
		}
		if (!values.location) {
			showNotification(
				'Location',
				'Location does not exist, cannot update sample incoming',
				'danger',
			);
		}
		if (!values.remark) {
			showNotification(
				'Remark',
				'Remark does not exist, cannot update sample incoming',
				'danger',
			);
		}
		formData.append('date_receive', values.date);
		formData.append('files', selectedFile);
		formData.append('clasification', clasificationsSelected.value);
		formData.append('material_name', values.material_name);
		formData.append('qty', values.qty);
		formData.append('uom', values.oum);
		formData.append('pic', picSelected.value);
		formData.append('pic_name', picSelected.label);
		formData.append('pic_email', picSelected.text);
		formData.append('Note', values.note);
		formData.append('manufacture', values.manufacture);
		formData.append('supplier', values.supplier);
		formData.append('origin', values.origin);
		formData.append('location', values.location);
		formData.append('remark', remarkSelected.value);
		formData.append('list_document', selectedListDocument);
		formData.append('_id', initialValues._id);
		if (clasificationsSelected) {
			values.clasification = clasificationsSelected.value;
			try {
				handleSubmitEdit(formData, reloadTable, setLoadingSubmit);
				setStatus({ success: true });
			} catch (error) {
				setStatus({ success: false });
				setSubmitting(false);
				setErrors({ submit: error.message });
			}
		}
	};

	const handleChangeFile = (value) => {
		if (value.target.files[0].size > 2000000) {
			showNotification('Document', 'File size can not more than 2 MB', 'danger');
			setSelectedFile(null);
			value.target.value = '';
		} else {
			setSelectedFile(value.target.files[0]);
			value.target.value = selectedFile;
		}
	};

	const handleSelectClasifications = (e) => {
		return setClasificationsSelected(e);
	};

	useEffect(() => {
		setClasifications(assets.clasifications);
		setRemark(assets.remark);
		setDocumentSample(assets.documentSample);
		setPic(assets.pic);
	}, [assets]);

	return (
		<div style={{ width: '80px' }}>
			<Button
				icon='Edit'
				isOutline
				type='button'
				color='primary'
				onClick={() => {
					if (initialValues.document) {
						sampleIncomingModule.download(initialValues.document).then((res) => {
							const base64 = `data:${res.res.mimetype};base64,${res.res.file}`;
							const fileName = `${initialValues.document}.${res.res.extension}`;
							setBase64Download(base64);
							setBase64FileName(fileName);
						});
					}
					setIsOpen(true);
				}}>
				Edit
			</Button>
			<Modal
				isStaticBackdrop
				isOpen={isOpen}
				setIsOpen={setIsOpen}
				size='lg'
				titleId='modal-edit-menu-crud'>
				<ModalHeader setIsOpen={setIsOpen} className='p-4'>
					<ModalTitle id='modal-edit-menu-crud'> Update Data</ModalTitle>
				</ModalHeader>
				<Formik enableReinitialize initialValues={{ ...initialValues }} onSubmit={onSubmit}>
					{(formikField) => {
						return (
							<Form>
								<ModalBody className='px-4'>
									<div className='row'>
										<div className='col-md-6'>
											<div className='w-100'>
												<div className='fload-end'>
													<FormGroup
														id='date'
														label='Date'
														className='col-md-12 mb-3'>
														<Input
															type='date'
															onChange={formikField.handleChange}
															onBlur={formikField.handleBlur}
															value={formikField.values.date}
															isValid={formikField.isValid}
															isTouched={formikField.touched.date}
															invalidFeedback={
																formikField.errors.date
															}
															autoComplete='off'
														/>
													</FormGroup>
												</div>
											</div>
											<FormGroup
												id='material_name'
												label='Material'
												className='col-md-12 mb-3'>
												<Input
													onChange={formikField.handleChange}
													onBlur={formikField.handleBlur}
													value={formikField.values.material_name}
													isValid={formikField.isValid}
													isTouched={formikField.touched.material_name}
													invalidFeedback={
														formikField.errors.material_name
													}
													autoComplete='off'
												/>
											</FormGroup>
											<FormGroup
												id='clasification'
												label='Clasification'
												className='col-md-12 mb-3'>
												<CustomSelect
													placeholder='Select Clasification'
													onChange={(e) => handleSelectClasifications(e)}
													value={clasificationsSelected}
													isSearchable
													options={clasifications}
													darkTheme={darkModeStatus}
												/>
											</FormGroup>
											<FormGroup
												id='qty'
												label='Quantity'
												className='col-md-12 mb-3'>
												<Input
													type='number'
													onChange={formikField.handleChange}
													onBlur={formikField.handleBlur}
													value={formikField.values.qty}
													isValid={formikField.isValid}
													isTouched={formikField.touched.qty}
													invalidFeedback={formikField.errors.qty}
													autoComplete='off'
												/>
											</FormGroup>
											<FormGroup
												id='oum'
												label='UOM'
												className='col-md-12 mb-3'>
												<Input
													onChange={formikField.handleChange}
													onBlur={formikField.handleBlur}
													value={formikField.values.oum}
													isValid={formikField.isValid}
													isTouched={formikField.touched.oum}
													invalidFeedback={formikField.errors.oum}
													autoComplete='off'
												/>
											</FormGroup>
											<FormGroup
												id='pic_name'
												label='PIC'
												className='col-md-12 mb-3'>
												<CustomSelect
													placeholder='Select PIC'
													onChange={(e) => setpicSelected(e)}
													value={picSelected}
													isSearchable
													options={pic}
													darkTheme={darkModeStatus}
												/>
											</FormGroup>
											<FormGroup
												id='note'
												label='Note'
												className='col-md-12 mb-3'>
												<Textarea
													onChange={formikField.handleChange}
													onBlur={formikField.handleBlur}
													value={formikField.values.note}
													isValid={formikField.isValid}
													isTouched={formikField.touched.note}
													invalidFeedback={formikField.errors.note}
													autoComplete='off'
												/>
											</FormGroup>
										</div>
										<div className='col-md-6'>
											<FormGroup
												id='manufacture'
												label='Manufacture'
												className='col-md-12 mb-3'>
												<Input
													onChange={formikField.handleChange}
													onBlur={formikField.handleBlur}
													value={formikField.values.manufacture}
													isValid={formikField.isValid}
													isTouched={formikField.touched.manufacture}
													invalidFeedback={formikField.errors.manufacture}
													autoComplete='off'
												/>
											</FormGroup>
											<FormGroup
												id='supplier'
												label='Supplier'
												className='col-md-12 mb-3'>
												<Input
													onChange={formikField.handleChange}
													onBlur={formikField.handleBlur}
													value={formikField.values.supplier}
													isValid={formikField.isValid}
													isTouched={formikField.touched.supplier}
													invalidFeedback={formikField.errors.supplier}
													autoComplete='off'
												/>
											</FormGroup>
											<FormGroup
												id='origin'
												label='Origin'
												className='col-md-12 mb-3'>
												<Input
													onChange={formikField.handleChange}
													onBlur={formikField.handleBlur}
													value={formikField.values.origin}
													isValid={formikField.isValid}
													isTouched={formikField.touched.origin}
													invalidFeedback={formikField.errors.origin}
													autoComplete='off'
												/>
											</FormGroup>

											{base64Download !== null && (
												<FormGroup
													id='document_'
													label='Document'
													className='col-md-12 mb-3'>
													<Button color='success'>
														<a
															type='button'
															download={base64FileName}
															href={base64Download}>
															Download File
														</a>
													</Button>
												</FormGroup>
											)}

											{base64Download === null && (
												<FormGroup
													id='document'
													label='Document'
													className='col-md-12 mb-3'>
													<Input
														ref={inputRef}
														type='file'
														onChange={(e) => handleChangeFile(e)}
													/>
												</FormGroup>
											)}

											<FormGroup
												id='location'
												label='Location'
												className='col-md-12 mb-3'>
												<Input
													onChange={formikField.handleChange}
													onBlur={formikField.handleBlur}
													value={formikField.values.location}
													isValid={formikField.isValid}
													isTouched={formikField.touched.location}
													invalidFeedback={formikField.errors.location}
													autoComplete='off'
												/>
											</FormGroup>
											<FormGroup
												id='document_sample'
												label='Document sample'
												className='mb-4'>
												<Select
													styles={react_select_styles(
														formikField,
														darkModeStatus,
													)}
													theme={(theme) =>
														react_select_themes(theme, darkModeStatus)
													}
													value={listDocumentSampe}
													options={documentSample}
													onChange={(e) => {
														setListDocumentSampe(e);
														changeDocumentSampleSelected(e);
													}}
													isMulti={isMulti}
												/>
											</FormGroup>
											<FormGroup
												id='remark'
												label='Remark'
												className='col-md-12 mb-3'>
												<CustomSelect
													placeholder='Select Remark'
													onChange={(e) => setremarkSelected(e)}
													value={remarkSelected}
													isSearchable
													options={remark}
													darkTheme={darkModeStatus}
												/>
											</FormGroup>
										</div>
									</div>
								</ModalBody>
								<ModalFooter className='px-4 pb-4'>
									<div className='col-md-12 '>
										<Button
											icon='Save'
											isOutline
											type='submit'
											color='success'
											className='float-end'>
											Update
										</Button>
									</div>
								</ModalFooter>
							</Form>
						);
					}}
				</Formik>
			</Modal>
		</div>
	);
};

const FormDelete = (dt) => {
	const { initialValues, reloadTable } = dt;
	const onDelete = () => {
		Swal.fire({
			title: 'You want to deleting this row?',
			text: 'click yes to continue!',
			icon: 'info',
			showCancelButton: true,
			confirmButtonText: 'Yes',
		}).then((result) => {
			if (result.value) {
				if (result.value) {
					sampleIncomingModule
						.destroy({
							_id: initialValues._id,
						})
						.then((res) => {
							reloadTable();
							showNotification('Success!', res.status, 'success');
						})
						.catch((err) => {
							showNotification('Warning!', err, 'danger');
						});
				} else if (result.dismiss === Swal.DismissReason.cancel) {
					Swal.fire('Cancelled', 'Your data is safe :)', 'error');
				}
			}
		});
	};

	return (
		<div style={{ width: '100px' }}>
			<Button icon='Delete' isOutline type='button' color='danger' onClick={() => onDelete()}>
				Delete
			</Button>
		</div>
	);
};

const CustomButton = (dt) => {
	const { row, reloadTable, customers, assets, setLoadingSubmit } = dt;
	let list_doct = '';
	if (row.list_document) {
		list_doct = row.list_document.split(',').map((x) => {
			return {
				value: x,
				label: x,
			};
		});
	}

	const initialValues = {
		loading: false,
		material_name: row.material_name,
		manufacture: row.manufacture,
		supplier: row.supplier,
		origin: row.origin,
		qty: row.qty,
		oum: row.uom,
		location: row.location,
		pic: {
			value: row.pic,
			text: row.pic_name,
			label: row.pic_name,
		},
		remark: {
			label: row.remark,
			value: row.remark,
			text: row.remark,
		},
		note: row.Note,
		date: moment(row.date_receive).format('YYYY-MM-DD'),
		clasification: {
			value: row.clasification,
			label: row.clasification,
			text: row.clasification,
		},
		document: row.documnet,
		list_document: list_doct,
		list_document_selected: row.list_document,
		send_notif: row.send_notif ? row.send_notif : [],
		_id: row._id,
	};

	return (
		<div style={{ display: 'flex' }}>
			{initialValues.send_notif?.length < 1 &&
			initialValues.note !== '' &&
			initialValues.remark !== 'undefined' ? (
				<FormNotif
					initialValues={initialValues}
					reloadTable={() => reloadTable()}
					assets={assets}
					setLoadingSubmit={setLoadingSubmit}
				/>
			) : (
				''
			)}
			<FormCopy
				cust={customers}
				initialValues={initialValues}
				reloadTable={() => reloadTable()}
				assets={assets}
				setLoadingSubmit={setLoadingSubmit}
			/>
			<FormEdit
				cust={customers}
				initialValues={initialValues}
				reloadTable={() => reloadTable()}
				assets={assets}
				setLoadingSubmit={setLoadingSubmit}
			/>
			<FormDelete initialValues={initialValues} reloadTable={() => reloadTable()} />
		</div>
	);
};

const FormInput = (dt) => {
	const { darkModeStatus } = useDarkMode();
	const inputRef = useRef(null);
	const { initialValues, reloadTable, assets, setLoadingSubmit } = dt;
	const [clasifications, setClasifications] = useState(
		assets.clasifications ? assets.clasifications : [],
	);
	const [remark, setremark] = useState(assets.remark ? assets.remark : []);
	const [documentSample, setDocumentSample] = useState(
		assets.documentSample ? assets.documentSample : [],
	);
	const [pic, setPic] = useState(assets.pic ? assets.pic : []);
	const [clasificationsSelected, setClasificationsSelected] = useState();
	const [remarkSelected, setremarkSelected] = useState('');
	const [documentSampleSelected2, setDocumnetSampleSelected2] = useState([]);
	const [documentSampleSelected, setDocumentSampleSelected] = useState([]);
	const [picSelected, setpicSelected] = useState();
	const [selectedFile, setSelectedFile] = useState(null);
	const [isMulti] = useState(true);

	const onSubmit = (values, { setSubmitting, resetForm, setStatus, setErrors }) => {
		const formData = new FormData();
		if (!values.date) {
			showNotification(
				'Date',
				'date does not exist, cannot create sample incoming',
				'danger',
			);
		}
		if (!clasificationsSelected) {
			showNotification(
				'Clasification',
				'clasification does not exist, cannot create sample incominf',
				'danger',
			);
		}
		if (!values.material) {
			showNotification(
				'Material',
				'material does not exist, cannot create sample incoming',
				'danger',
			);
		}
		if (!values.qty) {
			showNotification(
				'Quantity',
				'Quantity does not exist, cannot create sample incoming',
				'danger',
			);
		}
		if (!values.oum) {
			showNotification('UOM', 'UOM does not exist, cannot create sample incoming', 'danger');
		}
		if (!values.location) {
			showNotification(
				'Location',
				'Location does not exist, cannot create sample incoming',
				'danger',
			);
		}

		formData.append('date_receive', values.date);
		formData.append('clasification', clasificationsSelected.value);
		formData.append('files', selectedFile);
		formData.append('material_name', values.material);
		formData.append('qty', values.qty);
		formData.append('uom', values.oum);
		formData.append('pic', picSelected?.value);
		formData.append('pic_name', picSelected?.label);
		formData.append('pic_email', picSelected?.text);
		formData.append('Note', values.note);
		formData.append('manufacture', values.manufacture);
		formData.append('supplier', values.supplier);
		formData.append('origin', values.origin);
		formData.append('location', values.location);
		formData.append('remark', remarkSelected.value === undefined ? '' : remarkSelected?.value);
		formData.append('list_document', documentSampleSelected);

		if (clasificationsSelected) {
			values.clasification = clasificationsSelected.value;
			values.files = selectedFile;
			try {
				handleSubmit(
					values,
					formData,
					reloadTable,
					handleReset,
					resetForm,
					setLoadingSubmit,
				);
				setStatus({ success: true });
			} catch (error) {
				setStatus({ success: false });
				setSubmitting(false);
				setErrors({ submit: error.message });
			}
		}
	};

	const handleChangeFile = (value) => {
		if (value.target.files[0].size > 2000000) {
			showNotification('Document', 'File size can not more than 2 MB', 'danger');
			setSelectedFile(null);
			value.target.value = '';
		} else {
			setSelectedFile(value.target.files[0]);
			value.target.value = selectedFile;
		}
	};

	const handleReset = (resetForm) => {
		setDocumnetSampleSelected2(null);
		setClasificationsSelected(null);
		inputRef.current.value = null;
		setSelectedFile(null);
		setpicSelected(null);
		setremarkSelected(null);
		resetForm(initialValues);
	};

	const handleSelectClasifications = (e) => {
		return setClasificationsSelected(e);
	};

	const changeDocumentSampleSelected = (e) => {
		const res = e.map((x) => {
			return x.value;
		});
		setDocumnetSampleSelected2(e);
		setDocumentSampleSelected(res);
	};

	useEffect(() => {
		setClasifications(assets.clasifications);
		setremark(assets.remark);
		setDocumentSample(assets.documentSample);
		setPic(assets.pic);
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [assets]);

	return (
		<Formik enableReinitialize initialValues={{ ...initialValues }} onSubmit={onSubmit}>
			{(formikField) => {
				return (
					<Form>
						<div className='row'>
							<div className='col-md-6'>
								<div className='w-100'>
									<div className='fload-end'>
										<FormGroup
											id='date'
											label='Date'
											className='col-md-12 mb-3'>
											<Input
												type='date'
												onChange={formikField.handleChange}
												onBlur={formikField.handleBlur}
												value={formikField.values.date}
												isValid={formikField.isValid}
												isTouched={formikField.touched.date}
												invalidFeedback={formikField.errors.date}
												autoComplete='off'
											/>
										</FormGroup>
									</div>
								</div>
								<FormGroup
									id='material'
									label='Material'
									className='col-md-12 mb-3'>
									<Input
										onChange={formikField.handleChange}
										onBlur={formikField.handleBlur}
										value={formikField.values.material}
										isValid={formikField.isValid}
										isTouched={formikField.touched.material}
										invalidFeedback={formikField.errors.material}
										autoComplete='off'
									/>
								</FormGroup>
								<FormGroup
									id='clasification'
									label='Clasification'
									className='col-md-12 mb-3'>
									<CustomSelect
										placeholder='Select Clasification'
										onChange={(e) => handleSelectClasifications(e)}
										value={clasificationsSelected}
										isSearchable
										options={clasifications}
										darkTheme={darkModeStatus}
									/>
								</FormGroup>
								<FormGroup id='qty' label='Quantity' className='col-md-12 mb-3'>
									<Input
										type='number'
										onChange={formikField.handleChange}
										onBlur={formikField.handleBlur}
										value={formikField.values.qty}
										isValid={formikField.isValid}
										isTouched={formikField.touched.qty}
										invalidFeedback={formikField.errors.qty}
										autoComplete='off'
									/>
								</FormGroup>
								<FormGroup id='oum' label='UOM' className='col-md-12 mb-3'>
									<Input
										onChange={formikField.handleChange}
										onBlur={formikField.handleBlur}
										value={formikField.values.oum}
										isValid={formikField.isValid}
										isTouched={formikField.touched.oum}
										invalidFeedback={formikField.errors.oum}
										autoComplete='off'
									/>
								</FormGroup>
								<FormGroup id='pic' label='PIC' className='col-md-12 mb-3'>
									<CustomSelect
										placeholder='Select PIC'
										onChange={(e) => setpicSelected(e)}
										value={picSelected}
										isSearchable
										options={pic}
										darkTheme={darkModeStatus}
									/>
								</FormGroup>
								<FormGroup id='note' label='Note' className='col-md-12 mb-3'>
									<Textarea
										onChange={formikField.handleChange}
										onBlur={formikField.handleBlur}
										value={formikField.values.note}
										isValid={formikField.isValid}
										isTouched={formikField.touched.note}
										invalidFeedback={formikField.errors.note}
										autoComplete='off'
									/>
								</FormGroup>
							</div>
							<div className='col-md-6'>
								<FormGroup
									id='manufacture'
									label='Manufacture'
									className='col-md-12 mb-3'>
									<Input
										onChange={formikField.handleChange}
										onBlur={formikField.handleBlur}
										value={formikField.values.manufacture}
										isValid={formikField.isValid}
										isTouched={formikField.touched.manufacture}
										invalidFeedback={formikField.errors.manufacture}
										autoComplete='off'
									/>
								</FormGroup>
								<FormGroup
									id='supplier'
									label='Supplier'
									className='col-md-12 mb-3'>
									<Input
										onChange={formikField.handleChange}
										onBlur={formikField.handleBlur}
										value={formikField.values.supplier}
										isValid={formikField.isValid}
										isTouched={formikField.touched.supplier}
										invalidFeedback={formikField.errors.supplier}
										autoComplete='off'
									/>
								</FormGroup>
								<FormGroup id='origin' label='Origin' className='col-md-12 mb-3'>
									<Input
										onChange={formikField.handleChange}
										onBlur={formikField.handleBlur}
										value={formikField.values.origin}
										isValid={formikField.isValid}
										isTouched={formikField.touched.origin}
										invalidFeedback={formikField.errors.origin}
										autoComplete='off'
									/>
								</FormGroup>
								<FormGroup
									id='document'
									label='Document'
									className='col-md-12 mb-3'>
									<Input
										ref={inputRef}
										type='file'
										onChange={(e) => handleChangeFile(e)}
									/>
								</FormGroup>

								<FormGroup
									id='location'
									label='Location'
									className='col-md-12 mb-3'>
									<Input
										onChange={formikField.handleChange}
										onBlur={formikField.handleBlur}
										value={formikField.values.location}
										isValid={formikField.isValid}
										isTouched={formikField.touched.location}
										invalidFeedback={formikField.errors.location}
										autoComplete='off'
									/>
								</FormGroup>
								<FormGroup
									id='document_sample'
									label='Document sample'
									className='mb-4'>
									<Select
										styles={react_select_styles(formikField, darkModeStatus)}
										theme={(theme) =>
											react_select_themes(theme, darkModeStatus)
										}
										options={documentSample}
										onChange={(e) => {
											changeDocumentSampleSelected(e);
										}}
										value={documentSampleSelected2}
										isMulti={isMulti}
										ref={inputRef}
									/>
								</FormGroup>
								<FormGroup id='remark' label='Remark' className='col-md-12 mb-3'>
									<CustomSelect
										placeholder='Select Remark'
										onChange={(e) => setremarkSelected(e)}
										value={remarkSelected}
										isSearchable
										options={remark}
										darkTheme={darkModeStatus}
									/>
								</FormGroup>
							</div>
						</div>
						<div className='col-md-12 '>
							<Button
								icon='Save'
								isOutline
								type='submit'
								color='success'
								className='float-end'>
								Submit
							</Button>
						</div>
					</Form>
				);
			}}
		</Formik>
	);
};

const FilterTableCustom = ({ data, date, perPage, showAll, fetchData, listType, type }) => {
	const { darkModeStatus } = useDarkMode();
	const [newtype, setType] = useState(type);
	const formik = useFormik({
		initialValues: { ...date },
		validate: (values) => {
			const errors = {};

			if (!values?.start && !values?.end) {
				const start = moment(values.start);
				const end = moment(values.end);

				if (start.isAfter(end)) {
					errors.start = 'the date cannot be later than the end date';
				}
				if (end.isBefore(start)) {
					errors.end = 'the date cannot be less than the start date';
				}
			}

			return errors;
		},
		onSubmit: (values) => {
			values.type = newtype.value;
			fetchData(1, perPage, showAll, values);
		},
	});

	const customHandleReset = (e) => {
		formik.handleReset(e);
		formik.values.keyword = '';
		const newDate = { start: '', end: '', keyword: '' };
		fetchData(1, perPage, showAll, newDate);
	};

	return (
		<div className='row' tag='form' noValidate onSubmit={formik.handleSubmit}>
			<div className='col-sm-2 m-1'>
				<FormGroup id='type' label='Type'>
					<CustomSelect
						placeholder='Type'
						onChange={(e) => setType(e)}
						value={newtype}
						options={listType}
						darkTheme={darkModeStatus}
					/>
				</FormGroup>
			</div>
			<div className='col-sm-2 m-1'>
				<FormGroup id='keyword' label='Keyword'>
					<Input
						onChange={formik.handleChange}
						onBlur={formik.handleBlur}
						value={formik.values.keyword}
						isTouched={formik.touched.keyword}
						isValid={formik.isValid}
						invalidFeedback={formik.errors.keyword}
					/>
				</FormGroup>
			</div>
			<div className='col-sm-2 m-1'>
				<FormGroup id='start' label='Start'>
					<Input
						type='date'
						onChange={formik.handleChange}
						onBlur={formik.handleBlur}
						value={formik.values.start}
						isTouched={formik.touched.start}
						isValid={formik.isValid}
						invalidFeedback={formik.errors.start}
					/>
				</FormGroup>
			</div>
			<div className='col-sm-2 m-1'>
				<FormGroup id='end' label='End'>
					<Input
						type='date'
						onChange={formik.handleChange}
						onBlur={formik.handleBlur}
						value={formik.values.end}
						isTouched={formik.touched.end}
						isValid={formik.isValid}
						invalidFeedback={formik.errors.end}
					/>
				</FormGroup>
			</div>
			<div className='col-sm-3 m-1 d-flex align-items-end'>
				<Button
					icon='FilterAlt'
					color='success'
					type='button'
					className='m-1'
					onClick={formik.handleSubmit}
					isLight={darkModeStatus}>
					Filter
				</Button>
				<Button
					icon='Clear'
					color='danger'
					type='reset'
					className='m-1'
					onClick={customHandleReset}
					isLight={darkModeStatus}>
					Clear
				</Button>
				<ExportExcel excelData={data} fileName='Simple Incoming' />
			</div>
		</div>
	);
};

FilterTableCustom.propTypes = {
	date: PropTypes.instanceOf(Object),
	fetchData: PropTypes.func,
	perPage: PropTypes.number,
	showAll: PropTypes.bool,
	listType: PropTypes.instanceOf(Array),
	data: PropTypes.instanceOf(Array),
	type: PropTypes.instanceOf(Object),
};

FilterTableCustom.defaultProps = {
	data: [],
	date: {
		start: '',
		end: '',
	},
	listType: [
		{
			value: 'Clasification',
			text: 'Clasification',
			label: 'Clasification',
		},
		{
			value: 'Manufacture',
			text: 'Manufacture',
			label: 'Manufacture',
		},
		{
			value: 'Supplier',
			text: 'Supplier',
			label: 'Supplier',
		},
		{
			value: 'PIC',
			text: 'PIC',
			label: 'PIC',
		},
	],
	type: {
		value: 'Clasification',
		text: 'Clasification',
		label: 'Clasification',
	},
	perPage: 10,
	showAll: false,
	fetchData: () => {},
};

const SampleIncoming = () => {
	const customDataTableRef = useRef(null);
	const reloadTable = () => {
		const newDate = { start: '', end: '', keyword: '' };
		fetchData(1, perPage, true, newDate);
	};

	const [date, setDate] = useState({ start: '', end: '' });
	const [data, setData] = useState([]);
	const [dataXls, setDataXls] = useState([]);
	const [loading, setLoading] = useState(false);
	const [totalRows, setTotalRows] = useState(0);

	const [clasifications, setClasifications] = useState([]);
	const [remark, setremark] = useState([]);
	const [documentSample, setDocumentSample] = useState([]);
	const [pic, setpic] = useState([]);
	// eslint-disable-next-line no-unused-vars
	const [userNotif, setUserNotif] = useState([]);
	const [assets, setAssets] = useState('');

	const [perPage, setPerPage] = useState(10);
	const [curPage, setCurPage] = useState(1);

	const [show_all, setShowAll] = useState(false);
	const searchRef = useRef(null);

	const [loadingSubmit, setLoadingSubmit] = useState(false);

	const fetchData = async (newPage, newPerPage, all, newDate) => {
		setLoading(true);
		setShowAll(all);
		setDate(newDate);

		const query = {
			page: newPage,
			sizePerPage: newPerPage,
			showAll: all,
		};
		if (newDate?.start && newDate?.end) {
			query.date_start = newDate.start;
			query.date_end = newDate.end;
		}

		if (newDate?.keyword) {
			query.keyword = newDate.keyword;
		}
		if (newDate?.type) {
			query.type = newDate.type;
		}

		return sampleIncomingModule
			.readTable(query)
			.then((response) => {
				setData(response.foundData);
				const temp_array_data = [];
				response.dataXls.forEach((e) => {
					const _temp_data = {
						Date: moment(e.date_receive).format('YYYY-MM-DD'),
						Material: e.material_name,
						Clasification: e.clasification,
						QTY: e.qty,
						UoM: e.uom,
						PIC: e.pic_name,
						Manufacture: e.manufacture,
						Supplier: e.supplier,
						Origin: e.origin,
						Location: e.location,
						Remark: e.remark,
						Note: e.Note,
					};
					temp_array_data.push(_temp_data);
				});
				setDataXls(temp_array_data);
				setTotalRows(response.countData);
				setCurPage(response.currentPage);
				setPerPage(newPerPage);
			})
			.catch(() => {})
			.finally(() => {
				setLoading(false);
			});
	};

	const [customers, setCustomers] = useState([]);

	const fetchUserNotif = async () => {
		return UserModule.getSendNotifUser().then((res) => {
			const result = [];
			res.value.forEach((e) => {
				result.push({
					label: e.name,
					value: e.mail,
				});
			});
			setUserNotif(result);
		});
	};

	const fetchCustomers = async () => {
		return customersModule.readCustomers().then((res) => {
			setCustomers(res.foundData);
		});
	};

	const fetchClasifications = async () => {
		return clasificationsModule.list().then((res) => {
			setClasifications(res.foundData);
		});
	};

	const fetchPIC = async () => {
		return PICModule.list().then((res) => {
			const result = [];
			res.forEach((e) => {
				result.push({
					value: e.username,
					label: e.name,
					text: e.email,
				});
			});
			setpic(result);
		});
	};

	const fetchRemark = async () => {
		return sampleIncomingRemarkModule.list().then((res) => {
			setremark(res.foundData);
		});
	};

	const fetchDocumentSample = async () => {
		return DocumentSampleModule.readSelect().then((res) => {
			setDocumentSample(res);
		});
	};

	const formik = useFormik({
		initialValues: {
			search: '',
			keyword: '',
		},
	});

	useEffect(() => {
		setAssets({ clasifications, remark, documentSample, pic, userNotif });
	}, [clasifications, remark, documentSample, pic, userNotif]);

	useEffect(() => {
		fetchUserNotif();
		fetchClasifications();
		fetchRemark();
		fetchPIC();
		fetchDocumentSample();
		// searchRef.current.focus();
		fetchCustomers();
		const newDate = { start: '', end: '', keyword: '' };
		fetchData(1, perPage, true, newDate);
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	const initialValues = {
		loading: false,
		date: '',
		material: '',
		manufacture: '',
		supplier: '',
		clasification: '',
		origin: '',
		qty: '',
		oum: '',
		location: '',
		pic: '',
		remark: '',
		note: '',
	};

	const { t } = useTranslation('category');

	return (
		<PageWrapper title='Dashboard'>
			<PageLayoutHeader />
			<Page container='fluid'>
				<Card className='col-md-9'>
					<CardHeader borderSize={1}>
						<CardLabel>
							<CardTitle>{t('Incoming Sample')}</CardTitle>
						</CardLabel>
					</CardHeader>
					<CardBody>
						<div className='col-12 mb-3 g-4'>
							<FormInput
								customers={customers}
								reloadTable={() => reloadTable()}
								initialValues={initialValues}
								assets={assets}
								setLoadingSubmit={setLoadingSubmit}
							/>
						</div>
					</CardBody>
				</Card>
				<Card stretch>
					<CardHeader borderSize={1}>
						<CardLabel>
							<CardTitle>{t('Historical Data')}</CardTitle>
						</CardLabel>
					</CardHeader>
					<CardBody>
						<FilterTableCustom
							date={date}
							data={dataXls}
							perPage={perPage}
							showAll={show_all}
							fetchData={fetchData}
						/>

						<div className='mt-5 row'>
							{clasifications.length > 0 &&
							remark.length > 0 &&
							pic.length > 0 &&
							documentSample.length > 0 ? (
								<>
									<FormGroup
										label='Invoices Number'
										formText='Use either keyboard or scanner'
										className='col-12 col-md-5 col-lg-3 col-xl-2 d-none'>
										<InputGroup size='sm'>
											<Input
												id='search'
												value={formik.values.search}
												onChange={formik.handleChange}
												ref={searchRef}
											/>
											<Button
												color='primary'
												id='button-addon1'
												onClick={() =>
													onSearch(formik, customDataTableRef)
												}>
												Search
											</Button>
										</InputGroup>
									</FormGroup>

									<TableCustom
										data={data}
										totalRows={totalRows}
										perPage={perPage}
										curPage={curPage}
										showAll={show_all}
										loading={loading}
										date={date}
										fetchData={fetchData}
										assets={{
											clasifications,
											remark,
											documentSample,
											pic,
											userNotif,
										}}
										setLoadingSubmit={setLoadingSubmit}
									/>
								</>
							) : (
								''
							)}
						</div>
					</CardBody>
				</Card>
				<Modal
					isOpen={loadingSubmit}
					size='sm'
					isCentered
					setIsOpen={() => {}}
					isStaticBackdrop>
					<ModalBody
						style={{ backgroundColor: '#6c5dd3', color: 'white' }}
						className='text-center'>
						<button className='btn btn-primary' type='button' disabled>
							<span
								className='spinner-grow spinner-grow-sm'
								role='status'
								aria-hidden='true'
							/>
							&nbsp;
							<span
								className='spinner-grow spinner-grow-sm'
								role='status'
								aria-hidden='true'
							/>
							&nbsp;
							<span
								className='spinner-grow spinner-grow-sm'
								role='status'
								aria-hidden='true'
							/>
							&nbsp;
							<span className='sr-only'>Loading...</span>
						</button>
					</ModalBody>
				</Modal>
			</Page>
		</PageWrapper>
	);
};

export default SampleIncoming;
