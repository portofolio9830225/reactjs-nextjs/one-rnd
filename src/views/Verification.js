import React, { useEffect, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import moment from 'moment';
import Swal from 'sweetalert2';
import PropTypes from 'prop-types';
import Card, { CardBody, CardHeader, CardLabel, CardTitle } from '../components/bootstrap/Card';
import DarkDataTable from '../components/DarkDataTable';
import useDarkMode from '../hooks/useDarkMode';
import Page from '../layout/Page/Page';
import PageWrapper from '../layout/PageWrapper/PageWrapper';
import FormulaModule from '../modules/FormulaModule';
import PageLayoutHeader from '../pages/common/Headers/PageLayoutHeader';
import Button from '../components/bootstrap/Button';
import Spinner from '../components/bootstrap/Spinner';
import Modal, { ModalBody, ModalHeader, ModalTitle } from '../components/bootstrap/Modal';
import InputGroup from '../components/bootstrap/forms/InputGroup';
import Input from '../components/bootstrap/forms/Input';
import { getRequester } from '../helpers/helpers';
import Notifications from '../components/Notifications';
import FormulaEvaluationComponent from '../components/custom/FormulaEvaluationComponent';
import showNotification from '../components/extras/showNotification';
import FormCustomModalDetail from '../components/custom/FormCustomModalDetail';

const STATUS = [
	// Check QC
	'Selesai Trial Miniplant',
	'Sedang Pengecekan QC',
	// Sensory / Evaluation
	'Siap Verifikasi Hasil Miniplant',
	'Sedang Verifikasi Hasil Miniplant',
	// add here
];

const STATUS_CODE = {
	// Check QC
	SELESAI_TRIAL_MINIPLANT: 'selesai_trial_miniplant',
	SEDANG_PENGECEKAN_QC: 'sedang_pengecekan_qc',
	// Sensory / Evaluation
	SIAP_VERIFIKASI_HASIL_MINIPLANT: 'siap_verifikasi_hasil_miniplant',
	SEDANG_VERIFIKASI_HASIL_MINIPLANT: 'sedang_verifikasi_hasil_miniplant',
	// add here
};

const CATEGORY = ['ndc', 'The appearance & stability', 'Taste'];

const handleUpload = (values) => {
	const newResponse = new Promise((resolve, reject) => {
		try {
			FormulaModule.verificationFormula(values)
				.then(() => {
					Notifications.showNotif({
						header: 'Information!',
						message: 'Data has been upload successfully',
						type: Notifications.TYPE.SUCCESS,
					});
					resolve({ error: false, message: 'successfully' });
				})
				.catch((err) => {
					Notifications.showNotif({
						header: 'Warning!!',
						message: err,
						type: Notifications.TYPE.ERROR,
					});
					reject(new Error(err));
				});
		} catch (e) {
			reject(new Error(e.message));
		}
	});
	return newResponse;
};

const handleEvaluation = (values) => {
	const newResponse = new Promise((resolve, reject) => {
		try {
			//
			FormulaModule.sensoryFormula(values)
				.then(() => {
					Notifications.showNotif({
						header: 'Information!',
						message: 'Data has been saved successfully',
						type: Notifications.TYPE.SUCCESS,
					});
					resolve({ error: false, message: 'successfully' });
				})
				.catch((err) => {
					Notifications.showNotif({
						header: 'Warning!!',
						message: err,
						type: Notifications.TYPE.ERROR,
					});
					reject(new Error(err));
				});
		} catch (e) {
			reject(new Error(e.message));
		}
	});
	return newResponse;
};

const FormCustomModalQC = ({ initialValues, handleCustomUpload, options }) => {
	const { darkModeStatus } = useDarkMode();
	const { username, department } = getRequester();

	const [isOpen, setOpen] = useState(false);

	const [files, setFiles] = useState(null);
	const [loading, setLoading] = useState(false);

	const onCustomFile = (event) => {
		setFiles(event.target.files[0]);
	};

	const onCustomUpload = () => {
		try {
			Swal.fire({
				title: 'Are you sure?',
				text: 'Please check your entries !',
				icon: 'question',
				showCancelButton: true,
				confirmButtonText: 'Yes',
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
			}).then((result) => {
				if (result.isConfirmed) {
					const formData = new FormData();
					formData.append('_id', initialValues._id);
					formData.append('department', department);
					formData.append('document_number', initialValues.document_number);
					formData.append('requester', username);

					if (files) {
						formData.append('files', files);
					}

					setLoading(true);
					handleCustomUpload(formData, setLoading, options);
				} else if (result.dismiss === Swal.DismissReason.cancel) {
					Swal.fire('Cancelled', 'Your data is safe :)', 'error');
				}
			});
		} catch (error) {
			Swal.fire('Information ', 'Please check your entries again!', 'error');
		}
	};

	return (
		<>
			<Button
				icon='Pageview'
				color='warning'
				type='button'
				isLight={darkModeStatus}
				className='mx-1'
				onClick={() => setOpen(true)}>
				{' '}
				Check QC
			</Button>

			<Modal
				isOpen={isOpen}
				setIsOpen={setOpen}
				size='lg'
				titleId='modal-crud'
				isCentered
				isStaticBackdrop>
				<ModalHeader setIsOpen={setOpen} className='p-4'>
					<ModalTitle id='modal-crud'>
						Upload Data - ({initialValues?.document_number}) -{' '}
						{initialValues?.customer_name}
					</ModalTitle>
				</ModalHeader>
				<ModalBody>
					<div className='row py-2'>
						<div className='col-md-12'>
							<InputGroup>
								<Input type='file' onChange={onCustomFile} />
							</InputGroup>
						</div>
					</div>
					<div className='row py-2'>
						<div className='col-md-12'>
							{loading ? (
								<Button
									color='success'
									className='float-end'
									isLight={darkModeStatus}>
									<Spinner isSmall inButton />
									Loading...
								</Button>
							) : (
								<Button
									color='success'
									isLight={darkModeStatus}
									icon='UploadFile'
									className='float-end'
									onClick={onCustomUpload}>
									Upload
								</Button>
							)}
						</div>
					</div>
				</ModalBody>
			</Modal>
		</>
	);
};

FormCustomModalQC.propTypes = {
	initialValues: PropTypes.instanceOf(Object),
	handleCustomUpload: PropTypes.func,
	options: PropTypes.instanceOf(Object),
};
FormCustomModalQC.defaultProps = {
	initialValues: null,
	handleCustomUpload: () => {},
	options: {
		curPage: 1,
		perPage: 10,
		showAll: false,
	},
};

const FormCustomModalEvaluation = ({ initialValues, handleCustomEvaluation, options }) => {
	const { darkModeStatus } = useDarkMode();
	const { username } = getRequester();

	const [isOpen, setOpen] = useState(false);
	const [errors, setErrors] = useState({ error: false, message: '' });
	const [sensory, setSensory] = useState([]);
	const [data, setData] = useState([]);

	const options_category = CATEGORY.map((item) => {
		return { value: item.replace(/[^a-zA-Z0-9]/g, '_').toLowerCase(), label: item };
	});

	const onChangeCategory = (values, newRemark, files) => {
		const formData = new FormData();
		formData.append('remark', newRemark);
		formData.append('_id', initialValues._id);
		formData.append('requester', username);
		formData.append('files', files);
		files.forEach((file) => {
			formData.append('files', file);
		});

		let new_errors = { error: false, message: '' };
		const result = values.map((item) => {
			const parameter_code = item.category?.value;
			const parameter_name = item.category?.label;

			const detail = item.detail.map((detail_item) => {
				const name = detail_item.parameter?.value;
				const { value } = detail_item;

				if (!name && !new_errors.error) {
					new_errors = { error: true, message: 'please complete the parameter name' };
				}

				if (!value && !new_errors.error) {
					new_errors = { error: true, message: 'please complete the parameter value' };
				}

				return { name, value, remark: detail_item?.remark };
			});

			if (!parameter_code && !new_errors.error) {
				new_errors = { error: true, message: 'please complete the category' };
			}

			if (detail.length === 0 && !new_errors.error) {
				new_errors = { error: true, message: 'please add detail parameter' };
			}

			return { parameter_code, parameter_name, detail };
		});

		setErrors(new_errors);
		setSensory(result);
		formData.append('sensory', JSON.stringify(sensory));
		setData(formData);
	};

	const onCustomSubmit = () => {
		try {
			Swal.fire({
				title: 'Are you sure?',
				text: 'Please check your entries !',
				icon: 'question',
				showCancelButton: true,
				confirmButtonText: 'Yes',
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
			}).then((result) => {
				if (result.isConfirmed) {
					if (sensory.length === 0) {
						showNotification(
							'Information',
							'Please define category parameter',
							'danger',
						);
						return;
					}

					if (errors.error) {
						showNotification('Information', errors.message, 'danger');
						return;
					}					
					handleCustomEvaluation(data, sensory,setOpen, options);
				} else if (result.dismiss === Swal.DismissReason.cancel) {
					Swal.fire('Cancelled', 'Your data is safe :)', 'error');
				}
			});
		} catch (error) {
			Swal.fire('Information ', 'Please check your entries again!', 'error');
		}
	};

	return (
		<>
			<Button
				icon='FactCheck'
				color='success'
				type='button'
				isLight={darkModeStatus}
				className='mx-1'
				onClick={() => setOpen(true)}>
				{' '}
				Evaluation
			</Button>

			<Modal
				isOpen={isOpen}
				setIsOpen={setOpen}
				size='xl'
				titleId='modal-crud-evaluation'
				isStaticBackdrop>
				<ModalHeader setIsOpen={setOpen} className='p-4'>
					<ModalTitle id='modal-crud-evaluation'>
						Evaluation - ({initialValues?.document_number}) -{' '}
						{initialValues?.customer_name}
					</ModalTitle>
				</ModalHeader>
				<ModalBody>
					<FormulaEvaluationComponent
						optionsCategory={options_category}
						onChange={onChangeCategory}
					/>
					<div className='row'>
						<div className='col-md-12'>
							<Button
								icon='Save'
								color='success'
								type='button'
								isLight={darkModeStatus}
								className='float-end m-1'
								onClick={onCustomSubmit}>
								Submit
							</Button>
						</div>
					</div>
				</ModalBody>
			</Modal>
		</>
	);
};

FormCustomModalEvaluation.propTypes = {
	initialValues: PropTypes.instanceOf(Object),
	handleCustomEvaluation: PropTypes.func,
	options: PropTypes.instanceOf(Object),
};
FormCustomModalEvaluation.defaultProps = {
	initialValues: null,
	handleCustomEvaluation: () => {},
	options: {
		curPage: 1,
		perPage: 10,
		showAll: false,
	},
};

const CustomButtonTable = ({
	initialValues,
	options,
	row,
	handleCustomUpload,
	handleCustomEvaluation,
}) => {
	const { username } = getRequester();
	const newInitialValues = {};
	newInitialValues.ticket_id = row.ticket_id;
	newInitialValues.formula_id = row._id;
	newInitialValues.document_number = row.document_number;
	newInitialValues.customer_name = row.customer_name;
	if (row.pic == username) {
		return (
			<>
				<FormCustomModalDetail initialValues={newInitialValues} />

				{['NOT USED'].includes(initialValues.status_code) && (
					<FormCustomModalQC
						initialValues={initialValues}
						options={options}
						handleCustomUpload={handleCustomUpload}
					/>
				)}

				{[
					STATUS_CODE.SIAP_VERIFIKASI_HASIL_MINIPLANT,
					STATUS_CODE.SEDANG_VERIFIKASI_HASIL_MINIPLANT,
					STATUS_CODE.SELESAI_TRIAL_MINIPLANT,
					STATUS_CODE.SEDANG_PENGECEKAN_QC,
				].includes(initialValues.status_code) && (
					<FormCustomModalEvaluation
						initialValues={initialValues}
						options={options}
						handleCustomEvaluation={handleCustomEvaluation}
					/>
				)}
			</>
		);
	}
	return <FormCustomModalDetail initialValues={initialValues} />;
};

CustomButtonTable.propTypes = {
	initialValues: PropTypes.instanceOf(Object),
	options: PropTypes.instanceOf(Object),
	row: PropTypes.instanceOf(Object),
	handleCustomUpload: PropTypes.func,
	handleCustomEvaluation: PropTypes.func,
};
CustomButtonTable.defaultProps = {
	initialValues: null,
	options: {
		curPage: 1,
		perPage: 10,
		showAll: false,
	},
	row: {},
	handleCustomUpload: () => {},
	handleCustomEvaluation: () => {},
};

const TableCustom = ({
	data,
	totalRows,
	perPage,
	loading,
	fetchData,
	curPage,
	showAll,
	handleCustomUpload,
	handleCustomEvaluation,
}) => {
	const { darkModeStatus } = useDarkMode();

	const handlePageChange = (page) => {
		fetchData(page, perPage, showAll);
	};

	const handlePerRowsChange = async (newPerPage, page) => {
		const all = totalRows === newPerPage;
		return fetchData(page, newPerPage, all);
	};

	const paginationComponentOptions = {
		selectAllRowsItem: true,
		selectAllRowsItemText: 'ALL',
	};

	const columns = useMemo(
		() => [
			{
				name: 'REGISTER DATE',
				selector: (row) => moment(row.created_at).format('YYYY-MM-DD'),
				sortable: true,
				width: '140px',
			},
			{
				name: 'TICKET ID',
				selector: (row) => row.document_number,
				sortable: true,
				width: '220px',
			},
			{
				name: 'TARGET DATE',
				selector: (row) => moment(row.target_date).format('YYYY-MM-DD'),
				sortable: true,
				width: '140px',
			},
			{
				name: 'CUSTOMER',
				selector: (row) => row.customer_name,
				sortable: true,
				width: '420px',
			},
			{
				name: 'SCHEDULE DATE',
				selector: (row) => moment(row.schedule_date).format('YYYY-MM-DD'),
				sortable: true,
				width: '180px',
			},
			{
				name: 'JN NUMBER',
				selector: (row) => row.job_number,
				sortable: true,
				width: '180px',
			},
			{
				name: 'PIC',
				selector: (row) => row.pic_name,
				sortable: true,
			},
			{
				name: 'ACTION',
				width: '300px',
				// eslint-disable-next-line react/no-unstable-nested-components
				cell: (row) => {
					const { _id, customer_name, document_number, status_code, ticket_id } = row;
					const initialValues = {
						_id,
						customer_name,
						document_number,
						status_code,
						ticket_id,
					};

					const options = { curPage, perPage, showAll };

					return (
						<CustomButtonTable
							initialValues={initialValues}
							options={options}
							row={row}
							handleCustomUpload={handleCustomUpload}
							handleCustomEvaluation={handleCustomEvaluation}
						/>
					);
				},
			},
		],
		// eslint-disable-next-line react-hooks/exhaustive-deps
		[curPage, perPage, showAll],
	);

	return (
		<DarkDataTable
			columns={columns}
			data={data}
			pagination
			progressPending={loading}
			paginationServer
			paginationTotalRows={totalRows}
			onChangeRowsPerPage={handlePerRowsChange}
			onChangePage={handlePageChange}
			paginationComponentOptions={paginationComponentOptions}
			theme={darkModeStatus ? 'custom_dark' : 'light'}
		/>
	);
};
TableCustom.propTypes = {
	data: PropTypes.instanceOf(Array),
	loading: PropTypes.bool,
	totalRows: PropTypes.number,
	curPage: PropTypes.number,
	perPage: PropTypes.number,
	showAll: PropTypes.bool,
	fetchData: PropTypes.func,
	handleCustomUpload: PropTypes.func,
	handleCustomEvaluation: PropTypes.func,
};
TableCustom.defaultProps = {
	data: [],
	loading: false,
	totalRows: 0,
	curPage: 1,
	perPage: 10,
	showAll: false,
	fetchData: () => {},
	handleCustomUpload: () => {},
	handleCustomEvaluation: () => {},
};

const Verification = () => {
	const { t } = useTranslation('crud');

	const [data, setData] = useState([]);
	const [totalRows, setTotalRows] = useState(0);
	const [perPage, setPerPage] = useState(10);
	const [curPage, setCurPage] = useState(1);

	const [show_all, setShowAll] = useState(false);
	const [loading, setLoading] = useState(false);

	const uploadSubmit = (values, setLoadingUpload, options) => {
		handleUpload(values)
			.then(() => {
				fetchData(options.curPage, options.perPage, options.show_all);
			})
			.catch(() => {})
			.finally(() => {
				setLoadingUpload(false);
			});
	};

	const evaluationSubmit = (values, sensory, setModalEvaluation, options) => {
		// values.sensory = sensory
		handleEvaluation(values, sensory)
			.then(() => {
				fetchData(options.curPage, options.perPage, options.show_all);
			})
			.catch(() => {})
			.finally(() => {
				setModalEvaluation(false);
			});
	};

	useEffect(() => {
		fetchData(curPage, perPage, show_all);
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	const fetchData = async (newPage, newPerPage, all) => {
		setLoading(true);
		setShowAll(all);

		const query = {
			page: newPage,
			sizePerPage: newPerPage,
			showAll: all,
			status: STATUS,
		};

		return FormulaModule.readFormula(query)
			.then((response) => {
				setData(response.foundData);
				setTotalRows(response.countData);
				setCurPage(response.currentPage);
				setPerPage(newPerPage);
			})
			.catch(() => {})
			.finally(() => {
				setLoading(false);
			});
	};

	return (
		<PageWrapper title={t('Verification Trial')}>
			<PageLayoutHeader />
			<Page container='fluid'>
				<Card stretch>
					<CardHeader borderSize={1}>
						<CardLabel icon='DomainVerification' iconColor='info'>
							<CardTitle>{t('Verification Trial')}</CardTitle>
						</CardLabel>
					</CardHeader>
					<CardBody>
						<TableCustom
							data={data}
							totalRows={totalRows}
							perPage={perPage}
							curPage={curPage}
							showAll={show_all}
							loading={loading}
							fetchData={fetchData}
							handleCustomUpload={uploadSubmit}
							handleCustomEvaluation={evaluationSubmit}
						/>
					</CardBody>
				</Card>
			</Page>
		</PageWrapper>
	);
};

Verification.propTypes = {};
Verification.defaultProps = {};

export default Verification;
