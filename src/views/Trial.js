import React, { useState, useEffect, useMemo } from 'react';
import PropTypes from 'prop-types';
import Swal from 'sweetalert2';
import { useTranslation } from 'react-i18next';
import moment from 'moment';
import Accordion, { AccordionItem } from '../components/bootstrap/Accordion';
import Card, { CardBody, CardHeader, CardLabel, CardTitle } from '../components/bootstrap/Card';
import PageWrapper from '../layout/PageWrapper/PageWrapper';
import Page from '../layout/Page/Page';
import PageLayoutHeader from '../pages/common/Headers/PageLayoutHeader';
import FormGroup from '../components/bootstrap/forms/FormGroup';
import Input from '../components/bootstrap/forms/Input';
import Button from '../components/bootstrap/Button';
import TrialModule from '../modules/TrialModule';
import priorityModule from '../modules/PriorityModule';
import MaterialModule from '../modules/MaterialModule';
import VendorModule from '../modules/VendorModule';
import CurrencyModule from '../modules/CurrencyModule';
import DarkDataTable from '../components/DarkDataTable';
import useDarkMode from '../hooks/useDarkMode';
import showNotification from '../components/extras/showNotification';
import ParameterTrial from '../components/custom/ParameterTrial';
import FormulaMaterial from '../components/custom/FormulaMaterial';
import { getRequester } from '../helpers/helpers';
import Modal, { ModalBody, ModalHeader, ModalTitle } from '../components/bootstrap/Modal';
import DetailTicket from '../components/custom/rizky/ticket/DetailTicket';
import DetailTicketAndFormulaComponent from '../components/custom/DetailTicketAndFormulaComponent';
import ProductProperties from '../components/custom/sodiq/ProductProperties';
import AddInfromationService from '../modules/AddInfromationModule';

const handleSubmitStartTrial = (values, handleReloadData, handleShowFormTrial, setLoadingSubmit) => {
	setLoadingSubmit(true)
	TrialModule.startTrial(values)
		.then((res) => {
			setLoadingSubmit(false)
			showNotification('Success!', res.status, 'success');
			handleReloadData();
			handleShowFormTrial('', '');
		})
		.catch((err) => {
			setLoadingSubmit(false)
			showNotification('Warning!', err, 'danger');
		});
	return values;
};

const handleSubmitFinishTrial = (values, handleReloadData, handleShowFormTrial, setLoadingSubmit) => {
	setLoadingSubmit(true)
	TrialModule.finishTrial(values)
		.then((res) => {
			setLoadingSubmit(false)
			showNotification('Success!', res.status, 'success');
			handleReloadData();
			handleShowFormTrial('', '');
		})
		.catch((err) => {
			setLoadingSubmit(false)
			showNotification('Warning!', err, 'danger');
		});
	return values;
};

const FormButton = (dt) => {
	const { darkModeStatus } = useDarkMode();
	const { handleShowFormTrial, row } = dt;

	const new_initial_value = {};
	new_initial_value.ticket_id = row.ticket_id;
	new_initial_value.formula_id = row._id;
	new_initial_value.document_number = row.document_number;
	new_initial_value.customer_name = row.customer_name;

	return (
		<>
			<FormCustomModalDetail initialValues={new_initial_value} />

			{row.status_code == 'siap_trial_miniplant' || row.status_code == 'siap_penjadwalan' ? (
				<Button
					type='button'
					color='success'
					isLight={darkModeStatus}
					onClick={() => {
						handleShowFormTrial('Start Trial', row);
					}}>
					Start Trial
				</Button>
			) : (
				<Button
					type='button'
					color='danger'
					isLight={darkModeStatus}
					onClick={() => {
						handleShowFormTrial('Finish Trial', row);
					}}>
					Finish Trial
				</Button>
			)}
		</>
	);
};

const CustomButton = (dt) => {
	const { row, listPriority, handleReloadData, handleShowFormTrial } = dt;

	// restructure the obj
	const initialValues = {};
	initialValues.id = row._id;

	return (
		<FormButton
			initialValues={initialValues}
			listPriority={listPriority}
			handleReloadData={handleReloadData}
			handleShowFormTrial={handleShowFormTrial}
			row={row}
		/>
	);
};

const CustomDataTable = ({
	listPriority,
	data,
	loading,
	totalRows,
	handlePageChange,
	handlePerRowsChange,
	handleReloadData,
	handleShowFormTrial,
}) => {
	const { darkModeStatus } = useDarkMode();

	const columns = useMemo(
		() => [
			{
				name: 'Job Number',
				width: '200px',
				selector: (row) => row.job_number,
				sortable: true,
			},
			{
				name: 'Q Number',
				width: '150px',
				selector: (row) => row.queue_number,
				sortable: true,
			},
			{
				name: 'Due Date',
				width: '150px',
				selector: (row) => moment(row.due_date_miniplant).format('YYYY-MMM-DD'),
				sortable: true,
			},
			{
				name: 'Target Date',
				width: '150px',
				selector: (row) => moment(row.target_date).format('YYYY-MMM-DD'),
				sortable: true,
			},
			{
				name: 'Register Date',
				width: '150px',
				selector: (row) => moment(row.created_at).format('YYYY-MMM-DD'),
				sortable: true,
			},

			{
				name: 'Doc Number',
				width: '200px',
				selector: (row) => row.document_number,
				sortable: true,
			},
			{
				name: 'Customer',
				selector: (row) => row.customer_name,
				sortable: true,
			},
			{
				name: 'Schedule Date',
				width: '150px',
				selector: (row) =>
					row.schedule_date ? moment(row.schedule_date).format('YYYY-MMM-DD') : '',
				sortable: true,
			},
			{
				name: 'Priority',
				selector: (row) => row.priority,
				sortable: true,
			},
			{
				name: 'PIC',
				selector: (row) => row.pic_name,
				sortable: true,
			},
			{
				name: 'Action',
				width: '300px',
				// eslint-disable-next-line react/no-unstable-nested-components
				cell: (dt) => {
					return (
						<CustomButton
							row={dt}
							listPriority={listPriority}
							handleReloadData={handleReloadData}
							handleShowFormTrial={handleShowFormTrial}
						/>
					);
				},
			},
		],
		[listPriority, handleReloadData, handleShowFormTrial],
	);

	return (
		<DarkDataTable
			columns={columns}
			data={data}
			progressPending={loading}
			pagination
			paginationServer
			paginationTotalRows={totalRows}
			onChangeRowsPerPage={handlePerRowsChange}
			onChangePage={handlePageChange}
			theme={darkModeStatus ? 'custom_dark' : 'light'}
		/>
	);
};

const Trial = () => {
	const { darkModeStatus } = useDarkMode();
	const [data, setData] = useState([]);
	const [listPriority, setPriority] = useState([]);
	const { username } = getRequester();

	const [loading, setLoading] = useState(false);
	const [totalRows, setTotalRows] = useState(0);
	const [perPage, setPerPage] = useState(10);
	const [page, setPage] = useState(1);
	const [reloadData, setReloadData] = useState(false);
	const [targeDateStart, setTargeDateStart] = useState(null);
	const [targeDateEnd, setTargeDateEnd] = useState(null);
	const [formTrialShow, setFormTrialShow] = useState(false);

	const [titleTrial, setTitleTrial] = useState(null);
	const [dataFormula, setDataFormula] = useState(null);
	const [materialFomula, setMaterialFormula] = useState(null);
	const [dataParameter, setDataParameter] = useState([]);
	const [isReadOnlyFormula] = useState(true);
	const [resultFgName, setResultFgName] = useState(null);
	const [resultFgCode, setResultFgCode] = useState(null);

	const [listUom] = useState([{ label: 'KG', value: 'KG' }]);
	const [listCurrency, setCurrency] = useState([]);
	const [listMaterial, setMaterial] = useState([]);
	const [listVendor, setVendor] = useState([]);

	const [isReset, setReset] = useState(false);
	const [init, setInit] = useState(true);

	const [initProductProperties, setProductProperties] = useState([]);
	const [listProductProperties, setListProductProperties] = useState([]);
	const [loadingSubmit, setLoadingSubmit] = useState(false);

	const [list_parameter] = useState([
		{
			value: 'mixing_sequences',
			label: 'mixing_sequences',
		},
		{
			value: 'homogenizer',
			label: 'homogenizer',
		},
		{
			value: 'pasteurization',
			label: 'Pasteurization',
		},
		{
			value: 'mass_balance',
			label: 'Mass Balance',
		},
		{
			value: 'spray_dryer',
			label: 'Spray dryer',
		},
	]);

	const [isReadOnly] = useState(false);

	const handleShowFormTrial = async (title, data_formula) => {
		setTitleTrial(title);
		setDataFormula(data_formula);
		setMaterialFormula(data_formula ? data_formula.formula : []);
		setFormTrialShow(!formTrialShow);
		setInit(true);
		setResultFgName(data_formula.result_fg_name);
		setResultFgCode(data_formula.result_fg_code);

		const arr = [...data_formula.product_properties]
		const temp = []
		arr.forEach((item) => {
			temp.push({ product_properties: { value: item.properties, label: item.properties }, value: item.value })
		})
		if (temp.length > 0) {
			setProductProperties([{ properties: temp }])
		}
	};

	const fetchTrial = async (nPage, nPerPage, dateStart, dateFinish) => {
		setLoading(true);
		let query = ``;
		if (dateStart && dateFinish) {
			query = `&target_date_start=${dateStart}&target_date_finish=${dateFinish}`;
		}
		return TrialModule.read(`page=${nPage}&sizePerPage=${nPerPage}${query}`).then((res) => {
			setData(res.foundData);
			setTotalRows(res.countData);
			setLoading(false);
		});
	};

	const fetchPriority = async () => {
		return priorityModule.readSelect().then((res) => {
			setPriority(res);
		});
	};

	const fetchMaterial = async () => {
		const query = `material_type=ZRAW`;
		return MaterialModule.readSelect(query).then((res) => {
			setMaterial(res);
		});
	};
	const fetchVendor = async () => {
		const query = ``;
		return VendorModule.readSelect(query).then((res) => {
			setVendor(res);
		});
	};
	const fetchCurrency = async () => {
		const query = ``;
		return CurrencyModule.readSelect(query).then((res) => {
			setCurrency(res);
		});
	};

	const handleReloadData = () => {
		setReloadData(!reloadData);
	};

	const onChangeMainParameter = (e) => {
		setDataParameter(e);
	};
	const onChangeMainFormula = (e) => {
		setMaterialFormula(e);
	};

	const submitForm = (type) => {
		if (dataParameter.length == 0) {
			showNotification('Information', `Must fill in the parameters and details`, 'danger');
			return dataFormula;
		}

		if (materialFomula.length == 0) {
			showNotification('Information', `Must fill in the formula`, 'danger');
			return materialFomula;
		}

		let next = true;
		const BreakException = {};
		try {
			dataParameter.forEach((element) => {
				if (!element.detail) throw BreakException;
			});
		} catch (e) {
			if (e !== BreakException) throw e;
			next = false;
		}

		if (!next) {
			showNotification('Information', `Must fill in the detail paramater`, 'danger');
			return dataFormula;
		}

		const { _id } = dataFormula;
		if (_id) {
			const val = {};
			val._id = _id;
			val.details = dataParameter;
			val.formula = materialFomula;
			val.requester = username;
			val.product_properties = initProductProperties
			return Swal.fire({
				title: 'Are you sure?',
				text: 'Please check your entries !',
				icon: 'info',
				showCancelButton: true,
				confirmButtonText: 'Yes',
			}).then((result) => {
				if (result.value) {
					if (type == 'Finish Trial') {
						handleSubmitFinishTrial(val, handleReloadData, handleShowFormTrial, setLoadingSubmit);
					} else {
						handleSubmitStartTrial(val, handleReloadData, handleShowFormTrial, setLoadingSubmit);
					}
				}
			});
		}
		return _id;
	};

	const filter = () => {
		if (moment(moment(targeDateStart)).isAfter(moment(targeDateEnd))) {
			Swal.fire({
				title: 'Warning',
				text: 'start date cant be bigger than finish date',
				icon: 'warning',
				showCancelButton: false,
				confirmButtonText: 'Yes',
			});
		} else {
			fetchTrial(
				1,
				10,
				targeDateStart ? moment(targeDateStart).format('YYYY-MM-DD') : null,
				targeDateEnd ? moment(targeDateEnd).format('YYYY-MM-DD') : null,
			);
		}
	};

	const fetchProductProperties = async () => {
		const query = `category=product_properties`;
		return AddInfromationService.readSelect(query).then((res) => {
			setListProductProperties(res)
		});
	}

	useEffect(() => {
		fetchTrial(page, perPage, null, null);
		fetchPriority();
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [page, perPage, reloadData]);

	useEffect(() => {
		fetchTrial(1, 10, null, null);
		fetchPriority();
		fetchMaterial();
		fetchVendor();
		fetchCurrency();
		fetchProductProperties()
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	const { t } = useTranslation('menu');
	const [title] = useState({ title: 'Scheduling' });
	return (
		<PageWrapper title={title.title}>
			<PageLayoutHeader />
			<Page container='fluid'>
				{!formTrialShow && (
					<Card stretch>
						<CardHeader borderSize={1}>
							<CardLabel>
								<CardTitle>{t('trial_process')}</CardTitle>
							</CardLabel>
						</CardHeader>
						<CardBody>
							<div className='row pb-4'>
								<div className='col-sm-5 col-md-5'>
									<FormGroup
										id='target_date_start'
										label='Schedule Date Start'
										isColForLabel
										labelClassName='col-sm-4 text-capitalize'
										childWrapperClassName='col-sm-8'>
										<Input
											type='date'
											onInput={(value) => {
												setTargeDateStart(value.target.value);
											}}
											selected={targeDateStart}
										/>
									</FormGroup>
								</div>
								<div className='col-sm-5 col-md-5'>
									<FormGroup
										id='target_date_finish'
										label='Schedule Date Finish'
										isColForLabel
										labelClassName='col-sm-4 text-capitalize'
										childWrapperClassName='col-sm-8'>
										<Input
											type='date'
											onInput={(value) => {
												setTargeDateEnd(value.target.value);
											}}
											selected={targeDateEnd}
										/>
									</FormGroup>
								</div>
								<div className='col-sm-2 col-md-2'>
									<Button
										icon='FilterAlt'
										color='info'
										type='button'
										className='mx-1'
										onClick={filter}
										isLight={darkModeStatus}>
										Filter
									</Button>
								</div>
							</div>
							<CustomDataTable
								data={data}
								loading={loading}
								totalRows={totalRows}
								listPriority={listPriority}
								handlePageChange={setPage}
								handlePerRowsChange={setPerPage}
								handleReloadData={handleReloadData}
								handleShowFormTrial={handleShowFormTrial}
							/>
						</CardBody>
					</Card>
				)}
				{formTrialShow && (
					<Card stretch>
						<CardHeader borderSize={1}>
							<CardLabel>
								<CardTitle>
									{titleTrial} - {dataFormula.document_number} |{' '}
									{dataFormula.job_number ? dataFormula.job_number : ''}
								</CardTitle>
							</CardLabel>

							<div>
								<DetailTicket label={dataFormula.document_number} />
								<Button
									icon='Close'
									type='button'
									color='danger'
									onClick={() => {
										setDataParameter([]);
										handleShowFormTrial(null, null);
									}}
								/>
							</div>
						</CardHeader>

						<CardBody>
							<div className='row'>
								<div className='col-sm-4 col-md-4'>
									<table className='table table-border'>
										<tbody>
											<tr>
												<td>
													<b>FG Code</b>
												</td>
												<td>
													<Input
														type='text'
														onInput={(value) => {
															setResultFgCode(value.target.value);
														}}
														readOnly
														value={resultFgCode}
													/>
												</td>
											</tr>
											<tr>
												<td>
													<b>FG Name</b>
												</td>
												<td>
													<Input
														type='text'
														onInput={(value) => {
															setResultFgName(value.target.value);
														}}
														readOnly
														value={resultFgName}
													/>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<div className='row'>
								<div className='col-12 mt-2'>
									<Accordion
										activeItemId={formTrialShow ? false : 'form-default'}
										id='accordion-form'>
										<AccordionItem id='form-default' title='Product Properties'>
											<ProductProperties
												initTab={0}
												initProductProperties={initProductProperties}
												setProductProperties={setProductProperties}
												listProductProperties={listProductProperties}
											/>
											<br />
											<br />
										</AccordionItem>
									</Accordion>
								</div>
								<div className='col-md-12 mt-2'>
									<Accordion
										id='accordion-form'
										activeItemId={formTrialShow ? false : 'form-default'}>
										<AccordionItem id='form-default' title='Formula'>
											<FormulaMaterial
												listMaterial={listMaterial}
												listVendor={listVendor}
												listUom={listUom}
												listCurrency={listCurrency}
												initialValues={materialFomula}
												isReadOnly={!isReadOnlyFormula}
												setInit={setInit}
												onChange={(e) => onChangeMainFormula(e)}
											/>
										</AccordionItem>
									</Accordion>
								</div>

							</div>
							<br />
							<br />

							<div className='row'>
								<div className='col-md-12'>
									<ParameterTrial
										initialValues={
											dataFormula.details ? dataFormula.details : []
										}
										onChange={(e) => onChangeMainParameter(e)}
										list_parameter={list_parameter}
										isReset={isReset}
										setReset={setReset}
										isReadOnly={isReadOnly}
										init={init}
										setInit={setInit}
									/>
								</div>
								<div className='col-md-12'>
									<Button
										icon='Save'
										type='submit'
										color='success'
										onClick={() => {
											submitForm(titleTrial);
										}}
										className='float-end'>
										{titleTrial}
									</Button>
								</div>
							</div>
						</CardBody>
					</Card>
				)}

				<Modal
					isOpen={loadingSubmit}
					size='sm'
					isCentered
					setIsOpen={() => { }}
					isStaticBackdrop>
					<ModalBody
						style={{ backgroundColor: '#6c5dd3', color: 'white' }}
						className='text-center'>
						<button className='btn btn-primary' type='button' disabled>
							<span
								className='spinner-grow spinner-grow-sm'
								role='status'
								aria-hidden='true'
							/>
							&nbsp;
							<span
								className='spinner-grow spinner-grow-sm'
								role='status'
								aria-hidden='true'
							/>
							&nbsp;
							<span
								className='spinner-grow spinner-grow-sm'
								role='status'
								aria-hidden='true'
							/>
							&nbsp;
							<span className='sr-only'>Loading...</span>
						</button>
					</ModalBody>
				</Modal>
			</Page>
		</PageWrapper>
	);
};

CustomDataTable.propTypes = {
	listPriority: PropTypes.oneOfType([PropTypes.instanceOf(Object)]),
	data: PropTypes.arrayOf(PropTypes.instanceOf(Object)),
	loading: PropTypes.bool,
	totalRows: PropTypes.number,
	handlePageChange: PropTypes.func,
	handlePerRowsChange: PropTypes.func,
	handleReloadData: PropTypes.func,
	handleShowFormTrial: PropTypes.func,
};
CustomDataTable.defaultProps = {
	listPriority: {},
	data: [],
	loading: false,
	totalRows: 0,
	handlePageChange: null,
	handlePerRowsChange: null,
	handleReloadData: null,
	handleShowFormTrial: null,
};

const FormCustomModalDetail = ({ initialValues }) => {
	const { darkModeStatus } = useDarkMode();
	const [isOpen, setOpen] = useState(false);

	return (
		<>
			<Button
				color='info'
				type='button'
				isLight={darkModeStatus}
				className='m-1'
				onClick={() => setOpen(true)}>
				i
			</Button>

			<Modal
				isOpen={isOpen}
				setIsOpen={setOpen}
				size='xl'
				titleId='modal-crud-evaluation'
				isStaticBackdrop>
				<ModalHeader setIsOpen={setOpen} className='p-4'>
					<ModalTitle id='modal-crud-evaluation'>
						Detail Ticket - ({initialValues?.document_number}){' '}
						{initialValues?.customer_name}
					</ModalTitle>
				</ModalHeader>
				<ModalBody>
					<DetailTicketAndFormulaComponent
						ticket_id={initialValues.ticket_id}
						formula_id={initialValues.formula_id}
					/>
				</ModalBody>
			</Modal>
		</>
	);
};

FormCustomModalDetail.propTypes = {
	initialValues: PropTypes.instanceOf(Object),
};
FormCustomModalDetail.defaultProps = {
	initialValues: null,
};

export default Trial;
