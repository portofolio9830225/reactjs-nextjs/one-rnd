import React, { useState, useEffect, useMemo, useRef } from 'react';
import PropTypes from 'prop-types';
import { Formik, Form } from 'formik';
import Swal from 'sweetalert2';
import { useTranslation } from 'react-i18next';
import moment from 'moment';
import PageWrapper from '../layout/PageWrapper/PageWrapper';
import Page from '../layout/Page/Page';
import PageLayoutHeader from '../pages/common/Headers/PageLayoutHeader';
import Card, { CardBody, CardHeader, CardLabel, CardTitle } from '../components/bootstrap/Card';
import FormGroup from '../components/bootstrap/forms/FormGroup';
import TextArea from '../components/bootstrap/forms/Textarea';
import Input from '../components/bootstrap/forms/Input';
import Button from '../components/bootstrap/Button';
import ScheduleService from '../modules/ScheduleModule';
import priorityModule from '../modules/PriorityModule';
import showNotification from '../components/extras/showNotification';
import DarkDataTable from '../components/DarkDataTable';
import useDarkMode from '../hooks/useDarkMode';
import CustomSelect from '../components/CustomSelect';
import COLORS from '../common/data/enumColors';
import { getRequester } from '../helpers/helpers';
import Modal, {
	ModalBody,
	ModalHeader,
	ModalTitle,
	ModalFooter,
} from '../components/bootstrap/Modal';
import DetailTicketAndFormulaComponent from '../components/custom/DetailTicketAndFormulaComponent';

const handleSubmitSchedule = (values, handleReloadData) => {
	ScheduleService.schedule(values)
		.then((res) => {
			showNotification('Success!', res.status, 'success');
			handleReloadData();
		})
		.catch((err) => {
			showNotification('Warning!', err, 'danger');
		});
	return values;
};

const FormSchedule = (dt) => {
	const { darkModeStatus } = useDarkMode();
	const { initialValues, listPriority, handleReloadData, row } = dt;
	const { username } = getRequester();
	const [documentNumber] = useState(row.document_number);

	const [isOpen, setIsOpen] = useState(false);
	const [isReadOnly, setIsReadOnly] = useState(false);

	let refPriority = useRef(0);

	const submitForm = (values, resetForm) => {
		const { id } = values;
		if (id) {
			const val = {};
			val._id = values.id;
			val.priority = values.priority_code;
			val.schedule_date = values.schedule_date;
			val.remark = values.remark;
			val.requester = username;

			return Swal.fire({
				title: 'Are you sure?',
				text: 'Please check your entries !',
				icon: 'info',
				showCancelButton: true,
				confirmButtonText: 'Yes',
			}).then((result) => {
				if (result.value) {
					handleSubmitSchedule(val, handleReloadData);
					const refs = [];
					refs.push(refPriority);
					resetForm(initialValues);
				}
			});
		}
		return values;
	};

	const onSubmit = (values, { setSubmitting, resetForm, setStatus, setErrors }) => {
		try {
			delete values.loading;
			submitForm(values, resetForm);
			setStatus({ success: true });
		} catch (error) {
			setStatus({ success: false });
			setSubmitting(false);
			setErrors({ submit: error.message });
		}
	};

	const validate = (values) => {
		const errors = {};
		if (!values.priority_code) {
			errors.priority_code = 'Field is required';
		}
		if (!values.schedule_date) {
			errors.schedule_date = 'Field is required';
		}
		return errors;
	};

	return (
		<>
			<Button
				className='me-2 d-none'
				type='button'
				color='primary'
				onClick={() => {
					setIsOpen(true);
					setIsReadOnly(true);
				}}>
				View
			</Button>

			<Button
				className='me-2'
				type='button'
				color='success'
				onClick={() => {
					setIsOpen(true);
					setIsReadOnly(false);
				}}>
				Schedule
			</Button>

			<Modal
				isOpen={isOpen}
				setIsOpen={setIsOpen}
				size='lg'
				titleId='modal-edit-menu-crud'
				isStaticBackdrop>
				<ModalHeader setIsOpen={setIsOpen} className='p-4'>
					<ModalTitle id='modal-edit-menu-crud'>
						{isReadOnly ? '' : `Schedule - ${documentNumber}`}
					</ModalTitle>
				</ModalHeader>
				<Formik
					enableReinitialize
					initialValues={{ ...initialValues }}
					onSubmit={onSubmit}
					validate={validate}>
					{(formikField) => {
						return (
							<Form>
								<ModalBody className='px-4'>
									<div className='col-md-12'>
										<FormGroup id='schedule_date' label='Schedule Date'>
											<Input
												type='date'
												onChange={formikField.handleChange}
												onInput={(value) => {
													formikField.setFieldValue(
														'schedule_date',
														value,
													);
												}}
												onBlur={formikField.handleBlur}
												value={formikField.values.schedule_date}
												isValid={formikField.isValid}
												isTouched={formikField.touched.schedule_date}
												invalidFeedback={formikField.errors.schedule_date}
												min={moment().format('YYYY-MM-DD')}
												autoComplete='off'
											/>
										</FormGroup>
										{typeof formikField.errors.schedule_date !== 'undefined' &&
											formikField.submitCount > 0 && (
												<div style={{ color: COLORS.DANGER.code }}>
													Field is required
												</div>
											)}
										<br />

										<FormGroup
											id='priority_code'
											label='Priority'
											className='mb-4'>
											<CustomSelect
												options={listPriority}
												defaultValue={formikField.values.priority_code}
												value={formikField.values.priority_code}
												darkTheme={darkModeStatus}
												onChange={(value) => {
													formikField.setFieldValue(
														'priority_code',
														value,
													);
												}}
												isValid={
													typeof formikField.errors.priority_code ===
													'undefined'
												}
												ref={(ref) => {
													refPriority = ref;
													return refPriority;
												}}
											/>
										</FormGroup>
										{typeof formikField.errors.priority_code !== 'undefined' &&
											formikField.submitCount > 0 && (
												<div style={{ color: COLORS.DANGER.code }}>
													Field is required
												</div>
											)}

										<FormGroup id='remark' label='Remark'>
											<TextArea
												type='text'
												onChange={formikField.handleChange}
												onBlur={formikField.handleBlur}
												value={formikField.values.remark}
												isValid={formikField.isValid}
												isTouched={formikField.touched.remark}
												invalidFeedback={formikField.errors.remark}
												autoComplete='off'
											/>
										</FormGroup>
									</div>
								</ModalBody>
								<ModalFooter className='px-4 pb-4'>
									<div className='col-md-12 '>
										<Button
											icon='Save'
											type='submit'
											color='success'
											className='float-end'
											isDisable={
												!formikField.isValid && !!formikField.submitCount
											}>
											Schedule
										</Button>
									</div>
								</ModalFooter>
							</Form>
						);
					}}
				</Formik>
			</Modal>
		</>
	);
};

const CustomButton = (dt) => {
	const { row, listPriority, handleReloadData } = dt;

	// restructure the obj
	const initialValues = {};
	initialValues.id = row._id;

	const new_initial_value = {};
	new_initial_value.ticket_id = row.ticket_id;
	new_initial_value.formula_id = row._id;
	new_initial_value.document_number = row.document_number;
	new_initial_value.customer_name = row.customer_name;

	return (
		<>
			<FormCustomModalDetail initialValues={new_initial_value} />

			<FormSchedule
				initialValues={initialValues}
				listPriority={listPriority}
				handleReloadData={handleReloadData}
				row={row}
			/>
		</>
	);
};

const CustomDataTable = ({
	listPriority,
	data,
	loading,
	totalRows,
	handlePageChange,
	handlePerRowsChange,
	handleReloadData,
}) => {
	const { darkModeStatus } = useDarkMode();

	const columns = useMemo(
		() => [
			{
				name: 'Target Date',
				selector: (row) => moment(row.target_date).format('YYYY-MMM-DD'),
				sortable: true,
			},
			{
				name: 'Register Date',
				selector: (row) => moment(row.created_at).format('YYYY-MMM-DD'),
				sortable: true,
			},
			{
				name: 'Document Number',
				selector: (row) => row.document_number,
				sortable: true,
			},
			{
				name: 'Queue Number',
				selector: (row) => row.queue_number,
				sortable: true,
			},
			{
				name: 'Customer',
				selector: (row) => row.customer_name,
				sortable: true,
			},
			{
				name: 'Schedule Date',
				selector: (row) =>
					row.schedule_date ? moment(row.schedule_date).format('YYYY-MMM-DD') : '',
				sortable: true,
			},
			{
				name: 'Priority',
				selector: (row) => row.priority,
				sortable: true,
			},
			{
				name: 'Action',
				// eslint-disable-next-line react/no-unstable-nested-components
				cell: (dt) => {
					return (
						<CustomButton
							row={dt}
							listPriority={listPriority}
							handleReloadData={handleReloadData}
						/>
					);
				},
			},
		],
		[listPriority, handleReloadData],
	);

	return (
		<DarkDataTable
			columns={columns}
			data={data}
			progressPending={loading}
			pagination
			paginationServer
			paginationTotalRows={totalRows}
			onChangeRowsPerPage={handlePerRowsChange}
			onChangePage={handlePageChange}
			theme={darkModeStatus ? 'custom_dark' : 'light'}
		/>
	);
};

const Schedule = () => {
	const { darkModeStatus } = useDarkMode();
	const [data, setData] = useState([]);
	const [listPriority, setPriority] = useState([]);

	const [loading, setLoading] = useState(false);
	const [totalRows, setTotalRows] = useState(0);
	const [perPage, setPerPage] = useState(10);
	const [page, setPage] = useState(1);
	const [reloadData, setReloadData] = useState(false);
	const [targeDateStart, setTargeDateStart] = useState(null);
	const [targeDateEnd, setTargeDateEnd] = useState(null);

	const fetchSchedule = async (nPage, nPerPage, dateStart, dateFinish) => {
		setLoading(true);
		let query = ``;
		if (dateStart && dateFinish) {
			query = `&target_date_start=${dateStart}&target_date_finish=${dateFinish}`;
		}
		return ScheduleService.read(`page=${nPage}&sizePerPage=${nPerPage}${query}`).then((res) => {
			setData(res.foundData);
			setTotalRows(res.countData);
			setLoading(false);
		});
	};

	const fetchPriority = async () => {
		return priorityModule.readSelect().then((res) => {
			setPriority(res);
		});
	};

	const handleReloadData = () => {
		setReloadData(!reloadData);
	};

	const filter = () => {
		if (moment(moment(targeDateStart)).isAfter(moment(targeDateEnd))) {
			Swal.fire({
				title: 'Warning',
				text: 'start date cant be bigger than finish date',
				icon: 'warning',
				showCancelButton: false,
				confirmButtonText: 'Yes',
			});
		} else {
			fetchSchedule(
				1,
				10,
				targeDateStart ? moment(targeDateStart).format('YYYY-MM-DD') : null,
				targeDateEnd ? moment(targeDateEnd).format('YYYY-MM-DD') : null,
			);
		}
	};

	useEffect(() => {
		fetchSchedule(page, perPage, null, null);
		fetchPriority();
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [page, perPage, reloadData]);

	useEffect(() => {
		fetchSchedule(1, 10, null, null);
		fetchPriority();
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	const { t } = useTranslation('menu');
	const [title] = useState({ title: 'Scheduling' });
	return (
		<PageWrapper title={title.title}>
			<PageLayoutHeader />
			<Page container='fluid'>
				<Card stretch>
					<CardHeader borderSize={1}>
						<CardLabel>
							<CardTitle>{t('scheduling')}</CardTitle>
						</CardLabel>
					</CardHeader>
					<CardBody>
						<div className='row pb-4'>
							<div className='col-sm-2 col-md-2'>
								<FormGroup
									id='target_date_start'
									label='Target Date Start'
									isColForLabel
									labelClassName='col-sm-4 text-capitalize'
									childWrapperClassName='col-sm-8'>
									<Input
										type='date'
										onInput={(value) => {
											setTargeDateStart(value.target.value);
										}}
										selected={targeDateStart}
									/>
								</FormGroup>
							</div>
							<div className='col-sm-2 col-md-2'>
								<FormGroup
									id='target_date_finish'
									label='Target Date Finish'
									isColForLabel
									labelClassName='col-sm-4 text-capitalize'
									childWrapperClassName='col-sm-8'>
									<Input
										type='date'
										onInput={(value) => {
											setTargeDateEnd(value.target.value);
										}}
										selected={targeDateEnd}
									/>
								</FormGroup>
							</div>
							<div className='col-sm-2 col-md-2'>
								<Button
									icon='FilterAlt'
									color='info'
									type='button'
									className='mx-1'
									onClick={filter}
									isLight={darkModeStatus}>
									Filter
								</Button>
							</div>
						</div>
						<CustomDataTable
							data={data}
							loading={loading}
							totalRows={totalRows}
							listPriority={listPriority}
							handlePageChange={setPage}
							handlePerRowsChange={setPerPage}
							handleReloadData={handleReloadData}
						/>
					</CardBody>
				</Card>
			</Page>
		</PageWrapper>
	);
};

CustomDataTable.propTypes = {
	listPriority: PropTypes.oneOfType([PropTypes.instanceOf(Object)]),
	data: PropTypes.arrayOf(PropTypes.instanceOf(Object)),
	loading: PropTypes.bool,
	totalRows: PropTypes.number,
	handlePageChange: PropTypes.func,
	handlePerRowsChange: PropTypes.func,
	handleReloadData: PropTypes.func,
};
CustomDataTable.defaultProps = {
	listPriority: {},
	data: [],
	loading: false,
	totalRows: 0,
	handlePageChange: null,
	handlePerRowsChange: null,
	handleReloadData: null,
};

const FormCustomModalDetail = ({ initialValues }) => {
	const { darkModeStatus } = useDarkMode();
	const [isOpen, setOpen] = useState(false);

	return (
		<>
			<Button
				color='info'
				type='button'
				isLight={darkModeStatus}
				className='m-1'
				onClick={() => setOpen(true)}>
				i
			</Button>

			<Modal
				isOpen={isOpen}
				setIsOpen={setOpen}
				size='xl'
				titleId='modal-crud-evaluation'
				isStaticBackdrop>
				<ModalHeader setIsOpen={setOpen} className='p-4'>
					<ModalTitle id='modal-crud-evaluation'>
						Detail Ticket - ({initialValues?.document_number}){' '}
						{initialValues?.customer_name}
					</ModalTitle>
				</ModalHeader>
				<ModalBody>
					<DetailTicketAndFormulaComponent
						ticket_id={initialValues.ticket_id}
						formula_id={initialValues.formula_id}
					/>
				</ModalBody>
			</Modal>
		</>
	);
};

FormCustomModalDetail.propTypes = {
	initialValues: PropTypes.instanceOf(Object),
};
FormCustomModalDetail.defaultProps = {
	initialValues: null,
};

export default Schedule;
