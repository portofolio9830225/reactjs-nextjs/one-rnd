import React, { useEffect, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import moment from 'moment';
import Swal from 'sweetalert2';
import PropTypes from 'prop-types';
import Card, { CardBody, CardHeader, CardLabel, CardTitle } from '../components/bootstrap/Card';
import DarkDataTable from '../components/DarkDataTable';
import useDarkMode from '../hooks/useDarkMode';
import Page from '../layout/Page/Page';
import PageWrapper from '../layout/PageWrapper/PageWrapper';
import FormulaModule from '../modules/FormulaModule';
import PageLayoutHeader from '../pages/common/Headers/PageLayoutHeader';
import Button from '../components/bootstrap/Button';
import Modal, { ModalBody, ModalHeader, ModalTitle } from '../components/bootstrap/Modal';
import { getRequester } from '../helpers/helpers';
import Notifications from '../components/Notifications';
import FormulaEvaluationComponent from '../components/custom/FormulaEvaluationComponent';
import showNotification from '../components/extras/showNotification';
import FormCustomModalDetail from '../components/custom/FormCustomModalDetail';

const STATUS = [
	// Check QC
	'Siap Validasi Hasil Trial Plant',
];

const CATEGORY = ['ndc', 'The appearance & stability', 'Taste'];

const handleEvaluation = (values) => {
	const newResponse = new Promise((resolve, reject) => {
		try {
			FormulaModule.sensoryPlantFormula(values)
				.then(() => {
					Notifications.showNotif({
						header: 'Information!',
						message: 'Data has been saved successfully',
						type: Notifications.TYPE.SUCCESS,
					});
					resolve({ error: false, message: 'successfully' });
				})
				.catch((err) => {
					Notifications.showNotif({
						header: 'Warning!!',
						message: err,
						type: Notifications.TYPE.ERROR,
					});
					reject(new Error(err));
				});
		} catch (e) {
			reject(new Error(e.message));
		}
	});
	return newResponse;
};

const FormCustomModalEvaluation = ({ initialValues, handleCustomEvaluation, options }) => {
	const { darkModeStatus } = useDarkMode();
	const { username } = getRequester();

	const [isOpen, setOpen] = useState(false);

	const [errors, setErrors] = useState({ error: false, message: '' });
	const [data, setData] = useState([]);
	const [dataFiles, setDataFiles] = useState([]);

	const options_category = CATEGORY.map((item) => {
		return { value: item.replace(/[^a-zA-Z0-9]/g, '_').toLowerCase(), label: item };
	});

	const onChangeCategory = (values, newRemark) => {
		setDataFiles(values);
		const formData = new FormData();
		formData.append('remark', newRemark);
		formData.append('_id', initialValues._id);
		formData.append('requester', username);
		const keys = Object.keys(values);

		keys.forEach((element) => {
			formData.append('files', values[element]);
		});

		const new_errors = { error: false, message: '' };
		setErrors(new_errors);
		setData(formData);
	};

	const onCustomSubmit = () => {
		try {
			if (dataFiles.length === 0) {
				showNotification('Information', 'Please upload Document', 'danger');
				return;
			}
			if (dataFiles.length !== 0)  {
				Swal.fire({
					title: 'Are you sure?',
					text: 'Please check your entries !',
					icon: 'question',
					showCancelButton: true,
					confirmButtonText: 'Yes',
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
				}).then((result) => {
					if (result.isConfirmed) {
						if (errors.error) {
							showNotification('Information', errors.message, 'danger');
							return;
						}
						handleCustomEvaluation(data, setOpen, options);
					} else if (result.dismiss === Swal.DismissReason.cancel) {
					}
				});
			}

		} catch (error) {
			Swal.fire('Information ', 'Please check your entries again!', 'error');
		}
	};

	return (
		<>
			<Button
				icon='FactCheck'
				color='success'
				type='button'
				isLight={darkModeStatus}
				className='mx-1'
				onClick={() => setOpen(true)}>
				{' '}
				Evaluation
			</Button>

			<Modal
				isOpen={isOpen}
				setIsOpen={setOpen}
				size='xl'
				titleId='modal-crud-evaluation'
				isStaticBackdrop>
				<ModalHeader setIsOpen={setOpen} className='p-4'>
					<ModalTitle id='modal-crud-evaluation'>
						Evaluation Trial Plant - ({initialValues?.document_number}) -{' '}
						{initialValues?.customer_name}
					</ModalTitle>
				</ModalHeader>
				<ModalBody>
					<FormulaEvaluationComponent
						optionsCategory={options_category}
						onChange={onChangeCategory}
					/>
					<div className='row'>
						<div className='col-md-12'>
							<Button
								icon='Save'
								color='success'
								type='button'
								isLight={darkModeStatus}
								className='float-end m-1'
								onClick={onCustomSubmit}>
								Submit
							</Button>
						</div>
					</div>
				</ModalBody>
			</Modal>
		</>
	);
};

FormCustomModalEvaluation.propTypes = {
	initialValues: PropTypes.instanceOf(Object),
	handleCustomEvaluation: PropTypes.func,
	options: PropTypes.instanceOf(Object),
};
FormCustomModalEvaluation.defaultProps = {
	initialValues: null,
	handleCustomEvaluation: () => { },
	options: {
		curPage: 1,
		perPage: 10,
		showAll: false,
	},
};

const CustomButtonTable = ({ initialValues, options, handleCustomEvaluation, row }) => {
	const newInitialValues = { ...initialValues };
	newInitialValues.formula_id = row._id;
	return (
		<>
			<FormCustomModalDetail initialValues={newInitialValues} />
			<FormCustomModalEvaluation
				initialValues={initialValues}
				options={options}
				handleCustomEvaluation={handleCustomEvaluation}
			/>
		</>
	);
};

CustomButtonTable.propTypes = {
	initialValues: PropTypes.instanceOf(Object),
	options: PropTypes.instanceOf(Object),
	handleCustomEvaluation: PropTypes.func,
	row: PropTypes.instanceOf(Object),
};
CustomButtonTable.defaultProps = {
	initialValues: null,
	options: {
		curPage: 1,
		perPage: 10,
		showAll: false,
	},
	handleCustomEvaluation: () => { },
	row: null,
};

const TableCustom = ({
	data,
	totalRows,
	perPage,
	loading,
	fetchData,
	curPage,
	showAll,
	handleCustomEvaluation,
}) => {
	const { darkModeStatus } = useDarkMode();

	const handlePageChange = (page) => {
		fetchData(page, perPage, showAll);
	};

	const handlePerRowsChange = async (newPerPage, page) => {
		const all = totalRows === newPerPage;
		return fetchData(page, newPerPage, all);
	};

	const paginationComponentOptions = {
		selectAllRowsItem: true,
		selectAllRowsItemText: 'ALL',
	};

	const columns = useMemo(
		() => [
			{
				name: 'REGISTER DATE',
				selector: (row) => moment(row.created_at).format('YYYY-MM-DD'),
				sortable: true,
				width: '140px',
			},
			{
				name: 'TICKET ID',
				selector: (row) => row.document_number,
				sortable: true,
				width: '220px',
			},
			{
				name: 'TARGET DATE',
				selector: (row) => moment(row.target_date).format('YYYY-MM-DD'),
				sortable: true,
				width: '140px',
			},
			{
				name: 'CUSTOMER',
				selector: (row) => row.customer_name,
				sortable: true,
				width: '420px',
			},
			{
				name: 'SCHEDULE DATE',
				selector: (row) => moment(row.schedule_date).format('YYYY-MM-DD'),
				sortable: true,
				width: '180px',
			},
			{
				name: 'JN NUMBER',
				selector: (row) => row.job_number,
				sortable: true,
				width: '180px',
			},
			{
				name: 'ACTION',
				width: '300px',
				// eslint-disable-next-line react/no-unstable-nested-components
				cell: (row) => {
					const { _id, customer_name, document_number, status_code, ticket_id } = row;
					const initialValues = {
						_id,
						customer_name,
						document_number,
						status_code,
						ticket_id,
					};

					const options = { curPage, perPage, showAll };

					return (
						<CustomButtonTable
							initialValues={initialValues}
							options={options}
							handleCustomEvaluation={handleCustomEvaluation}
							row={row}
						/>
					);
				},
			},
		],
		// eslint-disable-next-line react-hooks/exhaustive-deps
		[curPage, perPage, showAll],
	);

	return (
		<DarkDataTable
			columns={columns}
			data={data}
			pagination
			progressPending={loading}
			paginationServer
			paginationTotalRows={totalRows}
			onChangeRowsPerPage={handlePerRowsChange}
			onChangePage={handlePageChange}
			paginationComponentOptions={paginationComponentOptions}
			theme={darkModeStatus ? 'custom_dark' : 'light'}
		/>
	);
};
TableCustom.propTypes = {
	data: PropTypes.instanceOf(Array),
	loading: PropTypes.bool,
	totalRows: PropTypes.number,
	curPage: PropTypes.number,
	perPage: PropTypes.number,
	showAll: PropTypes.bool,
	fetchData: PropTypes.func,
	handleCustomEvaluation: PropTypes.func,
};
TableCustom.defaultProps = {
	data: [],
	loading: false,
	totalRows: 0,
	curPage: 1,
	perPage: 10,
	showAll: false,
	fetchData: () => { },
	handleCustomEvaluation: () => { },
};

const VerificationPlant = () => {
	const { t } = useTranslation('crud');

	const [data, setData] = useState([]);
	const [totalRows, setTotalRows] = useState(0);
	const [perPage, setPerPage] = useState(10);
	const [curPage, setCurPage] = useState(1);

	const [show_all, setShowAll] = useState(false);
	const [loading, setLoading] = useState(false);

	const evaluationSubmit = (values, setModalEvaluation, options) => {
		handleEvaluation(values)
			.then(() => {
				fetchData(options.curPage, options.perPage, options.show_all);
			})
			.catch(() => { })
			.finally(() => {
				setModalEvaluation(false);
			});
	};

	useEffect(() => {
		fetchData(curPage, perPage, show_all);
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	const fetchData = async (newPage, newPerPage, all) => {
		setLoading(true);
		setShowAll(all);

		const query = {
			page: newPage,
			sizePerPage: newPerPage,
			showAll: all,
			status: STATUS,
		};

		return FormulaModule.readFormula(query)
			.then((response) => {
				setData(response.foundData);
				setTotalRows(response.countData);
				setCurPage(response.currentPage);
				setPerPage(newPerPage);
			})
			.catch(() => { })
			.finally(() => {
				setLoading(false);
			});
	};

	return (
		<PageWrapper title={t('Verification Trial Plant')}>
			<PageLayoutHeader />
			<Page container='fluid'>
				<Card stretch>
					<CardHeader borderSize={1}>
						<CardLabel icon='DomainVerification' iconColor='info'>
							<CardTitle>{t('Verification Trial Plant')}</CardTitle>
						</CardLabel>
					</CardHeader>
					<CardBody>
						<TableCustom
							data={data}
							totalRows={totalRows}
							perPage={perPage}
							curPage={curPage}
							showAll={show_all}
							loading={loading}
							fetchData={fetchData}
							handleCustomEvaluation={evaluationSubmit}
						/>
					</CardBody>
				</Card>
			</Page>
		</PageWrapper>
	);
};

VerificationPlant.propTypes = {};
VerificationPlant.defaultProps = {};

export default VerificationPlant;
