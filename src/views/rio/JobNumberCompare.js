import PropTypes from 'prop-types';
import React, { useState, useEffect } from 'react';

import moment from 'moment';
import { useFormik } from 'formik';

import PageWrapper from '../../layout/PageWrapper/PageWrapper';
import PageLayoutHeader from '../../pages/common/Headers/PageLayoutHeader';

import Page from '../../layout/Page/Page';
import Card, { CardBody, CardHeader, CardLabel, CardTitle } from '../../components/bootstrap/Card';
import useDarkMode from '../../hooks/useDarkMode';
import CustomSelect from '../../components/CustomSelect';
import Input from '../../components/bootstrap/forms/Input';
import FormGroup from '../../components/bootstrap/forms/FormGroup';
import Button from '../../components/bootstrap/Button';

import formulaModule from '../../modules/azhar/formulaModule';
import DarkDataTable from '../../components/DarkDataTable';
import showNotification from '../../components/extras/showNotification';

import Modal, { ModalBody, ModalHeader, ModalTitle } from '../../components/bootstrap/Modal';
import { isEmptyObject } from '../../helpers/helpers';

const ItemProduct = ({ name, row }) => {
	let angka;

	if (!isEmptyObject(row.product_properties)) {
		const val = row.product_properties.filter((e) => e.properties == name);

		if (!isEmptyObject(val)) {
			angka = val[0].value;
		} else {
			angka = 0;
		}
	} else {
		angka = 0;
	}

	return (
		<tr className=''>
			<td width='120px'>{name}</td>
			<td width='10px'>:</td>
			<td>{angka}%</td>
		</tr>
	);
};

ItemProduct.propTypes = {
	row: PropTypes.instanceOf(Object),
	name: PropTypes.string,
};

ItemProduct.defaultProps = {
	name: null,
	row: [],
};

const ButtonDetail = ({ row, handleAddCompare }) => {
	const { darkModeStatus } = useDarkMode();
	const [isOpen, setOpen] = useState(false);

	return (
		<>
			<Button
				color='info'
				type='button'
				isLight={darkModeStatus}
				className='m-1'
				onClick={() => setOpen(true)}>
				Detail
			</Button>
			<Button
				color='warning'
				type='button'
				isLight={darkModeStatus}
				className='m-1'
				onClick={() =>
					handleAddCompare(row.sensory, `${row.job_number} - ${row.document_number}`)
				}>
				Add to Compare
			</Button>

			<Modal
				isOpen={isOpen}
				setIsOpen={setOpen}
				size='xl'
				titleId='modal-crud-evaluation'
				isStaticBackdrop>
				<ModalHeader setIsOpen={setOpen}>
					<ModalTitle id='modal-crud-evaluation'>{row.job_number}</ModalTitle>
				</ModalHeader>
				<ModalBody>
					<table className='table table-borderless'>
						<ItemProduct name='TS' row={row} />
						<ItemProduct name='Fat' row={row} />
						<ItemProduct name='Protein' row={row} />
						<ItemProduct name='GP' row={row} />
					</table>

					<div className='row mt-3'>
						<div className='col-12'>
							<table className='table table-modern'>
								<thead>
									<tr>
										<th>Material Code</th>
										<th>Material Name</th>
										<th>Qty</th>
										<th>UoM</th>
										<th>Supplier</th>
										<th>Currency</th>
										<th>Price</th>
										<th>Notes</th>
									</tr>
								</thead>
								<tbody>
									{row?.formula?.map((iae, index_) => (
										<tr key={'tr-'.concat(index_)}>
											<td key={'td2-'.concat(index_)}>{iae.material_code}</td>
											<td key={'td-7-'.concat(index_)}>
												{iae.material_name}
											</td>
											<td key={'td3-'.concat(index_)}>{iae.qty}</td>
											<td key={'td4-'.concat(index_)}>KG</td>
											<td key={'td5-'.concat(index_)}>{iae.supplier}</td>
											<td key={'td6-'.concat(index_)}>{iae.currency}</td>
											<td key={'td7-'.concat(index_)}>{iae.price}</td>
											<td key={'td8-'.concat(index_)}>{iae.note}</td>
										</tr>
									))}
								</tbody>
							</table>
						</div>
						<div className='col-12 mt-3'>
							<hr
								style={{
									background: 'lime',
									color: 'lime',
									borderColor: 'lime',
									height: '1px',
								}}
							/>
							<CardLabel>
								<CardTitle>Parameter Trial</CardTitle>
							</CardLabel>
						</div>
						{row.details.map((dtls) => (
							<div className='col-12 mt-2'>
								{dtls.parameter_code === 'mixing_sequences' && (
									<table className='table table-modern'>
										<thead>
											<tr>
												<th>{dtls.parameter_name}</th>
												<th>Plan RPM</th>
												<th>Plan Minutes</th>
												<th>Actual RPM</th>
												<th>Actual Minutes</th>
											</tr>
										</thead>
										<tbody>
											{dtls?.detail?.map(
												(detailformula, index_dtlformula) => (
													<tr
														key={'tr-mixing_sequences-'.concat(
															index_dtlformula,
														)}>
														<td
															key={'td2-dtls-'.concat(
																index_dtlformula,
															)}>
															{detailformula.name}
														</td>
														<td
															key={'td-plan_rpm-7-'.concat(
																index_dtlformula,
															)}>
															{detailformula.plan_rpm}
														</td>
														<td
															key={'td3-plan_minutes-'.concat(
																index_dtlformula,
															)}>
															{detailformula.plan_minutes}
														</td>
														<td
															key={'td-actual_rpm-7-'.concat(
																index_dtlformula,
															)}>
															{detailformula.actual_rpm}
														</td>
														<td
															key={'td3-actual_minutes-'.concat(
																index_dtlformula,
															)}>
															{detailformula.actual_minutes}
														</td>
													</tr>
												),
											)}
										</tbody>
									</table>
								)}
								{dtls.parameter_code !== 'mixing_sequences' && (
									<table className='table table-modern'>
										<thead>
											<tr>
												<th>{dtls.parameter_name}</th>
												<th>Plan</th>
												<th>Actual</th>
											</tr>
										</thead>
										<tbody>
											{dtls?.detail?.map(
												(detailformula, index_dtlformula) => (
													<tr key={'tr-dtls-'.concat(index_dtlformula)}>
														<td
															key={'td2-dtls-'.concat(
																index_dtlformula,
															)}>
															{detailformula.name}
														</td>
														<td
															key={'td-dtls-material-7-'.concat(
																index_dtlformula,
															)}>
															{detailformula.plan}
														</td>
														<td
															key={'td3-dtls-'.concat(
																index_dtlformula,
															)}>
															{detailformula.actual}
														</td>
													</tr>
												),
											)}
										</tbody>
									</table>
								)}
							</div>
						))}
					</div>
				</ModalBody>
			</Modal>
		</>
	);
};

ButtonDetail.propTypes = {
	row: PropTypes.instanceOf(Object),
	handleAddCompare: PropTypes.func,
};

ButtonDetail.defaultProps = {
	row: null,
	handleAddCompare: null,
};

const CustomDataTable = ({
	data,
	totalRows,
	totalperPage,
	handlePageChange,
	loading,
	handlePerRowsChange,
	handleAddCompare,
}) => {
	const { darkModeStatus } = useDarkMode();

	const columns = [
		{
			name: 'Job Number',
			selector: (row) => row.job_number,
			sortable: true,
		},
		{
			name: 'Customer',
			selector: (row) => row.customer_name,
			sortable: true,
		},
		{
			name: 'PIC Name',
			width: '200px',
			selector: (row) => row.pic_name,
			sortable: true,
		},
		{
			name: 'Ticket ID',
			width: '200px',
			selector: (row) => row.document_number,
			sortable: true,
		},
		{
			name: 'Created At',
			selector: (row) => moment(row.created_at).format('YYYY-MMM-DD'),
			sortable: true,
		},
		{
			name: 'Action',
			width: '400px',
			// eslint-disable-next-line react/no-unstable-nested-components
			cell: (dt) => {
				return <ButtonDetail handleAddCompare={handleAddCompare} row={dt} />;
			},
		},
	];

	return (
		<DarkDataTable
			columns={columns}
			data={data}
			pagination
			paginationServer
			paginationTotalRows={totalRows}
			paginationPerPage={totalperPage}
			theme={darkModeStatus ? 'custom_dark' : 'light'}
			onChangePage={handlePageChange}
			progressPending={loading}
			onChangeRowsPerPage={handlePerRowsChange}
		/>
	);
};
const CustomCompareDataTable = ({ data, handleRemoveCompare }) => {
	const { darkModeStatus } = useDarkMode();

	const columns = [
		{
			name: 'Compare JN',
			width: '350px',
			selector: (row) => row.job_number,
			sortable: true,
		},
		{
			// eslint-disable-next-line react/no-unstable-nested-components
			cell: (dt) => {
				return (
					<Button
						color='danger'
						type='button'
						isLight={darkModeStatus}
						onClick={() => handleRemoveCompare(dt)}
						className='m-1'>
						X
					</Button>
				);
			},
		},
	];

	return (
		<DarkDataTable
			columns={columns}
			data={data}
			theme={darkModeStatus ? 'custom_dark' : 'light'}
		/>
	);
};

CustomDataTable.propTypes = {
	data: PropTypes.arrayOf(PropTypes.instanceOf(Object)),
	totalRows: PropTypes.number,
	totalperPage: PropTypes.number,
	handlePageChange: PropTypes.func,
	handlePerRowsChange: PropTypes.func,
	loading: PropTypes.bool,
	handleAddCompare: PropTypes.func,
};

CustomDataTable.defaultProps = {
	data: [],
	totalRows: 0,
	totalperPage: 0,
	handlePageChange: null,
	handlePerRowsChange: null,
	loading: false,
	handleAddCompare: null,
};

CustomCompareDataTable.propTypes = {
	data: PropTypes.arrayOf(PropTypes.instanceOf(Object)),
	handleRemoveCompare: PropTypes.func,
};

CustomCompareDataTable.defaultProps = {
	data: [],
	handleRemoveCompare: null,
};

const JobNumberCompare = () => {
	const [loading, setLoading] = useState(false);
	const [title] = useState('Compare JN Summary');
	const [data, setData] = useState([]);
	const [pages, setPages] = useState(1);
	const [perPages, setPerPages] = useState(10);
	const [total, setTotal] = useState(0);
	const [compareList, setCompareList] = useState([]);
	const [isOpen, setOpen] = useState(false);

	const handleAddCompare = (sensory, values) => {
		if (compareList.length < 5) {
			const isAlready = compareList.find((list) => list.job_number === values);
			if (isAlready !== undefined) {
				showNotification(
					'Compare JN',
					'Job number has already added in compare list',
					'danger',
				);
			} else {
				setCompareList([...compareList, { job_number: values, sensory }]);
			}
		} else {
			showNotification('Compare JN', 'Compare list cannot be more than 5 data', 'danger');
		}
	};

	const handleRemoveCompare = (values) => {
		const valueIndex = compareList.findIndex((list) => list.job_number === values.job_number);
		const compareListTemp = [...compareList];
		compareListTemp.splice(valueIndex, 1);
		setCompareList(compareListTemp);
	};

	const fetchData = async (page, perPage, query) => {
		setLoading(true);
		return formulaModule.readTable(page, perPage, query).then((results) => {
			setData(results.data);
			setTotal(results.countData);
			setLoading(false);
		});
	};

	useEffect(() => {}, [pages, perPages]);

	return (
		<PageWrapper title={title}>
			<PageLayoutHeader />
			<Page container='fluid'>
				<Card stretch>
					<CardHeader borderSize={1}>
						<CardLabel>
							<CardTitle>{title}</CardTitle>
						</CardLabel>
					</CardHeader>
					<CardBody>
						<FilterTableCustom fetch={fetchData} setData={setData} />
						<div className='mt-5 row'>
							<div
								className={compareList.length < 1 ? 'col-12' : 'col-8'}
								style={{ transition: '0.5s' }}>
								<CustomDataTable
									data={data}
									totalRows={total}
									totalperPage={perPages}
									loading={loading}
									handlePageChange={setPages}
									handlePerRowsChange={setPerPages}
									handleAddCompare={handleAddCompare}
								/>
							</div>
							<div
								className={compareList.length < 1 ? 'invisible' : 'col-4'}
								style={
									compareList.length > 0
										? {
												borderLeft: '1px solid lightgrey',
												transitionDelay: '0.5s',
										  }
										: {}
								}>
								<CustomCompareDataTable
									data={compareList}
									handleRemoveCompare={handleRemoveCompare}
								/>
								<div className='d-flex justify-content-end mt-3'>
									<Button
										onClick={() => setOpen(true)}
										className='float-right'
										color='primary'
										type='button'>
										Compare
									</Button>
								</div>
							</div>
						</div>
					</CardBody>
				</Card>
				<Modal
					isOpen={isOpen}
					setIsOpen={setOpen}
					size='xl'
					titleId='modal-crud-evaluation'
					isStaticBackdrop>
					<ModalHeader setIsOpen={setOpen}>
						<ModalTitle id='modal-crud-evaluation'>Compare JN</ModalTitle>
					</ModalHeader>
					<ModalBody>
						<div className='row mt-3'>
							<div className='col-12' style={{ overflowX: 'auto' }}>
								<table className='table table-bordered'>
									<thead>
										<tr>
											<th>Parameter</th>
											<th> </th>
											<th>Sample</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<th> </th>
											<th> </th>
											{compareList.map((item) => (
												<th>{item.job_number}</th>
											))}
										</tr>
										<tr>
											<th>NDC</th>
										</tr>
										<tr>
											<td>1</td>
											<td>Color of Powder</td>
											{compareList.map((item) => (
												<td className='text-center'>
													{item.sensory.map((sensory) =>
														sensory.parameter_code === 'ndc'
															? sensory.detail.map((detail) =>
																	detail.name ===
																	'color of powder'
																		? detail.value
																		: '',
															  )
															: '',
													)}
												</td>
											))}
										</tr>
										<tr>
											<td>2</td>
											<td>Level of Swetness</td>
											{compareList.map((item) => (
												<td className='text-center'>
													{item.sensory.map((sensory) =>
														sensory.parameter_code === 'ndc'
															? sensory.detail.map((detail) =>
																	detail.name ===
																	'level of sweetness'
																		? detail.value
																		: '',
															  )
															: '',
													)}
												</td>
											))}
										</tr>
										<tr>
											<td>3</td>
											<td>Flavored Cream</td>
											{compareList.map((item) => (
												<td className='text-center'>
													{item.sensory.map((sensory) =>
														sensory.parameter_code === 'ndc'
															? sensory.detail.map((detail) =>
																	detail.name === 'flavored cream'
																		? detail.value
																		: '',
															  )
															: '',
													)}
												</td>
											))}
										</tr>
										<tr>
											<td>4</td>
											<td>Flavored Milk</td>
											{compareList.map((item) => (
												<td className='text-center'>
													{item.sensory.map((sensory) =>
														sensory.parameter_code === 'ndc'
															? sensory.detail.map((detail) =>
																	detail.name === 'flavored milk'
																		? detail.value
																		: '',
															  )
															: '',
													)}
												</td>
											))}
										</tr>
										<tr>
											<td>5</td>
											<td>Body</td>
											{compareList.map((item) => (
												<td className='text-center'>
													{item.sensory.map((sensory) =>
														sensory.parameter_code === 'ndc'
															? sensory.detail.map((detail) =>
																	detail.name === 'Body'
																		? detail.value
																		: '',
															  )
															: '',
													)}
												</td>
											))}
										</tr>
										<tr>
											<td>6</td>
											<td>Saltiness</td>
											{compareList.map((item) => (
												<td className='text-center'>
													{item.sensory.map((sensory) =>
														sensory.parameter_code === 'ndc'
															? sensory.detail.map((detail) =>
																	detail.name === 'Saltiness'
																		? detail.value
																		: '',
															  )
															: '',
													)}
												</td>
											))}
										</tr>
										<tr>
											<td>7</td>
											<td>Aroma</td>
											{compareList.map((item) => (
												<td className='text-center'>
													{item.sensory.map((sensory) =>
														sensory.parameter_code === 'ndc'
															? sensory.detail.map((detail) =>
																	detail.name === 'Aroma'
																		? detail.value
																		: '',
															  )
															: '',
													)}
												</td>
											))}
										</tr>
										<tr>
											<th>The Appereance & Stability</th>
										</tr>
										<tr>
											<td>1</td>
											<td>Level of Whiteness</td>
											{compareList.map((item) => (
												<td className='text-center'>
													{item.sensory.map((sensory) =>
														sensory.parameter_code ===
														'the_appearance___stability'
															? sensory.detail.map((detail) =>
																	detail.name ===
																	'level of whiteness'
																		? detail.value
																		: '',
															  )
															: '',
													)}
												</td>
											))}
										</tr>
										<tr>
											<td>2</td>
											<td>Layer White</td>
											{compareList.map((item) => (
												<td className='text-center'>
													{item.sensory.map((sensory) =>
														sensory.parameter_code ===
														'the_appearance___stability'
															? sensory.detail.map((detail) =>
																	detail.name === 'layer white'
																		? detail.value
																		: '',
															  )
															: '',
													)}
												</td>
											))}
										</tr>
										<tr>
											<td>3</td>
											<td>White Spots</td>
											{compareList.map((item) => (
												<td className='text-center'>
													{item.sensory.map((sensory) =>
														sensory.parameter_code ===
														'the_appearance___stability'
															? sensory.detail.map((detail) =>
																	detail.name === 'white spots'
																		? detail.value
																		: '',
															  )
															: '',
													)}
												</td>
											))}
										</tr>
										<tr>
											<td>4</td>
											<td>Fat Globule</td>
											{compareList.map((item) => (
												<td className='text-center'>
													{item.sensory.map((sensory) =>
														sensory.parameter_code ===
														'the_appearance___stability'
															? sensory.detail.map((detail) =>
																	detail.name === 'fat globule'
																		? detail.value
																		: '',
															  )
															: '',
													)}
												</td>
											))}
										</tr>
										<tr>
											<td>5</td>
											<td>Sedimentation</td>
											{compareList.map((item) => (
												<td className='text-center'>
													{item.sensory.map((sensory) =>
														sensory.parameter_code ===
														'the_appearance___stability'
															? sensory.detail.map((detail) =>
																	detail.name === 'Sedimentation'
																		? detail.value
																		: '',
															  )
															: '',
													)}
												</td>
											))}
										</tr>
										<tr>
											<td>6</td>
											<td>Separation</td>
											{compareList.map((item) => (
												<td className='text-center'>
													{item.sensory.map((sensory) =>
														sensory.parameter_code ===
														'the_appearance___stability'
															? sensory.detail.map((detail) =>
																	detail.name === 'separation'
																		? detail.value
																		: '',
															  )
															: '',
													)}
												</td>
											))}
										</tr>
										<tr>
											<th>Taste</th>
										</tr>
										<tr>
											<td>1</td>
											<td>Flavored Cream</td>
											{compareList.map((item) => (
												<td className='text-center'>
													{item.sensory.map((sensory) =>
														sensory.parameter_code === 'taste'
															? sensory.detail.map((detail) =>
																	detail.name === 'flavored cream'
																		? detail.value
																		: '',
															  )
															: '',
													)}
												</td>
											))}
										</tr>
										<tr>
											<td>2</td>
											<td>Flavored Milk</td>
											{compareList.map((item) => (
												<td className='text-center'>
													{item.sensory.map((sensory) =>
														sensory.parameter_code === 'taste'
															? sensory.detail.map((detail) =>
																	detail.name === 'flavored milk'
																		? detail.value
																		: '',
															  )
															: '',
													)}
												</td>
											))}
										</tr>
										<tr>
											<td>3</td>
											<td>Cover Coffee</td>
											{compareList.map((item) => (
												<td className='text-center'>
													{item.sensory.map((sensory) =>
														sensory.parameter_code === 'taste'
															? sensory.detail.map((detail) =>
																	detail.name === 'cover coffee'
																		? detail.value
																		: '',
															  )
															: '',
													)}
												</td>
											))}
										</tr>
										<tr>
											<td>4</td>
											<td>Coffee Aroma</td>
											{compareList.map((item) => (
												<td className='text-center'>
													{item.sensory.map((sensory) =>
														sensory.parameter_code === 'taste'
															? sensory.detail.map((detail) =>
																	detail.name === 'Coffee Aroma'
																		? detail.value
																		: '',
															  )
															: '',
													)}
												</td>
											))}
										</tr>
										<tr>
											<td>5</td>
											<td>Body</td>
											{compareList.map((item) => (
												<td className='text-center'>
													{item.sensory.map((sensory) =>
														sensory.parameter_code === 'taste'
															? sensory.detail.map((detail) =>
																	detail.name === 'body'
																		? detail.value
																		: '',
															  )
															: '',
													)}
												</td>
											))}
										</tr>
										<tr>
											<td>6</td>
											<td>Palmitic</td>
											{compareList.map((item) => (
												<td className='text-center'>
													{item.sensory.map((sensory) =>
														sensory.parameter_code === 'taste'
															? sensory.detail.map((detail) =>
																	detail.name === 'Palmitic'
																		? detail.value
																		: '',
															  )
															: '',
													)}
												</td>
											))}
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</ModalBody>
				</Modal>
			</Page>
		</PageWrapper>
	);
};

const FilterTableCustom = ({ fetch, setData }) => {
	const formik = useFormik({
		initialValues: {
			category: '',
			keyword: '',
			start: '',
			end: '',
		},
		validate: (values) => {
			const errors = {};

			if (moment(moment(values.start)).isAfter(moment(values.end))) {
				errors.start = 'the date cannot be later than the end date';
				errors.end = 'the date cannot be less than the start date';
			}

			if (isEmptyObject(values.keyword)) {
				errors.keyword = 'required';
			}

			return errors;
		},
		onSubmit: (values) => {
			values.category = newtype.value;

			let query = `&category=${values.category}&keyword=${values.keyword}&sensory=true`;

			if (!isEmptyObject(values.start) && !isEmptyObject(values.start)) {
				query += `&start=${values.start}&end=${values.end}`;
			}

			fetch(1, 10, query);
		},
	});

	const { darkModeStatus } = useDarkMode();
	const [newtype, setType] = useState({
		value: 'Job Number',
		text: 'Job Number',
		label: 'Job Number',
	});
	const listType = [
		{
			value: 'Job Number',
			text: 'Job Number',
			label: 'Job Number',
		},
		{
			value: 'Customer',
			text: 'Customer',
			label: 'Customer',
		},
		{
			value: 'PIC',
			text: 'PIC',
			label: 'PIC',
		},
		{
			value: 'Ticket ID',
			text: 'Ticket ID',
			label: 'Ticket ID',
		},
	];

	const customHandleReset = (e) => {
		formik.handleReset(e);
		formik.values.category = '';

		setType({
			value: 'Job Number',
			text: 'Job Number',
			label: 'Job Number',
		});

		setData([]);
	};

	return (
		<form noValidate onSubmit={formik.handleSubmit} className='row'>
			<div className='col-sm-2 m-1'>
				<FormGroup id='category' label='Category'>
					<CustomSelect
						placeholder='Category'
						onChange={(e) => setType(e)}
						value={newtype}
						options={listType}
						darkTheme={darkModeStatus}
					/>
				</FormGroup>
			</div>
			<div className='col-sm-2 m-1'>
				<FormGroup id='keyword' label='Keyword'>
					<Input
						onChange={formik.handleChange}
						onBlur={formik.handleBlur}
						value={formik.values.keyword}
						isTouched={formik.touched.keyword}
						invalidFeedback={formik.errors.keyword}
					/>
				</FormGroup>
			</div>
			<div className='col-sm-2 m-1'>
				<FormGroup id='start' label='Start'>
					<Input
						type='date'
						onChange={formik.handleChange}
						onBlur={formik.handleBlur}
						value={formik.values.start}
						isTouched={formik.touched.start}
						isValid={formik.isValid}
						invalidFeedback={formik.errors.start}
					/>
				</FormGroup>
			</div>
			<div className='col-sm-2 m-1'>
				<FormGroup id='end' label='End'>
					<Input
						type='date'
						onChange={formik.handleChange}
						onBlur={formik.handleBlur}
						value={formik.values.end}
						isTouched={formik.touched.end}
						isValid={formik.isValid}
						invalidFeedback={formik.errors.end}
					/>
				</FormGroup>
			</div>
			<div className='col-sm-3 m-1 d-flex align-items-end'>
				<Button icon='FilterAlt' color='success' type='submit' className='m-1'>
					Filter
				</Button>
				<Button
					icon='Clear'
					color='danger'
					type='reset'
					className='m-1'
					onClick={customHandleReset}>
					Clear
				</Button>
			</div>
		</form>
	);
};

FilterTableCustom.propTypes = {
	fetch: PropTypes.func,
	setData: PropTypes.instanceOf(Array),
};

FilterTableCustom.defaultProps = {
	fetch: null,
	setData: [],
};

export default JobNumberCompare;
