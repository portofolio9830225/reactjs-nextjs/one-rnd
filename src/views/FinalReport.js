import React, { useEffect, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import moment from 'moment';
import Swal from 'sweetalert2';
import { useFormik } from 'formik';
import PropTypes from 'prop-types';
import Card, { CardBody, CardHeader, CardLabel, CardTitle } from '../components/bootstrap/Card';
import Page from '../layout/Page/Page';
import PageWrapper from '../layout/PageWrapper/PageWrapper';
import FormulaModule from '../modules/FormulaModule';
import PageLayoutHeader from '../pages/common/Headers/PageLayoutHeader';
import useDarkMode from '../hooks/useDarkMode';
import DarkDataTable from '../components/DarkDataTable';
import Button from '../components/bootstrap/Button';
import Modal, { ModalBody, ModalHeader, ModalTitle } from '../components/bootstrap/Modal';
import FormGroup from '../components/bootstrap/forms/FormGroup';
import Input from '../components/bootstrap/forms/Input';
import { getRequester } from '../helpers/helpers';
import Notifications from '../components/Notifications';
import CustomSelect from '../components/custom/CustomSelect';
import Textarea from '../components/bootstrap/forms/Textarea';
import FormCustomModalDetail from '../components/custom/FormCustomModalDetail';

const STATUS = ['Sedang Verifikasi PIC'];

const STATUS_CODE = {
	SEDANG_VERIFIKASI_PIC: 'sedang_verifikasi_pic',
};

const OPTIONS_FINAL_REPORT = [
	{ value: 'selesai', label: 'Selesai' },
	{ value: 'drop', label: 'Drop' },
	{ value: 'reformula', label: 'Reformula' },
];

const handleUpdateStatusFormula = (values) => {
	const newResponse = new Promise((resolve, reject) => {
		try {
			FormulaModule.updateStatusFormula(values)
				.then(() => {
					Notifications.showNotif({
						header: 'Information!',
						message: 'Data has been updated successfully',
						type: Notifications.TYPE.SUCCESS,
					});
					resolve({ error: false, message: 'successfully' });
				})
				.catch((err) => {
					Notifications.showNotif({
						header: 'Warning!!',
						message: err,
						type: Notifications.TYPE.ERROR,
					});
					reject(new Error(err));
				});
		} catch (e) {
			reject(new Error(e.message));
		}
	});
	return newResponse;
};

const FormCustomModal = ({ initialValues, handleCustomUpdateStatus, options }) => {
	const { darkModeStatus } = useDarkMode();
	const { username } = getRequester();

	const [isOpen, setOpen] = useState(false);

	const customHandleTrialPlant = () => {
		try {
			Swal.fire({
				title: 'Are you sure?',
				text: 'Are you sure to Trial Big Plant ?',
				icon: 'question',
				showCancelButton: true,
				confirmButtonText: 'Yes, Trial Plant',
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
			}).then((result) => {
				if (result.isConfirmed) {
					const new_values = {
						_id: initialValues._id,
						requester: username,
						update: [{ status: 'Siap Trial Plant' }],
					};

					handleCustomUpdateStatus(new_values, options);
				} else if (result.dismiss === Swal.DismissReason.cancel) {
					Swal.fire('Cancelled', 'Your data is safe :)', 'error');
				}
			});
		} catch (error) {
			Swal.fire('Information ', 'Please check your entries again!', 'error');
		}
	};

	const formik = useFormik({
		initialValues: { remark: '', type: null },
		validate: (values) => {
			const errors = {};

			if (!values?.remark) {
				errors.remark = 'Remark is required';
			}
			if (!values?.type) {
				errors.type = 'Type is required';
			}

			return errors;
		},
		onSubmit: (values) => {
			try {
				Swal.fire({
					title: 'Are you sure?',
					text: 'Please check your entries !',
					icon: 'question',
					showCancelButton: true,
					confirmButtonText: 'Yes',
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
				}).then((result) => {
					if (result.isConfirmed) {
						//
						const new_values = {
							_id: initialValues._id,
							requester: username,
							update: [
								{
									status: values.type?.label,
									remark: values.remark,
									data: {
										job_number: initialValues.job_number,
									},
								},
							],
							ticket_id: initialValues.ticket_id,
						};

						handleCustomUpdateStatus(new_values, options);
					} else if (result.dismiss === Swal.DismissReason.cancel) {
						Swal.fire('Cancelled', 'Your data is safe :)', 'error');
					}
				});
			} catch (error) {
				Swal.fire('Information ', 'Please check your entries again!', 'error');
			}
		},
	});

	const onChangeType = (e) => {
		formik.setFieldValue('type', e);
	};

	const onChangeWindowModal = (e) => {
		setOpen(e);
		formik.handleReset(e);
	};

	return (
		<>
			<Button
				color='warning'
				type='button'
				isLight={darkModeStatus}
				className='m-1 d-none'
				onClick={customHandleTrialPlant}>
				Trial Plant
			</Button>

			<Button
				color='success'
				type='button'
				isLight={darkModeStatus}
				className='m-1'
				onClick={() => setOpen(true)}>
				Final Report
			</Button>

			<Modal
				isOpen={isOpen}
				setIsOpen={setOpen}
				size='lg'
				titleId='modal-crud-final-report'
				isStaticBackdrop>
				<ModalHeader setIsOpen={onChangeWindowModal} className='p-4'>
					<ModalTitle id='modal-crud-final-report'>
						Final Report - ({initialValues?.document_number}) -{' '}
						{initialValues?.customer_name}
					</ModalTitle>
				</ModalHeader>
				<ModalBody>
					<div className='row p-2' tag='form' noValidate onSubmit={formik.handleSubmit}>
						<div className='col-md-12 py-2'>
							<FormGroup id='type' label='Type'>
								<CustomSelect
									options={OPTIONS_FINAL_REPORT}
									isSearchable={OPTIONS_FINAL_REPORT.length > 7}
									defaultValue={formik.values.type}
									value={formik.values.type}
									onChange={onChangeType}
									isValid={formik.values.type !== null || formik.isValid}
									invalidFeedback={formik.errors.type}
								/>
							</FormGroup>
						</div>
						<div className='col-md-12 py-2'>
							<FormGroup id='remark' label='Remark'>
								<Textarea
									placeholder='Remark'
									onChange={formik.handleChange}
									onBlur={formik.handleBlur}
									value={formik.values.remark}
									isValid={formik.isValid}
									isTouched={formik.touched.remark}
									invalidFeedback={formik.errors.remark}
									rows={7}
								/>
							</FormGroup>
						</div>
						<div className='col-md-12 py-2'>
							<Button
								icon='Save'
								color='success'
								type='button'
								className='float-end'
								onClick={formik.handleSubmit}
								isLight={darkModeStatus}>
								Submit
							</Button>
						</div>
					</div>
				</ModalBody>
			</Modal>
		</>
	);
};

FormCustomModal.propTypes = {
	initialValues: PropTypes.instanceOf(Object),
	handleCustomUpdateStatus: PropTypes.func,
	options: PropTypes.instanceOf(Object),
};
FormCustomModal.defaultProps = {
	initialValues: null,
	handleCustomUpdateStatus: () => {},
	options: {
		curPage: 1,
		perPage: 10,
		showAll: false,
		date: {
			start: moment(new Date()).subtract(1, 'months').format('YYYY-MM-DD'),
			end: moment(new Date()).format('YYYY-MM-DD'),
		},
	},
};

const CustomButtonTable = ({ initialValues, handleCustomUpdateStatus, options, row }) => {
	const { username } = getRequester();
	const newInitialValues = { ...initialValues };
	newInitialValues.formula_id = row._id;
	if (row.pic == username) {
		return (
			<>
				<FormCustomModalDetail initialValues={newInitialValues} />

				{[STATUS_CODE.SEDANG_VERIFIKASI_PIC].includes(initialValues.status_code) && (
					<FormCustomModal
						initialValues={initialValues}
						handleCustomUpdateStatus={handleCustomUpdateStatus}
						options={options}
					/>
				)}
			</>
		);
	}
	return <FormCustomModalDetail initialValues={newInitialValues} />;
};

CustomButtonTable.propTypes = {
	initialValues: PropTypes.instanceOf(Object),
	handleCustomUpdateStatus: PropTypes.func,
	options: PropTypes.instanceOf(Object),
	row: PropTypes.instanceOf(Object),
};
CustomButtonTable.defaultProps = {
	initialValues: null,
	handleCustomUpdateStatus: () => {},
	options: {
		curPage: 1,
		perPage: 10,
		showAll: false,
		date: {
			start: moment(new Date()).subtract(1, 'months').format('YYYY-MM-DD'),
			end: moment(new Date()).format('YYYY-MM-DD'),
		},
	},
	row: {},
};

const TableCustom = ({
	data,
	date,
	totalRows,
	perPage,
	loading,
	fetchData,
	curPage,
	showAll,
	handleCustomUpdateStatus,
	handleCustomFinalReport,
}) => {
	const { darkModeStatus } = useDarkMode();

	const handlePageChange = (page) => {
		fetchData(page, perPage, showAll, date);
	};

	const handlePerRowsChange = async (newPerPage, page) => {
		const all = totalRows === newPerPage;
		return fetchData(page, newPerPage, all, date);
	};

	const paginationComponentOptions = {
		selectAllRowsItem: true,
		selectAllRowsItemText: 'ALL',
	};

	const columns = useMemo(
		() => [
			{
				name: 'REGISTER DATE',
				selector: (row) => moment(row.created_at).format('YYYY-MM-DD'),
				sortable: true,
				width: '140px',
			},
			{
				name: 'TICKET ID',
				selector: (row) => row.document_number,
				sortable: true,
				width: '220px',
			},
			{
				name: 'CUSTOMER',
				selector: (row) => row.customer_name,
				sortable: true,
			},
			{
				name: 'JOB NUMBER',
				selector: (row) => row.job_number,
				sortable: true,
				width: '140px',
			},
			{
				name: 'STATUS',
				selector: (row) => row.status,
				sortable: true,
				width: '300px',
			},
			{
				name: 'PIC',
				selector: (row) => row.pic_name,
				sortable: true,
			},
			{
				name: 'ACTION',
				width: '300px',
				// eslint-disable-next-line react/no-unstable-nested-components
				cell: (row) => {
					const {
						_id,
						customer_name,
						document_number,
						status_code,
						ticket_id,
						job_number,
					} = row;
					const initialValues = {
						_id,
						customer_name,
						document_number,
						status_code,
						ticket_id,
						job_number,
					};

					const options = { curPage, perPage, showAll, date };

					return (
						<CustomButtonTable
							initialValues={initialValues}
							options={options}
							row={row}
							handleCustomUpdateStatus={handleCustomUpdateStatus}
							handleCustomFinalReport={handleCustomFinalReport}
						/>
					);
				},
			},
		],
		// eslint-disable-next-line react-hooks/exhaustive-deps
		[curPage, perPage, showAll, date],
	);

	return (
		<DarkDataTable
			columns={columns}
			data={data}
			pagination
			progressPending={loading}
			paginationServer
			paginationTotalRows={totalRows}
			onChangeRowsPerPage={handlePerRowsChange}
			onChangePage={handlePageChange}
			paginationComponentOptions={paginationComponentOptions}
			theme={darkModeStatus ? 'custom_dark' : 'light'}
		/>
	);
};
TableCustom.propTypes = {
	data: PropTypes.instanceOf(Array),
	date: PropTypes.instanceOf(Object),
	loading: PropTypes.bool,
	totalRows: PropTypes.number,
	curPage: PropTypes.number,
	perPage: PropTypes.number,
	showAll: PropTypes.bool,
	fetchData: PropTypes.func,
	handleCustomUpdateStatus: PropTypes.func,
	handleCustomFinalReport: PropTypes.func,
};
TableCustom.defaultProps = {
	data: [],
	date: {
		start: moment(new Date()).subtract(1, 'months').format('YYYY-MM-DD'),
		end: moment(new Date()).format('YYYY-MM-DD'),
	},
	loading: false,
	totalRows: 0,
	curPage: 1,
	perPage: 10,
	showAll: false,
	fetchData: () => {},
	handleCustomUpdateStatus: () => {},
	handleCustomFinalReport: () => {},
};

const FilterTableCustom = ({ date, perPage, showAll, fetchData }) => {
	const { darkModeStatus } = useDarkMode();

	const formik = useFormik({
		initialValues: { ...date },
		validate: (values) => {
			const errors = {};

			if (!values?.start) {
				errors.start = 'date required';
			}

			if (!values?.end) {
				errors.end = 'date required';
			}

			if (!errors) {
				const start = moment(values.start);
				const end = moment(values.end);

				if (start.isAfter(end)) {
					errors.start = 'the date cannot be later than the end date';
				}
				if (end.isBefore(start)) {
					errors.end = 'the date cannot be less than the start date';
				}
			}

			return errors;
		},
		onSubmit: (values) => {
			fetchData(1, perPage, showAll, values);
		},
	});

	const customHandleReset = (e) => {
		formik.handleReset(e);

		const newDate = { start: '', end: '' };
		fetchData(1, perPage, showAll, newDate);
	};

	return (
		<div className='row' tag='form' noValidate onSubmit={formik.handleSubmit}>
			<div className='col-sm-2 m-1'>
				<FormGroup id='start' label='Start'>
					<Input
						type='date'
						onChange={formik.handleChange}
						onBlur={formik.handleBlur}
						value={formik.values.start}
						isTouched={formik.touched.start}
						isValid={formik.isValid}
						invalidFeedback={formik.errors.start}
					/>
				</FormGroup>
			</div>
			<div className='col-sm-2 m-1'>
				<FormGroup id='end' label='End'>
					<Input
						type='date'
						onChange={formik.handleChange}
						onBlur={formik.handleBlur}
						value={formik.values.end}
						isTouched={formik.touched.end}
						isValid={formik.isValid}
						invalidFeedback={formik.errors.end}
					/>
				</FormGroup>
			</div>
			<div className='col-sm-4 m-1 d-flex align-items-end'>
				<Button
					icon='FilterAlt'
					color='success'
					type='button'
					className='m-1'
					onClick={formik.handleSubmit}
					isLight={darkModeStatus}>
					Filter
				</Button>
				<Button
					icon='Clear'
					color='danger'
					type='reset'
					className='m-1'
					onClick={customHandleReset}
					isLight={darkModeStatus}>
					Clear
				</Button>
			</div>
		</div>
	);
};

FilterTableCustom.propTypes = {
	date: PropTypes.instanceOf(Object),
	fetchData: PropTypes.func,
	perPage: PropTypes.number,
	showAll: PropTypes.bool,
};
FilterTableCustom.defaultProps = {
	date: {
		start: '',
		end: '',
	},
	perPage: 10,
	showAll: false,
	fetchData: () => {},
};

const FinalReport = () => {
	const { t } = useTranslation('crud');

	const [data, setData] = useState([]);
	const [totalRows, setTotalRows] = useState(0);
	const [perPage, setPerPage] = useState(10);
	const [curPage, setCurPage] = useState(1);

	const [show_all, setShowAll] = useState(false);
	const [loading, setLoading] = useState(false);

	const [date, setDate] = useState({ start: '', end: '' });

	const updateStatusSubmit = (values, options) => {
		handleUpdateStatusFormula(values)
			.then(() => {
				fetchData(options.curPage, options.perPage, options.showAll, options.date);
			})
			.catch(() => {})
			.finally(() => {});
	};

	useEffect(() => {
		fetchData(curPage, perPage, show_all, date);
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	const fetchData = async (newPage, newPerPage, all, newDate) => {
		setLoading(true);
		setShowAll(all);
		setDate(newDate);

		const query = {
			page: newPage,
			sizePerPage: newPerPage,
			showAll: all,
			status: STATUS,
		};
		if (newDate?.start && newDate?.end) {
			query.date_start = newDate.start;
			query.date_end = newDate.end;
		}

		return FormulaModule.readFormula(query)
			.then((response) => {
				setData(response.foundData);
				setTotalRows(response.countData);
				setCurPage(response.currentPage);
				setPerPage(newPerPage);
			})
			.catch(() => {})
			.finally(() => {
				setLoading(false);
			});
	};

	return (
		<PageWrapper title={t('Final Report')}>
			<PageLayoutHeader />
			<Page container='fluid'>
				<Card stretch>
					<CardHeader borderSize={1}>
						<CardLabel icon='InsertDriveFile' iconColor='info'>
							<CardTitle>{t('Final Report')}</CardTitle>
						</CardLabel>
					</CardHeader>
					<CardBody>
						<FilterTableCustom
							date={date}
							perPage={perPage}
							showAll={show_all}
							fetchData={fetchData}
						/>

						<TableCustom
							data={data}
							totalRows={totalRows}
							perPage={perPage}
							curPage={curPage}
							showAll={show_all}
							loading={loading}
							date={date}
							fetchData={fetchData}
							handleCustomUpdateStatus={updateStatusSubmit}
						/>
					</CardBody>
				</Card>
			</Page>
		</PageWrapper>
	);
};

FinalReport.propTypes = {};
FinalReport.defaultProps = {};

export default FinalReport;
