import React, { useEffect, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useFormik } from 'formik';
import Swal from 'sweetalert2';
// import * as yup from 'yup';
import PropTypes from 'prop-types';
import moment from 'moment';
import useDarkMode from '../hooks/useDarkMode';
import { getActiveRoles, getRequester } from '../helpers/helpers';
// module
import UserModule from '../modules/UserModule';
import RegionalModule from '../modules/RegionalModule';
import RolesModule from '../modules/RolesModule';
// component
import Accordion, { AccordionItem } from '../components/bootstrap/Accordion';
import Card, { CardBody, CardHeader, CardLabel, CardTitle } from '../components/bootstrap/Card';
import Modal, { ModalBody, ModalHeader, ModalTitle } from '../components/bootstrap/Modal';
import Notifications from '../components/Notifications';
import Page from '../layout/Page/Page';
import PageWrapper from '../layout/PageWrapper/PageWrapper';
import PageLayoutHeader from '../pages/common/Headers/PageLayoutHeader';
import DarkDataTable from '../components/DarkDataTable';
import Button from '../components/bootstrap/Button';
import CustomWizard, { CustomWizardItem } from '../components/custom/CustomWIzard';
import FormGroup from '../components/bootstrap/forms/FormGroup';
import Checks from '../components/bootstrap/forms/Checks';
import Input from '../components/bootstrap/forms/Input';
import CustomSelect from '../components/custom/CustomSelect';
import CustomParameter from '../components/custom/CustomParameter';

const handleCreate = (values) => {
	const newResponse = new Promise((resolve, reject) => {
		try {
			resolve(
				UserModule.create(values)
					.then(() => {
						Notifications.showNotif({
							header: 'Information',
							message: 'Data has been saved successfully',
							type: Notifications.TYPE.SUCCESS,
						});
					})
					.catch((err) => {
						Notifications.showNotif({
							header: 'Information',
							message: err,
							type: Notifications.TYPE.ERROR,
						});
					}),
			);
		} catch (e) {
			reject(new Error({ error: true }));
		}
	});
	return newResponse;
};

const handleUpdate = (values) => {
	const newResponse = new Promise((resolve, reject) => {
		try {
			resolve(
				UserModule.update(values)
					.then(() => {
						Notifications.showNotif({
							header: 'Information',
							message: 'Data has been update successfully',
							type: Notifications.TYPE.SUCCESS,
						});
					})
					.catch((err) => {
						Notifications.showNotif({
							header: 'Information',
							message: err,
							type: Notifications.TYPE.ERROR,
						});
					}),
			);
		} catch (e) {
			reject(new Error({ error: true }));
		}
	});
	return newResponse;
};

const handleDelete = (values) => {
	const { username } = getRequester();
	const newValues = { _id: values._id, requester: username };

	const newResponse = new Promise((resolve, reject) => {
		try {
			Swal.fire({
				title: 'Are you sure?',
				text: 'Please check your entries !',
				icon: 'question',
				showCancelButton: true,
				confirmButtonText: 'Yes',
			}).then((result) => {
				if (result.isConfirmed) {
					resolve(
						UserModule.destroy(newValues)
							.then(() => {
								Notifications.showNotif({
									header: 'Information',
									message: 'Data has been deleted successfully',
									type: Notifications.TYPE.SUCCESS,
								});
							})
							.catch((err) => {
								Notifications.showNotif({
									header: 'Information',
									message: err,
									type: Notifications.TYPE.ERROR,
								});
							}),
					);
				} else if (result.dismiss === Swal.DismissReason.cancel) {
					Swal.fire('Cancelled', 'Your data is safe :)', 'error');
				}
			});
		} catch (e) {
			reject(new Error({ error: true }));
		}
	});
	return newResponse;
};

const FormCustom = ({
	initialValues,
	handleCustomSubmit,
	isUpdate,
	isReadOnly,
	valueDepartment,
	valueEmployee,
	listDepartment,
	listRegional,
	listRole,
}) => {
	const { username, department } = getRequester();
	const { default_segment_code } = getActiveRoles();

	const [selectDepartment, setSelectDepartment] = useState(valueDepartment);
	const [selectEmployee, setSelectEmployee] = useState(valueEmployee);
	const [listEmployee, setListEmployee] = useState([]);

	const validate = (values) => {
		const errors = {};

		const pattern_email =
			/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

		if (values.is_employee) {
			if (!values.department && !selectDepartment) errors.department = 'Required';
			if (!values.employee && !selectEmployee) errors.employee = 'Required';
		} else {
			if (!values.nik) errors.nik = 'Required';
			if (!values.name) errors.name = 'Required';
			if (!values.email) errors.email = 'Required';
			else if (!values.email.toLowerCase().match(pattern_email))
				errors.email = 'Email must be a valid email';
		}
		if (!values.phone) errors.phone = 'Required';

		return errors;
	};

	const formik = useFormik({
		initialValues: { ...initialValues },
		validate,
		onSubmit: (values, { setSubmitting, resetForm, setErrors }) => {
			try {
				Swal.fire({
					title: 'Are you sure?',
					text: 'Please check your entries !',
					icon: 'question',
					showCancelButton: true,
					confirmButtonText: 'Yes',
				}).then((result) => {
					if (result.isConfirmed) {
						const new_values = {};
						if (isUpdate) {
							new_values._id = values._id;
						}
						new_values.requester = username;
						new_values.department = department;
						new_values.username = values.nik;
						new_values.name = values.name;
						new_values.email = values.email;

						handleCustomSubmit(new_values, resetForm);
					} else if (result.dismiss === Swal.DismissReason.cancel) {
						Swal.fire('Cancelled', 'Your data is safe :)', 'error');
					}
				});
			} catch (error) {
				setErrors({ submit: error.message });
				Swal.fire('Information ', 'Please check your entries again!', 'error');
			} finally {
				setSubmitting(false);
			}
		},
	});

	const onChangeIsEmployee = (e) => {
		formik.handleChange(e);
	};

	const onChangeDepartment = (e) => {
		setSelectDepartment(e);
	};

	const onChangeEmployee = (e) => {
		setSelectEmployee(e);
	};

	useEffect(() => {
		if (selectDepartment && !isReadOnly) {
			fetchDataEmployee(selectDepartment.value);
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [selectDepartment]);

	const fetchDataEmployee = async (dept) => {
		const query = `department=${dept}`;

		return UserModule.getUserByDept(query)
			.then((response) => {
				const employee = response.map((item) => {
					return {
						value: item.username,
						label: `(${item.username}) ${item.name}`,
						detail: item,
					};
				});
				setListEmployee(employee);
			})
			.catch(() => {
				setListEmployee([]);
			})
			.finally(() => {});
	};

	const onChangeRole = () => {};

	return (
		<CustomWizard
			className='mb-0'
			isHeader
			color='success'
			noValidate
			onSubmit={formik.handleSubmit}>
			<CustomWizardItem id='wizard-detail' title='Detail'>
				<div className='p-4'>
					<FormGroup
						className='mb-3'
						isColForLabel
						labelClassName='col-sm-2 text-capitalize'
						childWrapperClassName='col-sm-6'
						label=' '>
						<Checks
							type='switch'
							id='is_employee'
							label='Is Employee'
							onChange={onChangeIsEmployee}
							checked={formik.values.is_employee}
							disabled={isUpdate}
						/>
					</FormGroup>
					{formik.values.is_employee && (
						<>
							<FormGroup
								className='mb-3'
								isColForLabel
								labelClassName='col-sm-2 text-capitalize'
								childWrapperClassName='col-sm-6'
								id='department'
								label='Department'>
								<CustomSelect
									value={selectDepartment}
									defaultValue={selectDepartment}
									options={listDepartment}
									onChange={onChangeDepartment}
									isSearchable={listDepartment.length > 7}
									invalidFeedback={formik.values.department}
									isValid={
										typeof formik.values.department !== 'undefined' ||
										selectDepartment !== null
									}
								/>
							</FormGroup>
							<FormGroup
								className='mb-3'
								isColForLabel
								labelClassName='col-sm-2 text-capitalize'
								childWrapperClassName='col-sm-6'
								id='employee'
								label='Employee'>
								<CustomSelect
									value={selectEmployee}
									defaultValue={selectEmployee}
									options={listEmployee}
									onChange={onChangeEmployee}
									isSearchable={listEmployee.length > 7}
									invalidFeedback={formik.values.employee}
									isValid={
										typeof formik.values.employee !== 'undefined' ||
										selectEmployee !== null
									}
								/>
							</FormGroup>
						</>
					)}
					<FormGroup
						className='mb-3'
						isColForLabel
						labelClassName='col-sm-2 text-capitalize'
						childWrapperClassName='col-sm-6'
						id='nik'
						label='Code / NIK'>
						<Input
							type='text'
							onChange={formik.handleChange}
							onBlur={formik.handleBlur}
							value={formik.values.nik}
							isValid={formik.isValid}
							isTouched={formik.touched.nik}
							invalidFeedback={formik.errors.nik}
							disabled={formik.values.is_employee || isUpdate}
							autoComplete='off'
						/>
					</FormGroup>
					<FormGroup
						className='mb-3'
						isColForLabel
						labelClassName='col-sm-2 text-capitalize'
						childWrapperClassName='col-sm-6'
						id='name'
						label='name'>
						<Input
							type='text'
							onChange={formik.handleChange}
							onBlur={formik.handleBlur}
							value={formik.values.name}
							isValid={formik.isValid}
							isTouched={formik.touched.name}
							invalidFeedback={formik.errors.name}
							disabled={formik.values.is_employee || isReadOnly}
							autoComplete='off'
						/>
					</FormGroup>
					<FormGroup
						className='mb-3'
						isColForLabel
						labelClassName='col-sm-2 text-capitalize'
						childWrapperClassName='col-sm-6'
						id='email'
						label='Email'>
						<Input
							type='email'
							onChange={formik.handleChange}
							onBlur={formik.handleBlur}
							value={formik.values.email}
							isValid={formik.isValid}
							isTouched={formik.touched.email}
							invalidFeedback={formik.errors.email}
							disabled={formik.values.is_employee || isReadOnly}
							autoComplete='off'
						/>
					</FormGroup>
					<FormGroup
						className='mb-3'
						isColForLabel
						labelClassName='col-sm-2 text-capitalize'
						childWrapperClassName='col-sm-6'
						id='phone'
						label='Phone'>
						<Input
							type='number'
							onChange={formik.handleChange}
							onBlur={formik.handleBlur}
							value={formik.values.phone}
							isValid={formik.isValid}
							isTouched={formik.touched.phone}
							invalidFeedback={formik.errors.phone}
							autoComplete='off'
							disabled={isReadOnly}
						/>
					</FormGroup>
				</div>
			</CustomWizardItem>
			<CustomWizardItem id='wizard-role' title='Role'>
				<CustomParameter
					onChange={onChangeRole}
					listRole={listRole}
					listRegional={listRegional}
					isReadOnly={isReadOnly}
					initialValues={formik.values.role.filter(
						(item) => item.segment_code === default_segment_code,
					)}
				/>
			</CustomWizardItem>
		</CustomWizard>
	);
};

FormCustom.propTypes = {
	initialValues: PropTypes.instanceOf(Object),
	handleCustomSubmit: PropTypes.func,
	isUpdate: PropTypes.bool,
	isReadOnly: PropTypes.bool,
	valueDepartment: PropTypes.instanceOf(Object),
	valueEmployee: PropTypes.instanceOf(Object),
	listDepartment: PropTypes.instanceOf(Array),
	listRegional: PropTypes.instanceOf(Array),
	listRole: PropTypes.instanceOf(Array),
};
FormCustom.defaultProps = {
	initialValues: null,
	handleCustomSubmit: () => {},
	isUpdate: false,
	isReadOnly: false,
	valueDepartment: null,
	valueEmployee: null,
	listDepartment: [],
	listRegional: [],
	listRole: [],
};

const FormCustomModal = ({
	initialValues,
	handleCustomUpdate,
	handleCustomDelete,
	listDepartment,
	listRole,
	listRegional,
	select_department,
	select_employee,
}) => {
	const { darkModeStatus } = useDarkMode();
	const [isOpen, setOpen] = useState(false);
	const [isReadOnly, setReadOnly] = useState(false);

	const [title, setTitle] = useState('');
	return (
		<>
			<Button
				icon='GridOn'
				color='info'
				type='button'
				className='mx-1'
				isLight={darkModeStatus}
				onClick={() => {
					setTitle('Detail');
					setReadOnly(true);
					setOpen(true);
				}}
			/>
			<Button
				icon='Edit'
				color='success'
				type='button'
				className='mx-1'
				isLight={darkModeStatus}
				onClick={() => {
					setTitle('Update');
					setReadOnly(false);
					setOpen(true);
				}}
			/>
			<Button
				icon='Delete'
				color='danger'
				type='button'
				className='mx-1'
				isLight={darkModeStatus}
				onClick={() => handleCustomDelete(initialValues)}
			/>

			<Modal isOpen={isOpen} setIsOpen={setOpen} size='xl' titleId='modal-crud'>
				<ModalHeader setIsOpen={setOpen} className='p-4'>
					<ModalTitle id='modal-crud'>{title} Data</ModalTitle>
				</ModalHeader>
				<ModalBody>
					<FormCustom
						initialValues={initialValues}
						isReadOnly={isReadOnly}
						isUpdate={isOpen}
						handleCustomSubmit={handleCustomUpdate}
						listDepartment={listDepartment}
						listRegional={listRegional}
						listRole={listRole}
						select_department={select_department}
						select_employee={select_employee}
					/>
				</ModalBody>
			</Modal>
		</>
	);
};

FormCustomModal.propTypes = {
	initialValues: PropTypes.instanceOf(Object),
	handleCustomUpdate: PropTypes.func,
	handleCustomDelete: PropTypes.func,
	listDepartment: PropTypes.instanceOf(Array),
	listRole: PropTypes.instanceOf(Array),
	listRegional: PropTypes.instanceOf(Array),
	select_department: PropTypes.instanceOf(Object),
	select_employee: PropTypes.instanceOf(Object),
};
FormCustomModal.defaultProps = {
	handleCustomUpdate: () => {},
	handleCustomDelete: () => {},
	initialValues: {
		_id: null,
		department: 'guest',
		username: '',
		name: '',
		email: '',
		type: 'guest',
		detail_user: null,
		role: [],
	},
	listDepartment: [],
	listRole: [],
	listRegional: [],
	select_department: null,
	select_employee: null,
};

const TableCustom = ({
	data,
	totalRows,
	perPage,
	fetchData,
	handleCustomDelete,
	handleCustomUpdate,
	listDepartment,
	listRole,
	listRegional,
	loading,
}) => {
	const { darkModeStatus } = useDarkMode();

	const handlePageChange = (page) => {
		fetchData(page, perPage);
	};

	const handlePerRowsChange = async (newPerPage, page) => {
		return fetchData(page, newPerPage);
	};

	const columns = useMemo(
		() => [
			{
				name: 'Name',
				selector: (row) => row.name,
				sortable: true,
				width: '360px',
			},
			{
				name: 'Email',
				selector: (row) => row.email,
				sortable: true,
				width: '360px',
			},
			{
				name: 'Username',
				selector: (row) => row.username,
				sortable: true,
				width: '300px',
			},
			{
				name: 'Type',
				selector: (row) => row.type,
				sortable: true,
				width: '140px',
			},
			{
				name: 'Created',
				selector: (row) => moment(row.created_at).format('YYYY-MM-DD HH:mm'),
				sortable: true,
				width: '200px',
			},
			{
				name: 'Action',
				// eslint-disable-next-line react/no-unstable-nested-components
				cell: (row) => {
					const { department, detail_user, email, name, role, type, username, _id } = row;

					const initialValues = {
						is_employee: type === 'employee',
						department,
						phone: detail_user?.phone ?? '',
						email,
						name,
						role,
						type,
						nik: username,
						employee: username,
						_id,
					};

					let select_department = null;
					let select_employee = null;

					if (initialValues.is_employee) {
						select_department = { value: department, label: department.toUpperCase() };
						select_employee = { value: username, label: `(${username}) ${name}` };
					}

					return (
						<FormCustomModal
							initialValues={initialValues}
							handleCustomUpdate={handleCustomUpdate}
							handleCustomDelete={handleCustomDelete}
							listDepartment={listDepartment}
							listRegional={listRegional}
							listRole={listRole}
							select_department={select_department}
							select_employee={select_employee}
						/>
					);
				},
			},
		],
		// eslint-disable-next-line react-hooks/exhaustive-deps
		[listDepartment, listRegional, listRole],
	);

	return (
		<DarkDataTable
			columns={columns}
			data={data}
			progressPending={loading}
			pagination
			paginationServer
			paginationTotalRows={totalRows}
			onChangeRowsPerPage={handlePerRowsChange}
			onChangePage={handlePageChange}
			theme={darkModeStatus ? 'custom_dark' : 'light'}
		/>
	);
};

TableCustom.propTypes = {
	data: PropTypes.instanceOf(Array),
	totalRows: PropTypes.number,
	perPage: PropTypes.number,
	fetchData: PropTypes.func,
	handleCustomUpdate: PropTypes.func,
	handleCustomDelete: PropTypes.func,
	listDepartment: PropTypes.instanceOf(Array),
	listRole: PropTypes.instanceOf(Array),
	listRegional: PropTypes.instanceOf(Array),
	loading: PropTypes.bool,
};
TableCustom.defaultProps = {
	data: [],
	totalRows: 0,
	perPage: 10,
	fetchData: () => {},
	handleCustomUpdate: () => {},
	handleCustomDelete: () => {},
	listDepartment: [],
	listRole: [],
	listRegional: [],
	loading: false,
};

const MyDataUser = () => {
	const { t } = useTranslation('crud');

	const { default_segment_code } = getActiveRoles();

	const [data, setData] = useState([]);
	const [totalRows, setTotalRows] = useState(0);
	const [perPage, setPerPage] = useState(10);
	const [curPage, setCurPage] = useState(1);

	const [listDepartment, setListDepartment] = useState([]);
	const [listRegional, setListRegional] = useState([]);
	const [listRole, setListRole] = useState([]);

	const [loading, setLoading] = useState(false);

	const initialValues = {
		is_employee: false,
		nik: '',
		name: '',
		email: '',
		phone: 0,
		type: 'guest',
		role: [],
	};

	const createSubmit = (values, resetForm) => {
		handleCreate(values)
			.then(() => {
				fetchData(curPage, perPage);
				resetForm(initialValues);
			})
			.catch(() => {});
	};

	const updateSubmit = (values) => {
		handleUpdate(values)
			.then(() => {
				fetchData(curPage, perPage);
			})
			.catch(() => {});
	};

	const deleteSubmit = (values) => {
		handleDelete(values)
			.then(() => {
				fetchData(curPage, perPage);
			})
			.catch(() => {});
	};

	useEffect(() => {
		fetchData(1, 10);
		fetchDataDepartment();
		fetchDataRegional();
		fetchDataRole();

		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	const fetchData = async (newPage, newPerPage) => {
		setLoading(true);
		const query = `page=${newPage}&sizePerPage=${newPerPage}&segment_code=${default_segment_code}`;
		return UserModule.read(query)
			.then((response) => {
				setData(response.foundData);
				setTotalRows(response.countData);
				setCurPage(response.currentPage);
				setPerPage(newPerPage);
			})
			.catch(() => {})
			.finally(() => {
				setLoading(false);
			});
	};

	const fetchDataDepartment = async () => {
		return UserModule.getAllDepartment()
			.then((response) => {
				const role = response.map((item) => {
					return {
						value: item.toLowerCase(),
						label: item,
					};
				});
				setListDepartment(role);
			})
			.catch(() => {})
			.finally(() => {});
	};

	const fetchDataRegional = async () => {
		const query = `segment_code=${default_segment_code}`;
		return RegionalModule.readSelect(query)
			.then((response) => {
				setListRegional(response);
			})
			.catch(() => {})
			.finally(() => {});
	};

	const fetchDataRole = async () => {
		const query = `segment_code=${default_segment_code}`;
		return RolesModule.readSelect(query)
			.then((response) => {
				setListRole(response);
			})
			.catch(() => {})
			.finally(() => {});
	};

	return (
		<PageWrapper title={t('Data User')}>
			<PageLayoutHeader />
			<Page container='fluid'>
				<Card>
					<CardBody>
						<Accordion id='accordion-form' activeItemId={false} color='info'>
							<AccordionItem id='form-create' title={t('form')} icon='AddBox'>
								<FormCustom
									initialValues={initialValues}
									handleCustomSubmit={createSubmit}
									listDepartment={listDepartment}
									listRegional={listRegional}
									listRole={listRole}
								/>
							</AccordionItem>
						</Accordion>
					</CardBody>
				</Card>
				<Card stretch>
					<CardHeader borderSize={1}>
						<CardLabel icon='History' iconColor='info'>
							<CardTitle>{t('historical_data')}</CardTitle>
						</CardLabel>
					</CardHeader>
					<CardBody>
						<TableCustom
							data={data}
							totalRows={totalRows}
							perPage={perPage}
							fetchData={fetchData}
							handleCustomUpdate={updateSubmit}
							handleCustomDelete={deleteSubmit}
							listDepartment={listDepartment}
							listRegional={listRegional}
							listRole={listRole}
							loading={loading}
						/>
					</CardBody>
				</Card>
			</Page>
		</PageWrapper>
	);
};

MyDataUser.propTypes = {};
MyDataUser.defaultProps = {};

export default MyDataUser;
