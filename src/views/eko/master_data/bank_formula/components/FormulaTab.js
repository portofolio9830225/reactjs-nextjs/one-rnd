import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Tab, Tabs } from 'react-bootstrap';
import Button from '../../../../../components/bootstrap/Button';
import FormulaMaterial from './FormulaMaterial';
// import ParameterTrial from './ParameterTrial';
import ParameterTrial from '../../../../../components/custom/ParameterTrial';
import Modal, { ModalBody, ModalHeader, ModalTitle, ModalFooter } from '../../../../../components/bootstrap/Modal';
import FormGroup from '../../../../../components/bootstrap/forms/FormGroup';
import CustomSelect from '../../../../../components/CustomSelect';
import useDarkMode from '../../../../../hooks/useDarkMode';

const RenderTitleFormula = (item, editable) => {
	if (editable != false) {
		return (
			<div>
				{item}
			</div>
		);
	}
	return (
		<div>
			{item}
		</div>
	);
};


const FormulaTab = ({
	initialValues,
	onChange,
	selectedKey,
	listMaterial,
	listVendor,
	listUom,
	listCurrency,
	listFormulaCopy,
}) => {
	const { darkModeStatus } = useDarkMode();
	const [key, setKey] = useState(selectedKey);
	const [isReset, setReset] = useState(false);
	const [init, setInit] = useState(true);
	const [initMaterial, setInitMaterial] = useState(true);
	const [dataFormula, setDataFormula] = useState(initialValues);
	const [selectedCopyFormula, setSelectedCopyFormula] = useState(null);
	const [list_parameter] = useState([
		{
			value: 'mixing_sequences',
			label: 'Mixing Sequences',
		},
		{
			value: 'homogenizer',
			label: 'Homogenizer',
		},
		{
			value: 'pasteurization',
			label: 'Pasteurization',
		},
	]);
	const [isOpen, setIsOpen] = useState(false);

	const onChangeTab = (values, index) => {
		const new_data = [...dataFormula];

		if (values.length === 0) {
			delete new_data[index].detail;
		} else {
			new_data[index] = {
				...new_data.at(index),
				formula: values,
			};
		}

		setDataFormula(new_data);

		if (onChange) {
			onChange(new_data);
		}
	};


	const onChangeMainParameter = (values, index) => {
		const new_data = [...dataFormula];

		if (values.length === 0) {
			delete new_data[index].detail;
		} else {
			new_data[index] = {
				...new_data.at(index),
				details: values,
			};
		}
		setDataFormula(new_data);

		if (onChange) {
			onChange(new_data);
		}
	};

	const copyFormula = () => {
		setSelectedCopyFormula(null);
		const dataFormulaCopy = selectedCopyFormula.detail;
		dataFormulaCopy._id = null;
		dataFormulaCopy.title = 'New Bank Formula';
		dataFormulaCopy.editable = true;
		dataFormulaCopy.due_date_miniplant = '';
		setDataFormula([...dataFormula, dataFormulaCopy]);
		onChange([...dataFormula, dataFormulaCopy]);
		setInitMaterial(true);
		setInit(true);
		const total_arr = dataFormula.length + 1;
		setKey(total_arr - 1);
	};

	useEffect(() => {
		return <div>{/* */}</div>;
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [initialValues]);

	return (
		<div>
			<Tabs
				id='controlled-tab-example'
				activeKey={key}
				onSelect={(k) => {
					setKey(k);
				}}
				className='mb-3'>
				{dataFormula.map((item, index) => (
					<Tab
						key={'key-tab-'.concat(index)}
						eventKey={index}
						title={RenderTitleFormula(
							item.title,
							item.editable
						)}
						tabClassName={`${index}`}>
						<div className='col-12 mt-2'>
							<FormulaMaterial
								key={'formula-material-'.concat(index)}
								onChange={(e) => onChangeTab(e, index)}
								listMaterial={listMaterial}
								listVendor={listVendor}
								listUom={listUom}
								listCurrency={listCurrency}
								initialValues={
									item.formula
										? item.formula
										: [
												{
													key: 0,
													material_name: '',
													material_code: '',
													material: null,
													qty: '',
													uom: null,
													supplier_name: null,
													supplier_code: null,
													currency: null,
													price: '',
													note: '',
													readOnlyMaterialName: false,
												},
										  ]
								}
								initMaterial={initMaterial}
								setInitMaterial={setInitMaterial}
								isReadOnly={item.editable == false}
							/>
						</div>
						<div className='col-12 mb-3 g-4'>
							<ParameterTrial
								key={'parameter-material-'.concat(index)}
								initialValues={item ? item.details : []}
								onChange={(e) => onChangeMainParameter(e, index)}
								list_parameter={list_parameter}
								isReset={isReset}
								setReset={setReset}
								isReadOnly={item.editable == false}
								init={init}
								setInit={setInit}
							/>
						</div>
					</Tab>
				))}
			</Tabs>

			<Modal
				isOpen={isOpen}
				setIsOpen={setIsOpen}
				size='lg'
				titleId='modal-edit-menu-crud'
				isStaticBackdrop>
				<ModalHeader setIsOpen={setIsOpen} className='p-4'>
					<ModalTitle id='modal-edit-menu-crud'>Copy from another formula</ModalTitle>
				</ModalHeader>

				<ModalBody className='px-4'>
					<div className='col-md-12'>
						<FormGroup id='priority_code' label='Select Formula' className='mb-4'>
							<CustomSelect
								options={listFormulaCopy}
								defaultValue={selectedCopyFormula}
								value={selectedCopyFormula}
								darkTheme={darkModeStatus}
								isSearchable={isOpen}
								onChange={(value) => {
									setSelectedCopyFormula(value);
								}}
							/>
						</FormGroup>
					</div>
				</ModalBody>
				<ModalFooter className='px-4 pb-4'>
					<div className='col-md-12 '>
						<Button
							icon='Save'
							type='submit'
							color='success'
							className='float-end'
							onClick={() => {
								copyFormula();
								setIsOpen(false);
							}}>
							Copy
						</Button>
					</div>
				</ModalFooter>
			</Modal>
		</div>
	);
};

FormulaTab.propTypes = {
	onChange: PropTypes.func,
	initialValues: PropTypes.instanceOf(Array),
	selectedKey: PropTypes.number,
	listMaterial: PropTypes.instanceOf(Array),
	listVendor: PropTypes.instanceOf(Array),
	listUom: PropTypes.instanceOf(Array),
	listCurrency: PropTypes.instanceOf(Array),
	listFormulaCopy: PropTypes.instanceOf(Array),
};
FormulaTab.defaultProps = {
	onChange: null,
	initialValues: [],
	selectedKey: 0,
	listMaterial: [],
	listVendor: [],
	listUom: [],
	listCurrency: [],
	listFormulaCopy: [],
};

export default FormulaTab;
