import PropTypes from 'prop-types';
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import { Tab, Tabs } from 'react-bootstrap';
import Card, { CardBody, CardTabItem, CardTitle, CardHeader, CardLabel } from '../../../../../components/bootstrap/Card';
import TicketModule from '../../../../../modules/TicketModule';
import FormGroup from '../../../../../components/bootstrap/forms/FormGroup';
import Input from '../../../../../components/bootstrap/forms/Input';
import Button from '../../../../../components/bootstrap/Button';

const RenderTitleFormula = (item, idx) => {
	return (
		<div>
			{item} {`(idx${idx})`}{' '}
		</div>
	);
};

const DetailTicketAndFormulaComponent = ({ ticket_id, formula_id }) => {
	const [data_ticket, setDataTicket] = useState({
		currency: '',
		customer: '',
		date: '',
		date_of_communication: '',
		sample_product: '',
		status: '',
		target: '',
		target_date: '',
		target_price: '',
		information: [],
		sample: [],
		history: [],
		formula: [],
	});
	const [key, setKey] = useState(0);

	useEffect(() => {
		if (ticket_id) {
			fetchDataTicket();
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [ticket_id]);

	const fetchDataTicket = async () => {
		return TicketModule.readById(ticket_id, formula_id)
			.then((res) => {
				const new_response = {
					currency: res?.currency,
					customer: `(${res?.customer_code}) ${res?.customer_name}`,
					date: moment(res?.date).format('DD MMM YYYY'),
					date_of_communication: moment(res?.date_of_communication).format('DD MMM YYYY'),
					sample_product: res?.sample_product ? 'YES' : 'NO',
					status: res?.status,
					target: res?.category,
					target_date: moment(res?.target_date).format('DD MMM YYYY'),
					target_price: res?.target_price,
					information: res?.information,
					sample: res?.sample,
					history: res?.history,
					formula: res?.formula,
				};
				setDataTicket(new_response);
			})
			.catch(() => {
				setDataTicket({});
			})
			.finally(() => {});
	};

	return (
		<div>
			<div className='row pt-2 px-2 pb-4'>
				<div className='col-md-6 py-1'>
					<FormGroup id='date' label='Date'>
						<Input disabled value={data_ticket?.date} />
					</FormGroup>
				</div>
				<div className='col-md-6 py-1'>
					<FormGroup id='target_date' label='Target Date'>
						<Input disabled value={data_ticket?.target_date} />
					</FormGroup>
				</div>
				<div className='col-md-6 py-1'>
					<FormGroup id='sample_product' label='Sample Product'>
						<Input disabled value={data_ticket?.sample_product} />
					</FormGroup>
				</div>
				<div className='col-md-6 py-1'>
					<FormGroup id='date_of_communication' label='Date of Communication'>
						<Input disabled value={data_ticket?.date_of_communication} />
					</FormGroup>
				</div>
				<div className='col-md-6 py-1'>
					<FormGroup id='customer' label='Customer'>
						<Input disabled value={data_ticket?.customer} />
					</FormGroup>
				</div>
				<div className='col-md-6 py-1'>
					<FormGroup id='target_price' label='Target Price'>
						<Input disabled value={data_ticket?.target_price} />
					</FormGroup>
				</div>
				<div className='col-md-6 py-1'>
					<FormGroup id='target' label='Target'>
						<Input disabled value={data_ticket?.target} />
					</FormGroup>
				</div>
				<div className='col-md-6 py-1'>
					<FormGroup id='currency' label='Currency'>
						<Input disabled value={data_ticket?.currency} />
					</FormGroup>
				</div>
				<div className='col-md-6 py-1'>
					<FormGroup id='status' label='Status'>
						<Input disabled value={data_ticket?.status} />
					</FormGroup>
				</div>
			</div>
			<div className='row'>
				<div className='col-md-12'>
					<Card shadow='sm' style={{ borderRadius: '10px' }} hasTab tabButtonColor='info'>
						<CardTabItem id='tab_additional_information' title='Additional Information'>
							<CardBody>
								<table className='table table-sm table-modern'>
									<thead>
										<tr>
											<th scope='col'>#</th>
											<th scope='col'>Name</th>
											<th scope='col'>Value</th>
										</tr>
									</thead>
									<tbody>
										{data_ticket?.information?.map((item, index) => (
											<tr key={'add-infor.'.concat(index)}>
												<th
													key={'add-infor.'.concat(index, '.', 0)}
													scope='row'>
													{index + 1}
												</th>
												<td key={'add-infor.'.concat(index, '.', 1)}>
													{item.name}
												</td>
												<td key={'add-infor.'.concat(index, '.', 2)}>
													{item.value}
												</td>
											</tr>
										))}
									</tbody>
								</table>
							</CardBody>
						</CardTabItem>
						<CardTabItem id='tab_sample' title='Sample'>
							<CardBody>
								<table className='table table-sm table-modern'>
									<thead>
										<tr>
											<th scope='col'>#</th>
											<th scope='col'>Sample Code</th>
											<th scope='col'>Sample Name</th>
											<th scope='col'>Batch</th>
											<th scope='col'>Quantity</th>
											<th scope='col'>UOM</th>
										</tr>
									</thead>
									<tbody>
										{data_ticket?.sample?.map((item, index) => (
											<tr key={'sample.'.concat(index)}>
												<th
													key={'sample.'.concat(index, '.', 0)}
													scope='row'>
													{index + 1}
												</th>
												<td key={'sample.'.concat(index, '.', 1)}>
													{item.sampel_code}
												</td>
												<td key={'sample.'.concat(index, '.', 2)}>
													{item.sampel_name}
												</td>
												<td key={'sample.'.concat(index, '.', 3)}>
													{item.batch}
												</td>
												<td key={'sample.'.concat(index, '.', 4)}>
													{item.qty}
												</td>
												<td key={'sample.'.concat(index, '.', 5)}>
													{item.uom}
												</td>
											</tr>
										))}
									</tbody>
								</table>
							</CardBody>
						</CardTabItem>
						<CardTabItem id='tab_history' title='History'>
							<CardBody>
								<table className='table table-sm table-modern'>
									<thead>
										<tr>
											<th scope='col'>#</th>
											<th scope='col'>User</th>
											<th scope='col'>Date</th>
											<th scope='col'>Status</th>
											<th scope='col'>Remark</th>
										</tr>
									</thead>
									<tbody>
										{data_ticket?.history?.map((item, index) => (
											<tr key={'history.'.concat(index)}>
												<th
													key={'history.'.concat(index, '.', 0)}
													scope='row'>
													{index + 1}
												</th>
												<td key={'history.'.concat(index, '.', 1)}>
													{item.requester}
												</td>
												<td key={'history.'.concat(index, '.', 2)}>
													{moment(item.date).format(
														'YYYY-MM-DD HH:mm:ss',
													)}
												</td>
												<td key={'history.'.concat(index, '.', 3)}>
													{item.status}
												</td>
												<td key={'history.'.concat(index, '.', 4)}>
													{item.remark}
												</td>
											</tr>
										))}
									</tbody>
								</table>
							</CardBody>
						</CardTabItem>
					</Card>
				</div>
			</div>
			<div className='row'>
				<div className='col-md-12'>
					<Card shadow='sm' style={{ borderRadius: '10px' }}>
						<CardHeader borderSize={3} borderColor='success'>
							<CardLabel>
								<CardTitle>Formula</CardTitle>
							</CardLabel>
						</CardHeader>
						<CardBody>
							<Tabs
								id='controlled-tab-example'
								activeKey={key}
								onSelect={(k) => {
									setKey(k);
								}}
								className='mb-3'>
								{data_ticket?.formula?.map((item, index) => (
									<Tab
										key={'key-tab-'.concat(index)}
										eventKey={index}
										title={RenderTitleFormula(
											item.job_number ? item.job_number : 'formula',
											index,
										)}
										tabClassName={`${index}`}>
										<div className='col-12 mt-2'>
											<table className='table table-modern'>
												<thead>
													<tr>
														<th style={{ width: '200px' }}>
															Material Code
														</th>
														<th style={{ width: '200px' }}>
															Material Name
														</th>
														<th style={{ width: '150px' }}>Qty</th>
														<th style={{ width: '100px' }}>UoM</th>
														<th style={{ width: '200px' }}>Supplier</th>
														<th style={{ width: '100px' }}>Currency</th>
														<th style={{ width: '150px' }}>Price</th>
														<th style={{ width: '150px' }}>Notes</th>
													</tr>
												</thead>
												<tbody>
													{item?.formula?.map((iae, index_) => (
														<tr key={'tr-'.concat(index_)}>
															<td key={'td2-'.concat(index_)}>
																{iae.material_code}
															</td>
															<td key={'td-7-'.concat(index_)}>
																{iae.material_name}
															</td>
															<td key={'td3-'.concat(index_)}>
																{iae.qty}
															</td>
															<td key={'td4-'.concat(index_)}>KG</td>
															<td key={'td5-'.concat(index_)}>
																{iae.supplier}
															</td>
															<td key={'td6-'.concat(index_)}>
																{iae.currency}
															</td>
															<td key={'td7-'.concat(index_)}>
																{iae.price}
															</td>
															<td key={'td8-'.concat(index_)}>
																{iae.note}
															</td>
														</tr>
													))}
												</tbody>
											</table>
										</div>
										<br />
										<div className='col-12'>
											<hr
												style={{
													background: 'lime',
													color: 'lime',
													borderColor: 'lime',
													height: '1px',
												}}
											/>
											<CardLabel>
												<CardTitle>Parameter Trial</CardTitle>
											</CardLabel>
										</div>
										{item?.details?.map((dtls) => (
											<div className='col-12 mt-2'>
												{dtls.parameter_code == 'mixing_sequences' && (
													<table className='table table-modern'>
														<thead>
															<tr>
																<th>{dtls.parameter_name}</th>
																<th>Plan RPM</th>
																<th>Plan Minutes</th>
																<th>Actual RPM</th>
																<th>Actual Minutes</th>
															</tr>
														</thead>
														<tbody>
															{dtls?.detail?.map(
																(
																	detailformula,
																	index_dtlformula,
																) => (
																	<tr
																		key={'tr-mixing_sequences-'.concat(
																			index_dtlformula,
																		)}>
																		<td
																			key={'td2-dtls-'.concat(
																				index_dtlformula,
																			)}>
																			{detailformula.name}
																		</td>
																		<td
																			key={'td-plan_rpm-7-'.concat(
																				index_dtlformula,
																			)}>
																			{detailformula.plan_rpm}
																		</td>
																		<td
																			key={'td3-plan_minutes-'.concat(
																				index_dtlformula,
																			)}>
																			{
																				detailformula.plan_minutes
																			}
																		</td>
																		<td
																			key={'td-actual_rpm-7-'.concat(
																				index_dtlformula,
																			)}>
																			{
																				detailformula.actual_rpm
																			}
																		</td>
																		<td
																			key={'td3-actual_minutes-'.concat(
																				index_dtlformula,
																			)}>
																			{
																				detailformula.actual_minutes
																			}
																		</td>
																	</tr>
																),
															)}
														</tbody>
													</table>
												)}
												{dtls.parameter_code !== 'mixing_sequences' && (
													<table className='table table-modern'>
														<thead>
															<tr>
																<th>{dtls.parameter_name}</th>
																<th>Plan</th>
																<th>Actual</th>
															</tr>
														</thead>
														<tbody>
															{dtls?.detail?.map(
																(
																	detailformula,
																	index_dtlformula,
																) => (
																	<tr
																		key={'tr-dtls-'.concat(
																			index_dtlformula,
																		)}>
																		<td
																			key={'td2-dtls-'.concat(
																				index_dtlformula,
																			)}>
																			{detailformula.name}
																		</td>
																		<td
																			key={'td-dtls-material-7-'.concat(
																				index_dtlformula,
																			)}>
																			{detailformula.plan}
																		</td>
																		<td
																			key={'td3-dtls-'.concat(
																				index_dtlformula,
																			)}>
																			{detailformula.actual}
																		</td>
																	</tr>
																),
															)}
														</tbody>
													</table>
												)}
											</div>
										))}
										<br />
										<div className='col-12'>
											<hr
												style={{
													background: 'lime',
													color: 'lime',
													borderColor: 'lime',
													height: '1px',
												}}
											/>
											<CardLabel>
												<CardTitle>Evaluasi Miniplant</CardTitle>
											</CardLabel>
										</div>

										{item?.sensory?.map((snsr) => (
											<table className='table table-modern'>
												<thead>
													<tr>
														<th>{snsr.parameter_name}</th>
														<th>Value</th>
														<th>Remark</th>
													</tr>
												</thead>
												<tbody>
													{snsr?.detail?.map(
														(detailsensory, index_dtlsensory) => (
															<tr
																key={'tr-snsr-'.concat(
																	index_dtlsensory,
																)}>
																<td
																	key={'td2-snsr-'.concat(
																		index_dtlsensory,
																	)}>
																	{detailsensory.name}
																</td>
																<td
																	key={'td3-snsr-'.concat(
																		index_dtlsensory,
																	)}>
																	{detailsensory.value}
																</td>
																<td
																	key={'td4-snsr-'.concat(
																		index_dtlsensory,
																	)}>
																	{detailsensory.remark}
																</td>
															</tr>
														),
													)}
												</tbody>
											</table>
										))}
										<div className='col-12'>
											<hr
												style={{
													background: 'lime',
													color: 'lime',
													borderColor: 'lime',
													height: '1px',
												}}
											/>
											<CardLabel>
												<CardTitle>Status History</CardTitle>
											</CardLabel>

											<table className='table table-modern'>
												<thead>
													<tr>
														<th>Date</th>
														<th>Username</th>
														<th>Status</th>
														<th>Remark</th>
														<th>File</th>
													</tr>
												</thead>
												<tbody>
													{item?.history?.map(
														(dtl_history, index_history) => (
															<tr
																key={'tr-hstr-'.concat(
																	index_history,
																)}>
																<td
																	key={'td1-hstr-'.concat(
																		index_history,
																	)}>
																	{moment(
																		dtl_history.date,
																	).format('YYYY-MM-DD H:mm:ss')}
																</td>
																<td
																	key={'td2-hstr-'.concat(
																		index_history,
																	)}>
																	{dtl_history.requester}
																</td>
																<td
																	key={'td3-hstr-'.concat(
																		index_history,
																	)}>
																	{dtl_history.status}
																</td>
																<td
																	key={'td4-hstr-'.concat(
																		index_history,
																	)}>
																	{dtl_history.remark}
																</td>
																<td
																	key={'td5-hstr-'.concat(
																		index_history,
																	)}>
																	{dtl_history.data != null &&
																		dtl_history?.data.length >
																			0 &&
																		dtl_history.data?.map(
																			(dt_file, index_file) =>
																				dt_file.document && (
																					<Button color='success'>
																						<a
																							type='button'
																							download={`${dt_file.document}.${dt_file.extension}`}
																							href={`data:${dt_file.mimetype};base64,${dt_file.file}`}>
																							File{' '}
																							{index_file +
																								1}
																						</a>
																					</Button>
																				),
																		)}
																</td>
															</tr>
														),
													)}
												</tbody>
											</table>
										</div>
									</Tab>
								))}
							</Tabs>
						</CardBody>
					</Card>
				</div>
			</div>
		</div>
	);
};

DetailTicketAndFormulaComponent.propTypes = {
	ticket_id: PropTypes.string.isRequired,
	formula_id: PropTypes.string,
};
DetailTicketAndFormulaComponent.defaultProps = { formula_id: null };

export default DetailTicketAndFormulaComponent;
