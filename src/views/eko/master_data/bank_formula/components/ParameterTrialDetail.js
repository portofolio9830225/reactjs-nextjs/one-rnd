import React, { useRef, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import useDarkMode from '../../../../../hooks/useDarkMode';
import Button from '../../../../../components/bootstrap/Button';
import FormGroup from '../../../../../components/bootstrap/forms/FormGroup';
import InputGroup, { InputGroupText } from '../../../../../components/bootstrap/forms/InputGroup';
import Input from '../../../../../components/bootstrap/forms/Input';
import showNotification from '../../../../../components/extras/showNotification';
import CustomSelect from '../../../../../components/CustomSelect';
import AddInfromationModule from '../../../../../modules/AddInfromationModule';

const ParameterTrialDetail = ({
	initialValues,
	isReset,
	setReset,
	isReadOnly,
	onChange,
	parameter,
	index_paramater,
}) => {
	const { darkModeStatus } = useDarkMode();

	const [data, setData] = useState([]);
	const count = useRef(0);

	const [list_detail_parameter, setListDetailParameter] = useState([]);
	const [tempParameter, setTempParameter] = useState();

	const handleCustomAdd = () => {
		if (!parameter) {
			showNotification(
				'Information',
				"can't add an detail if the parameter is not selected",
				'danger',
			);
			return;
		}

		if (list_detail_parameter.length === 0) {
			showNotification(
				'Information',
				"detail parameter list does not exist. can't add more",
				'danger',
			);
			return;
		}

		if (data.length === list_detail_parameter.length) {
			showNotification(
				'Information',
				'has reached the maximum detail parameter limit that can be added',
				'danger',
			);
			return;
		}

		const new_data = [...data];
		new_data.push({ key: count.current, name: null, plan: '', actual: '' });
		setData(new_data);

		count.current += 1;
	};

	const handleCustomRemove = (index) => {
		const new_data = [...data];
		new_data.splice(index, 1);
		setData(new_data);

		if (onChange) {
			const result = getFormatValue(new_data);
			onChange(result, index_paramater);
		}
	};

	const onChangeParameterDetail = (e, index) => {
		const new_data = [...data];

		// find index
		const find_index = new_data.findIndex((find) => find.name?.value === e.value);

		// if it is the same as the previously selected data, the process will not continue
		if (find_index === index) {
			return;
		}

		// if the selected data already exists, then the process will not continue
		if (find_index !== -1) {
			showNotification(
				'Information',
				`${e.label}, the paramater detail has been selected, try choosing another one`,
				'danger',
			);
			return;
		}

		// set new data
		new_data[index] = {
			...new_data.at(index),
			name: e,
		};
		setData(new_data);

		if (onChange) {
			const result = getFormatValue(new_data);
			onChange(result, index_paramater);
		}
	};

	const onChangeText = (e, index, name) => {
		const new_data = [...data];
		new_data[index] = { ...new_data[index], [name]: e.target.value };

		new_data[index] = {
			...new_data.at(index),
			[name]: e.target.value,
		};
		setData(new_data);

		if (onChange) {
			const result = getFormatValue(new_data);
			onChange(result, index_paramater);
		}
	};

	useEffect(() => {
		if (parameter !== tempParameter) {
			fetchDataDetailParameter();
			setTempParameter(parameter);

			setData([]);
			count.current = 0;
		}

		return <div>{/* */}</div>;
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [parameter]);

	const fetchDataDetailParameter = async () => {
		const query = `category=${parameter}`;
		return AddInfromationModule.readSelect(query)
			.then((response) => {
				if (Array.isArray(response)) {
					setListDetailParameter(response);
				}
			})
			.catch(() => {})
			.finally(() => {});
	};

	useEffect(() => {
		if (isReset) {
			setData([]);
			count.current = 0;

			setReset(false);
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [isReset]);

	useEffect(() => {
		if (initialValues.length !== 0 && data.length === 0) {
			const { value_data, value_count } = getFormatSelect(initialValues);
			setData(value_data);
			count.current = value_count;
		}

		return <div>{/* */}</div>;
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [initialValues]);

	const getFormatSelect = (params) => {
		const _data = [];

		params.forEach((item, index) => {
			let new_data = { key: index, name: '', plan: '', actual: '' };

			if (item.name) {
				new_data = {
					...new_data,
					name: item.name,
				};
			}
			if (item.plan) {
				new_data = {
					...new_data,
					plan: item.plan,
				};
			}
			if (item.actual) {
				new_data = {
					...new_data,
					actual: item.actual,
				};
			}
			if (item.plan_rpm) {
				new_data = {
					...new_data,
					plan_rpm: item.plan_rpm,
				};
			}
			if (item.plan_minutes) {
				new_data = {
					...new_data,
					plan_minutes: item.plan_minutes,
				};
			}
			if (item.actual_rpm) {
				new_data = {
					...new_data,
					actual_rpm: item.actual_rpm,
				};
			}
			if (item.actual_minutes) {
				new_data = {
					...new_data,
					actual_minutes: item.actual_minutes,
				};
			}

			_data.push(new_data);
		});

		return {
			value_data: _data,
			value_count: _data.length,
		};
	};

	const getFormatValue = (params) => {
		const result = params
			.map((item) => {
				let new_value = {};

				if (item.name) {
					if (typeof item.name === 'object') {
						new_value = { ...new_value, name: item.name.label };
					} else {
						new_value = { ...new_value, name: item.name };
					}
				}
				if (item.plan) {
					new_value = { ...new_value, plan: item.plan };
				}
				if (item.actual) {
					new_value = { ...new_value, actual: item.actual };
				}
				if (item.plan_rpm) {
					new_value = {
						...new_value,
						plan_rpm: item.plan_rpm,
					};
				}
				if (item.plan_minutes) {
					new_value = {
						...new_value,
						plan_minutes: item.plan_minutes,
					};
				}
				if (item.actual_rpm) {
					new_value = {
						...new_value,
						actual_rpm: item.actual_rpm,
					};
				}
				if (item.actual_minutes) {
					new_value = {
						...new_value,
						actual_minutes: item.actual_minutes,
					};
				}
				return new_value;
			})
			.filter((item) => Object.keys(item).length !== 0);
		return result;
	};

	return (
		<div>
			{!isReadOnly && parameter && (
				<div className='row mb-1'>
					<div className='col-md-6'>
						<Button
							icon='Add'
							color='success'
							type='button'
							isLight={darkModeStatus}
							onClick={handleCustomAdd}
							style={{ minHeight: '2.95rem' }}>
							Add Detail Parameter
						</Button>
					</div>
				</div>
			)}
			<div className='row'>
				<div className='col-md-12'>
					{data.map((item, index) => (
						<div key={'div-'.concat(item.key)}>
							<div
								key={'div-border-'.concat(item.key)}
								className='col-md-12 border-bottom mb-1'
							/>
							{parameter != 'mixing_sequences' && (
								<InputGroup key={'input-group-'.concat(item.key)} className='mb-1'>
									<Button
										key={'button-remove-'.concat(item.key)}
										icon='Clear'
										color='danger'
										type='button'
										isLight={darkModeStatus}
										style={{ minHeight: '2.95rem' }}
										onClick={() => handleCustomRemove(index)}
										isDisable={isReadOnly}
									/>
									<InputGroupText key={'igt-'.concat(item.key)}>
										Detail {index + 1}
									</InputGroupText>
									<FormGroup key={'fg'.concat(item.key)} className='col-md-3'>
										<CustomSelect
											key={'cs'.concat(item.key)}
											options={list_detail_parameter}
											onChange={(e) => onChangeParameterDetail(e, index)}
											value={item.name}
											isDisable={isReadOnly}
											isSearchable={list_detail_parameter.length > 7}
										/>
									</FormGroup>
									<InputGroupText key={'ig-target-'.concat(item.key)}>
										Plan
									</InputGroupText>
									<FormGroup
										key={'fg-target-'.concat(item.key)}
										className='col-md-2'>
										<Input
											key={'input-'.concat(item.key)}
											style={{
												borderRadius: '4px',
												minHeight: '2.95rem',
											}}
											type='text'
											value={item.plan}
											onChange={(e) => onChangeText(e, index, 'plan')}
											disabled={isReadOnly}
										/>
									</FormGroup>
									<InputGroupText key={'ig-plan-'.concat(item.key)}>
										Actual
									</InputGroupText>
									<FormGroup
										key={'fg-plan-'.concat(item.key)}
										className='col-md-2'>
										<Input
											key={'input-'.concat(item.key)}
											style={{
												borderRadius: '4px',
												minHeight: '2.95rem',
											}}
											type='text'
											value={item.actual}
											onChange={(e) => onChangeText(e, index, 'actual')}
											disabled={isReadOnly}
										/>
									</FormGroup>
								</InputGroup>
							)}
							{parameter == 'mixing_sequences' && (
								<InputGroup key={'input-group0-'.concat(item.key)} className='mb-1'>
									<Button
										key={'button-remove-'.concat(item.key)}
										icon='Clear'
										color='danger'
										type='button'
										isLight={darkModeStatus}
										style={{ minHeight: '2.95rem' }}
										onClick={() => handleCustomRemove(index)}
										isDisable={isReadOnly}
									/>
									<InputGroupText key={'igt-'.concat(item.key)}>
										Detail {index + 1}
									</InputGroupText>
									<FormGroup
										key={'mix-squence'.concat(item.key)}
										className='col-md-3'>
										<CustomSelect
											key={'cs'.concat(item.key)}
											options={list_detail_parameter}
											onChange={(e) => onChangeParameterDetail(e, index)}
											value={item.name}
											isDisable={isReadOnly}
											isSearchable={list_detail_parameter.length > 7}
										/>
									</FormGroup>
									<InputGroupText key={'ig-target-'.concat(item.key)}>
										Plan RPM
									</InputGroupText>
									<FormGroup
										key={'fg-planrpm-'.concat(item.key)}
										className='col-md-1'>
										<Input
											key={'input-'.concat(item.key)}
											style={{
												borderRadius: '4px',
												minHeight: '2.95rem',
											}}
											type='text'
											value={item.plan_rpm}
											onChange={(e) => onChangeText(e, index, 'plan_rpm')}
											disabled={isReadOnly}
										/>
									</FormGroup>
									<InputGroupText key={'ig-plan-'.concat(item.key)}>
										Plan Minutes
									</InputGroupText>
									<FormGroup
										key={'fg-planminutes-'.concat(item.key)}
										className='col-md-1'>
										<Input
											key={'input-'.concat(item.key)}
											style={{
												borderRadius: '4px',
												minHeight: '2.95rem',
											}}
											type='text'
											value={item.plan_minutes}
											onChange={(e) => onChangeText(e, index, 'plan_minutes')}
											disabled={isReadOnly}
										/>
									</FormGroup>

									<InputGroupText key={'ig-target-'.concat(item.key)}>
										Actual RPM
									</InputGroupText>
									<FormGroup
										key={'fg-actualrpm-'.concat(item.key)}
										className='col-md-1'>
										<Input
											key={'input-'.concat(item.key)}
											style={{
												borderRadius: '4px',
												minHeight: '2.95rem',
											}}
											type='text'
											value={item.actual_rpm}
											onChange={(e) => onChangeText(e, index, 'actual_rpm')}
											disabled={isReadOnly}
										/>
									</FormGroup>
									<InputGroupText key={'ig-plan-'.concat(item.key)}>
										Actual Minutes
									</InputGroupText>
									<FormGroup
										key={'fg-actualminus-'.concat(item.key)}
										className='col-md-1'>
										<Input
											key={'input-'.concat(item.key)}
											style={{
												borderRadius: '4px',
												minHeight: '2.95rem',
											}}
											type='text'
											value={item.actual_minutes}
											onChange={(e) =>
												onChangeText(e, index, 'actual_minutes')
											}
											disabled={isReadOnly}
										/>
									</FormGroup>
								</InputGroup>
							)}
						</div>
					))}
				</div>
			</div>
		</div>
	);
};

ParameterTrialDetail.propTypes = {
	onChange: PropTypes.func,
	initialValues: PropTypes.instanceOf(Array),
	isReset: PropTypes.bool,
	setReset: PropTypes.func,
	isReadOnly: PropTypes.bool,
	parameter: PropTypes.string,
	index_paramater: PropTypes.number,
};
ParameterTrialDetail.defaultProps = {
	onChange: null,
	initialValues: [],
	isReset: false,
	setReset: null,
	isReadOnly: false,
	parameter: null,
	index_paramater: 0,
};

export default ParameterTrialDetail;
