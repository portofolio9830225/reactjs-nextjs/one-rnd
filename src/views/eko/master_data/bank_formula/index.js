import React, { useState, useEffect, useMemo } from 'react';
import PropTypes from 'prop-types';
import Swal from 'sweetalert2';
import { useTranslation } from 'react-i18next';
import PageWrapper from '../../../../layout/PageWrapper/PageWrapper';
import Page from '../../../../layout/Page/Page';
import PageLayoutHeader from '../../../../pages/common/Headers/PageLayoutHeader';
import Card, {
	CardBody,
	CardHeader,
	CardLabel,
	CardTitle,
} from '../../../../components/bootstrap/Card';
import Input from '../../../../components/bootstrap/forms/Input';
import Button from '../../../../components/bootstrap/Button';
import DarkDataTable from '../../../../components/DarkDataTable';
import useDarkMode from '../../../../hooks/useDarkMode';
import showNotification from '../../../../components/extras/showNotification';
import { getRequester } from '../../../../helpers/helpers';
import Modal, { ModalBody, ModalHeader, ModalTitle } from '../../../../components/bootstrap/Modal';
import BankFormulaModule from '../../../../modules/eko/BankFormulaModule';
import priorityModule from '../../../../modules/PriorityModule';
import CurrencyModule from '../../../../modules/CurrencyModule';
import MaterialModule from '../../../../modules/MaterialModule';
import VendorModule from '../../../../modules/VendorModule';
import UomModule from '../../../../modules/UomModule';
import FormulaTab from './components/FormulaTab';
import DetailTicketAndFormulaComponent from './components/DetailTicketAndFormulaComponent';

const handleSubmitFormulasi = (values, handleReloadData, handleShowFormTrial) => {
	BankFormulaModule.formulasi(values)
		.then((res) => {
			showNotification('Success!', res.status, 'success');
			handleReloadData();
			handleShowFormTrial('', '');
		})
		.catch((err) => {
			showNotification('Warning!', err, 'danger');
		});
	return values;
};

const handleSubmitDelete = (values, handleReloadData) => {
	BankFormulaModule.deleteFormula(values)
		.then((res) => {
			showNotification('Success!', res.status, 'success');
			handleReloadData();
		})
		.catch((err) => {
			showNotification('Warning!', err, 'danger');
		});
	return values;
};

const submitDelete = async (row, handleReloadData) => {
	const { username } = getRequester();
	const { _id } = row;
	if (_id) {
		const val = {};
		val._id = _id;
		val.requester = username;
		return Swal.fire({
			title: 'Are you sure?',
			text: 'next to the trial process',
			icon: 'info',
			showCancelButton: true,
			confirmButtonText: 'Yes',
		}).then((result) => {
			if (result.value) {
				handleSubmitDelete(val, handleReloadData);
			}
		});
	}
	return _id;
};

const FormButton = (dt) => {
	const { handleShowFormulasi, row, handleReloadData } = dt;
	const { username } = getRequester();
	const initialValues = {
		loading: false,
		target_price: row.target_price,
		sample_product: row.sample_product,
		category: row.category,
		currency: row.currency,
		customer_name: row.customer_name,
		customer_code: row.customer_code,
		information: row.information,
		sample: row.sample,
		date_of_communication: row.date_of_communication,
		target_date: row.target_date,
		ticket_id: row._id,
		document_number: row.document_number,
		_id: row._id,
	};

	if (row.pic == username) {
		return (
			<>
				<Button
					className='me-2'
					icon='edit'
					type='button'
					color='success'
					onClick={() => {
						handleShowFormulasi('Save Bank Formulasi', row);
					}}>
					Edit
				</Button>
				<Button
					className='me-2'
					icon='trash'
					type='button'
					color='danger'
					onClick={() => {
						submitDelete(row, handleReloadData);
					}}>
					Delete
				</Button>
			</>
		);
	}

	return <FormCustomModalDetail initialValues={initialValues} />;
};

const CustomButton = (dt) => {
	const { row, listPriority, handleReloadData, handleShowFormulasi } = dt;

	// restructure the obj
	const initialValues = {};
	initialValues.id = row._id;

	return (
		<FormButton
			initialValues={initialValues}
			listPriority={listPriority}
			handleReloadData={handleReloadData}
			handleShowFormulasi={handleShowFormulasi}
			row={row}
		/>
	);
};

const CustomDataTable = ({
	listPriority,
	data,
	loading,
	totalRows,
	handlePageChange,
	handlePerRowsChange,
	handleReloadData,
	handleShowFormulasi,
}) => {
	const { darkModeStatus } = useDarkMode();

	const columns = useMemo(
		() => [
			{
				name: 'FG Name',
				selector: (row) => row.result_fg_name,
				sortable: true,
			},
			{
				name: 'FG Code',
				selector: (row) => row.result_fg_code,
				sortable: true,
			},
			{
				name: 'Created By',
				selector: (row) => row.pic_name,
				sortable: true,
			},
			{
				name: 'Action',
				width: '300px',
				// eslint-disable-next-line react/no-unstable-nested-components
				cell: (dt) => {
					return (
						<CustomButton
							row={dt}
							listPriority={listPriority}
							handleReloadData={handleReloadData}
							handleShowFormulasi={handleShowFormulasi}
						/>
					);
				},
			},
		],
		[listPriority, handleReloadData, handleShowFormulasi],
	);

	return (
		<DarkDataTable
			columns={columns}
			data={data}
			progressPending={loading}
			pagination
			paginationServer
			paginationTotalRows={totalRows}
			onChangeRowsPerPage={handlePerRowsChange}
			onChangePage={handlePageChange}
			theme={darkModeStatus ? 'custom_dark' : 'light'}
		/>
	);
};

const Formulasi = () => {
	const { darkModeStatus } = useDarkMode();
	const [data, setData] = useState([]);
	const [listPriority, setPriority] = useState([]);
	const { username, person_name } = getRequester();

	const [loading, setLoading] = useState(false);
	const [totalRows, setTotalRows] = useState(0);
	const [perPage, setPerPage] = useState(10);
	const [page, setPage] = useState(1);
	const [reloadData, setReloadData] = useState(false);
	const [search, setSearch] = useState(null);
	const [formTrialShow, setFormTrialShow] = useState(false);

	const [titleTrial, setTitleTrial] = useState(null);
	const [dataFormula, setDataFormula] = useState(null);
	const [initialTab, setInitialTab] = useState([
		{ title: 'Formula', index: 1, _id: null, due_date_miniplant: '' },
	]);
	const [listUom, setListUom] = useState([{ label: 'KG', value: 'KG' }]);
	const [listCurrency, setCurrency] = useState([]);
	const [listMaterial, setMaterial] = useState([]);
	const [listVendor, setVendor] = useState([]);
	const [listFormulaCopy, setListFormulaCopy] = useState([]);
	const [resultFgName, setResultFgName] = useState(null);
	const [resultFgCode, setResultFgCode] = useState(null);

	const [newDataFormula, setNewDataFormula] = useState([]);
	const [loadingSubmit, setLoadingSubmit] = useState(false);

	const handleShowFormulasi = async (title, data_formula) => {
		setTitleTrial(title);
		if (data_formula) {
			setDataFormula(data_formula);
			setLoadingSubmit(true);
			const query = `id=${data_formula._id}`;
			BankFormulaModule.readDetail(query)
				.then((res) => {
					setLoadingSubmit(false);
					if (res.length > 0) {
						setInitialTab(res);
						setResultFgName(res[0].result_fg_name);
						setResultFgCode(res[0].result_fg_code);
					}
					setFormTrialShow(!formTrialShow);
				})
				.catch(() => {})
				.finally(() => {
					setLoadingSubmit(false);
				});
		} else {
			setFormTrialShow(!formTrialShow);
			setResultFgName(null);
			setResultFgCode(null);
			setInitialTab([
				{
					title: 'New Formula',
					index: 1,
					_id: null,
					due_date_miniplant: '',
				},
			]);
		}
	};

	const fetchFormula = async (nPage, nPerPage, fg_name) => {
		setLoading(true);
		let query = ``;
		if (fg_name) {
			query = `&result_fg_name=${fg_name}`;
		}
		return BankFormulaModule.read(`page=${nPage}&sizePerPage=${nPerPage}${query}`).then(
			(res) => {
				setData(res.foundData);
				setTotalRows(res.countData);
				setLoading(false);
			},
		);
	};

	const fetchPriority = async () => {
		return priorityModule.readSelect().then((res) => {
			setPriority(res);
		});
	};
	const fetchUom = async () => {
		const query = ``;
		return UomModule.readSelect(query).then((res) => {
			setListUom(res);
		});
	};
	const fetchMaterial = async () => {
		const query = `material_type=ZRAW`;
		return MaterialModule.readSelect(query).then((res) => {
			setMaterial(res);
		});
	};
	const fetchVendor = async () => {
		const query = ``;
		return VendorModule.readSelect(query).then((res) => {
			setVendor(res);
		});
	};
	const fetchCurrency = async () => {
		const query = ``;
		return CurrencyModule.readSelect(query).then((res) => {
			setCurrency(res);
		});
	};
	const fetchAllFormula = async () => {
		const query = ``;
		return BankFormulaModule.readSelect(query).then((res) => {
			setListFormulaCopy(res);
		});
	};

	const handleReloadData = () => {
		setReloadData(!reloadData);
	};

	const submitForm = async () => {
		if (newDataFormula.length == 0) {
			showNotification('Information', `Must fill in the parameters and details`, 'danger');
			return newDataFormula;
		}

		if ([null, ''].includes(resultFgCode)) {
			showNotification('Information', `Please completed FG Code`, 'danger');
			return newDataFormula;
		}

		if ([null, ''].includes(resultFgName)) {
			showNotification('Information', `Please completed FG Name`, 'danger');
			return newDataFormula;
		}

		const checkFormula = await Promise.all(
			newDataFormula
				.map((x, index) => {
					if (!x.formula) {
						return `<br/>Material not completed on idx${index}`;
					}
					if (!x.details) {
						return `<br/>Parameter not completed on idx${index}`;
					}
					return '';
				})
				.filter((a) => a),
		);

		if (checkFormula.length > 0) {
			return Swal.fire({
				title: 'Warning',
				html: checkFormula.toString(),
				icon: 'warning',
			});
		}

		const val = {};
		val._id = dataFormula?._id;
		val.formula = newDataFormula;
		val.fg_code = resultFgCode;
		val.fg_name = resultFgName;
		val.requester = username;
		val.person_name = person_name;
		console.log(val);
		return Swal.fire({
			title: 'Are you sure?',
			text: 'Please check your entries !',
			icon: 'info',
			showCancelButton: true,
			confirmButtonText: 'Yes',
		}).then((result) => {
			if (result.value) {
				handleSubmitFormulasi(val, handleReloadData, handleShowFormulasi);
			}
		});
	};

	const searchSubmit = () => {
		fetchFormula(1, 10, search ?? null);
	};

	const resetSubmit = () => {
		setSearch('');
		fetchFormula(1, 10, null);
	};

	useEffect(() => {
		fetchFormula(page, perPage, null, null);
		fetchPriority();
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [page, perPage, reloadData]);

	useEffect(() => {
		fetchFormula(1, 10, null, null);
		fetchVendor();
		fetchPriority();
		fetchMaterial();
		fetchCurrency();
		fetchAllFormula();
		fetchUom();
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	const onChangeTabFormula = (e) => {
		setNewDataFormula(e);
	};

	const { t } = useTranslation('menu');
	const [title] = useState({ title: 'Bank Formulasi' });
	return (
		<PageWrapper title={title.title}>
			<PageLayoutHeader />
			<Page container='fluid'>
				{!formTrialShow && (
					<Card stretch>
						<CardHeader borderSize={1}>
							<CardLabel>
								<CardTitle>{t('bank_formula')}</CardTitle>
							</CardLabel>
							<Button
								icon='Add'
								type='button'
								color='primary'
								onClick={() => {
									handleShowFormulasi('Add Bank Formula', null);
								}}>
								Add
							</Button>
						</CardHeader>
						<CardBody>
							<div className='row pb-4'>
								<div className='col-sm-2 col-md-2'>
									<Input
										type='text'
										onInput={(value) => {
											setSearch(value.target.value);
										}}
										value={search}
										placeholder='Keyword of FG Name'
									/>
								</div>
								<div className='col-sm-2 col-md-2'>
									<Button
										icon='Search'
										color='info'
										type='button'
										className='mx-1'
										onClick={searchSubmit}
										isLight={darkModeStatus}>
										Search
									</Button>
									<Button
										icon='Refresh'
										color='warning'
										type='button'
										className='mx-1'
										onClick={resetSubmit}
										isLight={darkModeStatus}>
										Reset
									</Button>
								</div>
							</div>

							<CustomDataTable
								data={data}
								loading={loading}
								totalRows={totalRows}
								listPriority={listPriority}
								handlePageChange={setPage}
								handlePerRowsChange={setPerPage}
								handleReloadData={handleReloadData}
								handleShowFormulasi={handleShowFormulasi}
							/>
						</CardBody>
					</Card>
				)}
				{formTrialShow && (
					<Card stretch>
						<CardHeader borderSize={1}>
							<CardLabel>
								<CardTitle>{titleTrial}</CardTitle>
							</CardLabel>
							<Button
								icon='Close'
								type='button'
								color='danger'
								onClick={() => {
									handleShowFormulasi(null, null);
								}}
							/>
						</CardHeader>
						<CardBody>
							<div className='row'>
								<div className='col-sm-6 col-md-6'>
									<table className='table table-border'>
										<tbody>
											<tr>
												<td>
													<b>FG Code</b>
												</td>
												<td>
													<Input
														type='text'
														onInput={(value) => {
															setResultFgCode(value.target.value);
														}}
														value={resultFgCode}
													/>
												</td>
											</tr>
											<tr>
												<td>
													<b>FG Name</b>
												</td>
												<td>
													<Input
														type='text'
														onInput={(value) => {
															setResultFgName(value.target.value);
														}}
														value={resultFgName}
													/>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<div className='row'>
								<div className='col-12 mb-3 g-4'>
									<FormulaTab
										initialValues={initialTab}
										onChange={(values) => onChangeTabFormula(values)}
										listMaterial={listMaterial}
										listVendor={listVendor}
										listUom={listUom}
										listCurrency={listCurrency}
										listFormulaCopy={listFormulaCopy}
									/>
								</div>
								<div className='col-md-12'>
									<Button
										icon='Save'
										type='submit'
										color='primary'
										onClick={() => {
											submitForm();
										}}
										className='float-end'>
										{titleTrial}
									</Button>
								</div>
							</div>
						</CardBody>
					</Card>
				)}

				<Modal
					isOpen={loadingSubmit}
					size='sm'
					isCentered
					setIsOpen={() => {}}
					isStaticBackdrop>
					<ModalBody
						style={{ backgroundColor: '#6c5dd3', color: 'white' }}
						className='text-center'>
						<button className='btn btn-primary' type='button' disabled>
							<span
								className='spinner-grow spinner-grow-sm'
								role='status'
								aria-hidden='true'
							/>
							&nbsp;
							<span
								className='spinner-grow spinner-grow-sm'
								role='status'
								aria-hidden='true'
							/>
							&nbsp;
							<span
								className='spinner-grow spinner-grow-sm'
								role='status'
								aria-hidden='true'
							/>
							&nbsp;
							<span className='sr-only'>Loading...</span>
						</button>
					</ModalBody>
				</Modal>
			</Page>
		</PageWrapper>
	);
};

CustomDataTable.propTypes = {
	listPriority: PropTypes.oneOfType([PropTypes.instanceOf(Object)]),
	data: PropTypes.arrayOf(PropTypes.instanceOf(Object)),
	loading: PropTypes.bool,
	totalRows: PropTypes.number,
	handlePageChange: PropTypes.func,
	handlePerRowsChange: PropTypes.func,
	handleReloadData: PropTypes.func,
	handleShowFormulasi: PropTypes.func,
};
CustomDataTable.defaultProps = {
	listPriority: {},
	data: [],
	loading: false,
	totalRows: 0,
	handlePageChange: null,
	handlePerRowsChange: null,
	handleReloadData: null,
	handleShowFormulasi: null,
};

const FormCustomModalDetail = ({ initialValues }) => {
	const { darkModeStatus } = useDarkMode();
	const [isOpen, setOpen] = useState(false);

	return (
		<>
			<Button
				color='info'
				type='button'
				isLight={darkModeStatus}
				className='m-1'
				onClick={() => setOpen(true)}>
				i
			</Button>

			<Modal
				isOpen={isOpen}
				setIsOpen={setOpen}
				size='xl'
				titleId='modal-crud-evaluation'
				isStaticBackdrop>
				<ModalHeader setIsOpen={setOpen} className='p-4'>
					<ModalTitle id='modal-crud-evaluation'>
						Detail Ticket - ({initialValues?.document_number}){' '}
						{initialValues?.customer_name}
					</ModalTitle>
				</ModalHeader>
				<ModalBody>
					<DetailTicketAndFormulaComponent ticket_id={initialValues.ticket_id} />
				</ModalBody>
			</Modal>
		</>
	);
};

FormCustomModalDetail.propTypes = {
	initialValues: PropTypes.instanceOf(Object),
};
FormCustomModalDetail.defaultProps = {
	initialValues: null,
};

export default Formulasi;
