import axios from 'axios';
import authHeader from './auth-header';

const API_URL_DEFAULT = process.env.REACT_APP_API;

const read = async (query) => {
	return axios.get(`${API_URL_DEFAULT}schedule/?${query}`, {
		headers: await authHeader(),
	});
};
const schedule = async (payload) => {
	return axios.put(`${API_URL_DEFAULT}schedule`, payload, {
		headers: await authHeader(),
	});
};

export default {
	read,
	schedule,
};
