import axios from 'axios';
import authHeader from './auth-header';

const API_URL_DEFAULT = process.env.REACT_APP_API;

const read = async (query) => {
	return axios.get(`${API_URL_DEFAULT}formula/?${query}`, {
		headers: await authHeader(),
	});
};

const readDetail = async (query) => {
	return axios.get(`${API_URL_DEFAULT}formula/detail/?${query}`, {
		headers: await authHeader(),
	});
};

const readSelect = async (query) => {
	return axios.get(`${API_URL_DEFAULT}formula/select/?${query}`, {
		headers: await authHeader(),
	});
};

const formulasi = async (payload) => {
	return axios.put(`${API_URL_DEFAULT}formula`, payload, {
		headers: await authHeader(),
	});
};

const nextTrial = async (payload) => {
	return axios.put(`${API_URL_DEFAULT}formula/next/trial`, payload, {
		headers: await authHeader(),
	});
};

const deleteFormula = async (payload) => {
	return axios.delete(`${API_URL_DEFAULT}formula`, {
		data: payload,
		headers: await authHeader(),
	});
};

const readFormula = async (query) => {
	return axios.get(`${API_URL_DEFAULT}formula/data`, {
		headers: await authHeader(),
		params: query,
	});
};

const verificationFormula = async (payload) => {
	return axios.post(`${API_URL_DEFAULT}formula/verification`, payload, {
		headers: await authHeader(),
	});
};

const sensoryFormula = async (payload) => {
	return axios.put(`${API_URL_DEFAULT}formula/sensory`, payload, {
		headers: await authHeader(),
	});
};

const sensoryPlantFormula = async (payload) => {
	return axios.put(`${API_URL_DEFAULT}formula/sensory/plant`, payload, {
		headers: await authHeader(),
	});
};

const updateStatusFormula = async (payload) => {
	return axios.put(`${API_URL_DEFAULT}formula/update-status`, payload, {
		headers: await authHeader(),
	});
};

export default {
	read,
	readFormula,
	formulasi,
	readDetail,
	nextTrial,
	readSelect,
	deleteFormula,
	verificationFormula,
	sensoryFormula,
	sensoryPlantFormula,
	updateStatusFormula,
};
