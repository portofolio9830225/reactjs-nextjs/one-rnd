// eslint-disable-next-line eslint-comments/disable-enable-pair
/* eslint-disable prettier/prettier */
import axios from 'axios';
import authHeader from '../../auth-header';

const modelName = 'ticket';
const API_URL_DEFAULT = process.env.REACT_APP_API;
const publicKey = process.env.REACT_APP_PUBLIC_KEY;
const token = JSON.parse(localStorage.getItem('accessToken'));
const applicationToken = JSON.parse(localStorage.getItem('appToken'));

const list = async (query_string) => {
	return axios.get(`${API_URL_DEFAULT}${modelName}/?${query_string}`, {
		headers: await authHeader(),
	});
};

const uploadFile = async (payload) => {
	return axios.post(`${API_URL_DEFAULT}${modelName}/upload/?${payload._id}`, payload.formData, {
		headers: {
			'x-public-key': publicKey,
			'x-application-token': `Bearer ${applicationToken.appToken}`,
			'x-user-token': `Bearer ${token.accessToken}`,
			'Content-Type': 'application/x-www-form-urlencoded',
			'Access-Control-Allow-Origin': '*',
		},
	});
};

const uploadFileSelesaiAnalisa = async (payload) => {
	return axios.post(
		`${API_URL_DEFAULT}${modelName}/uploadSelesai/?${payload._id}`,
		payload.formData,
		{
			headers: {
				'x-public-key': publicKey,
				'x-application-token': `Bearer ${applicationToken.appToken}`,
				'x-user-token': `Bearer ${token.accessToken}`,
				'Content-Type': 'application/x-www-form-urlencoded',
				'Access-Control-Allow-Origin': '*',
			},
		},
	);
};

const store = async (payload) => {
	return axios.post(`${API_URL_DEFAULT}${modelName}`, payload, { headers: await authHeader() });
};

const update = async (payload) => {
	return axios.put(`${API_URL_DEFAULT}${modelName}`, payload, { headers: await authHeader() });
};

const destroy = async (payload) => {
	return axios.delete(`${API_URL_DEFAULT}${modelName}`, {
		data: payload,
		headers: await authHeader(),
	});
};

const prosesFormulasi = async (payload) => {
	return axios.post(
		`${API_URL_DEFAULT}${modelName}/proses-formulasi`,
		{ data: payload },
		{
			headers: await authHeader(),
		},
	);
};

export default {
	list,
	store,
	update,
	destroy,
	uploadFile,
	uploadFileSelesaiAnalisa,
	prosesFormulasi,
};
