// eslint-disable-next-line eslint-comments/disable-enable-pair
/* eslint-disable prettier/prettier */
import axios from 'axios';
import authHeader from '../../auth-header';

const modelName = 'ticket';
const API_URL_DEFAULT = process.env.REACT_APP_API;

const getTicket = async (query_string) => {
	return axios.get(`${API_URL_DEFAULT}${modelName}/get-ticket/?${query_string}`, {
		headers: await authHeader(),
	});
};

export default {
    getTicket
}