// eslint-disable-next-line eslint-comments/disable-enable-pair
/* eslint-disable prettier/prettier */
import axios from 'axios';
import authHeader from '../../auth-header';

const API_URL_DEFAULT = process.env.REACT_APP_API;

const list = async () => {
	return axios.get(`${API_URL_DEFAULT}user/detail/filter/?department=RND`, {
		headers: await authHeader(),
	});
};

export default {
	list,
};
