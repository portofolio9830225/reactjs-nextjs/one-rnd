// eslint-disable-next-line eslint-comments/disable-enable-pair
/* eslint-disable prettier/prettier */
import axios from 'axios';
import authHeader from '../../auth-header';

const modelName = 'sample-incoming';
const API_URL_DEFAULT = process.env.REACT_APP_API;
const publicKey = process.env.REACT_APP_PUBLIC_KEY; 
const token = JSON.parse(localStorage.getItem('accessToken'));
const applicationToken = JSON.parse(localStorage.getItem('appToken'));

const list = async (query) => {
	return axios.get(`${API_URL_DEFAULT}${modelName}`, {
		headers: await authHeader(),
		params: query,
	});
};

const download = async (query_string) => {
	return axios.get(`${API_URL_DEFAULT}${modelName}/download/?${query_string}`, {
		headers: await authHeader(),
	});
};


const create = async (payload) => {
	return axios.post(`${API_URL_DEFAULT}${modelName}`, payload, {
		headers: {
			'x-public-key': publicKey,
			'x-application-token': `Bearer ${applicationToken.appToken}`,
			'x-user-token': `Bearer ${token.accessToken}`,
			'Content-Type': 'application/x-www-form-urlencoded',
			'Access-Control-Allow-Origin': '*',
		},
	});
};

const copy = async (payload) => {
	return axios.post(`${API_URL_DEFAULT}${modelName}/copy`, payload, {
		headers: {
			'x-public-key': publicKey,
			'x-application-token': `Bearer ${applicationToken.appToken}`,
			'x-user-token': `Bearer ${token.accessToken}`,
			'Content-Type': 'application/x-www-form-urlencoded',
			'Access-Control-Allow-Origin': '*',
		},
	});
};

const update = async (payload) => {
	return axios.put(`${API_URL_DEFAULT}${modelName}`, payload, { headers: await authHeader() });
};

const destroy = async (payload) => {
	return axios.delete(`${API_URL_DEFAULT}${modelName}`, {
		data: payload,
		headers: await authHeader(),
	});
};


export default {
	list,
    create,
	copy,
	update,
	destroy,
	download
};
