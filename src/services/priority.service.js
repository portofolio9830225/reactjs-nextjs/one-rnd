import axios from 'axios';
import authHeader from './auth-header';

const API_URL_DEFAULT = process.env.REACT_APP_API;

const readSelect = async (query) => {
	return axios.get(`${API_URL_DEFAULT}priority/select/?${query}`, {
		headers: await authHeader(),
	});
};

export default {
	readSelect,
};
