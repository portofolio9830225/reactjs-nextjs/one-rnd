import axios from 'axios';
import authHeader from './auth-header';

const API_URL_DEFAULT = process.env.REACT_APP_API;

const startTrial = async (payload) => {
	return axios.put(`${API_URL_DEFAULT}trial_plant/start`, payload, {
		headers: await authHeader(),
	});
};
const finishTrial = async (payload) => {
	return axios.put(`${API_URL_DEFAULT}trial_plant/finish`, payload, {
		headers: await authHeader(),
	});
};

export default {
	startTrial,
	finishTrial,
};
