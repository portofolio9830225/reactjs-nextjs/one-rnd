import axios from 'axios';
import authHeader from './auth-header';

const API_URL_DEFAULT = process.env.REACT_APP_API;

const readById = async (id, ticket_id) => {
	let additional_query = '';
	if (ticket_id) {
		additional_query = `/?ticket_id=${ticket_id}`;
	}
	return axios.get(`${API_URL_DEFAULT}tickets/id/${id}${additional_query}`, {
		headers: await authHeader(),
	});
};

export default { readById };
