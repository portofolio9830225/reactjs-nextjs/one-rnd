import axios from 'axios';
import authHeader from '../auth-header';

const API_URL_DEFAULT = process.env.REACT_APP_API;

const list = async (page, countPerPage, query) => {
	return axios.get(
		`${API_URL_DEFAULT}azhar/formula?page=${page}&sizePerPage=${countPerPage}${query}`,
		{
			headers: await authHeader(),
		},
	);
};

export default { list };
