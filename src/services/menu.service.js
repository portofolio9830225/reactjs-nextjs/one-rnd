import axios from 'axios';
import authHeader from './auth-header';

const API_URL_DEFAULT = process.env.REACT_APP_API;

const create = async (payload) => {
	return axios.post(`${API_URL_DEFAULT}menu`, payload, { headers: await authHeader() });
};
const readMenu = async (query_string) => {
	return axios.get(`${API_URL_DEFAULT}menu/?${query_string}`, { headers: await authHeader() });
};
const edit = async (payload) => {
	return axios.put(`${API_URL_DEFAULT}menu`, payload, { headers: await authHeader() });
};

const deleteMenu = async (payload) => {
	return axios.delete(`${API_URL_DEFAULT}menu`, { data: payload, headers: await authHeader() });
};

const create_submenu = async (payload) => {
	return axios.post(`${API_URL_DEFAULT}submenu`, payload, { headers: await authHeader() });
};
const readSubmenu = async (payload) => {
	const url = `${API_URL_DEFAULT}submenu/${payload}`;
	return axios.get(url, { headers: await authHeader() });
};
const edit_submenu = async (payload) => {
	return axios.put(`${API_URL_DEFAULT}submenu`, payload, { headers: await authHeader() });
};

const delete_submenu = async (payload) => {
	return axios.delete(`${API_URL_DEFAULT}submenu`, {
		data: payload,
		headers: await authHeader(),
	});
};

export default {
	create,
	readMenu,
	edit,
	deleteMenu,
	readSubmenu,
	create_submenu,
	edit_submenu,
	delete_submenu,
};
