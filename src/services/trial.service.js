import axios from 'axios';
import authHeader from './auth-header';

const API_URL_DEFAULT = process.env.REACT_APP_API;

const read = async (query) => {
	return axios.get(`${API_URL_DEFAULT}trial/?${query}`, {
		headers: await authHeader(),
	});
};
const startTrial = async (payload) => {
	return axios.put(`${API_URL_DEFAULT}trial/start`, payload, {
		headers: await authHeader(),
	});
};
const finishTrial = async (payload) => {
	return axios.put(`${API_URL_DEFAULT}trial/finish`, payload, {
		headers: await authHeader(),
	});
};

export default {
	read,
	startTrial,
	finishTrial,
};
