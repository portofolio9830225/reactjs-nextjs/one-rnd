import React from 'react';
import PropTypes, { arrayOf } from 'prop-types';
import Card, { CardBody, CardHeader, CardLabel, CardTitle } from './bootstrap/Card';
import FormGroup from './bootstrap/forms/FormGroup';
import Input from './bootstrap/forms/Input';
import InputGroup, { InputGroupText } from './bootstrap/forms/InputGroup';
import Checks, { ChecksGroup } from './bootstrap/forms/Checks';
import DynamicSelect from './DynamicSelect';

const RenderItemCheck = ({ item, onChange, onChangeSelection }) => {
	const handleChangeDetail = (id, type, is_active, e) => {
		const arrStandard = ['range_first', 'range_second', 'standard_value'];
		const targetName = e && e.target.name;
		const standardValue =
			e && e.target && arrStandard.includes(targetName) ? e.target.name : null;
		const value = e && e.target ? e.target.value : null;
		return onChange(id, type, is_active, value, standardValue);
	};
	const onChangeData = (newData, id) => {
		return onChangeSelection(newData, id);
	};
	const getLoadOption = () => {
		const newRes = new Promise((resolve, reject) => {
			try {
				resolve([]);
			} catch (e) {
				reject(new Error({ error: true }));
			}
		});
		return newRes;
	};
	const errors = {};
	const isInlineCustom = true;
	return (
		item.length > 0 &&
		item.map((it) => {
			return (
				<div className='row g-2' key={`row${it.id}`}>
					<div className='col-md-2'>
						<Card stretch className='mb-0'>
							<CardHeader borderSize={3} borderColor='success'>
								<CardLabel>
									<CardTitle>
										<span>{it.parentName}</span>
									</CardTitle>
								</CardLabel>
							</CardHeader>
							<CardBody>
								<h4>{it.name}</h4>
							</CardBody>
						</Card>
					</div>
					{it.detail &&
						it.detail.length > 0 &&
						it.detail.map((z) => {
							// eslint-disable-next-line no-unneeded-ternary
							const isValid = z.value || z.value == 0 ? true : false;
							return (
								<div className='col-md-2' key={`it-c-${it.id}-${z.type}`}>
									{z.type === 'data_type' && (
										<Card stretch className='mb-0'>
											<CardHeader borderSize={3} borderColor='success'>
												<CardLabel>
													<CardTitle>
														<>
															Data Type <small>{it.name}</small>
														</>
													</CardTitle>
												</CardLabel>
											</CardHeader>
											<CardBody>
												{z.is_active && (
													<FormGroup
														label='Data Type'
														className='col-md-12'>
														{z.option && (
															<select
																name='data_type'
																className='form-control form-control-xs'
																onChange={(target) =>
																	handleChangeDetail(
																		it.id,
																		z.type,
																		null,
																		target,
																	)
																}
																value={z.value || ''}
																placeholder='Select'>
																<option value='' disabled>
																	Select
																</option>
																{z.option &&
																	z.option.length > 0 &&
																	z.option.map((opt) => {
																		return (
																			<option
																				value={opt}
																				key={`it-c-opt-dt-${opt}-${it.id}-${z.type}`}>
																				{opt}
																			</option>
																		);
																	})}
															</select>
														)}
														{z.value == 'selection' && z.value && (
															<>
																<br />
																<DynamicSelect
																	data={z.option_value || []}
																	select_items={
																		z.option_value || []
																	}
																	onChange={(kk) =>
																		onChangeData(kk, it.id)
																	}
																	readOnly={false}
																	isEdit={false}
																	errors={errors}
																	defaultOptions
																	loadOptions={getLoadOption}
																	placeholder='Choose ...'
																/>
															</>
														)}
													</FormGroup>
												)}
											</CardBody>
										</Card>
									)}
									{z.type === 'uom' && (
										<Card stretch className='mb-0'>
											<CardHeader borderSize={3} borderColor='success'>
												<CardLabel>
													<CardTitle>
														<FormGroup className='col-md-12'>
															<ChecksGroup isInline={isInlineCustom}>
																<Checks
																	type='switch'
																	label='UOM'
																	name='uom'
																	onChange={() =>
																		handleChangeDetail(
																			it.id,
																			z.type,
																			z.is_active,
																			null,
																		)
																	}
																	checked={z.is_active}
																	isInline={isInlineCustom}
																/>
																<small>{it.name}</small>
															</ChecksGroup>
														</FormGroup>
													</CardTitle>
												</CardLabel>
											</CardHeader>
											<CardBody>
												{z.is_active && (
													<FormGroup label='UOM' className='col-md-12'>
														<Input
															onChange={(target) =>
																handleChangeDetail(
																	it.id,
																	z.type,
																	null,
																	target,
																)
															}
															value={z.value || ''}
															isValid={isValid}
															isTouched={isValid}
															autoComplete='off'
															placeholder='Type here ...'
														/>
													</FormGroup>
												)}
											</CardBody>
										</Card>
									)}
									{z.type === 'standard' && (
										<Card stretch className='mb-0'>
											<CardHeader borderSize={3} borderColor='success'>
												<CardLabel>
													<CardTitle>
														<FormGroup className='col-md-12'>
															<ChecksGroup isInline={isInlineCustom}>
																<Checks
																	type='switch'
																	label='Standard'
																	name='standard'
																	onChange={() =>
																		handleChangeDetail(
																			it.id,
																			z.type,
																			z.is_active,
																			null,
																		)
																	}
																	checked={z.is_active}
																/>
																<small>{it.name}</small>
															</ChecksGroup>
														</FormGroup>
													</CardTitle>
												</CardLabel>
											</CardHeader>
											<CardBody>
												{z.is_active && (
													<FormGroup
														label='Standard '
														className='col-md-12'>
														{z.option && (
															<select
																name='standard'
																className='form-control form-control-xs'
																onChange={(target) =>
																	handleChangeDetail(
																		it.id,
																		z.type,
																		null,
																		target,
																	)
																}
																value={z.value || ''}
																placeholder='Select'>
																<option value='' disabled>
																	Select
																</option>
																{z.option &&
																	z.option.length > 0 &&
																	z.option.map((opt) => {
																		return (
																			<option
																				value={opt}
																				key={`it-c-opt-${opt}-${it.id}-${z.type}`}>
																				{opt}
																			</option>
																		);
																	})}
															</select>
														)}
														{z.value == 'range' && (
															<>
																<br />
																<FormGroup>
																	<InputGroup>
																		<Input
																			placeholder='0'
																			aria-label='0'
																			name='range_first'
																			onChange={(target) =>
																				handleChangeDetail(
																					it.id,
																					z.type,
																					null,
																					target,
																				)
																			}
																			value={
																				z.range_first || ''
																			}
																		/>
																		<InputGroupText>
																			&nbsp;-&nbsp;
																		</InputGroupText>
																		<Input
																			placeholder='0'
																			aria-label='0'
																			name='range_second'
																			onChange={(target) =>
																				handleChangeDetail(
																					it.id,
																					z.type,
																					null,
																					target,
																				)
																			}
																			value={
																				z.range_second || ''
																			}
																		/>
																	</InputGroup>
																</FormGroup>
															</>
														)}
														{z.value != '' &&
															z.value != 'range' &&
															z.value && (
																<Input
																	onChange={(target) =>
																		handleChangeDetail(
																			it.id,
																			z.type,
																			null,
																			target,
																		)
																	}
																	type={
																		z.value == 'min' ||
																		z.value == 'max'
																			? 'number'
																			: 'text'
																	}
																	name='standard_value'
																	value={z.standard_value || ''}
																	isValid={isValid}
																	isTouched={isValid}
																	autoComplete='off'
																	placeholder={
																		z.value == 'min' ||
																		z.value == 'max'
																			? '0'
																			: 'Type here ...'
																	}
																/>
															)}
													</FormGroup>
												)}
											</CardBody>
										</Card>
									)}
									{z.type === 'remark' && (
										<Card stretch className='mb-0'>
											<CardHeader borderSize={3} borderColor='success'>
												<CardLabel>
													<CardTitle>
														<FormGroup className='col-md-12'>
															<ChecksGroup isInline={isInlineCustom}>
																<Checks
																	type='switch'
																	label='Remark'
																	name='remark'
																	onChange={() =>
																		handleChangeDetail(
																			it.id,
																			z.type,
																			z.is_active,
																			null,
																		)
																	}
																	checked={z.is_active}
																/>
																<small>{it.name}</small>
															</ChecksGroup>
														</FormGroup>
													</CardTitle>
												</CardLabel>
											</CardHeader>
										</Card>
									)}
								</div>
							);
						})}
				</div>
			);
		})
	);
};
RenderItemCheck.propTypes = {
	item: arrayOf(PropTypes.instanceOf(Object)),
	onChange: PropTypes.func,
	onChangeSelection: PropTypes.func,
};
export default RenderItemCheck;
