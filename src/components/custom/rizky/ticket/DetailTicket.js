// eslint-disable-next-line eslint-comments/disable-enable-pair
/* eslint-disable prettier/prettier */
import React, {
    useState,
    useEffect
} from 'react';
// eslint-disable-next-line import/no-extraneous-dependencies 
import { useTranslation } from 'react-i18next';
import { Form, Formik } from 'formik';
import { CardHeader, CardLabel, CardTitle } from '../../../bootstrap/Card';
import FormGroup from '../../../bootstrap/forms/FormGroup';
import TicketModule from '../../../../modules/rizky/ticket/ticketModule';
import Input from '../../../bootstrap/forms/Input';
import DateTimeButton from '../atoms/DateTimeButton';
import Button from '../../../bootstrap/Button';
import Modal, {
    ModalBody,
    ModalHeader,
    ModalTitle,
} from '../../../bootstrap/Modal';


const ModalDetail = (dt) => {
    const { isOpen, label, setIsOpen, initialValues } = dt;
    const [ticket, setTicket] = useState();
    const { t } = useTranslation('registrasi');
    const [get, setGet] = useState(0);
    if (isOpen && get === 0) {
        TicketModule.readTicketById(label).then((res) => {
            const obj = JSON.stringify(res)
            setTicket(JSON.parse(obj));
            setGet(1);
        })
    }

    useEffect(() => {
        if (isOpen) {
            TicketModule.readTicketById(label).then((res) => {
                const obj = JSON.stringify(res)
                setTicket(JSON.parse(obj));
            })
        }
    }, [isOpen, label]);

    if (ticket) {
        return (
            <Modal isOpen={isOpen} setIsOpen={setIsOpen} size='lg' titleId='modal-edit-menu-crud'>
                <ModalHeader setIsOpen={setIsOpen} className='p-4'>
                    <ModalTitle id='modal-edit-menu-crud'> Detail Ticket - <b>{label}</b></ModalTitle>
                </ModalHeader>
                <Formik
                    enableReinitialize
                    initialValues={{ ...initialValues }}
                >
                    {(formikField) => {
                        return (
                            <Form>
                                <ModalBody className='px-4'>
                                    <div className='row'>
                                        <div className='col-md-6'>
                                            <div className='w-100'>
                                                <div className='fload-end'>
                                                    <DateTimeButton
                                                        readonly="true"
                                                        defaultValue={ticket.date}
                                                        // selected={date}
                                                        label='Date'
                                                    />
                                                </div>
                                            </div>
                                            <FormGroup id="sample_product" label="Sampel Product" className="col-md-12 mb-3">
                                                <Input
                                                    readOnly
                                                    // value={formikField.values.sample_product}
                                                    value={ticket.sample_product ? 'Yes' : 'No'}
                                                    autoComplete='off'
                                                />
                                            </FormGroup>
                                            <FormGroup id="customer" label="Customer" className="col-md-12 mb-3">

                                                <Input
                                                    readOnly
                                                    // value={formikField.values.sample_product}
                                                    value={`${ticket.customer_code} - ${ticket.customer_name}`}
                                                    autoComplete='off'
                                                />
                                            </FormGroup>
                                            <FormGroup id="target" label="Target" className="col-md-12">
                                                <Input
                                                    readOnly
                                                    // value={formikField.values.sample_product}
                                                    value={`${ticket.category}`}
                                                    autoComplete='off'
                                                />
                                            </FormGroup>
                                        </div>
                                        <div className='col-md-6'>
                                            <div className='w-100'>
                                                <div className='fload-end'>
                                                    <DateTimeButton
                                                        readonly="true"
                                                        defaultValue={ticket.target_date}
                                                        label='Target Date'
                                                    />
                                                    <DateTimeButton
                                                        readonly="true"
                                                        defaultValue={ticket.date_of_communication}
                                                        label='Date of Communication'
                                                    />
                                                </div>

                                            </div>
                                            <FormGroup id="target_price" label="Target Price" className="col-md-12 mb-3">
                                                <Input
                                                    readOnly
                                                    type='number'
                                                    onChange={formikField.handleChange}
                                                    onBlur={formikField.handleBlur}
                                                    value={ticket.target_price}
                                                    isValid={formikField.isValid}
                                                    isTouched={formikField.touched.target_price}
                                                    invalidFeedback={formikField.errors.target_price}
                                                    autoComplete='off'
                                                />
                                            </FormGroup>
                                            <FormGroup id="currencies" label="Currency" className="col-md-12 mb-3">
                                                <Input
                                                    readOnly
                                                    // value={formikField.values.sample_product}
                                                    value={`${ticket.currency}`}
                                                    autoComplete='off'
                                                />
                                            </FormGroup>
                                            <FormGroup id="status" label="Status" className="col-md-12 mb-3">
                                                <Input
                                                    readOnly
                                                    value={ticket.status}
                                                    autoComplete='off'
                                                />
                                            </FormGroup>
                                        </div>
                                    </div>
                                    <div className='mt-3'>
                                        <div className='row'>
                                            <CardHeader borderSize={1}>
                                                <CardLabel>
                                                    <CardTitle>{t('Additional Information')}</CardTitle>
                                                </CardLabel>
                                            </CardHeader>
                                        </div>
                                        <table className="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        Pilih Additional Information
                                                    </th>
                                                    <th>
                                                        Value
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {
                                                    ticket.information && ticket.information.map((el) => {
                                                        return (
                                                            <tr>
                                                                <td>
                                                                    <Input
                                                                        readOnly
                                                                        value={`${el.name}`}
                                                                        autoComplete='off'
                                                                    />
                                                                </td>
                                                                <td>
                                                                    <div>
                                                                        <input
                                                                            readOnly
                                                                            className='form-control'
                                                                            value={el.value || null}
                                                                            name='value'
                                                                            placeholder='Value'
                                                                            aria-label='test'
                                                                        />
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        );
                                                    })
                                                }
                                            </tbody>
                                        </table>
                                    </div>
                                    <div className='mt-3'>
                                        <div className='row'>
                                            <CardHeader borderSize={1}>
                                                <CardLabel>
                                                    <CardTitle>{t('Sample')}</CardTitle>
                                                </CardLabel>
                                            </CardHeader>
                                        </div>
                                        <table className="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        Sample Code
                                                    </th>
                                                    <th>
                                                        Sample Name
                                                    </th>
                                                    <th>
                                                        Batch
                                                    </th>
                                                    <th>
                                                        Quantity
                                                    </th>
                                                    <th>
                                                        UOM
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {
                                                    ticket.sample && ticket.sample.map((el) => {
                                                        return (
                                                            <tr>
                                                                <td>
                                                                    <div>
                                                                        <input
                                                                            readOnly
                                                                            className='form-control'
                                                                            value={el.sampel_code || null}
                                                                            name='sample_code'
                                                                            placeholder='Sampel Code'
                                                                            aria-label='test'
                                                                        />
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div>
                                                                        <input
                                                                            readOnly
                                                                            className='form-control'
                                                                            value={el.sampel_name || null}
                                                                            name='sample_name'
                                                                            placeholder='Sampel Name'
                                                                            aria-label='test'
                                                                        />
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div>
                                                                        <input
                                                                            readOnly
                                                                            className='form-control'
                                                                            value={el.batch || null}
                                                                            name='batch'
                                                                            placeholder='Batch'
                                                                            aria-label='test'
                                                                        />
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div>
                                                                        <input
                                                                            readOnly
                                                                            className='form-control'
                                                                            value={el.qty || null}
                                                                            name='qty'
                                                                            placeholder='Qty'
                                                                            aria-label='test'
                                                                        />
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div>
                                                                        <input
                                                                            readOnly
                                                                            className='form-control'
                                                                            value={el.uom || null}
                                                                            name='uom'
                                                                            placeholder='UOM'
                                                                            aria-label='test'
                                                                        />
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        );
                                                    })
                                                }
                                            </tbody>
                                        </table>
                                    </div>
                                </ModalBody>
                            </Form>
                        );
                    }}
                </Formik>
            </Modal>
        );
    }
    return (
        <Modal
            isOpen={isOpen}
            size='sm'
            isCentered
            setIsOpen={() => { }}
            isStaticBackdrop>
            <ModalBody
                style={{ backgroundColor: '#6c5dd3', color: 'white' }}
                className='text-center'>
                <button className='btn btn-primary' type='button' disabled>
                    <span
                        className='spinner-grow spinner-grow-sm'
                        role='status'
                        aria-hidden='true'
                    />
                    &nbsp;
                    <span
                        className='spinner-grow spinner-grow-sm'
                        role='status'
                        aria-hidden='true'
                    />
                    &nbsp;
                    <span
                        className='spinner-grow spinner-grow-sm'
                        role='status'
                        aria-hidden='true'
                    />
                    &nbsp;
                    <span className='sr-only'>Loading...</span>
                </button>
            </ModalBody>
        </Modal>
    );
}

export default (props) => {
    // eslint-disable-next-line react/prop-types
    const { label, initialValues } = props;
    const [isOpen, setIsOpen] = useState(false);
    return (
        <>
            <Button
                type='button'
                color='primary'
                className='ml-2 me-2'
                onClick={() => setIsOpen(true)}>
                i
            </Button>
            <ModalDetail isOpen={isOpen} label={label} setIsOpen={setIsOpen} initialValues={initialValues} />

        </>
    );
}