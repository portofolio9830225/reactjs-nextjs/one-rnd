import moment from 'moment/moment';
import { Calendar as DatePicker } from 'react-date-range';
// import { Views } from 'react-big-calendar';
import React, { useReducer, useCallback, useState, useRef } from 'react';
import useEventOutside from '@omtanke/react-use-event-outside';
import Button from '../../../bootstrap/Button';

// import { getLabel } from '../../../extras/calendarHelper';
// import Popovers from '../../../bootstrap/Popovers';

export default (props) => {
	// eslint-disable-next-line react/prop-types
	const { label, onChange, defaultValue, readonly } = props;
	const buttonRef = useRef(null);
	const today = new Date();
	const tomorrow = new Date();
	const [date] = useState(new Date());

	// const [viewMode] = useState(Views.MONTH);
	// const calendarDateLabel = getLabel(date, viewMode);
	// Add 1 Day
	tomorrow.setDate(today.getDate() + 1);
	let initialState;
	if (typeof defaultValue === 'undefined') {
		initialState = {
			hasFilled:  false,
			dateValue: defaultValue|| date,
		};
	} else {
		initialState = {
			hasFilled:  true,
			dateValue: defaultValue || today,
		};
	}

	const [state, updateState] = useReducer(
		(states, updates) => ({ ...states, ...updates }),
		initialState,
	);

	const onDateChange = (item) => {
		updateState({
			dateValue: item,
			hasFilled: true
		});
	};

	const ref = useRef(null);
	const [isOpen, setIsOpen] = useState(false);

	const closeMenu = useCallback(() => {
		const { dateValue } = state;
		setTimeout(() => {
			setIsOpen(false);
			updateState({
				dateValue,
			});
			if (onChange && typeof onChange === 'function' && state.dateValue) {
				onChange(state.dateValue);
			}
		}, 150);
	}, [state, onChange]);

	useEventOutside(ref, 'mousedown', closeMenu);

	const onToggleDateRange = () => {
		const { dateValue } = state;
		if (dateValue) {
			setIsOpen(true);
			// eslint-disable-next-line no-unused-expressions
			dateValue;
			updateState({
				dateValue,
				hasFilled: true,
			});
		} else {
			//
		}
	};

	return (
		// eslint-disable-next-line react/jsx-props-no-spreading
		<div {...props} className='align-items-center mb-3' style={{ position: 'relative' }}>
			<div className='w-100 mb-2' style={{ marginRight: '10px' }}>{label || 'Select date'}</div>
			<div>
				{readonly ? (
					<Button className='w-100' color='light' ref={buttonRef}>
						{state.dateValue
							? `${moment(state.dateValue).format('DD MMM YYYY')}`
							: 'Select date'}
					</Button>
				) : (
					<Button className='w-100' color='light' onClick={() => onToggleDateRange()} ref={buttonRef}>
						{defaultValue
							? `${moment(state.dateValue).format('DD MMM YYYY')}`
							: 'Select date'}
					</Button>
				)
				}

				{state.dateValue && isOpen ? (
					<div
						style={{
							position: 'absolute',
							marginTop: '5px',
							right: 0,
							zIndex: 999,
							backgroundColor: '#333',
							border: '1px solid gray',
						}}
						ref={ref}>
						<DatePicker
							label='Payment Date'
							showSelectionPreview
							onChange={(item) => onDateChange(item)}
							date={state.dateValue}
							moveRangeOnFirstSelection={false}
						/>
						{/* <DateTimePicker
                            direction='horizontal'
                            showSelectionPreview
                            onChange={(item) => onDateRangeChange(item)}
                            moveRangeOnFirstSelection={false}
                            months={2}
                            ranges={state.dateRange}
                            rangeColors={['#7C6DD7']}
                        /> */}
					</div>
				) : null}
			</div>
		</div>
	);
};
