import React, { useRef, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import useDarkMode from '../../hooks/useDarkMode';
import Button from '../bootstrap/Button';
import showNotification from '../extras/showNotification';
import CustomSelect from './CustomSelect';
import Input from '../bootstrap/forms/Input';
import PoDetailModule from '../../modules/PoDetailModule';

const FormulaMaterial = ({
	initialValues,
	onChange,
	isReadOnly,
	listMaterial,
	listVendor,
	listUom,
	listCurrency,
	initMaterial,
	setInitMaterial,
}) => {
	const { darkModeStatus } = useDarkMode();
	const [data, setData] = useState([]);
	const count = useRef(0);
	const [data_main, setDataMain] = useState([]);

	const handleCustomAdd = () => {
		if (listMaterial.length === 0) {
			showNotification(
				'Information',
				"meterial list does not exist. can't add new",
				'danger',
			);
			return;
		}

		const new_data = [...data];
		new_data.push({
			key: count.current,
			material_name: '',
			material_code: '',
			material: null,
			qty: '',
			uom: null,
			supplier_name: null,
			supplier_code: null,
			currency: null,
			price: '',
			note: '',
			readOnlyMaterialName: false,
			readOnlySupplierlName: false
		});
		setData(new_data);

		const new_data_main = [...data_main];
		new_data_main.push({ value: 'null', label: 'null' });
		setDataMain(new_data_main);

		count.current += 1;
	};

	const handleCustomRemove = (index) => {
		const new_data = [...data];
		new_data.splice(index, 1);
		setData(new_data);

		const new_data_main = [...data_main];
		new_data_main.splice(index, 1);
		setDataMain(new_data_main);

		if (onChange) {
			const result = getFormatValue(new_data);
			onChange(result);
		}
	};

	const onChangeSelect = async (e, index, name) => {
		const new_data = [...data];

		new_data[index] = {
			...new_data.at(index),
			[name]: e,
		};
		if (name === 'material') {
			new_data[index] = {
				...new_data.at(index),
				material_name: e.label,
				readOnlyMaterialName: true,
				readOnlyUom: true,
				uom: 'KG',
			};
			if (new_data[index].supplier) {
				const price = await readPriceMaterial(e.value, new_data[index].supplier.value);
				if (price) {
					new_data[index] = {
						...new_data.at(index),
						price: price.net_price,
					};
					new_data[index] = {
						...new_data.at(index),
						currency: price.currency,
					};
				}
			}
		}
		if (name === 'supplier') {
			new_data[index] = {
				...new_data.at(index),
				supplier_name: e.label,
				readOnlySupplierlName: true
			};
			if (new_data[index].material) {
				const price = await readPriceMaterial(new_data[index].material.value, e.value);
				if (price) {
					new_data[index] = {
						...new_data.at(index),
						price: price.net_price,
					};
					new_data[index] = {
						...new_data.at(index),
						currency: price.currency
					};
				}
			}
		}
		setData(new_data);

		const new_main = [...data_main];
		new_main[index] = e;

		setDataMain(new_main);

		if (onChange) {
			const result = getFormatValue(new_data);
			onChange(result);
		}
	};

	const onChangeText = (e, index, name) => {
		const new_data = [...data];
		new_data[index] = { ...new_data[index], [name]: e.target.value };

		new_data[index] = {
			...new_data.at(index),
			[name]: e.target.value,
		};
		if (name == 'material_name') {
			const selectOnlyMaterial = e.target.value.length > 0;
			new_data[index] = {
				...new_data.at(index),
				material_name: e.target.value,
				readOnlySelectMaterial: selectOnlyMaterial,
			};
		}
		if (name === 'supplier_name') {
			const selectOnly = e.target.value.length > 0;
			new_data[index] = {
				...new_data.at(index),
				supplier_name: e.target.value,
				readOnlySelectSupplier: selectOnly,
			};
		}
		setData(new_data);

		if (onChange) {
			const result = getFormatValue(new_data);
			onChange(result);
		}
	};

	const getFormatValue = (params) => {
		const result = params
			.map((item) => {
				let new_value = {};
				if (item.material) {
					if (typeof item.material === 'object') {
						new_value = {
							...new_value,
							material_name: item.material.label,
							material_code: item.material.value,
							material: item.material,
						};
					} else {
						new_value = {
							...new_value,
							material_name: item.material_name,
							material_code: item.material_code,
							material: {
								value: item.material_code,
								label: item.material_name,
							},
						};
					}
				}

				if (item.currency) {
					if (typeof item.currency === 'object') {
						new_value = {
							...new_value,
							currency: item.currency.value,
						};
					} else {
						new_value = {
							...new_value,
							currency: item.currency,
						};
					}
				}

				if (item.supplier) {
					if (typeof item.material === 'object') {
						new_value = {
							...new_value,
							supplier_name: item.supplier.label,
							supplier_code: item.supplier.value,
						};
					} else {
						new_value = {
							...new_value,
							supplier_name: item.supplier_name,
							supplier_code: item.supplier_code,
						};
					}
				}
				if (item.uom) {
					if (typeof item.uom === 'object') {
						new_value = {
							...new_value,
							uom: item.uom.value,
						};
					} else {
						new_value = {
							...new_value,
							uom: item.uom,
						};
					}
				}
				if (item.material_name) {
					new_value = { ...new_value, material_name: item.material_name };
				}
				if (item.supplier_name) {
					new_value = { ...new_value, supplier_name: item.supplier_name };
				}
				if (item.qty) {
					new_value = { ...new_value, qty: item.qty };
				}
				if (item.price) {
					new_value = { ...new_value, price: item.price };
				}
				if (item.note) {
					new_value = { ...new_value, note: item.note };
				}
				if (item.batch) {
					new_value = { ...new_value, batch: item.batch };
				}

				return new_value;
			})
			.filter((item) => Object.keys(item).length !== 0);
		return result;
	};

	const readPriceMaterial = async (material_code, vendor_code) => {
		const query = `vendor_code=${vendor_code}&material_code=${material_code}`;
		return PoDetailModule.readPrice(query).then((res) => {
			return res;
		});
	}

	useEffect(() => {
		if (initialValues.length !== 0 && initMaterial) {
			if (onChange) {
				onChange(initialValues);
			}
			setInitMaterial(false);
			getFormatSelect(initialValues).then((response) => {
				const { value_count, value_data, value_list_main } = response;

				setData(value_data);
				setDataMain(value_list_main);
				count.current = value_count;
			});
		}

		return <div>{/* */}</div>;
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [initialValues]);

	const getFormatSelect = (params) => {
		return new Promise((resolve, reject) => {
			try {
				const _list_main = [];
				const _data = [];

				const _promises = [];

				params.forEach((item, index) => {
					let new_data = {
						key: index,
						material_name: '',
						material_code: '',
						material: null,
						qty: '',
						uom: null,
						supplier_name: null,
						supplier_code: null,
						currency: null,
						price: '',
						note: '',
						readOnlyMaterialName: false,
						readOnlySelectMaterial: false,
						readOnlyUom: false,
					};

					if (item.material) {
						if (typeof item.material === 'object') {
							new_data = {
								...new_data,
								material_name: item.material.label,
								material_code: item.material.value,
								readOnlyMaterialName: item.material_code.length > 0,
								readOnlyUom: item.material_code.length > 0,
							};
						} else {
							new_data = {
								...new_data,
								material_name: item.material_name,
								material_code: item.material_code,
							};
						}
					}
					if (item.material_name) {
						if (item.material_code) {
							new_data = {
								...new_data,
								material_name: item.material_name,
								material_code: item.material_code,
								material: {
									label: item.material_name,
									value: item.material_code,
								},
								readOnlySupplierlName: true
							};
						} else {
							new_data = {
								...new_data,
								material_name: item.material_name,
								readOnlySelectMaterial: !item.material_code,
							};
						}
					}
					if (item.supplier) {
						if (typeof item.supplier === 'object') {
							new_data = {
								...new_data,
								supplier_name: item.supplier.label,
								supplier_code: item.supplier.value,
							};
						} else {
							new_data = {
								...new_data,
								supplier_name: item.supplier_name,
								supplier_code: item.supplier_code,
							};
						}
					}
					if (item.supplier_name) {
						if (item.supplier_code) {
							new_data = {
								...new_data,
								supplier_name: item.supplier_name,
								supplier_code: item.supplier_code,
								supplier: {
									label: item.supplier_name,
									value: item.supplier_code,
								}
							};
						} else {
							new_data = {
								...new_data,
								supplier_name: item.supplier_name,
								readOnlySelectSupplier: !item.supplier_code,
							};
						}
					}
					if (item.currency) {
						new_data = {
							...new_data,
							currency: {
								label: item.currency,
								value: item.currency,
							},
						};
					}
					if (item.uom) {
						if (typeof item.uom === 'object') {
							new_data = {
								...new_data,
								uom: item.uom.value,
							};
						} else {
							new_data = {
								...new_data,
								uom: item.uom,
							};
						}
					}
					if (item.qty) {
						new_data = { ...new_data, qty: item.qty };
					}
					if (item.price) {
						new_data = { ...new_data, price: item.price };
					}
					if (item.note) {
						new_data = { ...new_data, note: item.note };
					}
					if (item.batch) {
						new_data = { ...new_data, batch: item.batch };
					}

					_data.push(new_data);
				});

				Promise.all(_promises).then(() => {
					resolve({
						// value_select_main: _select_main,
						value_list_main: _list_main,
						value_data: _data,
						value_count: _data.length,
					});
				});
			} catch (e) {
				reject(new Error({ error: true }));
			}
		});
	};

	return (
		<div>
			<div>
				<div className='row mb-2'>
					{!isReadOnly && (
						<div className='col-md-6'>
							<Button
								icon='Add'
								color='success'
								type='button'
								className='btn btn-sm'
								isLight={darkModeStatus}
								onClick={handleCustomAdd}
							>Add Raw Material</Button>
						</div>
					)}
				</div>
				<div style={{ overflowX: 'auto' }}>
					<div className='row'>
						<div className='col-md-12'>
							<table className='table table-modern' style={{ width: '2500px', marginBottom: '200px' }}>
								<thead>
									<tr>
										<th width='3%'>-</th>
										<th style={{ width: '350px' }}>Material Code</th>
										<th style={{ width: '350px' }}>Material Name</th>
										<th>Qty</th>
										<th style={{ width: '150px' }}>UoM</th>
										<th>Batch</th>
										<th style={{ width: '350px' }}>Supplier</th>
										<th style={{ width: '350px' }}>Supplier Name</th>
										<th style={{ width: '150px' }}>Currency</th>
										<th style={{ width: '200px' }}>Price</th>
										<th style={{ width: '200px' }}>Notes</th>
									</tr>
								</thead>
								<tbody>
									{data.map((item, index) => (
										<tr key={'tr-'.concat(item.key)}>
											<td key={'td1-'.concat(item.key)}>
												<Button
													key={'button-remove-'.concat(item.key)}
													icon='Clear'
													color='danger'
													type='button'
													isLight={darkModeStatus}
													style={{ minHeight: '1.95rem' }}
													onClick={() => handleCustomRemove(index)}
													isDisable={isReadOnly}
												/>
											</td>
											<td key={'td2-'.concat(item.key)} style={{ verticalAlign: 'middle' }}>
												<CustomSelect
													key={'cs-material'.concat(item.key)}
													options={listMaterial}
													onChange={(e) =>
														onChangeSelect(e, index, 'material')
													}
													value={item.material}
													isDisable={
														isReadOnly || item.readOnlySelectMaterial
													}
													isSearchable={listMaterial.length > 7}
												/>
											</td>
											<td key={'td-material-7-'.concat(item.key)}>
												<Input
													key={'input-material-name-'.concat(item.key)}
													style={{
														borderRadius: '4px',
														minHeight: '2.95rem',
													}}
													type='text'
													value={item.material_name}
													onChange={(e) =>
														onChangeText(e, index, 'material_name')
													}
													disabled={isReadOnly || item.readOnlyMaterialName}
												/>
											</td>
											<td key={'td3-'.concat(item.key)}>
												<Input
													key={'input-qty-'.concat(item.key)}
													style={{
														borderRadius: '4px',
														minHeight: '2.95rem',
													}}
													min='0'
													type='number'
													value={item.qty}
													onChange={(e) => onChangeText(e, index, 'qty')}
													disabled={isReadOnly}
												/>
											</td>
											<td key={'td4-'.concat(item.key)}>
												<CustomSelect
													key={'cs-uom'.concat(item.key)}
													options={listUom}
													onChange={(e) => onChangeSelect(e, index, 'uom')}
													value={listUom.length == 1 ? listUom[0] : item.uom}
													themes='info'
													isDisable={isReadOnly}
												/>
											</td>
											<td key={'td3-batch-'.concat(item.key)}>
												<Input
													key={'input-batch-'.concat(item.key)}
													style={{
														borderRadius: '4px',
														minHeight: '2.95rem',
													}}
													min='0'
													type='text'
													value={item.batch}
													onChange={(e) => onChangeText(e, index, 'batch')}
													disabled={isReadOnly}
												/>
											</td>
											<td key={'td5-'.concat(item.key)} style={{ verticalAlign: 'middle' }}>
												<CustomSelect
													key={'cs-supplier'.concat(item.key)}
													options={listVendor}
													onChange={(e) =>
														onChangeSelect(e, index, 'supplier')
													}
													value={item.supplier}
													isDisable={isReadOnly || item.readOnlySelectSupplier}
													isSearchable={listVendor.length > 7}
												/>

											</td>
											<td key={'td-supplier-7-'.concat(item.key)}>
												<Input
													key={'input-supplier-name-'.concat(item.key)}
													style={{
														borderRadius: '4px',
														minHeight: '2.95rem',
													}}
													type='text'
													value={item.supplier_name}
													onChange={(e) =>
														onChangeText(e, index, 'supplier_name')
													}
													disabled={isReadOnly || item.readOnlySupplierlName}
												/>
											</td>
											<td key={'td6-'.concat(item.key)}>
												<CustomSelect
													key={'cs-currency'.concat(item.key)}
													options={listCurrency}
													onChange={(e) =>
														onChangeSelect(e, index, 'currency')
													}
													themes='info'
													value={item.currency}
													isDisable={isReadOnly}
												/>
											</td>
											<td key={'td7-'.concat(item.key)}>
												<Input
													key={'input-price-'.concat(item.key)}
													style={{
														borderRadius: '4px',
														minHeight: '2.95rem',
													}}
													type='number'
													value={item.price}
													onChange={(e) => onChangeText(e, index, 'price')}
													disabled={isReadOnly}
												/>
											</td>
											<td key={'td8-'.concat(item.key)}>
												<Input
													key={'input-note-'.concat(item.key)}
													style={{
														borderRadius: '4px',
														minHeight: '2.95rem',
													}}
													type='text'
													value={item.note}
													onChange={(e) => onChangeText(e, index, 'note')}
													disabled={isReadOnly}
												/>
											</td>
										</tr>
									))}
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<br />
			<br />
		</div>
	);
};

FormulaMaterial.propTypes = {
	onChange: PropTypes.func,
	initialValues: PropTypes.instanceOf(Array),
	isReadOnly: PropTypes.bool,
	listMaterial: PropTypes.instanceOf(Array),
	listVendor: PropTypes.instanceOf(Array),
	listUom: PropTypes.instanceOf(Array),
	listCurrency: PropTypes.instanceOf(Array),
	initMaterial: PropTypes.bool,
	setInitMaterial: PropTypes.func,
};
FormulaMaterial.defaultProps = {
	onChange: null,
	initialValues: [],
	isReadOnly: false,
	listMaterial: [],
	listVendor: [],
	listUom: [],
	listCurrency: [],
	initMaterial: true,
	setInitMaterial: () => { },
};

export default FormulaMaterial;
