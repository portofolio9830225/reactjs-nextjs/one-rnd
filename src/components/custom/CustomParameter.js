import React, { useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import InputGroup, { InputGroupText } from '../bootstrap/forms/InputGroup';
import FormGroup from '../bootstrap/forms/FormGroup';
import CustomSelect from './CustomSelect';
import Button from '../bootstrap/Button';
import useDarkMode from '../../hooks/useDarkMode';
import CustomParameterRole from './CustomParameterRole';
import showNotification from '../extras/showNotification';

const CustomParameter = ({ onChange, isReadOnly, listRole, listRegional, initialValues }) => {
	const { darkModeStatus } = useDarkMode();

	const [data, setData] = useState([]);
	const count = useRef(0);

	const [dataMain, setDataMain] = useState([]);
	const [selectMain, setSelectMain] = useState(null);

	const handleAdd = () => {
		if (listRole.length === 0) {
			showNotification('Information', "role list does not exist. can't add role", 'danger');
			return;
		}

		if (data.length === listRole.length) {
			showNotification(
				'Information',
				'has reached the maximum role limit that can be added',
				'danger',
			);
			return;
		}

		const new_data = [...data];
		new_data.push({ key: count.current });
		setData(new_data);

		const new_data_main = [...dataMain];
		new_data_main.push({ value: 'null', label: 'null' });
		setDataMain(new_data_main);

		const new_object = new_data;
		onChange(new_object);

		count.current += 1;
	};

	const handleRemove = (index) => {
		const new_data = [...data];
		new_data.splice(index, 1);
		setData(new_data);

		const new_data_main = [...dataMain];
		new_data_main.splice(index, 1);
		setDataMain(new_data_main);

		setSelectMain(null);

		const new_object = new_data;
		onChange(new_object);
	};

	const onChangeMain = (e) => {
		if (e.value === 'null') {
			showNotification('Information', "can't choose an empty role", 'danger');
			return;
		}
		setSelectMain(e);
	};

	const onChangeRole = (values, index) => {
		const new_data = [...data];
		new_data[index] = { ...new_data[index], ...values };
		setData(new_data);

		const new_data_main = [...dataMain];
		new_data_main[index] = values?.role;
		setDataMain(new_data_main);
	};

	useEffect(() => {
		if (initialValues.length > 0) {
			getFormatSelect();
		}

		return <div>{/* */}</div>;
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [initialValues]);

	const getFormatSelect = () => {};

	return (
		<div className='p-4' style={{ minHeight: '450px' }}>
			<div className='row mb-1'>
				<div className='col-md-8'>
					<InputGroup>
						<InputGroupText>Main Role</InputGroupText>
						<FormGroup className='col-md-6'>
							<CustomSelect
								options={dataMain}
								value={selectMain}
								defaultValue={selectMain}
								onChange={onChangeMain}
								isDisable={isReadOnly}
							/>
						</FormGroup>
					</InputGroup>
				</div>
				{!isReadOnly && (
					<div className='col-md-4'>
						<Button
							icon='Add'
							color='info'
							type='button'
							className='float-end'
							isLight={darkModeStatus}
							onClick={handleAdd}
							style={{ minHeight: '2.95rem' }}>
							Add Role
						</Button>
					</div>
				)}
			</div>
			<div className='row'>
				<div className='col-md-12'>
					{data.map((item, index) => (
						<div key={'div-'.concat(item.key)}>
							<div
								key={'div-border-'.concat(item.key)}
								className='col-md-12 border-bottom mb-1'
							/>
							<CustomParameterRole
								key={'param-role-'.concat(item.key)}
								isReadOnly={isReadOnly}
								indexRole={index}
								listRole={listRole}
								listRegional={listRegional}
								handleRemove={handleRemove}
								onChange={onChangeRole}
								dataRole={data}
								valueRole={item}
							/>
						</div>
					))}
				</div>
			</div>
		</div>
	);
};

CustomParameter.propTypes = {
	initialValues: PropTypes.instanceOf(Array),
	isReadOnly: PropTypes.bool,
	listRole: PropTypes.instanceOf(Array),
	listRegional: PropTypes.instanceOf(Array),
	onChange: PropTypes.func,
};
CustomParameter.defaultProps = {
	initialValues: [],
	isReadOnly: false,
	listRole: [],
	listRegional: [],
	onChange: () => {},
};

export default CustomParameter;
