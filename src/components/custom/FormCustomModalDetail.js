import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Button from '../bootstrap/Button';
import useDarkMode from '../../hooks/useDarkMode';
import Modal, { ModalBody, ModalHeader, ModalTitle } from '../bootstrap/Modal';
import DetailTicketAndFormulaComponent from './DetailTicketAndFormulaComponent';

const FormCustomModalDetail = ({ initialValues }) => {
	const { darkModeStatus } = useDarkMode();
	const [isOpen, setOpen] = useState(false);

	return (
		<>
			<Button
				color='info'
				type='button'
				isLight={darkModeStatus}
				className='m-1'
				onClick={() => setOpen(true)}>
				i
			</Button>

			<Modal
				isOpen={isOpen}
				setIsOpen={setOpen}
				size='xl'
				titleId='modal-crud-evaluation'
				isStaticBackdrop>
				<ModalHeader setIsOpen={setOpen} className='p-4'>
					<ModalTitle id='modal-crud-evaluation'>
						Detail Ticket - ({initialValues?.document_number}){' '}
						{initialValues?.customer_name}
					</ModalTitle>
				</ModalHeader>
				<ModalBody>
					<DetailTicketAndFormulaComponent
						ticket_id={initialValues.ticket_id}
						formula_id={initialValues.formula_id}
					/>
				</ModalBody>
			</Modal>
		</>
	);
};

FormCustomModalDetail.propTypes = {
	initialValues: PropTypes.instanceOf(Object),
};
FormCustomModalDetail.defaultProps = {
	initialValues: null,
};

export default FormCustomModalDetail;
