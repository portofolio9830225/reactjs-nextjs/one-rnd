import PropTypes from 'prop-types';
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import { Tab, Tabs } from 'react-bootstrap';
import Card, { CardBody, CardTabItem } from '../bootstrap/Card';
// import Card, { CardBody, CardTabItem, CardTitle, CardHeader, CardLabel } from '../bootstrap/Card';
import TicketModule from '../../modules/TicketModule';
import FormGroup from '../bootstrap/forms/FormGroup';
import Input from '../bootstrap/forms/Input';
import Button from '../bootstrap/Button';
import Accordion, { AccordionItem } from '../bootstrap/Accordion';

const RenderTitleFormula = (item, idx) => {
	return (
		<div>
			{item} {`(${idx + 1})`}{' '}
		</div>
	);
};

const DetailTicketAndFormulaComponent = ({ ticket_id, formula_id }) => {
	const backgroundColor = {
		tableHeader: '#BFDCE5',
	};

	const [data_ticket, setDataTicket] = useState({
		currency: '',
		customer: '',
		date: '',
		date_of_communication: '',
		sample_product: '',
		status: '',
		target: '',
		target_date: '',
		target_price: '',
		information: [],
		sample: [],
		history: [],
		formula: [],
	});
	const [key, setKey] = useState(0);

	useEffect(() => {
		if (ticket_id) {
			fetchDataTicket();
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [ticket_id]);

	const fetchDataTicket = async () => {
		return TicketModule.readById(ticket_id, formula_id)
			.then((res) => {
				const new_response = {
					currency: res?.currency,
					customer: `(${res?.customer_code}) ${res?.customer_name}`,
					date: moment(res?.date).format('DD MMM YYYY'),
					date_of_communication: moment(res?.date_of_communication).format('DD MMM YYYY'),
					sample_product: res?.sample_product ? 'YES' : 'NO',
					status: res?.status,
					target: res?.category,
					target_date: moment(res?.target_date).format('DD MMM YYYY'),
					target_price: res?.target_price,
					information: res?.information,
					sample: res?.sample,
					history: res?.history,
					formula: res?.formula,
					initial_project: res?.initial_project,
				};
				setDataTicket(new_response);
			})
			.catch(() => {
				setDataTicket({});
			})
			.finally(() => {});
	};

	return (
		<div>
			<div className='row pt-2 px-2 pb-4'>
				<div className='col-md-6 py-1'>
					<FormGroup id='date' label='Date'>
						<Input disabled value={data_ticket?.date} />
					</FormGroup>
				</div>
				<div className='col-md-6 py-1'>
					<FormGroup id='target_date' label='Target Date'>
						<Input disabled value={data_ticket?.target_date} />
					</FormGroup>
				</div>
				<div className='col-md-6 py-1'>
					<FormGroup id='sample_product' label='Sample Product'>
						<Input disabled value={data_ticket?.sample_product} />
					</FormGroup>
				</div>
				<div className='col-md-6 py-1'>
					<FormGroup id='date_of_communication' label='Date of Communication'>
						<Input disabled value={data_ticket?.date_of_communication} />
					</FormGroup>
				</div>
				<div className='col-md-6 py-1'>
					<FormGroup id='customer' label='Customer'>
						<Input disabled value={data_ticket?.customer} />
					</FormGroup>
				</div>
				<div className='col-md-6 py-1'>
					<FormGroup id='target_price' label='Target Price'>
						<Input disabled value={data_ticket?.target_price} />
					</FormGroup>
				</div>
				<div className='col-md-6 py-1'>
					<FormGroup id='target' label='Target'>
						<Input disabled value={data_ticket?.target} />
					</FormGroup>
				</div>
				<div className='col-md-6 py-1'>
					<FormGroup id='currency' label='Currency'>
						<Input disabled value={data_ticket?.currency} />
					</FormGroup>
				</div>
				<div className='col-md-6 py-1'>
					<FormGroup id='initial_project' label='Initial Project'>
						<Input disabled value={data_ticket?.initial_project} />
					</FormGroup>
				</div>
			</div>
			<div className='row'>
				<div className='col-md-12'>
					{data_ticket?.sample?.length > 0 ? (
						<Card
							shadow='sm'
							style={{ borderRadius: '10px' }}
							hasTab
							tabButtonColor='info'>
							<CardTabItem
								id='tab_additional_information'
								title='Additional Information'>
								<CardBody>
									<table className='table table-sm table-modern'>
										<thead>
											<tr>
												<th scope='col'>#</th>
												<th scope='col'>Name</th>
												<th scope='col'>Value</th>
											</tr>
										</thead>
										<tbody>
											{data_ticket?.information?.map((item, index) => (
												<tr key={'add-infor.'.concat(index)}>
													<th
														key={'add-infor.'.concat(index, '.', 0)}
														scope='row'>
														{index + 1}
													</th>
													<td key={'add-infor.'.concat(index, '.', 1)}>
														{item.name}
													</td>
													<td key={'add-infor.'.concat(index, '.', 2)}>
														{item.value}
													</td>
												</tr>
											))}
										</tbody>
									</table>
								</CardBody>
							</CardTabItem>
							<CardTabItem id='tab_sample' title='Sample'>
								<CardBody>
									<table className='table table-sm table-modern'>
										<thead>
											<tr>
												<th scope='col'>#</th>
												<th scope='col'>Sample Code</th>
												<th scope='col'>Sample Name</th>
												<th scope='col'>Batch</th>
												<th scope='col'>Quantity</th>
												<th scope='col'>UOM</th>
											</tr>
										</thead>
										<tbody>
											{data_ticket?.sample?.map((item, index) => (
												<tr key={'sample.'.concat(index)}>
													<th
														key={'sample.'.concat(index, '.', 0)}
														scope='row'>
														{index + 1}
													</th>
													<td key={'sample.'.concat(index, '.', 1)}>
														{item.sampel_code}
													</td>
													<td key={'sample.'.concat(index, '.', 2)}>
														{item.sampel_name}
													</td>
													<td key={'sample.'.concat(index, '.', 3)}>
														{item.batch}
													</td>
													<td key={'sample.'.concat(index, '.', 4)}>
														{item.qty}
													</td>
													<td key={'sample.'.concat(index, '.', 5)}>
														{item.uom}
													</td>
												</tr>
											))}
										</tbody>
									</table>
								</CardBody>
							</CardTabItem>
						</Card>
					) : (
						<Card
							shadow='sm'
							style={{ borderRadius: '10px' }}
							hasTab
							tabButtonColor='info'>
							<CardTabItem
								id='tab_additional_information'
								title='Additional Information'>
								<CardBody>
									<table className='table table-sm table-modern'>
										<thead>
											<tr>
												<th scope='col'>#</th>
												<th scope='col'>Name</th>
												<th scope='col'>Value</th>
											</tr>
										</thead>
										<tbody>
											{data_ticket?.information?.map((item, index) => (
												<tr key={'add-infor.'.concat(index)}>
													<th
														key={'add-infor.'.concat(index, '.', 0)}
														scope='row'>
														{index + 1}
													</th>
													<td key={'add-infor.'.concat(index, '.', 1)}>
														{item.name}
													</td>
													<td key={'add-infor.'.concat(index, '.', 2)}>
														{item.value}
													</td>
												</tr>
											))}
										</tbody>
									</table>
								</CardBody>
							</CardTabItem>
						</Card>
					)}
				</div>
			</div>
			<div className='row'>
				<div className='col-md-12 mb-4'>
					<Accordion activeItemId='form-default-formula-2' id='accordion-form'>
						<AccordionItem id='form-default-formula' title='Formula'>
							<Tabs
								id='controlled-tab-example'
								activeKey={key}
								onSelect={(k) => {
									setKey(k);
								}}
								className='mb-3'>
								{data_ticket?.formula?.map((item, index) => (
									<Tab
										key={'key-tab-'.concat(index)}
										eventKey={index}
										title={RenderTitleFormula(
											item.job_number ? item.job_number : 'formula',
											index,
										)}
										tabClassName={`${index}`}>
										<div className='col-12 mt-2'>
											<table className='table table-modern'>
												<thead
													style={{
														backgroundColor:
															backgroundColor.tableHeader,
													}}>
													<tr>
														<th style={{ width: '200px' }}>
															Material Code
														</th>
														<th style={{ width: '200px' }}>
															Material Name
														</th>
														<th style={{ width: '150px' }}>Qty</th>
														<th style={{ width: '100px' }}>UoM</th>
														<th style={{ width: '200px' }}>Supplier</th>
														<th style={{ width: '100px' }}>Currency</th>
														<th style={{ width: '150px' }}>Price</th>
														<th style={{ width: '150px' }}>Notes</th>
													</tr>
												</thead>
												<tbody>
													{item?.formula?.map((iae, index_) => (
														<tr key={'tr-'.concat(index_)}>
															<td key={'td2-'.concat(index_)}>
																{iae.material_code}
															</td>
															<td key={'td-7-'.concat(index_)}>
																{iae.material_name}
															</td>
															<td key={'td3-'.concat(index_)}>
																{iae.qty}
															</td>
															<td key={'td4-'.concat(index_)}>KG</td>
															<td key={'td5-'.concat(index_)}>
																{iae.supplier}
															</td>
															<td key={'td6-'.concat(index_)}>
																{iae.currency}
															</td>
															<td key={'td7-'.concat(index_)}>
																{iae.price}
															</td>
															<td key={'td8-'.concat(index_)}>
																{iae.note}
															</td>
														</tr>
													))}
												</tbody>
											</table>
										</div>
										{/* Parameter Trial */}
										<div className='col-md-12 mb-4'>
											<Accordion
												activeItemId='form-default-parameter-trial-2'
												id='accordion-form'>
												<AccordionItem
													id='form-default-parameter-trial'
													title='Parameter Trial'>
													{item?.details?.map((dtls) => (
														<div className='col-12 mt-2'>
															{dtls.parameter_code ===
																'mixing_sequences' && (
																<table className='table table-modern'>
																	<thead
																		style={{
																			backgroundColor:
																				backgroundColor.tableHeader,
																		}}>
																		<tr>
																			<th>
																				{
																					dtls.parameter_name
																				}
																			</th>
																			<th>Plan RPM</th>
																			<th>Plan Minutes</th>
																			<th>Actual RPM</th>
																			<th>Actual Minutes</th>
																		</tr>
																	</thead>
																	<tbody>
																		{dtls?.detail?.map(
																			(
																				detailformula,
																				index_dtlformula,
																			) => (
																				<tr
																					key={'tr-mixing_sequences-'.concat(
																						index_dtlformula,
																					)}>
																					<td
																						key={'td2-dtls-'.concat(
																							index_dtlformula,
																						)}>
																						{
																							detailformula.name
																						}
																					</td>
																					<td
																						key={'td-plan_rpm-7-'.concat(
																							index_dtlformula,
																						)}>
																						{
																							detailformula.plan_rpm
																						}
																					</td>
																					<td
																						key={'td3-plan_minutes-'.concat(
																							index_dtlformula,
																						)}>
																						{
																							detailformula.plan_minutes
																						}
																					</td>
																					<td
																						key={'td-actual_rpm-7-'.concat(
																							index_dtlformula,
																						)}>
																						{
																							detailformula.actual_rpm
																						}
																					</td>
																					<td
																						key={'td3-actual_minutes-'.concat(
																							index_dtlformula,
																						)}>
																						{
																							detailformula.actual_minutes
																						}
																					</td>
																				</tr>
																			),
																		)}
																	</tbody>
																</table>
															)}
															{dtls.parameter_code !==
																'mixing_sequences' && (
																<table className='table table-modern'>
																	<thead
																		style={{
																			backgroundColor:
																				backgroundColor.tableHeader,
																		}}>
																		<tr>
																			<th>
																				{
																					dtls.parameter_name
																				}
																			</th>
																			<th>Plan</th>
																			<th>Actual</th>
																		</tr>
																	</thead>
																	<tbody>
																		{dtls?.detail?.map(
																			(
																				detailformula,
																				index_dtlformula,
																			) => (
																				<tr
																					key={'tr-dtls-'.concat(
																						index_dtlformula,
																					)}>
																					<td
																						key={'td2-dtls-'.concat(
																							index_dtlformula,
																						)}>
																						{
																							detailformula.name
																						}
																					</td>
																					<td
																						key={'td-dtls-material-7-'.concat(
																							index_dtlformula,
																						)}>
																						{
																							detailformula.plan
																						}
																					</td>
																					<td
																						key={'td3-dtls-'.concat(
																							index_dtlformula,
																						)}>
																						{
																							detailformula.actual
																						}
																					</td>
																				</tr>
																			),
																		)}
																	</tbody>
																</table>
															)}
														</div>
													))}
												</AccordionItem>
											</Accordion>
										</div>
										{/* End Parameter Trial */}
										{/* Status History */}
										<div className='col-md-12 mb-4'>
											<Accordion
												activeItemId='form-default-status-history-2'
												id='accordion-form'>
												<AccordionItem
													id='form-default-status-history'
													title='Status History'>
													<div className='pt-4'>
														{item?.history?.map((dtl_history) => (
															<div
																className='col-md-12'
																style={{ marginTop: '-1.2rem' }}>
																<div
																	className='d-flex justify-content-between align-items-center'
																	style={{
																		color: '#3d76ff',
																		fontSize: '1.1rem',
																	}}>
																	<div className='d-flex'>
																		<div
																			style={{
																				width: '20px',
																				height: '20px',
																				marginRight:
																					'1.5rem',
																				borderRadius:
																					'100%',
																				border: '3px solid #18bedb',
																			}}>
																			{}
																		</div>
																		<p>
																			{dtl_history.status} -{' '}
																			{dtl_history.requester}
																		</p>
																	</div>
																	<p>
																		{moment(
																			dtl_history.date,
																		).format(
																			'YYYY-MM-DD H:mm:ss',
																		)}
																	</p>
																</div>
																<div
																	style={{
																		borderLeft:
																			'2px solid lightgrey',
																		marginLeft: '0.7rem',
																		marginTop: '-1.4rem',
																		marginBottom: '1rem',
																	}}>
																	<div
																		className='d-flex align-items-center w-100'
																		style={{
																			paddingLeft: '2.1rem',
																			paddingTop: '1.7rem',
																			paddingBottom: '2.1rem',
																		}}>
																		<div>
																			{dtl_history.data !=
																				null &&
																				dtl_history?.data
																					.length > 0 &&
																				dtl_history.data?.map(
																					(
																						dt_file,
																						index_file,
																					) =>
																						dt_file.document && (
																							<div className='d-inline-block'>
																								<Button color='success'>
																									<a
																										type='button'
																										download={`${dt_file.document}.${dt_file.extension}`}
																										href={`data:${dt_file.mimetype};base64,${dt_file.file}`}>
																										File{' '}
																										{index_file +
																											1}
																									</a>
																								</Button>{' '}
																								-&nbsp;&nbsp;
																							</div>
																						),
																				)}{' '}
																		</div>
																		<div>
																			<p
																				style={{
																					marginBottom:
																						'0px',
																				}}>
																				{dtl_history.remark}
																			</p>
																		</div>
																	</div>
																</div>
															</div>
														))}
													</div>
												</AccordionItem>
											</Accordion>
										</div>
										{/* End Status History */}
									</Tab>
								))}
							</Tabs>
						</AccordionItem>
					</Accordion>
				</div>
			</div>
		</div>
	);
};

DetailTicketAndFormulaComponent.propTypes = {
	ticket_id: PropTypes.string.isRequired,
	formula_id: PropTypes.string,
};
DetailTicketAndFormulaComponent.defaultProps = { formula_id: null };

export default DetailTicketAndFormulaComponent;
