import React, { useRef, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import useDarkMode from '../../hooks/useDarkMode';
import Button from '../bootstrap/Button';
import FormGroup from '../bootstrap/forms/FormGroup';
import InputGroup, { InputGroupText } from '../bootstrap/forms/InputGroup';
import showNotification from '../extras/showNotification';
import CustomSelect from './CustomSelect';
import ParameterTrialDetail from './ParameterTrialDetail';

const ParameterTrial = ({
	initialValues,
	list_parameter,
	isReset,
	setReset,
	isReadOnly,
	onChange,
	init,
	setInit,
}) => {
	const { darkModeStatus } = useDarkMode();

	const [data, setData] = useState([]);
	const count = useRef(0);

	const [data_main, setDataMain] = useState([]);
	const [select_main, setSelectMain] = useState();

	const handleCustomAdd = () => {
		if (list_parameter.length === 0) {
			showNotification(
				'Information',
				"parameter list does not exist. can't add parameter",
				'danger',
			);
			return;
		}

		if (data.length === list_parameter.length) {
			showNotification(
				'Information',
				'has reached the maximum parameter limit that can be added',
				'danger',
			);
			return;
		}

		const new_data = [...data];
		new_data.push({ key: count.current, parameter: null });
		setData(new_data);

		const new_data_main = [...data_main];
		new_data_main.push({ value: 'null', label: 'null' });
		setDataMain(new_data_main);

		count.current += 1;
	};

	const handleCustomRemove = (index) => {
		const new_data = [...data];
		new_data.splice(index, 1);
		setData(new_data);

		const new_data_main = [...data_main];
		new_data_main.splice(index, 1);
		setDataMain(new_data_main);

		setSelectMain(null);

		if (onChange) {
			const result = getFormatValue(new_data, select_main);
			onChange(result);
		}
	};

	const onChangeParamater = (e, index) => {
		const new_data = [...data];

		// find index
		const find_index = new_data.findIndex((find) => find.parameter_name?.value === e.value);

		// if it is the same as the previously selected data, the process will not continue
		if (find_index === index) {
			return;
		}

		// if the selected data already exists, then the process will not continue
		if (find_index !== -1) {
			showNotification(
				'Information',
				`${e.label}, the paramater has been selected, try choosing another one`,
				'danger',
			);
			return;
		}

		// set new data
		new_data[index] = {
			...new_data.at(index),
			parameter_name: e,
		};
		setData(new_data);

		const new_main = [...data_main];
		new_main[index] = e;
		setDataMain(new_main);

		if (onChange) {
			const result = getFormatValue(new_data, select_main);
			onChange(result);
		}
	};

	const onChangeParameterDetail = (values, index) => {
		const new_data = [...data];

		if (values.length === 0) {
			delete new_data[index].detail;
		} else {
			new_data[index] = {
				...new_data.at(index),
				detail: values,
			};
		}
		setData(new_data);

		if (onChange) {
			const result = getFormatValue(new_data);
			onChange(result);
		}
	};

	useEffect(() => {
		if (isReset) {
			setData([]);
			setDataMain([]);
			setSelectMain(null);
			count.current = 0;

			setReset(false);
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [isReset]);

	useEffect(() => {
		if (initialValues.length !== 0 && init) {
			if (onChange) {
				onChange(initialValues);
			}
			setInit(false);
			getFormatSelect(initialValues).then((response) => {
				const { value_count, value_data, value_list_main, value_select_main } = response;

				setData(value_data);
				setDataMain(value_list_main);
				setSelectMain(value_select_main);
				count.current = value_count;
			});
		}

		return <div>{/* */}</div>;
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [initialValues]);

	const getFormatSelect = (params) => {
		return new Promise((resolve, reject) => {
			try {
				// let _select_main = null;
				const _list_main = [];
				const _data = [];

				const _promises = [];

				params.forEach((item, index) => {
					let new_data = { key: index, parameter_code: null, parameter_name: null };

					if (item.parameter_name) {
						new_data = {
							...new_data,
							parameter_name: item.parameter_name,
							parameter_code: item.parameter_code,
						};
					}
					if (item.detail) {
						new_data = {
							...new_data,
							detail: item.detail,
						};
					}

					_data.push(new_data);
				});

				Promise.all(_promises).then(() => {
					resolve({
						// value_select_main: _select_main,
						value_list_main: _list_main,
						value_data: _data,
						value_count: _data.length,
					});
				});
			} catch (e) {
				reject(new Error({ error: true }));
			}
		});
	};

	const getFormatValue = (params) => {
		const result = params
			.map((item) => {
				let new_value = {};
				if (item.parameter_name) {
					if (typeof item.parameter_name === 'object') {
						new_value = {
							...new_value,
							parameter_name: item.parameter_name.label,
							parameter_code: item.parameter_name.value,
						};
					} else {
						new_value = {
							...new_value,
							parameter_name: item.parameter_name,
							parameter_code: item.parameter_code,
						};
					}
				}
				if (item.detail) {
					new_value = { ...new_value, detail: item.detail };
				}

				return new_value;
			})
			.filter((item) => Object.keys(item).length !== 0);
		return result;
	};

	return (
		<div>
			<div className='row mb-1'>
				{!isReadOnly && (
					<div className='col-md-6'>
						<Button
							icon='Add'
							color='success'
							type='button'
							isLight={darkModeStatus}
							onClick={handleCustomAdd}
							style={{ minHeight: '2.95rem' }}>
							Add Parameter
						</Button>
					</div>
				)}
			</div>
			<div className='row'>
				<div className='col-md-12'>
					{data.map((item, index) => (
						<div key={'div-'.concat(item.key)}>
							<div
								key={'div-border-'.concat(item.key)}
								className='col-md-12 border-bottom mb-1'
							/>
							<InputGroup key={'input-group-'.concat(item.key)} className='mb-1'>
								<Button
									key={'button-remove-'.concat(item.key)}
									icon='Clear'
									color='danger'
									type='button'
									isLight={darkModeStatus}
									style={{ minHeight: '2.95rem' }}
									onClick={() => handleCustomRemove(index)}
									isDisable={isReadOnly}
								/>
								<InputGroupText key={'igt-parameter-'.concat(item.key)}>
									Parameter {index + 1}
								</InputGroupText>
								<FormGroup
									key={'fg-parameter'.concat(item.key)}
									className='col-md-4'>
									<CustomSelect
										key={'cs-parameter'.concat(item.key)}
										options={list_parameter}
										onChange={(e) => onChangeParamater(e, index)}
										value={item.parameter_name}
										isDisable={isReadOnly}
										isSearchable
									/>
								</FormGroup>
							</InputGroup>
							<div key={'div-detail-'.concat(item.key)} className='mb-1 ps-5'>
								<ParameterTrialDetail
									key={'parameter-detail-'.concat(item.key)}
									onChange={(e) => onChangeParameterDetail(e, index)}
									initialValues={item.detail ? item.detail : []}
									parameter={
										typeof item.parameter_name === 'object'
											? item.parameter_name.value
											: item.parameter_code
									}
									index_parameter={index}
									isReadOnly={isReadOnly}
								/>
							</div>
						</div>
					))}
				</div>
			</div>
		</div>
	);
};

ParameterTrial.propTypes = {
	onChange: PropTypes.func,
	initialValues: PropTypes.instanceOf(Array),
	list_parameter: PropTypes.instanceOf(Array),
	isReset: PropTypes.bool,
	setReset: PropTypes.func,
	isReadOnly: PropTypes.bool,
	init: PropTypes.bool,
	setInit: PropTypes.func,
};
ParameterTrial.defaultProps = {
	onChange: null,
	initialValues: [],
	list_parameter: [],
	isReset: false,
	setReset: null,
	isReadOnly: false,
	init: true,
	setInit: () => { }
};

export default ParameterTrial;