import PropTypes from 'prop-types';
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import Card, { CardBody, CardTabItem } from '../bootstrap/Card';
import TicketModule from '../../modules/TicketModule';
import FormGroup from '../bootstrap/forms/FormGroup';
import Input from '../bootstrap/forms/Input';

const DetailTicketComponent = ({ ticket_id }) => {
	const [data_ticket, setDataTicket] = useState({
		currency: '',
		customer: '',
		date: '',
		date_of_communication: '',
		sample_product: '',
		status: '',
		target: '',
		target_date: '',
		target_price: '',
		information: [],
		sample: [],
		history: [],
	});

	useEffect(() => {
		if (ticket_id) {
			fetchDataTicket();
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [ticket_id]);

	const fetchDataTicket = async () => {
		return TicketModule.readById(ticket_id)
			.then((res) => {
				const new_response = {
					currency: res?.currency,
					customer: `(${res?.customer_code}) ${res?.customer_name}`,
					date: moment(res?.date).format('DD MMM YYYY'),
					date_of_communication: moment(res?.date_of_communication).format('DD MMM YYYY'),
					sample_product: res?.sample_product ? 'YES' : 'NO',
					status: res?.status,
					target: res?.category,
					target_date: moment(res?.target_date).format('DD MMM YYYY'),
					target_price: res?.target_price,
					information: res?.information,
					sample: res?.sample,
					history: res?.history,
				};
				setDataTicket(new_response);
			})
			.catch(() => {
				setDataTicket({});
			})
			.finally(() => {});
	};

	return (
		<div>
			<div className='row pt-2 px-2 pb-4'>
				<div className='col-md-6 py-1'>
					<FormGroup id='date' label='Date'>
						<Input disabled value={data_ticket?.date} />
					</FormGroup>
				</div>
				<div className='col-md-6 py-1'>
					<FormGroup id='target_date' label='Target Date'>
						<Input disabled value={data_ticket?.target_date} />
					</FormGroup>
				</div>
				<div className='col-md-6 py-1'>
					<FormGroup id='sample_product' label='Sample Product'>
						<Input disabled value={data_ticket?.sample_product} />
					</FormGroup>
				</div>
				<div className='col-md-6 py-1'>
					<FormGroup id='date_of_communication' label='Date of Communication'>
						<Input disabled value={data_ticket?.date_of_communication} />
					</FormGroup>
				</div>
				<div className='col-md-6 py-1'>
					<FormGroup id='customer' label='Customer'>
						<Input disabled value={data_ticket?.customer} />
					</FormGroup>
				</div>
				<div className='col-md-6 py-1'>
					<FormGroup id='target_price' label='Target Price'>
						<Input disabled value={data_ticket?.target_price} />
					</FormGroup>
				</div>
				<div className='col-md-6 py-1'>
					<FormGroup id='target' label='Target'>
						<Input disabled value={data_ticket?.target} />
					</FormGroup>
				</div>
				<div className='col-md-6 py-1'>
					<FormGroup id='currency' label='Currency'>
						<Input disabled value={data_ticket?.currency} />
					</FormGroup>
				</div>
				<div className='col-md-6 py-1'>
					<FormGroup id='status' label='Status'>
						<Input disabled value={data_ticket?.status} />
					</FormGroup>
				</div>
			</div>
			<div className='row'>
				<div className='col-md-12'>
					<Card shadow='sm' style={{ borderRadius: '10px' }} hasTab tabButtonColor='info'>
						<CardTabItem id='tab_additional_information' title='Additional Information'>
							<CardBody>
								<table className='table table-sm table-modern'>
									<thead>
										<tr>
											<th scope='col'>#</th>
											<th scope='col'>Name</th>
											<th scope='col'>Value</th>
										</tr>
									</thead>
									<tbody>
										{data_ticket?.information?.map((item, index) => (
											<tr key={'add-infor.'.concat(index)}>
												<th
													key={'add-infor.'.concat(index, '.', 0)}
													scope='row'>
													{index + 1}
												</th>
												<td key={'add-infor.'.concat(index, '.', 1)}>
													{item.name}
												</td>
												<td key={'add-infor.'.concat(index, '.', 2)}>
													{item.value}
												</td>
											</tr>
										))}
									</tbody>
								</table>
							</CardBody>
						</CardTabItem>
						<CardTabItem id='tab_sample' title='Sample'>
							<CardBody>
								<table className='table table-sm table-modern'>
									<thead>
										<tr>
											<th scope='col'>#</th>
											<th scope='col'>Sample Code</th>
											<th scope='col'>Sample Name</th>
											<th scope='col'>Batch</th>
											<th scope='col'>Quantity</th>
											<th scope='col'>UOM</th>
										</tr>
									</thead>
									<tbody>
										{data_ticket?.sample?.map((item, index) => (
											<tr key={'sample.'.concat(index)}>
												<th
													key={'sample.'.concat(index, '.', 0)}
													scope='row'>
													{index + 1}
												</th>
												<td key={'sample.'.concat(index, '.', 1)}>
													{item.sampel_code}
												</td>
												<td key={'sample.'.concat(index, '.', 2)}>
													{item.sampel_name}
												</td>
												<td key={'sample.'.concat(index, '.', 3)}>
													{item.batch}
												</td>
												<td key={'sample.'.concat(index, '.', 4)}>
													{item.qty}
												</td>
												<td key={'sample.'.concat(index, '.', 5)}>
													{item.uom}
												</td>
											</tr>
										))}
									</tbody>
								</table>
							</CardBody>
						</CardTabItem>
						<CardTabItem id='tab_history' title='History'>
							<CardBody>
								<table className='table table-sm table-modern'>
									<thead>
										<tr>
											<th scope='col'>#</th>
											<th scope='col'>User</th>
											<th scope='col'>Date</th>
											<th scope='col'>Status</th>
											<th scope='col'>Remark</th>
										</tr>
									</thead>
									<tbody>
										{data_ticket?.history?.map((item, index) => (
											<tr key={'history.'.concat(index)}>
												<th
													key={'history.'.concat(index, '.', 0)}
													scope='row'>
													{index + 1}
												</th>
												<td key={'history.'.concat(index, '.', 1)}>
													{item.requester}
												</td>
												<td key={'history.'.concat(index, '.', 2)}>
													{moment(item.date).format(
														'YYYY-MM-DD HH:mm:ss',
													)}
												</td>
												<td key={'history.'.concat(index, '.', 3)}>
													{item.status}
												</td>
												<td key={'history.'.concat(index, '.', 4)}>
													{item.remark}
												</td>
											</tr>
										))}
									</tbody>
								</table>
							</CardBody>
						</CardTabItem>
					</Card>
				</div>
			</div>
		</div>
	);
};

DetailTicketComponent.propTypes = {
	ticket_id: PropTypes.string.isRequired,
};
DetailTicketComponent.defaultProps = {};

export default DetailTicketComponent;
