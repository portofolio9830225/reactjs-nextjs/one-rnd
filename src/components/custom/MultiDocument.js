import React, { useRef, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import useDarkMode from '../../hooks/useDarkMode';
import Button from '../bootstrap/Button';
import Input from '../bootstrap/forms/Input';

const MultiDocument = ({
	initialValues,
	isReset,
	setReset,
	isReadOnly,
	onChange,
	init,
	setInit,
}) => {
	const { darkModeStatus } = useDarkMode();

	const [data, setData] = useState([]);
	const count = useRef(0);

	const [data_main, setDataMain] = useState([]);
	const [select_main, setSelectMain] = useState();

	const handleCustomAdd = () => {
		const new_data = [...data];
		new_data.push({ key: count.current, file: '' });
		setData(new_data);

		const new_data_main = [...data_main];
		new_data_main.push({ value: 'null', label: '' });
		setDataMain(new_data_main);

		count.current += 1;
	};

	const handleCustomRemove = (index) => {
		const new_data = [...data];
		new_data.splice(index, 1);
		setData(new_data);

		const new_data_main = [...data_main];
		new_data_main.splice(index, 1);
		setDataMain(new_data_main);

		setSelectMain(null);

		if (onChange) {
			const result = getFormatValue(new_data, select_main);
			onChange(result);
		}
	};

	const onChangeFile = (e, index) => {
		const new_data = [...data];
		// set new data
		new_data[index] = {
			...new_data.at(index),
			file: e,
		};
		setData(new_data);

		const new_main = [...data_main];
		new_main[index] = e;
		setDataMain(new_main);

		if (onChange) {
			const result = getFormatValue(new_data, select_main);
			onChange(result);
		}
	};

	useEffect(() => {
		if (isReset) {
			setData([]);
			setDataMain([]);
			setSelectMain(null);
			count.current = 0;

			setReset(false);
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [isReset]);

	useEffect(() => {
		if (initialValues.length !== 0 && init) {
			if (onChange) {
				onChange(initialValues);
			}
			setInit(false);
			getFormatSelect(initialValues).then((response) => {
				const { value_count, value_data, value_list_main, value_select_main } = response;

				setData(value_data);
				setDataMain(value_list_main);
				setSelectMain(value_select_main);
				count.current = value_count;
			});
		}

		return <div>{/* */}</div>;
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [initialValues]);

	const getFormatSelect = (params) => {
		return new Promise((resolve, reject) => {
			try {
				const _list_main = [];
				const _data = [];

				const _promises = [];

				params.forEach((item, index) => {
					let new_data = { key: index, parameter_code: null, parameter_name: null };

					if (item.parameter_name) {
						new_data = {
							...new_data,
							parameter_name: item.parameter_name,
							parameter_code: item.parameter_code,
						};
					}
					if (item.detail) {
						new_data = {
							...new_data,
							detail: item.detail,
						};
					}

					_data.push(new_data);
				});

				Promise.all(_promises).then(() => {
					resolve({
						// value_select_main: _select_main,
						value_list_main: _list_main,
						value_data: _data,
						value_count: _data.length,
					});
				});
			} catch (e) {
				reject(new Error({ error: true }));
			}
		});
	};

	const getFormatValue = (params) => {
		const result = params
			.map((item) => {
				let new_value = {};
				if (item.file) {
					new_value = { ...new_value, file: item.file };
				}

				return new_value;
			})
			.filter((item) => Object.keys(item).length !== 0);
		return result;
	};

	return (
		<div>
			<div className='row mb-1'>
				{!isReadOnly && (
					<div className='col-md-6'>
						<Button
							icon='Add'
							color='success'
							type='button'
							isLight={darkModeStatus}
							onClick={handleCustomAdd}
							style={{ minHeight: '2.95rem' }}>
							Add Document
						</Button>
					</div>
				)}
			</div>
			<div className='row'>
				<div className='col-md-6'>
					<table className='table table-modern'>
						<thead>
							<tr>
								<th style={{ width: '5px' }}>-</th>
								<th style={{ width: '100%' }}>Document</th>
							</tr>
						</thead>
						<tbody>
							{data.map((item, index) => (
								<tr key={'tr-'.concat(item.key)}>
									<td key={'td2-'.concat(item.key)}>
										<Button
											key={'button-remove-'.concat(item.key)}
											icon='Clear'
											color='danger'
											type='button'
											isLight={darkModeStatus}
											onClick={() => handleCustomRemove(index)}
											isDisable={isReadOnly}
										/>
									</td>
									<td key={'td1-'.concat(item.key)}>
										<Input
											type='file'
											placeholder='File'
											onChange={(e) => onChangeFile(e, index)}
										/>
									</td>
								</tr>
							))}
						</tbody>
					</table>
				</div>
			</div>
		</div>
	);
};

MultiDocument.propTypes = {
	onChange: PropTypes.func,
	initialValues: PropTypes.instanceOf(Array),
	isReset: PropTypes.bool,
	setReset: PropTypes.func,
	isReadOnly: PropTypes.bool,
	init: PropTypes.bool,
	setInit: PropTypes.func,
};
MultiDocument.defaultProps = {
	onChange: null,
	initialValues: [],
	isReset: false,
	setReset: null,
	isReadOnly: false,
	init: true,
	setInit: () => {},
};

export default MultiDocument;
