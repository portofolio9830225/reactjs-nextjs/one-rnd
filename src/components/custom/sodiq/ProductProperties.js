import React, { useRef } from 'react';
import Swal from 'sweetalert2';
import AsyncSelect from 'react-select/async';
import useDarkMode from '../../../hooks/useDarkMode';
import Button from '../../bootstrap/Button';
import Input from '../../bootstrap/forms/Input';
import Label from '../../bootstrap/forms/Label';

const ProductProperties = (dt) => {
    const {
        initTab,
        initProductProperties,
        listProductProperties,
        setProductProperties,
    } = dt
    const selectProduct = useRef(null)
    const { darkModeStatus } = useDarkMode();

    const handleAdd = () => {
        const listProduct = [...initProductProperties]
        if (listProduct[initTab]?.properties === undefined) {
            listProduct.push({ properties: [{ product_properties: { value: '', label: '' }, value: null }] })
        } else {
            listProduct[initTab].properties.push({ product_properties: { value: '', label: '' }, value: null })
        }
        setProductProperties(listProduct)
    }

    const handleRemove = (index) => {
        const arr = [...initProductProperties]
        arr[initTab].properties.splice(index, 1);
        setProductProperties(arr)
    }

    const handleValue = (val, index, type) => {
        const arr = [...initProductProperties]
        if (type === 'properties') {
            let check = false;
            arr[initTab].properties.forEach((item) => {
                if (item.product_properties.value === val.value) check = true;
            })

            if (check) {
                Swal.fire('Warning', `<b>${val.value}</b> already exits in data product properties`, 'error');
            } else {
                arr[initTab].properties[index].product_properties = val
            }
        }
        if (type === 'value') {
            arr[initTab].properties[index].value = val
        }
        setProductProperties(arr)
    }

    const getProperties = (find) => {
        return new Promise((resolve) => {
            setTimeout(() => {
                resolve(fetchDataAsyncSelect(find));
            }, 1000);
        });
    }

    const fetchDataAsyncSelect = (find) => {
        const search = listProductProperties.filter(e => (e.value.toUpperCase()).includes(find.toUpperCase()))
        return search.length > 3 ? search.slice(0, 3) : search
    }

    return (<><div className='row mb-2'>
        <div className='col-12'>
            <Button
                onClick={handleAdd}
                icon='Add'
                color='success'
                type='button'
                className='btn btn-sm'
                isLight={darkModeStatus}>
                Add Properties
            </Button>
        </div>
    </div>
        <hr />
        {initProductProperties.length > 0 &&
            initProductProperties[initTab]?.properties !== undefined ?
            initProductProperties[initTab].properties.map((item, i) => (
                < div className={i > 0 ? 'row mt-3' : 'row'}>
                    <div className='col-4'>
                        <Label>Properties</Label>
                        <AsyncSelect
                            key={'product_properties'.concat(i)}
                            style={{ marginTop: '-5px' }}
                            ref={selectProduct}
                            isClearable
                            cacheOptions
                            defaultOptions={listProductProperties.length > 4 ? listProductProperties.slice(0, 4) : listProductProperties}
                            loadOptions={getProperties}
                            onChange={(e) => handleValue(e, i, 'properties')}
                            value={item.product_properties || ''}
                            thema='secondary'
                            className='loading'
                            defaultValue={item.product_properties || ''}
                            placeholder='Type product properties...' />
                    </div>
                    <div className='col-4'>
                        <Label>Value</Label>
                        <Input
                            key={'valu_product_properties'.concat(i)}
                            style={{ marginTop: '-5px' }}
                            type='number'
                            value={item.value}
                            onInput={(e) => {
                                handleValue(e.target.value, i, 'value');
                            }}
                        />
                    </div>
                    <div className='col-1'>
                        <Button
                            style={{ marginTop: '20px' }}
                            onClick={() => handleRemove(i)}
                            icon='Clear'
                            color='danger'
                            type='button'
                            isLight={darkModeStatus} />
                    </div>
                </div>
            )) : ''
        }
    </>)
}

export default ProductProperties