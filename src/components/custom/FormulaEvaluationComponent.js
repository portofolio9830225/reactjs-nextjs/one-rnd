import React, { useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import useDarkMode from '../../hooks/useDarkMode';
import Button from '../bootstrap/Button';
import FormGroup from '../bootstrap/forms/FormGroup';
import Input from '../bootstrap/forms/Input';
import CustomSelect from './CustomSelect';
import showNotification from '../extras/showNotification';
import AddInfromationModule from '../../modules/AddInfromationModule';
import InputGroup, { InputGroupText } from '../bootstrap/forms/InputGroup';
import Spinner from '../bootstrap/Spinner';
import Textarea from '../bootstrap/forms/Textarea';

const CustomComponent = ({ indexCategory, category, onChange }) => {
	const { darkModeStatus } = useDarkMode();

	const [data, setData] = useState([]);
	const count = useRef(0);

	const [list_add_information, setListAddInformation] = useState([]);
	const [loading_add_information, setLoadingAddInformation] = useState(false);

	const handleAdd = () => {
		if (list_add_information.length === 0) {
			showNotification('Information', "no parameter data, can't add parameter", 'danger');
			return;
		}

		if (data.length === list_add_information.length) {
			showNotification(
				'Information',
				'has reached the maximum limit of parameters that can be added',
				'danger',
			);
			return;
		}

		const new_data = [...data];
		new_data.push({ key: count.current, parameter: null, value: '', remark: '' });
		setData(new_data);

		count.current += 1;

		// result data
		const result = new_data;
		onChange(result, indexCategory);
	};

	const handleRemove = (index) => {
		const new_data = [...data];
		new_data.splice(index, 1);
		setData(new_data);

		// result data
		const result = new_data;
		onChange(result, indexCategory);
	};

	const onChangeParameter = (e, index) => {
		const new_data = [...data];

		const find_index = new_data.findIndex((item) => item.parameter?.value === e.value);

		// if it is the same as the previously selected data, the process will not continue
		if (find_index === index) {
			return;
		}

		// if the selected data already exists, then the process will not continue
		if (find_index !== -1) {
			showNotification(
				'Information',
				`${e.label}, Parameter already exists, please select another parameter`,
				'danger',
			);
			return;
		}

		new_data[index] = { ...new_data[index], parameter: e };
		setData(new_data);

		// result data
		const result = new_data;
		onChange(result, indexCategory);
	};

	const onChangeValue = (e, index) => {
		const new_data = [...data];
		new_data[index] = { ...new_data[index], value: e.target.value };
		setData(new_data);

		// result data
		const result = new_data;
		onChange(result, indexCategory);
	};

	const onChangeRemark = (e, index) => {
		const new_data = [...data];
		new_data[index] = { ...new_data[index], remark: e.target.value };
		setData(new_data);

		// result data
		const result = new_data;
		onChange(result, indexCategory);
	};

	useEffect(() => {
		if (category) {
			fetchDataAddInformation();
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [category]);

	const fetchDataAddInformation = async () => {
		setLoadingAddInformation(true);
		const query = `category_code=${category?.value}`;
		return AddInfromationModule.readSelect(query)
			.then((response) => {
				setListAddInformation(response);
			})
			.catch(() => {
				setListAddInformation([]);
			})
			.finally(() => {
				setLoadingAddInformation(false);
			});
	};

	return (
		<div className='my-1 ps-5'>
			<div className='row py-1'>
				<div className='col-md-12'>
					{loading_add_information ? (
						<Button color='success' isLight={darkModeStatus}>
							<Spinner isSmall inButton />
							Loading...
						</Button>
					) : (
						<Button
							icon='Add'
							color='success'
							type='button'
							isLight={darkModeStatus}
							className='m-1'
							onClick={handleAdd}>
							Add Parameter
						</Button>
					)}
				</div>
			</div>
			{data.length !== 0 && (
				<div className='row'>
					<table className='table table-border' style={{ width: '100%' }}>
						<tbody>
							{data.map((item, index) => (
								<tr key={''.concat(item.key)}>
									<td key={''.concat(0, '-', item.key)} style={{ width: '40%' }}>
										<FormGroup>
											<CustomSelect
												options={list_add_information}
												onChange={(e) => onChangeParameter(e, index)}
												defaultValue={item.parameter}
												value={item.parameter}
											/>
										</FormGroup>
									</td>
									<td key={''.concat(1, '-', item.key)} style={{ width: '20%' }}>
										<FormGroup>
											<Input
												placeholder='Value'
												value={item.value}
												onChange={(e) => onChangeValue(e, index)}
											/>
										</FormGroup>
									</td>
									<td key={''.concat(2, '-', item.key)} style={{ width: '30%' }}>
										<FormGroup>
											<Input
												placeholder='Remark'
												value={item.remark}
												onChange={(e) => onChangeRemark(e, index)}
											/>
										</FormGroup>
									</td>
									<td key={''.concat(3, '-', item.key)} style={{ width: '10%' }}>
										<Button
											icon='Delete'
											color='danger'
											type='button'
											isLight={darkModeStatus}
											onClick={() => handleRemove(index)}
										/>
									</td>
								</tr>
							))}
						</tbody>
					</table>
				</div>
			)}
		</div>
	);
};

CustomComponent.propTypes = {
	indexCategory: PropTypes.number,
	category: PropTypes.instanceOf(Object),
	onChange: PropTypes.func,
};
CustomComponent.defaultProps = {
	indexCategory: 0,
	category: null,
	onChange: () => {},
};

const FormulaEvaluationComponent = ({ optionsCategory, onChange }) => {
	const { darkModeStatus } = useDarkMode();

	const [dataFile, setDataFile] = useState([]);
	const [dataText, setData] = useState([]);
	const [remark, setRemark] = useState('');
	const count = useRef(0);

	const handleAdd = () => {
		if (optionsCategory.length === 0) {
			showNotification('Information', "no category data, can't add category", 'danger');
			return;
		}

		if (dataText.length === optionsCategory.length) {
			showNotification(
				'Information',
				'has reached the maximum limit of categories that can be added',
				'danger',
			);
			return;
		}

		const new_data = [...dataText];
		new_data.push({ key: count.current, category: null, detail: [] });
		setData(new_data);
		count.current += 1;

		// result data
		const result = new_data;
		onChange(result, remark);
	};

	const handleRemove = (index) => {
		const new_data = [...dataText];
		new_data.splice(index, 1);
		setData(new_data);

		// result data
		const result = new_data;
		onChange(result, remark, dataFile);
	};

	const onChangeCategory = (e, index) => {
		const new_data = [...dataText];

		const find_index = new_data.findIndex((item) => item.category?.value === e.value);

		// if it is the same as the previously selected data, the process will not continue
		if (find_index === index) {
			return;
		}

		// if the selected data already exists, then the process will not continue
		if (find_index !== -1) {
			showNotification(
				'Information',
				`${e.label}, Category already exists, please select another category`,
				'danger',
			);
			return;
		}

		new_data[index] = { ...new_data[index], category: e };
		setData(new_data);

		// result data
		const result = new_data;
		onChange(result, remark, dataFile);
	};

	const onChangeFile = (e) => {
		const temp = [...dataFile];
		temp.push(e.target.files[0]);
		setDataFile(temp);
		onChange(dataText, remark, dataFile);
	};

	const onChangeRemark = (e) => {
		setRemark(e.target.value);

		// result data
		const result = [...dataText];
		onChange(result, e.target.value, dataFile);
	};

	const onChangeParameter = (value, index) => {
		const new_data = [...dataText];
		new_data[index] = { ...new_data[index], detail: value };
		setData(new_data);

		// result data
		const result = new_data;
		onChange(result, remark, dataFile);
	};

	return (
		<div className='row py-1'>
			<div className='col-md-12'>
				<Button
					icon='Add'
					color='success'
					type='button'
					isLight={darkModeStatus}
					className='m-1'
					onClick={handleAdd}>
					Add Category
				</Button>
			</div>
			{dataText.map((item, index) => (
				<div>
					<div className='row py-1'>
						<div className='col-md-12 border-bottom mb-1' />
					</div>
					<div key={''.concat(item.key)} className='row py-1'>
						<div className='col-md-10'>
							<InputGroup>
								<InputGroupText>Category</InputGroupText>
								<FormGroup className='col-md-6'>
									<CustomSelect
										options={optionsCategory}
										onChange={(e) => onChangeCategory(e, index)}
										defaultValue={item.category}
										value={item.category}
									/>
								</FormGroup>
								<Button
									icon='Delete'
									color='danger'
									type='button'
									isLight={darkModeStatus}
									style={{ minHeight: '2.95rem' }}
									onClick={() => handleRemove(index)}
								/>
							</InputGroup>
						</div>
					</div>
					<div>
						<CustomComponent
							indexCategory={index}
							category={item.category}
							onChange={onChangeParameter}
						/>
					</div>
				</div>
			))}
			<div className='col-md-12'>
				<hr />
			</div>
			<div className='col-md-12'>
				<FormGroup id='upload_document' label='Upload Document' className='col-md-12'>
					<Input
						multiple
						type='file'
						placeholder='File'
						onChange={(e) => onChangeFile(e)}
					/>
				</FormGroup>
				<FormGroup id='remark' label='Remark'>
					<Textarea rows={5} value={remark} onChange={onChangeRemark} />
				</FormGroup>
			</div>
		</div>
	);
};

FormulaEvaluationComponent.propTypes = {
	optionsCategory: PropTypes.instanceOf(Array),
	onChange: PropTypes.func,
};
FormulaEvaluationComponent.defaultProps = {
	optionsCategory: [],
	onChange: () => {},
};

export default FormulaEvaluationComponent;
