import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import Swal from 'sweetalert2';
import { Tab, Tabs } from 'react-bootstrap';
import * as XLSX from 'xlsx';
import moment from 'moment';
import Button from '../bootstrap/Button';
import FormulaMaterial from './FormulaMaterial';
import showNotification from '../extras/showNotification';
import ParameterTrial from './ParameterTrial';
import DetailTicket from './rizky/ticket/DetailTicket';
import Modal, { ModalBody, ModalHeader, ModalTitle, ModalFooter } from '../bootstrap/Modal';
import FormGroup from '../bootstrap/forms/FormGroup';
import Input from '../bootstrap/forms/Input';
import CustomSelect from '../CustomSelect';
import useDarkMode from '../../hooks/useDarkMode';
import FormulaModule from '../../modules/FormulaModule';
import { getRequester } from '../../helpers/helpers';
import Accordion, { AccordionItem } from '../bootstrap/Accordion';
import ProductProperties from './sodiq/ProductProperties';

const RenderTitleFormula = (item, id, editable, idx, DeleteRecipe) => {
	if (editable != false) {
		return (
			<div>
				{item} {`(${idx + 1})`}
				<Button
					icon='Delete'
					variant='light'
					className='ml-2'
					onClick={() =>
						Swal.fire({
							title: 'Are you sure?',
							text: 'This version will be deleted',
							icon: 'warning',
							showCancelButton: true,
							confirmButtonText: 'Yes',
						}).then((result) => {
							if (result.value) {
								if (id) {
									deleteFormula(id, idx, DeleteRecipe);
								} else {
									DeleteRecipe(idx);
								}
							} else if (result.dismiss === Swal.DismissReason.cancel) {
								Swal.fire('Cancelled', 'Your data is safe :)', 'error');
							}
						})
					}
					size='sm'
				/>
			</div>
		);
	}
	return (
		<div>
			{item} {`(idx${idx})`}{' '}
		</div>
	);
};

const deleteFormula = (id, idx, DeleteRecipe) => {
	const { username } = getRequester();
	const values = {};
	values._id = id;
	values.requester = username;
	FormulaModule.deleteFormula(values)
		.then((res) => {
			showNotification('Success!', res.status, 'success');
			DeleteRecipe(idx);
		})
		.catch((err) => {
			showNotification('Warning!', err, 'danger');
		});
	return values;
};

const FormulaTab = ({
	initialValues,
	onChange,
	selectedKey,
	listMaterial,
	listVendor,
	listUom,
	listCurrency,
	listFormulaCopy,
	listProductProperties,
	initProductProperties,
	setProductProperties,
	setLoadingSubmit,
}) => {
	const { darkModeStatus } = useDarkMode();
	const [key, setKey] = useState(selectedKey);
	const [isReset, setReset] = useState(false);
	const [init, setInit] = useState(true);
	const [initMaterial, setInitMaterial] = useState(true);
	const [dataFormula, setDataFormula] = useState(initialValues);
	const [selectedCopyFormula, setSelectedCopyFormula] = useState(null);
	const [formulaImport, setFormulaImport] = useState([]);
	const [parameterImport, setParameterImport] = useState([]);
	const [propertiesImport, setPropertiesImport] = useState([]);
	const [isHistory, setIsHistory] = useState(false);
	const [list_parameter] = useState([
		{
			value: 'mixing_sequences',
			label: 'Mixing Sequences',
		},
		{
			value: 'mixing_properties',
			label: 'Mixing Properties',
		},
		{
			value: 'homogenizer',
			label: 'Homogenizer',
		},
		{
			value: 'spray_dryer',
			label: 'Spray Dryer',
		},
		{
			value: 'pasteurizer',
			label: 'Pasteurizer',
		},
		{
			value: 'mass_balance',
			label: 'Mass Balance',
		},
	]);
	const [isOpen, setIsOpen] = useState(false);
	const [isExcel, setIsExcel] = useState(false);

	const AddFormula = () => {
		const total_arr = dataFormula.length + 1;
		dataFormula.push({
			title: `New Formula`,
			index: total_arr,
			_id: null,
			due_date_miniplant: '',
		});
		setKey(total_arr - 1);
		onChange(dataFormula);
	};

	const onChangeTab = (values, index) => {
		const new_data = [...dataFormula];

		if (values.length === 0) {
			delete new_data[index].detail;
		} else {
			new_data[index] = {
				...new_data.at(index),
				formula: values,
			};
		}

		setDataFormula(new_data);

		if (onChange) {
			onChange(new_data);
		}
	};

	const DeleteRecipe = (idx) => {
		if (dataFormula.length == 1) {
			showNotification('Information', 'Formula cannot be empty', 'danger');
			return;
		}

		if (dataFormula != 1) {
			dataFormula.splice(idx, 1);
			setDataFormula(dataFormula);
			onChange(dataFormula);
			setInit(true);
			setInitMaterial(true);
			setKey(idx > 0 ? idx - 1 : 0);
		}
		const arr = [...initProductProperties];
		arr.splice(idx, 1);
		setProductProperties(arr);
	};

	const onChangeMainParameter = (values, index) => {
		const new_data = [...dataFormula];

		if (values.length === 0) {
			delete new_data[index].detail;
		} else {
			new_data[index] = {
				...new_data.at(index),
				details: values,
			};
		}
		setDataFormula(new_data);

		if (onChange) {
			onChange(new_data);
		}
	};

	const onChangeDate = (e, index) => {
		const new_data = [...dataFormula];

		new_data[index] = {
			...new_data.at(index),
			due_date_miniplant: e,
		};

		setDataFormula(new_data);

		if (onChange) {
			onChange(new_data);
		}
	};

	const handleExcelFile = async (e) => {
		setPropertiesImport([]);
		setFormulaImport([]);
		setParameterImport([]);

		setLoadingSubmit(true);
		const file = e.target.files[0];
		const data = await file.arrayBuffer();
		const workbook = XLSX.readFile(data);
		const worksheet = workbook.Sheets[workbook.SheetNames[0]];
		const jsonData = XLSX.utils.sheet_to_json(worksheet, {
			header: 1,
			defval: '',
		});
		const parameter = list_parameter;
		let isFormula = false;
		let currentParameter = '';
		const detail = [];
		const param = [];
		jsonData.forEach((d) => {
			if (
				parameter.filter((p) => p.label.toUpperCase() === d[1].toString().toUpperCase())
					.length > 0
			) {
				// eslint-disable-next-line no-unused-vars
				const [x, y] = d;
				currentParameter = y;
				param.push({
					parameter_name: y.replace(/^.{1}/g, y[0].toUpperCase()),
					parameter_code: y.toString().toLowerCase().replace(/ /g,"_")
				});
				detail.push({detail: []})
			} else if (d[1].toString().toUpperCase() === 'RAW MATERIAL') {
				isFormula = true
			} else if (typeof d[1] === 'number' && isFormula === true && d[2] !== '') {
				setFormulaImport((prev) => [
					...prev,
					{
						material_name: d[2],
						supplier_name: d[3],
						qty: d[4],
						batch: d[5],
						note: d[6],
					},
				]);
			} else if (d[2].toString().toUpperCase() === 'TOTAL') {
				isFormula = false;
			} else if (typeof d[1] === 'number' && isFormula === false) {
				if (currentParameter.toString().toUpperCase() === 'MIXING SEQUENCES') {
					detail[detail.length - 1].detail.push({
							name: d[2],
							plan_rpm: d[3],
							plan_minutes: d[4],
							actual_rpm: d[5],
							actual_minutes: d[6],
					})
				} else {
					detail[detail.length - 1].detail.push({
						name: d[2],
						plan: d[3],
						actual: d[4],
					})
				}
			} else if (
				typeof d[3] === 'number' &&
				typeof d[4] === 'number' &&
				d[7].toString().toUpperCase() === 'CUSTOMER'
			) {
				setPropertiesImport((prev) => [
					...prev,
					{
						product_properties: {value: 'TS', label: 'TS', type: 'text'},
						value: d[3],
					},
					{
						product_properties: {value: 'Protein', label: 'Protein', type: 'text'},
						value: d[4],
					},
				]);
			} else if (
				typeof d[3] === 'number' &&
				typeof d[4] === 'number' &&
				d[7].toString().toUpperCase() === 'PIC DEVELOPMENT'
			) {
				setPropertiesImport((prev) => [
					...prev,
					{
						product_properties: {value: 'Fat', label: 'Fat', type: 'text'},
						value: d[3],
					},
					{
						product_properties: {value: 'GP', label: 'GP', type: 'text'},
						value: d[4],
					},
				]);
			} else {
			}
		});
		const newParams = [
			{...param[0], ...detail[0]},
			{...param[1], ...detail[1]},
			{...param[2], ...detail[2]},
			{...param[3], ...detail[3]},
			{...param[4], ...detail[4]},
			{...param[5], ...detail[5]},
		];
		setParameterImport(newParams);
		setLoadingSubmit(false);
	};

	const importExcel =() => {
		if (propertiesImport.length < 1 && formulaImport.length < 1) {
			showNotification('Import Failed', 'File template is not match with requirement', 'danger');
		} else {
			initProductProperties[key] = {
				properties: propertiesImport
			}
			setProductProperties(initProductProperties);
			dataFormula[key].formula = formulaImport;
			dataFormula[key].details = parameterImport;
			setDataFormula(dataFormula);

			setInitMaterial(true);
			setInit(true);
			setIsExcel(false);
		}
	};

	const copyFormula = () => {
		setSelectedCopyFormula(null);
		const dataFormulaCopy = selectedCopyFormula.detail;
		dataFormulaCopy._id = null;
		dataFormulaCopy.title = 'New Formula';
		dataFormulaCopy.editable = true;
		dataFormulaCopy.due_date_miniplant = '';
		setDataFormula([...dataFormula, dataFormulaCopy]);
		onChange([...dataFormula, dataFormulaCopy]);
		setInitMaterial(true);
		setInit(true);
		const total_arr = dataFormula.length + 1;
		setKey(total_arr - 1);
	};

	useEffect(() => {
		return <div>{/* */}</div>;
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [initialValues, dataFormula]);

	return (
		<div>
			<Button
				icon='AddCircleOutline'
				color='success'
				className='ml-2 me-2'
				onClick={() => AddFormula()}
				size='sm'>
				Add Formula
			</Button>
			{/* rizky */}
			<DetailTicket
				label={initialValues[0].document_number}
				initialValues={initialValues[0]}
			/>
			{/* end rizky */}
			<Button
				color='warning'
				className='ml-2 me-2 float-end'
				onClick={() => setIsOpen(true)}
				size='sm'>
				Copy from another formula
			</Button>
			<Button
				color='warning'
				className='ml-2 me-2 float-end'
				onClick={() => {
					setIsExcel(true);
				}}
				size='sm'>
				Import
			</Button>
			<br />
			<br />
			<Tabs
				id='controlled-tab-example'
				activeKey={key}
				onSelect={(k) => {
					setKey(k);
				}}
				className='mb-3'>
				{dataFormula.map((item, index) => (
					<Tab
						key={'key-tab-'.concat(index)}
						eventKey={index}
						title={RenderTitleFormula(
							item.title,
							item._id,
							item.editable,
							index,
							DeleteRecipe,
						)}
						tabClassName={`${index}`}>
						<div className='row d-flex justify-content-between'>
							<div className='col-sm-4 col-md-4'>
								<table className='table table-border'>
									<tbody>
										<tr>
											<td>
												<b>Due Date Miniplant</b>
											</td>
											<td>
												<Input
													type='date'
													onInput={(value) => {
														onChangeDate(value.target.value, index);
													}}
													value={item?.due_date_miniplant}
												/>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
							<div className='col-sm-2 col-md-2'>
								<Button color="primary" style={{float: 'right'}} onClick={() => {
									setIsHistory(true)
								} }>History</Button>
							</div>
							{
								initialValues[key]?.history ? <Modal
								isOpen={isHistory}
								setIsOpen={setIsHistory}
								size='lg'
								titleId='modal-edit-menu-crud'
								isStaticBackdrop>
							<ModalHeader setIsOpen={setIsHistory} className='p-4  mb-5 border border-bottom-1'>
								<ModalTitle id='modal-edit-menu-crud'>{initialValues[key]?.job_number ? initialValues[key].job_number : 'New Formula'}</ModalTitle>
							</ModalHeader>
							<ModalBody className='px-4'>
								{initialValues[key]?.history && initialValues[key].history.map(history => (
									<div className='col-md-12'style={{marginTop: '-1.2rem'}}>
										<div className='d-flex justify-content-between align-items-center' style={{color: '#3d76ff', fontSize: '1.1rem'}}>
											<div className='d-flex'>
												<div style={{width: '20px', height: '20px', marginRight: '1.5rem', borderRadius: '100%', border: '3px solid #18bedb'}}>{}</div>
												<p>{history.status} - {history.requester}</p>
											</div>
											<p>{moment(history.date).format('D MMMM YYYY')}</p>
										</div>
										<div style={{borderLeft: '2px solid lightgrey', marginLeft: '0.7rem', marginTop: '-1.4rem'}}>
											<p style={{paddingLeft: '2.1rem', paddingTop: '1.5rem', paddingBottom: '2.1rem'}}>{history.remark}</p>
										</div>
									</div>
								))}
							</ModalBody>
							<ModalFooter className='px-4 pb-4'>
								<div className='col-md-12 d-flex justify-content-end'>
									<Button color="primary" onClick={() => setIsHistory(false)} >Close</Button>
								</div>
							</ModalFooter>
						</Modal> : <Modal
							isOpen={isHistory}
								setIsOpen={setIsHistory}
								size='lg'
								titleId='modal-edit-menu-crud'
								isStaticBackdrop>
							<ModalHeader setIsOpen={setIsHistory} className="mb-5 border border-bottom-1"><ModalTitle>New Formula</ModalTitle></ModalHeader>
							<ModalBody className="text-center">This formula doesn't have any history data</ModalBody>
							<ModalFooter className='px-4 pb-4'>
								<div className='col-md-12 d-flex justify-content-end'>
									<Button color="primary" onClick={() => setIsHistory(false)} >Close</Button>
								</div>
							</ModalFooter>
						</Modal>}
						</div>
						<div className='col-12 mt-2'>
							<Accordion activeItemId='form-default' id='accordion-form'>
								<AccordionItem id='form-default' title='Product Properties'>
									<ProductProperties
										initTab={index}
										onChange={(e) => onChangeTab(e, index)}
										initProductProperties={initProductProperties}
										setProductProperties={setProductProperties}
										listProductProperties={listProductProperties}
									/>
									<br />
									<br />
								</AccordionItem>
							</Accordion>
						</div>
						<div className='col-12 mt-2 mb-4'>
							<Accordion
								activeItemId='form-default-formula'
								id='accordion-form-formula'>
								<AccordionItem id='form-default-formula' title='Formula'>
									<div className='row col-12 mt-2'>
										<FormulaMaterial
											key={'formula-material-'.concat(index)}
											onChange={(e) => onChangeTab(e, index)}
											listMaterial={listMaterial}
											listVendor={listVendor}
											listUom={listUom}
											listCurrency={listCurrency}
											initialValues={
												item.formula
													? item.formula
													: [
															{
																key: 0,
																material_name: '',
																material_code: '',
																material: null,
																qty: '',
																uom: null,
																supplier_name: null,
																supplier_code: null,
																currency: null,
																price: '',
																note: '',
																readOnlyMaterialName: false,
															},
													  ]
											}
											initMaterial={initMaterial}
											setInitMaterial={setInitMaterial}
											isReadOnly={item.editable == false}
										/>
									</div>
								</AccordionItem>
							</Accordion>
						</div>
						<div className='col-12 mb-3 g-4'>
							<ParameterTrial
								key={'parameter-material-'.concat(index)}
								initialValues={item ? item.details : []}
								onChange={(e) => onChangeMainParameter(e, index)}
								list_parameter={list_parameter}
								isReset={isReset}
								setReset={setReset}
								isReadOnly={item.editable == false}
								init={init}
								setInit={setInit}
							/>
						</div>
					</Tab>
				))}
			</Tabs>

			<Modal
				isOpen={isOpen}
				setIsOpen={setIsOpen}
				size='lg'
				titleId='modal-edit-menu-crud'
				isStaticBackdrop>
				<ModalHeader setIsOpen={setIsOpen} className='p-4'>
					<ModalTitle id='modal-edit-menu-crud'>Copy from another formula</ModalTitle>
				</ModalHeader>

				<ModalBody className='px-4'>
					<div className='col-md-12'>
						<FormGroup id='priority_code' label='Select Formula' className='mb-4'>
							<CustomSelect
								options={listFormulaCopy}
								defaultValue={selectedCopyFormula}
								value={selectedCopyFormula}
								darkTheme={darkModeStatus}
								isSearchable={isOpen}
								onChange={(value) => {
									setSelectedCopyFormula(value);
								}}
							/>
						</FormGroup>
					</div>
				</ModalBody>
				<ModalFooter className='px-4 pb-4'>
					<div className='col-md-12 '>
						<Button
							icon='Save'
							type='submit'
							color='success'
							className='float-end'
							onClick={() => {
								copyFormula();
								setIsOpen(false);
							}}>
							Copy
						</Button>
					</div>
				</ModalFooter>
			</Modal>

			<Modal
				isOpen={isExcel}
				setIsOpen={setIsExcel}
				size='lg'
				titleId='modal-edit-menu-crud'
				isStaticBackdrop>
				<ModalHeader setIsOpen={setIsExcel} className='p-4'>
					<ModalTitle id='modal-edit-menu-crud'>Import from excel</ModalTitle>
				</ModalHeader>
				<ModalBody className='px-4'>
					<div className='col-md-12'>
						<FormGroup id='priority_code' label='Select excel' className='mb-4'>
							<Input type='file' onChange={(e) => handleExcelFile(e)} />
						</FormGroup>
					</div>
				</ModalBody>
				<ModalFooter className='px-4 pb-4'>
					<div className='col-md-12 '>
						<a
						  download
						  type='button'
						  href='/files/BLANKO_ONE_RND.xlsx'>
						  Download Template
						</a>
						<Button
							icon='Save'
							type='submit'
							color='success'
							className='float-end'
							onClick={() => {
								importExcel();
								setIsOpen(false);
								setIsExcel(false);
							}}>
							Import
						</Button>
					</div>
				</ModalFooter>
			</Modal>
		</div>
	);
};

FormulaTab.propTypes = {
	onChange: PropTypes.func,
	initialValues: PropTypes.instanceOf(Array),
	selectedKey: PropTypes.number,
	listMaterial: PropTypes.instanceOf(Array),
	listVendor: PropTypes.instanceOf(Array),
	listUom: PropTypes.instanceOf(Array),
	listCurrency: PropTypes.instanceOf(Array),
	listFormulaCopy: PropTypes.instanceOf(Array),
	listProductProperties: PropTypes.instanceOf(Array),
	initProductProperties: PropTypes.instanceOf(Array),
	setProductProperties: PropTypes.func,
	setLoadingSubmit: PropTypes.bool,
};
FormulaTab.defaultProps = {
	onChange: null,
	initialValues: [],
	selectedKey: 0,
	listMaterial: [],
	listVendor: [],
	listUom: [],
	listCurrency: [],
	listFormulaCopy: [],
	listProductProperties: [],
	initProductProperties: [],
	setProductProperties: {},
	setLoadingSubmit: false,
};

export default FormulaTab;
